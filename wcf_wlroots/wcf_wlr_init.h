#ifndef WCF_WLR_INIT_H
#define WCF_WLR_INIT_H

struct wlr_state;

 /*
 ** Declarations for the wcf_wlr_<object> init routines.  These
 ** routines are used by wcf_initiaiize() to create and initialize WLR
 ** global objects.
 */

typedef enum {
    OK    = 0,
    ERROR = 1
} error_t;

extern
error_t
wcf_wl_display(
    struct wlr_state *wlr
);

extern
error_t
wcf_wlr_backend(
    struct wlr_state *wlr
);

extern
error_t
wcf_wlr_compositor(
    struct wlr_state *wlr
);

extern
error_t
wcf_wlr_subcompositor(
    struct wlr_state *wlr
);

extern
error_t
wcf_wlr_output_layout(
    struct wlr_state *wlr
);

extern
error_t
wcf_wlr_data_device_manager(
    struct wlr_state *wlr
);

extern
error_t
wcf_wlr_primary_selection_device_manager(
    struct wlr_state *wlr
);

extern
error_t
wcf_wlr_text_input_manager(
    struct wlr_state *wlr
);

extern
error_t
wcf_wlr_seat(
    struct wlr_state *wlr
);

extern
error_t
wcf_wlr_cursor(
    struct wlr_state *wlr
);

extern
error_t
wcf_wlr_xdg_shell(
    struct wlr_state *wlr
);

extern
error_t
wcf_wlr_xwayland(
    struct wlr_state *wlr
);

extern
error_t
wcf_wlr_xcursor_manager(
    struct wlr_state *wlr
);

extern
error_t
wcf_wlr_xdg_decoration_manager(
    struct wlr_state *wlr
);

#endif // WCF_WLR_INIT_H
