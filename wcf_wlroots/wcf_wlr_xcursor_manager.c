
  // System
#include <stddef.h>    // NULL

  // Wlroots
#include <wlr/types/wlr_xcursor_manager.h>

  // TWC
#include <twc/twc_log.h>

  // APIs
#include "wcf_wlr_init.h"    // Our API to ensure consistancy

  // Imported Data Types
#include "wcf_wlr.h"

 /*
 ** WLR xcursor manager
 **
 ** The wlr_xcursor manager provides a database of named cursor
 ** themes from x.org.  Each theme contains a set of named cursor
 ** images.  See
 **
 **   https://www.x.org/releases/current/doc/man/man3/Xcursor.3.xhtml
 **    <wlroots_src>/include/wlr/types/wlr_xcursor_manager.h
 **
 ** A WLR xcursor manager has no events.  See
 **
 **   <wlroots_src>//include/wlr/types/wlr_xcursor_manager.h
 */


 // ------------------------------
 // WLR xcursor manager initialize

extern
error_t
wcf_wlr_xcursor_manager(
    struct wlr_state *wlr
) {
    LOG_INFO("wcf_wlr_xcursor_manager.");

#   define XCURSOR_DEFAULT_THEME_NAME ((char*)NULL)

      // Use the default theme with base size 24 (in pixels???).
    wlr->xcursor_manager = (
        wlr_xcursor_manager_create(
            XCURSOR_DEFAULT_THEME_NAME,
            24
        )
    );
    if ( wlr->xcursor_manager == NULL ) {
        LOG_ERROR("WLR xcursor manager create failed.");
        return ERROR;
    }

      // Ask if the theme requested above is available at a particular
      // scale, in this case a scale of 1.
    if ( ! wlr_xcursor_manager_load(wlr->xcursor_manager, 1) ) {
        return ERROR;
    }

    return OK;
}
