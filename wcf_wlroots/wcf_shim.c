#define _POSIX_C_SOURCE 200112L     // required for setenv()

  // System
#include <stddef.h>     // NULL
#include <stdbool.h>
#include <stdlib.h>     // setenv()
#include <libinput.h>   // libinput_button_state

  // Wayland
#include <wayland-util.h>  // wl_fixed_t

  // Wlroots
#include <wlr/backend.h>
#include <wlr/types/wlr_compositor.h>
#include <wlr/types/wlr_cursor.h>
#include <wlr/types/wlr_xcursor_manager.h>
#include <wlr/types/wlr_seat.h>
#include <wlr/types/wlr_xdg_shell.h>
#include <wlr/render/interface.h>
#include <wlr/xwayland.h>
#include <wlr/types/wlr_screencopy_v1.h>
#include <wlr/types/wlr_output_management_v1.h>

  // Protocols
#include <xdg-shell-client-protocol.h>

  //TWC
#include <twc/wayland_types.h>      // wld_coordinate_t
#include <twc/twc_log.h>

  // bic_server APIs
#include "bic_server/wl_surface.h"


  // APIs
#include "wcf_shim.h"          // Our API to ensure consistancy
#include "wcf_wlr_init.h"
#include <assert.h>

  // Imported Data Types
#include "wcf_wlr.h"
#include "wcf_external_surface.h"

 /*
 ** Initialization and control of the underlying Wayland Compositor
 ** Framework (WCF).  In the current implementation, the WCF is
 ** provided by wlroots.
 **
 ** Each WLR object has "events" which it will signal using
 ** registered callbacks.  To find the events for each WLR object
 ** see:
 **    <wlroots-src>/include/wlr/types/wlr_<object>.h
 */

  // Define the wcf state.
struct wlr_state WLR;   // Fix: return to static

extern
void
log_init(
    void
) {
    LOG_INIT;

    return;
}

extern
void
wcf_initialize(
    void
) {
    LOG_INFO("Initializing WCF.");

      // Wayland passes enums as uint32_t.  So does Wlroots.  Make
      // sure edges are consistant.
    assert(
        (int)WLR_EDGE_TOP    == (int)XDG_TOPLEVEL_RESIZE_EDGE_TOP    &&
        (int)WLR_EDGE_BOTTOM == (int)XDG_TOPLEVEL_RESIZE_EDGE_BOTTOM &&
        (int)WLR_EDGE_LEFT   == (int)XDG_TOPLEVEL_RESIZE_EDGE_LEFT   &&
        (int)WLR_EDGE_RIGHT  == (int)XDG_TOPLEVEL_RESIZE_EDGE_RIGHT
    );

    assert(
          // button state values must be consistant
        (int)WLR_BUTTON_RELEASED == (int)LIBINPUT_KEY_STATE_RELEASED &&
        (int)WLR_BUTTON_PRESSED  == (int)LIBINPUT_KEY_STATE_PRESSED
    );

    assert(
          // pointer axis directions must be consistant
        (int)WLR_AXIS_ORIENTATION_VERTICAL   == (int)WL_POINTER_AXIS_VERTICAL_SCROLL  &&
        (int)WLR_AXIS_ORIENTATION_HORIZONTAL == (int)WL_POINTER_AXIS_HORIZONTAL_SCROLL
    );

      // For each of the global WLR objects that we're interested in,
      // call a corresponding wcf shim creation routine.(*)
    if ((wcf_wl_display                          (&WLR) != OK) ||
        (wcf_wlr_backend                         (&WLR) != OK) ||
        (wcf_wlr_compositor                      (&WLR) != OK) ||
        (wcf_wlr_subcompositor                   (&WLR) != OK) ||
        (wcf_wlr_cursor                          (&WLR) != OK) ||
        (wcf_wlr_data_device_manager             (&WLR) != OK) ||
        (wcf_wlr_primary_selection_device_manager(&WLR) != OK) ||
        (wcf_wlr_output_layout                   (&WLR) != OK) ||
        (wcf_wlr_seat                            (&WLR) != OK) ||
        (wcf_wlr_text_input_manager              (&WLR) != OK) ||
        (wcf_wlr_xcursor_manager                 (&WLR) != OK) ||
        (wcf_wlr_xdg_decoration_manager          (&WLR) != OK) ||
        (wcf_wlr_xdg_shell                       (&WLR) != OK) ||
        (wcf_wlr_xwayland                        (&WLR) != OK)
    ) {
        LOG_ERROR("WCF initialization failed.");
        return;
    }

      // Startup the output and screenshot managers.
    WLR.wlr_output_manager_v1 = (
        wlr_output_manager_v1_create(WLR.wl_display)
    );
    WLR.wlr_screencopy_manager_v1 = (
        wlr_screencopy_manager_v1_create(WLR.wl_display)
    );

      // Attach the cursor to an output_layout.  This lets wlroots
      // know the extent to which the pointer may roam before hitting
      // output_layout boundaries.  Fix: Can you request that the
      // output_layout wrap around???
      //
      // This must occur after both cursor and output layout are
      // created.
    wlr_cursor_attach_output_layout(
        WLR.wlr_cursor,
        WLR.output_layout
    );

      // Add a socket for clients to connect.
    WLR.socket = wl_display_add_socket_auto(WLR.wl_display);
    if ( ! WLR.socket ) {
        LOG_ERROR("Unable to add socket to Wayland display.");
        return;
    }

      // Start the WLR backend.
    if ( ! wlr_backend_start(WLR.backend) ) {

        wlr_backend_destroy(WLR.backend);
        wl_display_destroy (WLR.wl_display);

        return;
    }

      // Set WAYLAND_DISPLAY environment variable to the socket name
    setenv("WAYLAND_DISPLAY", WLR.socket, true);

#   if 0
    // Fix: Should there be a default cursor in case there's no menu
    // agent or it doesn't set one?
    wlr_xcursor_manager_set_cursor_image(
        WLR.xcursor_manager,
        "left_ptr",
        WLR.wlr_cursor
    );
#   endif

      // Fix: Should this be the middle of some output?
      // Initialize the cursor position to the middle of the layout
    wlr_cursor_warp_absolute(
        WLR.wlr_cursor,
         NULL,
         .5,
         .5
    );

    return;

    // (*) Non-global wlr_<object>'s are initialized as they
    // materialize through some listener routine.
    //   wlr_input_device
    //   wlr_output
    //   wlr_xdg_surface
    //   wlr_xdg_toplevel_decoration
}

extern
void
wcf_start(
    void
) {
    // Once WLR objects are initailized, set everything in motion
    // with calls to libwayland-server.

    LOG_INFO("Starting WCF.");
    LOG_INFO("WCF Running on %s", WLR.socket);

      // Start the Wayland event loop.  This will run until
      // wl_display_terminate() is called.  See below.
    wl_display_run(WLR.wl_display);

    LOG_INFO("WCF terminating.");

    /*
    ** Has no destroy routine:
    **
    **   wlr_data_device_manager
    **   wlr_primary_selection_device_manager
    **   text_input_manager
    **   wlr_xdg_decoration_manager
    **   wlr_xdg_shell
    **
    ** Called automatically when the wl_display is destroyed:
    **
    **   wlr_backend_destroy()
    **   wlr_cursor_destroy()  ???
    **   wlr_seat_destroy
    */

    wl_display_destroy_clients (WLR.wl_display);
    wlr_output_layout_destroy  (WLR.output_layout);
    wlr_xcursor_manager_destroy(WLR.xcursor_manager);
    wlr_xwayland_destroy       (WLR.wlr_xwayland);
    wl_display_destroy         (WLR.wl_display);

    return;
}

extern
void
wcf_terminate(
    void
) {
      // This should eventually cause the wl_display_run() above to
      // return
    wl_display_terminate(WLR.wl_display);
}

extern
nested_surface_t*
wcf_external_surface_at(
    struct wcf_external_surface *wcf_external_surface,
    wld_coordinate_t        ptr_loc_sx,      // int   format, surface coord
    wld_coordinate_t        ptr_loc_sy,
    wl_fixed_t             *ptr_loc_fix_sx,  // fixed format, surface coord
    wl_fixed_t             *ptr_loc_fix_sy
) {
      // wlroots uses double format for location
    double pl_DS_x = (double)(ptr_loc_sx ); // surface
    double pl_DS_y = (double)(ptr_loc_sy ); //   "

    double pl_DN_x = 0.0;   // nested surface
    double pl_DN_y = 0.0;   //   "

    struct wlr_surface *wlr_surface = NULL;

    if ( wcf_external_surface->external_surface_type == ET_XDG_SURFACE ) {

          // Find the nested wlr_surface containing the pointer, if any.
        wlr_surface = (
            wlr_xdg_surface_surface_at(
                 wcf_external_surface->wcf_xdg_surface.wlr_xdg_surface,
                 pl_DS_x,
                 pl_DS_y,
                &pl_DN_x,
                &pl_DN_y
            )
        );
    }

#   define WLROOTS_BUG
#   ifdef  WLROOTS_BUG

    // Fix: WLROOTS_BUG.  The #else case doesn't work for brand new surfaces.

    if ( wcf_external_surface->external_surface_type == ET_XWAYLAND_SURFACE ) {

          // Is the pointer location between 0 and the surface
          // boundaries?
        struct wlr_xwayland_surface *wlr_xwayland_surface = wcf_external_surface->wcf_xwayland_surface.wlr_xwayland_surface;
        if ( (0 <= ptr_loc_sx) && (ptr_loc_sx < wlr_xwayland_surface->width ) &&
             (0 <= ptr_loc_sy) && (ptr_loc_sy < wlr_xwayland_surface->height)
        ) {
            *ptr_loc_fix_sx = wl_fixed_from_int(ptr_loc_sx);
            *ptr_loc_fix_sy = wl_fixed_from_int(ptr_loc_sy);

            pl_DN_x = wl_fixed_to_double( wl_fixed_from_int(ptr_loc_sx) );
            pl_DN_y = wl_fixed_to_double( wl_fixed_from_int(ptr_loc_sy) );

            wlr_surface = wlr_xwayland_surface->surface;

        } else {
            wlr_surface = NULL;
        }
    }

#   else

    // Fix: WLROOTS_BUG.  At the moment wlroots emits
    // wlr_xwayland_surface_listener_associate(), the wlr_surface is
    // supposed to be ready.  It is vaild at that moment, but the
    // wlr_surface_surface_at() doesn't "hit" when it should.  Later
    // the below code works as expected.  It's only just after the
    // associate that it fails.  See
    // wlr_xwayland_surface_listener_associate().

    if ( wcf_external_surface->external_surface_type == ET_XWAYLAND_SURFACE ) {

          // Find the nested wlr_surface containing the pointer, if any.
        wlr_surface = (
            wlr_surface_surface_at(
                wcf_external_surface->wcf_xwayland_surface.wlr_xwayland_surface->surface,
                 pl_DS_x,
                 pl_DS_y,
                &pl_DN_x,
                &pl_DN_y
            )
        );

    }

#   endif

      // Return location of pointer in nested surface coordinates.
    *ptr_loc_fix_sx = wl_fixed_from_double( pl_DN_x );
    *ptr_loc_fix_sy = wl_fixed_from_double( pl_DN_y );

    return wlr_surface;

}

extern
struct wlr_texture*
wcf_pixel_texture_create(
    int32_t   width,
    int32_t   height,
    int32_t   stride,
    uint32_t  format,
    void     *pix_data,
    bool     *in_use
) {
    // See <wlroots-src>/include/wlr/render/wlr_texture.h
    // This routine converts a pix_buf into a
    // wcf_pixel_texture for use by wcf_dsply_ctrl_display_surface().
    // For wlroots, a wcf_pixel_texture is a wlr_texture.

      // wlr_texture_from_pixels() probably allocates new wlr_texture
      // memory, so the pix_buf should no longer be needed.  But it's
      // alway safe to assume otherwise.
    *in_use = true;

    return(
        wlr_texture_from_pixels(
            WLR.renderer,
            format,
            stride,
            width,
            height,
            pix_data
        )
    );
}

#if 0
extern
bool
wcf_pixel_texture_update(
    void
) {
    // Fix: ToDo

    // bit-blit new pixels into an existing texture

    //wlr_texture_update_from_buffer()

}
#endif

extern
void
wcf_pixel_texture_destroy(
    struct wlr_texture *wcf_pixel_texture
) {
    if ( wcf_pixel_texture != NULL ) {
        wlr_texture_destroy(wcf_pixel_texture);
    }

    return;
}
