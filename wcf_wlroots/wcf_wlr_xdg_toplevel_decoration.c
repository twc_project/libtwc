
  // System
#include <stdlib.h>    // NULL, calloc, free

  // Wayland
#include <wayland-util.h>           // wl_container_of
#include <wayland-server-core.h>    // wl_listener, wl_signal

  // Wlroots
#include <wlr/types/wlr_xdg_decoration_v1.h>

  // APIs
#include "wcf_wlr_init.h"       // Our API to ensure consistancy
#include "wcf_wlr_listeners.h"  // wcf_wlr_xdg_decoration_manager_v1_listener_new_toplevel_decoration listener routine
#include "wcf_client.h"
#include <twc/twc_log.h>

  // Imported Data Types
#include "wcf_external_surface.h"
#include "twc_decoration.h"

 /*
 ** WLR xdg_toplevel_decoration
 **
 ** wlr_xdg_toplevel_decoration objects are created by the
 ** wlr_wlr_xdg_decoration_manager_v1.  The xdg_decoration_manager_v1
 ** listener for new_toplevel_decoration is defined in this file by
 ** the routine
 **
 **     wcf_wlr_xdg_decoration_manager_v1_listener_new_toplevel_decoration()
 **
 ** A wlr_xdg_toplevel_decoration object has its own listeners which must be
 ** dynamically created, initialized and registered.
 **
 **
 ** WLR xdg_toplevel_decoration events can be found here:
 **
 **   <wlroots_src>/include/wlr/types/wlr_xdg_decoration_v1.h
 */


 // -----------------------------------------
 // xdg_toplevel_decoration listener routines

static
void
xdg_decoration_listener_destroy(
    struct wl_listener *listener,
    void               *data
) {
    struct wcf_external_surface *wcf_external_surface = (
        wl_container_of(listener, wcf_external_surface, destroy_xdg_decoration)
    );

    wcf_decoration_destroy(
        wcf_external_surface
    );

    wcf_external_surface->wlr_xdg_toplevel_decoration = NULL;

    return;
}

static
void
xdg_decoration_listener_request_mode(
    struct wl_listener *listener,
    void               *data
) {
    struct wlr_xdg_toplevel_decoration_v1 *wlr_xdg_decoration = data;

    struct wcf_external_surface *wcf_external_surface = (
        wl_container_of(listener, wcf_external_surface, request_mode)
    );

    //LOG_INFO("xdg_decoration_listener_request_mode %d", wlr_xdg_decoration->requested_mode);

    if ( wlr_xdg_decoration->requested_mode ==
         WLR_XDG_TOPLEVEL_DECORATION_V1_MODE_NONE
    ) {
          // This is wlroots way of saying that the user called
          // zxdg_toplevel_decoration_v1_unset_mode().
        wcf_decoration_unset_mode( wcf_external_surface );

    } else {
        wcf_decoration_set_mode(
            wcf_external_surface,
            wlr_xdg_decoration->requested_mode
        );
    }

    return;
}


 // -----------------------------
 // WLR xdg_decoration initialize
 //
 // wlr_xdg_decoration_v1 objects are created by the
 // xdg_decoration_manager_v1.

extern
void
wcf_wlr_xdg_decoration_manager_v1_listener_new_toplevel_decoration(
    struct wl_listener *listener,
    void               *data
) {
    //LOG_INFO("wcf_wlr_xdg_decoration_manager_v1_listener_new_toplevel_decoration");

    // Creates a decoration object for the surface.  A client will
    // later invoke a "decoration_mode" request on this object to
    // specify their preferred mode.
    // Server side decorating of the surface is controled only through
    // the "decoration_mode" request.
    //
    // Clients must not create a decoration object for a surface which
    // has a buffer attached.  Perhaps wlroots already filters for
    // this?

    struct wlr_xdg_toplevel_decoration_v1 *wlr_xdg_decoration = data;
    struct wlr_xdg_toplevel               *wlr_xdg_toplevel   = wlr_xdg_decoration->toplevel;
    struct wlr_xdg_surface                *wlr_xdg_surface    = wlr_xdg_toplevel->base;

      // Earlier, we left a cookie in the wlr_xdg_surface.
    struct wcf_external_surface *wcf_external_surface = wlr_xdg_surface->data;
    if ( ! wcf_external_surface ) {
        LOG_ERROR("No surface for decoration.");
        return;
    }

    wcf_external_surface->wlr_xdg_toplevel_decoration = wlr_xdg_decoration;

    // Setup the decoration listeners.

      // Specify the routines for wlr_xdg_decoration listeners.
    wcf_external_surface->request_mode          .notify = xdg_decoration_listener_request_mode;
    wcf_external_surface->destroy_xdg_decoration.notify = xdg_decoration_listener_destroy;

    wl_signal_add(&wlr_xdg_decoration->events.request_mode, &wcf_external_surface->request_mode);
    wl_signal_add(&wlr_xdg_decoration->events.destroy,      &wcf_external_surface->destroy_xdg_decoration);

    wcf_decoration_get_toplevel_decoration(
        wcf_external_surface
    );

      // wlroots sometimes bundles a requested mode along with a new
      // toplevel_decoration object.
    enum wlr_xdg_toplevel_decoration_v1_mode requested_mode  = wlr_xdg_decoration->requested_mode;

    if ( requested_mode != WLR_XDG_TOPLEVEL_DECORATION_V1_MODE_NONE ) {
        wcf_decoration_set_mode(
            wcf_external_surface,
            requested_mode
        );
    }

    return;
}
