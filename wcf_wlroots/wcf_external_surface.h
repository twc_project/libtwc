#ifndef WCF_EXTERNAL_SURFACE_H
#define WCF_EXTERNAL_SURFACE_H

  // Wayland
#include <wayland-server-core.h>    // wl_listener

  // APIs
#include "external_surface.h"

  // Opaque types
struct wl_surface;
struct wl_buffer;
struct xdg_surface;
struct xdg_toplevel;
struct wlr_xdg_surface;
struct wlr_xdg_toplevel_decoration_v1;
struct zxdg_toplevel_decoration_v1;

struct wcf_external_surface {

      // Each external surface, is wrapped up as an external buffer.
      // The wcf_client then creates a complex of objects against the
      // BiC serever starting with the external buffer and culminating
      // in a toplevel.
    struct wl_surface   *wl_surface;
    struct wl_buffer    *wl_buffer[2];  // will be external buffers
    int                  next_buffer;
    bool                 is_mapped;
    struct xdg_surface  *xdg_surface;
    struct xdg_toplevel *xdg_toplevel;

      // Each type of external surface has its own set of listeners.
    enum external_surface_type external_surface_type;

    union {
        struct {
              // ET_XDG_SURFACE
            struct wlr_xdg_surface *wlr_xdg_surface;

              // Listeners to register in the wlr_surface.
            struct wl_listener client_commit;
            struct wl_listener precommit;
            struct wl_listener commit;
            struct wl_listener map;
            struct wl_listener unmap;
            struct wl_listener new_subsurface;
            struct wl_listener destroy_wlr_surface;

              // Listeners to register in the wlr_xdg_surface.
            struct wl_listener destroy_xdg_surface;
            struct wl_listener ping_timeout;
            struct wl_listener new_popup;
            struct wl_listener configure;
            struct wl_listener ack_configure;

              // Listeners to register in the wlr_xdg_toplevel.
            struct wl_listener request_maximize;
            struct wl_listener request_fullscreen;
            struct wl_listener request_minimize;
            struct wl_listener request_move;
            struct wl_listener request_resize;
            struct wl_listener request_show_window_menu;
            struct wl_listener set_parent;
            struct wl_listener set_title;
            struct wl_listener set_app_id;
        } wcf_xdg_surface;

        struct {
              // ET_XWAYLAND_SURFACE
            struct wlr_xwayland_surface *wlr_xwayland_surface;

              // Listeners to register in the wlr_xwayland_surface.
            struct wl_listener destroy;
            struct wl_listener request_configure;
            struct wl_listener request_move;
            struct wl_listener request_resize;
            struct wl_listener request_minimize;
            struct wl_listener request_maximize;
            struct wl_listener request_fullscreen;
            struct wl_listener request_activate;

            struct wl_listener map;
            struct wl_listener unmap;
            struct wl_listener set_title;
            struct wl_listener set_class;
            struct wl_listener set_role;
            struct wl_listener set_parent;
            struct wl_listener associate;
            struct wl_listener dissociate;
            struct wl_listener set_pid;
            struct wl_listener set_startup_id;
            struct wl_listener set_window_type;
            struct wl_listener set_hints;
            struct wl_listener set_decorations;
            struct wl_listener set_override_redirect;
            struct wl_listener set_geometry;
            struct wl_listener ping_timeout;
        } wcf_xwayland_surface;
    };

    bool xdg_surface_destroyed;

      // There is a disagreement between wlroots and many clients as
      // to when an app_id should be set by the client.  The result is
      // that, for some clients, wlroots does not report an app_id to
      // a compositor, but it is available when title is reported.
    bool app_id_set;
    bool title_set;

      // Clients may request a decoration preference for the
      // xdg_toplevel object.  Track the current mode.
    struct wlr_xdg_toplevel_decoration_v1 *wlr_xdg_toplevel_decoration;
    struct zxdg_toplevel_decoration_v1    *xdg_decoration;

      // Listeners to register in the wlr_xdg_toplevel_decoration.
    struct wl_listener request_mode;
    struct wl_listener destroy_xdg_decoration;
};

#endif  // WCF_EXTERNAL_SURFACE_H
