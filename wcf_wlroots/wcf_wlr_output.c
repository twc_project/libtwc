
  // System
#include <stdlib.h>                 // NULL, calloc, free

  // Wayland
#include <wayland-util.h>           // wl_container_of, wl_list
#include <wayland-server-core.h>    // wl_listener, wl_signal

  // Wlroots
#include <wlr/types/wlr_output.h>
#include <wlr/render/wlr_renderer.h>

  // TWC
#include <twc/twc_log.h>

  // APIs
#include "wcf_wlr_init.h"       // Our API to ensure consistancy
#include "wcf_wlr_listeners.h"  // new_output listener routines
#include "twc_device.h"         // twc_output_announce()

  // Imported Data Types
#include "wcf_wlr.h"
#include "wcf_output.h"


 /*
 ** WLR output
 **
 ** wlr_output objects are created by the wlr_backend.  The backend
 ** listener for new_ouput is defined in this file by the routine
 **
 **     wcf_wlr_backend_listener_new_output()
 **
 ** The WLR output events can be found here:
 **
 **   <wlroots_src>/include/wlr/types/wlr_output.h
 */

 // ------------------------
 // output listener routines

static
void
output_listener_frame(
    struct wl_listener    *listener,
    void                  *data
) {
    struct wlr_output *wlr_output = data;

    struct wcf_output *wcf_output = (
        wl_container_of(listener, wcf_output, frame)
    );

    struct wlr_renderer *wlr_renderer = wcf_output->wlr->renderer;

    if ( ! wlr_output_attach_render(wlr_output, NULL) ) {
        return;
    }

      //               Red  Green   Blue  Alpha
//  float color[4] = {0.25,  0.41,  1.00,  1.00};   // RoyalBlue
//  float color[4] = {0.00,  0.00,  1.00,  1.00};   // Blue
    float color[4] = {0.00,  0.00,  0.00,  1.00};   // Black

    wlr_renderer_begin(wlr_renderer, wlr_output->width, wlr_output->height);
    wlr_renderer_clear(wlr_renderer, color);

      // Instruct display server to repaint output framebuffer
    twc_output_compose(wcf_output);

    wlr_output_render_software_cursors(wlr_output, NULL);
    wlr_renderer_end (wlr_renderer);
    wlr_output_commit(wlr_output);

    return;
}

#if 0  // Below are wlr_output events currently unused by TWC.

void
output_listener_damage_frame(
    struct wl_listener    *listener,
    void                  *data
) {
    struct wlr_output_damage *wlr_output_damage = data;
    struct wcf_output *wcf_output = (
        wl_container_of(listener, wcf_output, damage_frame)
    );

    LOG_INFO("output_listener_damage_frame %p", (void *) data);
    LOG_INFO("output_listener_damage_frame %p", (void *) wlr_output_damage);

    return;
}

static
void
output_listener_needs_frame(
    struct wl_listener *listener,
    void               *data
) { return; }

static
void
output_listener_precommit(
    struct wl_listener *listener,
    void               *data
) { return; }

static
void
output_listener_commit(
    struct wl_listener *listener,
    void               *data
) { return; }

static
void
output_listener_present(
    struct wl_listener *listener,
    void               *data
) { return; }

static
void
output_listener_bind(
    struct wl_listener *listener,
    void               *data
) { return; }

static
void
output_listener_enable(
    struct wl_listener *listener,
    void               *data
) { return; }

static
void
output_listener_mode(
    struct wl_listener *listener,
    void               *data
) { return; }

static
void
output_listener_description(
    struct wl_listener *listener,
    void               *data
) { return; }

#endif // Currently unused.


static
void
output_listener_destroy(
    struct wl_listener    *listener,
    void                  *data
) {
    LOG_INFO("output_listener_destroy: wlr_output %p", (void *) data);

      // Fix: wlroots is broken.  On termination,
      // output_listener_destroy is called twice for one output.  We
      // end up calling free() twice on the same wcf_output, resulting
      // in a crash.  The problem is that in the version of wlroots I
      // have, the backend_destroy() routine in backend/drm/backend.c
      // calls wlr_output_destroy() for each connector on the output.
      // Later versions call destroy_drm_connector(conn) instead.
    static int output_destroyed = 0;
    if ( output_destroyed ) {
        return;
    }
    output_destroyed = 1;

    struct wcf_output *wcf_output = (
        wl_container_of(listener, wcf_output, destroy)
    );
LOG_INFO("output_listener_destroy: wcf_output %p", wcf_output);

      // The counterpart to twc_output_announce()
    twc_output_remove(wcf_output);

      // Remove output from the output list.
    wl_list_remove(&wcf_output->link);

    free(wcf_output);

    return;
}

 // ---------------------
 // WLR output initialize
 //
 // wlr_output objects are created by the wlr_backend.

// Fix: remove this?
#include <wlr/types/wlr_output_management_v1.h>
extern struct wlr_state WLR;

extern
void
wcf_wlr_backend_listener_new_output(
    struct wl_listener *listener,
    void               *data
) {
    // Registered in ./wcf_wlr_backend.c

    struct wlr_output  *wlr_output = data;
    struct wlr_state   *wlr        = (
        wl_container_of(listener, wlr, new_output)
    );

    //LOG_INFO("wcf_wlr_backend_listener_new_output: wlr_output %p", (void *) data);

    wlr_output_init_render(wlr_output, wlr->allocator, wlr->renderer);

#   define notTEN_24
      // Set the output resolution, aka mode.
    if ( ! wl_list_empty(&wlr_output->modes) ) {

        LOG_INFO("wcf_wlr_backend_listener_new_output not empty");

        struct wlr_output_mode *mode;
#       ifdef TEN_24
        wl_list_for_each( mode, &wlr_output->modes, link) {
            LOG_INFO(
                "wcf_wlr_backend_listener_new_output %d %d %d",
                 mode->width,
                 mode->height,
                 mode->refresh
            );

            if ( mode->width == 1024 ) { break; }
        }
#       else
        mode = wlr_output_preferred_mode(wlr_output);
#       endif

        wlr_output_set_mode(wlr_output, mode);
        wlr_output_enable  (wlr_output, true);
        if ( ! wlr_output_commit(wlr_output) ) {
            LOG_INFO("wcf_wlr_backend_listener_new_output commit failed");
            return;
        }
    }

      // wlr_output->name is derived from /sys/class/drm/*
      // Eg, HDMI-A-1
      // Use this to arrange output-layout as requested in config file.
    LOG_INFO(
        "wcf_wlr_backend_listener_new_output name %s width %d height %d",
         wlr_output->name,
         wlr_output->width,
         wlr_output->height
    );

    struct wcf_output *wcf_output = (
       calloc( 1, sizeof(struct wcf_output) )
    );

    wcf_output->wlr        = wlr;
    wcf_output->wlr_output = wlr_output;

      // Specify listener routines for each wlr_output event.
    wcf_output->frame.notify   = output_listener_frame;
    wcf_output->destroy.notify = output_listener_destroy;

      // Register the output listeners.
    wl_signal_add(&wlr_output->events.frame,   &wcf_output->frame);
    wl_signal_add(&wlr_output->events.destroy, &wcf_output->destroy);

    // Fix: enable damage
    //wcf_output->wlr_output_damage   = wlr_output_damage_create(wlr_output);
    //wcf_output->damage_frame.notify = output_listener_damage_frame;
    //wl_signal_add(&wcf_output->wlr_output_damage->events.frame, &wcf_output->damage_frame);

      // Add output to the output list.
    wl_list_insert(&wlr->output_list, &wcf_output->link);

    LOG_INFO("wcf_wlr_backend_listener_new_output: wcf_output %p", wcf_output);

    wcf_output->srv_output = (
        twc_output_announce(
            wcf_output,
            wlr_output->name,
            wlr_output->width,
            wlr_output->height
        )
    );

      // Enable screenshots on this output for grim.  https://git.sr.ht/~emersion/grim
    struct wlr_output_configuration_v1 *wlr_output_configuration_v1 = (
        wlr_output_configuration_v1_create()
    );
    wlr_output_configuration_head_v1_create( wlr_output_configuration_v1, wlr_output );
    wlr_output_manager_v1_set_configuration( WLR.wlr_output_manager_v1,   wlr_output_configuration_v1 );

    return;
}
