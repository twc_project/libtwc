
  // System
#include <stddef.h>    // NULL

  // Wayland
#include <wayland-server-core.h>    // wl_listener, wl_signal

  // Wlroots
#include <wlr/types/wlr_xdg_decoration_v1.h>

  // TWC
#include <twc/twc_log.h>

  // APIs
#include "wcf_wlr_init.h"       // Our API to ensure consistancy
#include "wcf_wlr_listeners.h"  // wcf_wlr_xdg_decoration_manager_v1_listener_new_toplevel_decoration listener routine

  // Imported Data Types
#include "wcf_wlr.h"

 /*
 ** WLR xdg decoration manager
 **
 ** wlr_xdg_decoration_manager_v1_create() places the global
 ** singleton zxdg_decoration_manager_v1 object into the server
 ** registry for use by clients.
 **
 ** A zxdg_decoration_manager_v1 is a factory for
 ** zxdg_toplevel_decoration_v1 objects.  See
 **
 **    <wayland_src>/wayland-protocols/unstable/xdg-decoration/xdg-decoration-unstable-v1.xml
 **
 ** WLR xdg decoration manager events can be found here:
 **
 **   <wlroots_src>/include/wlr/types/wlr_xdg_decoration_v1.h
 **
 ** Xdg decoration manager listeners are part of the WLR state.
 */


 // ----------------------------------------
 // xdg decoration manager listener routines
 //
 // Since each new xdg_toplevel_decoration is a complex object with
 // its own listeners etc, the listener routine
 //
 //     wcf_wlr_xdg_decoration_manager_v1_listener_new_toplevel_decoration()
 //
 // is defined in
 //
 //    wcf_wlr_xdg_toplevel_decoration.c

static
void
xdg_decoration_manager_v1_listener_destroy(
    struct wl_listener *listener,
    void               *data
) {
    /*

    struct wlr_xdg_toplevel_decoration_v1 *wlr_xdg_toplevel_decoration = data;
    LOG_INFO(
        "xdg_decoration_manager_v1_listener_destroy %p",
        (void *) wlr_xdg_toplevel_decoration
    );

    */

    return;
}

 // -------------------------------------
 // WLR xdg decoration manager initialize

extern
error_t
wcf_wlr_xdg_decoration_manager(
    struct wlr_state *wlr
) {
    wlr->xdg_decoration_manager =
        wlr_xdg_decoration_manager_v1_create(wlr->wl_display);

    if ( wlr->xdg_decoration_manager == NULL ) {
        LOG_ERROR("WLR xdg_decoration_manager create failed.");
        return ERROR;
    }

    //LOG_INFO("Created WLR xdg_decoration_manager %p.", wlr->xdg_decoration_manager);

      // Specify listener routines for each xdg_decoration_manager event.
    wlr->new_toplevel_decoration       .notify = wcf_wlr_xdg_decoration_manager_v1_listener_new_toplevel_decoration;
    wlr->destroy_xdg_decoration_manager.notify =         xdg_decoration_manager_v1_listener_destroy;

      // Register the xdg_decoration_manager_v1 listeners
    wl_signal_add(&wlr->xdg_decoration_manager->events.new_toplevel_decoration, &wlr->new_toplevel_decoration       );
    wl_signal_add(&wlr->xdg_decoration_manager->events.destroy,                 &wlr->destroy_xdg_decoration_manager);

    return OK;
}
