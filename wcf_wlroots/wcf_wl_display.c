
  // System
#include <stddef.h>  // NULL

  //TWC
#include <twc/twc_log.h>

  // APIs
#include "wcf_wlr_init.h"  // Our API to ensure consistancy

  // Imported Data Types
#include "wcf_wlr.h"

 /*
 ** WL display
 **
 ** The Wayland global singleton wl_display is the Wayland "core
 ** global object."  Wlroots has no wrapper function for creating
 ** this object.  It will be placed into the server registry for use
 ** by clients.  See
 **
 **   <wayland_src>/wayland/protocol/wayland.xml
 **
 ** A wl_display has no state visible outside of libwayland-server
 ** code.
 */

extern
error_t
wcf_wl_display(
    struct wlr_state *wlr
) {
      // Ask libwayland to instantiate a nascent Wayland server.
    wlr->wl_display = wl_display_create();

    if (wlr->wl_display == NULL) {
        LOG_ERROR("Wayland display create failed.");
        return ERROR;
    }

    LOG_INFO("Created Wayland display.");
    return OK;
}
