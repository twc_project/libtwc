
  // System
#include <stddef.h>    // NULL
#include <stdbool.h>

  // Wayland
#include <wayland-util.h>           // wl_container_of, wl_list
#include <wayland-server-core.h>    // wl_listener, wl_signal

  // Wlroots
#include <wlr/backend.h>
#include <wlr/render/wlr_renderer.h>
#include <wlr/render/allocator.h>
#include <wlr/types/wlr_compositor.h>
#include <wlr/types/wlr_scene.h>

  //TWC
#include <twc/twc_log.h>

  // APIs
#include "wcf_wlr_init.h"       // Our API to ensure consistancy
#include "wcf_wlr_listeners.h"  // new_input, new_output listener routines

  // Imported Data Types
#include "wcf_wlr.h"

 /*
 ** WLR backend
 **
 ** Wayland does not have a wl_backend object.  A wlr_backend is an
 ** abstraction of the underlying IO system.  It is a factory for
 ** wlr_input_device and wlr_output objects.  These correspeond to
 ** input devices (keyboard, mouse, ...)  and graphics output
 ** devices.  Several types of backend are provided.  See
 **
 **     <wlroots_src>/README.md
 **
 ** wlr_backend_autocreate() chooses an appropriate backend type.
 **
 ** WLR backend events can be found here:
 **
 **   <wlroots_src>/include/wlr/backend.h
 **
 ** Backend listeners are part of the WLR state.
 **
 ** WLR renderer routines can be found here:
 **   <wlroots_src>/include/render/wlr_renderer.h
 */

 /*
 ** Backend listener routines.
 **
 ** Since each new input device and new output is a complex object
 ** with their own listeners etc, the listener routines
 **
 **     wcf_wlr_backend_listener_new_input()
 **     wcf_wlr_backend_listener_new_output()
 **
 ** are defined in
 **
 **    wcf_wlr_input_device.c
 **    wcf_wlr_output.c
 */

static
void
backend_listener_destroy(
    struct wl_listener *listener,
    void               *data
) {
    // Fix: ToDo

//  struct wlr_backend *wlr_backend = data;
    struct wlr_state   *wlr = (
        wl_container_of(listener, wlr, destroy_backend)
    );

    // destroy
    //   output_list
    //   keyboard_list
    //   pointer_list

   return;
}


 // ----------------------
 // WLR backend initialize
 //
 // Only one wlr_backend can be created.  The wlr_backend contains a
 // wlr_renderer.
 //

extern
error_t
wcf_wlr_backend(
    struct wlr_state *wlr
) {
    // This routine follows main() in tinywl.c

    // Fix: It seems that wlr_scene is an optional convenience which
    // manages the Z-axis stacking of surfaces and can draw the whole
    // stack using one easy subroutine call.  We don't use it.  The
    // reason is that TWC creates its own images (wlr_texture) and it
    // isn't clear how to insert a wlr_texture into a scene.

    wlr->backend = (
        wlr_backend_autocreate(
            wlr->wl_display,
           &wlr->session
        )
    );

    if ( wlr->backend == NULL ) {
        LOG_ERROR("WLR backend create failed.");
        return ERROR;
    } else {
        LOG_INFO("Created WLR backend.");
    }

      // Create a WLR renderer base on the WLR backend.  We'll use it
      // to draw each wlr_texture of a client surface.
    wlr->renderer = wlr_renderer_autocreate( wlr->backend );

    if ( wlr->renderer == NULL ) {
        LOG_ERROR("WLR backend renderer not found.");
        return ERROR;
    } else {
        LOG_INFO("Found WLR backend renderer.");
    }

      // Initialize the backend renderer.
    bool wlr_renderer_ok = (
        wlr_renderer_init_wl_display(
            wlr->renderer,
            wlr->wl_display
        )
    );

    if ( wlr_renderer_ok ) {
        LOG_INFO("Initialized WLR backend renderer.");
    } else {
        LOG_ERROR("WLR backend rendered failed to initialize.");
        return ERROR;
    }

    wlr->allocator = wlr_allocator_autocreate(
        wlr->backend,
        wlr->renderer
    );

    if ( wlr->allocator == NULL ) {
        LOG_ERROR("WLR allocator not found.");
        return ERROR;
    } else {
        LOG_INFO("Found WLR allocator.");
    }

      // Initialize the lists of inputs and outputs
    wl_list_init(&wlr->keyboard_list);
    wl_list_init(&wlr-> pointer_list);
    wl_list_init(&wlr->  output_list);
    wlr->pointer_count = 0;

      // Specify listener routines for each wlr_backend event.  The
      // routines for the new_input and new_output listeners are
      // defined externally as described above.
    wlr->new_input      .notify = wcf_wlr_backend_listener_new_input;
    wlr->new_output     .notify = wcf_wlr_backend_listener_new_output;
    wlr->destroy_backend.notify =         backend_listener_destroy;

      // Register the backend listeners.
    wl_signal_add(&wlr->backend->events.new_input,  &wlr->new_input);
    wl_signal_add(&wlr->backend->events.new_output, &wlr->new_output);
    wl_signal_add(&wlr->backend->events.destroy,    &wlr->destroy_backend);

    return OK;
}
