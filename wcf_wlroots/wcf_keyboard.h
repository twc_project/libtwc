#ifndef WCF_KEYBOARD_H
#define WCF_KEYBOARD_H

  // Wayland
#include <wayland-server-core.h>  // wl_listener, wl_list

  // Opaque types
struct wlr_state;
struct wlr_input_device;
struct srv_keyboard;

struct wcf_keyboard {
    // Announced (indirectly) by wlr_backend listener new_input.

    // There may be many keyboards.

    struct wl_list           link;
    struct wlr_state        *wlr;
    struct wlr_input_device *wlr_input_device;
    struct wlr_keyboard     *wlr_keyboard;

    struct srv_keyboard *srv_keyboard;

    struct wl_listener key;
    struct wl_listener modifiers;
    struct wl_listener keymap;
    struct wl_listener repeat_info;
    struct wl_listener destroy;
};

#endif // WCF_KEYBOARD_H
