
  // System
#include <stddef.h>    // NULL

  // Wayland
#include <wayland-server-core.h>    // wl_listener, wl_signal

  // Wlroots
#include <wlr/types/wlr_text_input_v3.h>

  // TWC
#include <twc/twc_log.h>

  // APIs
#include "wcf_wlr_init.h"    // Our API to ensure consistancy

  // Imported Data Types
#include "wcf_wlr.h"


 /*
 ** WLR text input manager
 **
 ** WLR text input manager events can be found here:
 **
 **   <wlroots_src>/include/wlr/types/wlr_text_input_v3.h
 **
 ** Text input manager listeners are part of the WLR state.
 */


 // ------------------------------------
 // Text input manager listener routines

static
void
text_input_manager_listener_destroy(
    struct wl_listener *listener,
    void               *data
) {
    LOG_INFO("text_input_manager_listener_destroy.");

    return;
}


 // ---------------------------------
 // WLR text input manager initialize
 //
 // Since text_input_manager is a singleton, only one is created.

extern
error_t
wcf_wlr_text_input_manager(
    struct wlr_state *wlr
) {
    wlr->text_input_manager = (
        wlr_text_input_manager_v3_create(wlr->wl_display)
    );

    if ( wlr->text_input_manager == NULL ) {
        LOG_ERROR("WLR text_input_device_manager create failed.");
        return ERROR;
    }

      // Specify listener routines for each text_input_device_manager event.
    wlr->destroy_text_input_manager.notify = text_input_manager_listener_destroy;

      // Register the text_input_device_manager listeners.
    wl_signal_add(&wlr->text_input_manager->events.destroy, &wlr->destroy_text_input_manager);

    LOG_INFO("Created WLR text_input_manager.");

    return OK;
}
