
  // System
#include <stddef.h>  // NULL

  // Wayland
#include <wayland-util.h>           // wl_container_of
#include <wayland-server-core.h>    // wl_listener, wl_signal

  // Wlroots
#include <wlr/types/wlr_compositor.h>
#include <wlr/types/wlr_xdg_shell.h>
#include <wlr/xwayland.h>

  // TWC
#include <twc/twc_log.h>

  // APIs
#include "wcf_wlr_init.h"       // Our API to ensure consistancy
#include "wcf_wlr_listeners.h"  // wcf_wlr_xdg_shell_listener_new_surface listener()
#include "wcf_client.h"

  // Imported Data Types
#include "wcf_wlr.h"
#include "twc_decoration.h"
#include "wcf_external_surface.h"

 /*
 ** WLR xdg_surface
 **
 ** wlr_xdg_surface objects are created by the wlr_xdg_shell.  The
 ** xdg_shell listener for new_surface is defined in this file by the
 ** routine
 **
 **     wcf_wlr_xdg_shell_listener_new_surface()
 **
 ** Some xdg_surfaces have the xdg_toplevel role.  TWC only oversees
 ** xdg_surface objects.  And then only those which have the
 ** xdg_toplevel role.  All other surfaces are left to be managed
 ** internally by wlroots.
 **
 ** Every wlr_xdg_toplevel object is associated with additional
 ** surface objects as follows:
 **
 **     wlr_xdg_toplevel    (the subject toplevel)
 **     wlr_xdg_surface
 **     wlr_surface
 **
 ** Each has its own listeners which must be dynamically created,
 ** initialized and registered.  All these listeners are stored in a
 ** single wrapper object called
 **
 **     wcf_external_surface
 **
 ** The WLR xdg_surface and xdg_toplevel events can be found here:
 **
 **     <wlroots_src>/include/wlr/types/wlr_xdg_shell.h
 **
 ** The WLR surface events can be found here:
 **
 **     <wlroots_src>/include/wlr/types/wlr_compositor.h
 */


 // -----------------------------
 // wlr_surface listener routines

static
void
wlr_surface_listener_client_commit(
    struct wl_listener *listener,
    void               *data
) {
    //LOG_INFO("wlr_surface_listener_client_commit");

    return;
}

static
void
wlr_surface_listener_precommit(
    struct wl_listener *listener,
    void               *data
) {
    //LOG_INFO("wlr_surface_listener_precommit");

    //struct wlr_surface_state *next = data;

    return;
}

static
void
wlr_surface_listener_commit(
    struct wl_listener *listener,
    void               *data
) {
    // In wlr_compositor.c (version 17), data is set to *wlr_surface.

    //LOG_INFO("wlr_surface_listener_commit");

    struct wcf_external_surface *wcf_external_surface = (
        wl_container_of(listener, wcf_external_surface, wcf_xdg_surface.commit)
    );

    struct wlr_xdg_surface *wlr_xdg_surface = (
        wcf_external_surface->wcf_xdg_surface.wlr_xdg_surface
    );

    struct wlr_xdg_toplevel *wlr_xdg_toplevel = (
        wcf_external_surface->wcf_xdg_surface.wlr_xdg_surface->toplevel
    );

    if ( wcf_external_surface->xdg_surface_destroyed ) {
          // Between a wlr_xdg_surface destroy and a wlr_surface
          // destroy, the client (libreoffice) may commit.
        return;
    }

      // See the comments in xdg_toplevel_listener_set_app_id() below.
    if (   wlr_xdg_toplevel->app_id    != NULL &&
           wlr_xdg_toplevel->app_id[0] != '\0' &&
         ! wcf_external_surface->app_id_set
    ) {
        //LOG_INFO("GOT APP ID %s", wlr_xdg_toplevel->app_id);

        /*
        ** See the comments for set_app_id.  Sometimes the app_id is
        ** available even tho the compositor is not notified.
        */

        wcf_external_surface->app_id_set = true;

        //LOG_INFO("xdg_toplevel_listener_set_app_id %s", wlr_xdg_toplevel->app_id);

        wcf_xdg_toplevel_set_app_id(
            wcf_external_surface,
            wlr_xdg_toplevel->app_id
        );
    }
    if (   wlr_xdg_toplevel->title    != NULL &&
           wlr_xdg_toplevel->title[0] != '\0' &&
         ! wcf_external_surface->title_set
    ) {
        //LOG_INFO("GOT APP ID %s", wlr_xdg_toplevel->app_id);

        /*
        ** See the comments for set_app_id.  Sometimes the app_id is
        ** available even tho the compositor is not notified.
        */

        wcf_external_surface->title_set = true;

        //LOG_INFO("xdg_toplevel_listener_set_app_id %s", wlr_xdg_toplevel->app_id);

        wcf_xdg_toplevel_set_title(
            wcf_external_surface,
            wlr_xdg_toplevel->title
        );
    }

    /*
    ** In the Wayland Protocol, a wl_surface may have wl_subsurfaces.
    ** They form a hierarchy where the root surface, called the main
    ** surface, has no parent.  A main surface with its sub-surfaces
    ** forms a (compound) window, to be treated as a single unit.
    **
    ** The "visible bounds" of a window is determined by the main
    ** surface.  Sub-surfaces are allowed to extend beyond the
    ** rectangle of the main surface.  However, most of the time,
    ** "spillover" pixels should remain "invisible."  A (compound)
    ** window must be located based on its main surface.  That way, if
    ** any spillover region changes size, the window does not move.
    **
    ** In the xdg_shell Protocol, the xdg_surface.set_window_geometry
    ** request can be used to explicity set the "visible" portion of a
    ** (compound) window.  The compositor will clamp (ie,clip) the
    ** (compound) window, including the main surface, to the set
    ** geometry.  In this case, the set_window_geometry is considered
    ** to be the visible bounds used to size and locate the window.
    **
    ** In wlroots, the set_window geometry is given by a wlr_box found
    ** in wlr_xdg_surface.current.  It will be empty if the client has
    ** not specified a set_window_geometry.
    **
    ** The routine wlr_surface_get_extends(wlr_surface, &compound_box)
    ** can be used to discover a bounding rectangle for the entire
    ** compound window.
    **
    ** The routine wlr_xdg_surface_get_geometry(wlr_xdg_surface, &box)
    ** yields either
    **
    **   A) the clamped window geometry, if set_window_geometry is
    **      non-empty, or the compound box if not.
    **
    **   B) the compound box.
    **
    ** In case A,, the visible box is known as it is set by fiat.
    **
    ** In case B, we cannot calculate the visible box.  The user has
    ** not used the set_window_geometry request.  As an example,
    ** suppose compound_box.x is zero.  The right side of the compound
    ** is at compound_box.width, but there's no way to know the right
    ** side of the main surface.
    **
    ** In case B, the best we can do it assume the compound box is
    ** also the visible bounds.
    **
    ** wlr_surface_get_extends(     wlr_xdg_surface->surface, &compound_box);
    */

    struct wlr_box visible_box;
    wlr_xdg_surface_get_geometry(wlr_xdg_surface,  &visible_box);

    wld_geometry_t visible_geometry;
    visible_geometry.x      = visible_box.x;
    visible_geometry.y      = visible_box.y;
    visible_geometry.width  = visible_box.width;
    visible_geometry.height = visible_box.height;

#   if 0
    struct wlr_box compound_box;
    wlr_surface_get_extends(wlr_xdg_surface->surface, &compound_box);
    LOG_INFO("compound_box %3d %3d %3d %3d", compound_box.x, compound_box.y, compound_box.width, compound_box.height);
    LOG_INFO(" visible_box %3d %3d %3d %3d",  visible_box.x,  visible_box.y,  visible_box.width,  visible_box.height);
#   endif

    if ( visible_geometry.width  == 0 ||
         visible_geometry.height == 0
    ) {
        wld_geometry_t zero_geometry = {0,0,0,0};
        wcf_external_surface_commit(
            wcf_external_surface,
            zero_geometry
        );

    } else {

        wcf_external_surface_commit(
            wcf_external_surface,
            visible_geometry
        );
    }

    return;
}

static
void
wlr_surface_listener_map(
    struct wl_listener *listener,
    void               *data
) {
    //LOG_INFO("wlr_surface_listener_map");

    /*
    struct wcf_external_surface *wcf_external_surface = (
        wl_container_of(listener, wcf_external_surface, wcf_xdg_surface.map)
    );

    struct wlr_xdg_surface *wlr_xdg_surface = (
        wcf_external_surface->wcf_xdg_surface.wlr_xdg_surface
    );
    */

    return;
}

static
void
wlr_surface_listener_unmap(
    struct wl_listener *listener,
    void               *data
) {
    //LOG_INFO("wlr_surface_listener_unmap\n");

    return;
}

static
void
wlr_surface_listener_new_subsurface(
    struct wl_listener *listener,
    void               *data
) {
    //struct wlr_surface *wlr_surface = data;

    //LOG_INFO("wlr_surface_listener_new_subsurface %p", wlr_surface);

    return;
}

static
void
wlr_surface_listener_destroy(
    struct wl_listener *listener,
    void               *data
) {
    // In wlr_compositor.c (version 17), data is set to *wlr_surface.

    //LOG_INFO("wlr_surface_listener_destroy %p", data);

    struct wcf_external_surface *wcf_external_surface = (
        wl_container_of(listener, wcf_external_surface, wcf_xdg_surface.destroy_wlr_surface)
    );

    wcf_external_surface_destroy(wcf_external_surface);

    //LOG_INFO("wlr_surface_listener_destroy DONE");

    return;
}

 // -----------------------------
 // xdg_surface listener routines

static
void
xdg_surface_listener_destroy(
    struct wl_listener *listener,
    void               *data
) {
    // In wlr_xdg_surface.c (version 16), data is set to NULL.

    //LOG_INFO("xdg_surface_listener_destroy wlr_xdg_surface");

    struct wcf_external_surface *wcf_external_surface = (
        wl_container_of(listener, wcf_external_surface, wcf_xdg_surface.destroy_xdg_surface)
    );

    //LOG_INFO("xdg_surface_listener_destroy wcf_external_surface %p", (void *) wcf_external_surface);

      // wlroots does not report the demize of the toplevel separately
    wcf_xdg_toplevel_destroy(wcf_external_surface);
    wcf_xdg_surface_destroy( wcf_external_surface);

    return;
}

static
void
xdg_surface_listener_ping_timeout(
    struct wl_listener *listener,
    void               *data
) {
    //LOG_INFO("xdg_surface_listener_ping_timeout");

    return;
}

static
void
xdg_surface_listener_new_popup(
    struct wl_listener *listener,
    void               *data
) {
    //LOG_INFO("xdg_surface_listener_new_popup");

    return;
}

static
void
xdg_surface_listener_configure(
    struct wl_listener *listener,
    void               *data
) {
    // In wlr_xdg_surface.c (version 16), data is set to *wlr_xdg_surface_configure.

    // Wlroots tells us it sent a configure event to the client.

    //struct wlr_xdg_surface_configure *wlr_xdg_surface_configure = data;

    //LOG_INFO("xdg_surface_listener_configure %d", wlr_xdg_surface_configure->serial);

    return;
}

static
void
xdg_surface_listener_ack_configure(
    struct wl_listener *listener,
    void               *data
) {
    // In wlr_xdg_surface.c (version 16), data is set to *wlr_xdg_surface_configure.

    struct wcf_external_surface *wcf_external_surface = (
        wl_container_of(listener, wcf_external_surface, wcf_xdg_surface.ack_configure)
    );

    struct wlr_xdg_surface_configure *wlr_xdg_surface_configure = data;

    //LOG_INFO("xdg_surface_listener_ack_configure %d", wlr_xdg_surface_configure->serial);

    wcf_xdg_surface_ack_configure(
        wcf_external_surface,
        wlr_xdg_surface_configure->serial
    );

    return;
}

 // ------------------------------
 // xdg_toplevel listener routines

static
void
xdg_toplevel_listener_request_maximize(
    struct wl_listener *listener,
    void               *data
) {
    // In wlr_xdg_toplevel.c (version 16), data is set to NULL.

    //LOG_INFO("xdg_surface_listener_request_maximize");

    return;
}

static
void
xdg_toplevel_listener_request_fullscreen(
    struct wl_listener *listener,
    void               *data
) {
    // In wlr_xdg_toplevel.c (version 16), data is set to NULL.

    //LOG_INFO("xdg_surface_listener_request_fullscreen");

    return;
}

static
void
xdg_toplevel_listener_request_minimize(
    struct wl_listener *listener,
    void               *data
) {
    // In wlr_xdg_toplevel.c (version 16), data is set to NULL.

    //LOG_INFO("xdg_surface_listener_request_minimize");

    struct wcf_external_surface *wcf_external_surface = (
        wl_container_of(listener, wcf_external_surface, wcf_xdg_surface.request_minimize)
    );

    wcf_xdg_toplevel_set_minimized(wcf_external_surface);

    return;
}

static
void
xdg_toplevel_listener_request_move(
    struct wl_listener *listener,
    void               *data
) {
    // In wlr_xdg_toplevel.c (version 16), data is set to
    // *wlr_xdg_toplevel_move_event.

    //LOG_INFO("xdg_toplevel_listener_move %p", (void *) data);

    struct wcf_external_surface *wcf_external_surface = (
        wl_container_of(listener, wcf_external_surface, wcf_xdg_surface.request_move)
    );

    wcf_xdg_toplevel_move(wcf_external_surface);

    return;
}

static
void
xdg_toplevel_listener_request_resize(
    struct wl_listener *listener,
    void               *data
) {
    // In wlr_xdg_toplevel.c (version 16), data is set to
    // *wlr_xdg_toplevel_resize_event.

    //LOG_INFO("xdg_toplevel_listener_resize %p", (void *) data);

    struct wlr_xdg_toplevel_resize_event *event = data;

    struct wcf_external_surface *wcf_external_surface = (
        wl_container_of(listener, wcf_external_surface, wcf_xdg_surface.request_resize)
    );

    wcf_xdg_toplevel_resize(wcf_external_surface, event->edges);

    return;
}

static
void
xdg_toplevel_listener_request_show_window_menu(
    struct wl_listener *listener,
    void               *data
) {
    // In wlr_xdg_toplevel.c (version 16), data is set to
    // *wlr_xdg_toplevel_show_window_menu_event.

    //LOG_INFO("xdg_toplevel_listener_request_show_window_menu");

    return;
}


static
void
xdg_toplevel_listener_set_parent(
    struct wl_listener *listener,
    void               *data
) {
    // In wlr_xdg_toplevel.c (version 16), data is set to NULL.

    //LOG_INFO("xdg_toplevel_listener_set_parent");

    return;
}

static
void
xdg_toplevel_listener_set_title(
    struct wl_listener *listener,
    void               *data
) {
    // In wlr_xdg_toplevel.c (version 16), data is set to NULL.

    struct wcf_external_surface *wcf_external_surface = (
        wl_container_of(listener, wcf_external_surface, wcf_xdg_surface.set_title)
    );

    struct wlr_xdg_toplevel *wlr_xdg_toplevel = (
        wcf_external_surface->wcf_xdg_surface.wlr_xdg_surface->toplevel
    );

    //LOG_INFO("xdg_toplevel_listener_set_title %s", wlr_xdg_toplevel->title);

      // See the comments in xdg_toplevel_listener_set_app_id() below.
    if (   wlr_xdg_toplevel->app_id    != NULL &&
           wlr_xdg_toplevel->app_id[0] != '\0' &&
         ! wcf_external_surface->app_id_set
    ) {
        //LOG_INFO("GOT APP ID %s", wlr_xdg_toplevel->app_id);

        /*
        ** See the comments for set_app_id.  Sometimes the app_id is
        ** available even tho the compositor is not notified.
        */

        wcf_external_surface->app_id_set = true;

        //LOG_INFO("xdg_toplevel_listener_set_app_id %s", wlr_xdg_toplevel->app_id);

        wcf_xdg_toplevel_set_app_id(
            wcf_external_surface,
            wlr_xdg_toplevel->app_id
        );
    }

    wcf_xdg_toplevel_set_title(
        wcf_external_surface,
        wlr_xdg_toplevel->title
    );

    return;
}

static
void
xdg_toplevel_listener_set_app_id(
    struct wl_listener *listener,
    void               *data    // data points to wlr_xdg_surface
) {
    // In wlr_xdg_toplevel.c (version 16), data is set to NULL.

    /*
    ** There is a disagreement between wlroots and many clients as to
    ** when an app_id should be set by the client.  The result is
    ** that, for some clients, wlroots does not report an app_id to a
    ** compositor.
    **
    ** According to <vyivel> on wlroots IRC:
    **
    **   <FadedAce> Since I run Debian(12) Stable(bookworm), I'm using wlroots 0.15.1.
    **   <FadedAce> I never get a set_app_id event from the wlr_xdg_suface.
    **   <FadedAce> However, on recieving a map event, I check if the
    **              wlr_xdg_toplevel has an app_id set and so far it
    **              does.
    **   <FadedAce> Is this just because I'm on an older version?
    **
    **   <vyivel> FadedAce: before 0.18 (iirc) the new_surface event is
    **            emitted only after the first initial commit and
    **            xdg_toplevel.set_app_id is usually sent before that
    **
    **  <FadedAce> So the app_id event is sent before I even told there is a surface?
    **
    **   <vyivel> yep
    **
    **   <FadedAce> Since the map can't happen until after buffers are
    **              attached and that can't happen til after the initial
    **              commit my work around is a reasonable solution.
    **
    **   <vyivel> correct
    **   <FadedAce> vyivel: Ok Thanks.
    **   <vyivel> you're welcome
    **
    ** In wlroots 0.17, the map event was removed.  So we check for an
    ** app_id when the title is set.
    **
    */

    struct wcf_external_surface *wcf_external_surface = (
        wl_container_of(listener, wcf_external_surface, wcf_xdg_surface.set_app_id)
    );

    struct wlr_xdg_toplevel *wlr_xdg_toplevel = (
        wcf_external_surface->wcf_xdg_surface.wlr_xdg_surface->toplevel
    );

    //LOG_INFO("xdg_toplevel_listener_set_app_id %s", wlr_xdg_toplevel->app_id);

    wcf_external_surface->app_id_set = true;

    wcf_xdg_toplevel_set_app_id(
        wcf_external_surface,
        wlr_xdg_toplevel->app_id
    );

    return;
}

 // --------------------------
 // WLR xdg_surface initialize
 //
 // wlr_xdg_surface objects are created by the xdg_shell.

extern
void
wcf_wlr_xdg_shell_listener_new_surface(
    struct wl_listener *listener,
    void               *data
) {
    // This listener registered in ./wcf_wlr_xdg_shell.c

    // In wlr_xdg_toplevel.c (version 17), data is set to wlr_xdg_surface.

    // For wlroots < 0.18, invoked only when the new surface has a
    // role.  For wlroots >= 0.18, invoked immediately when
    // xdg_surface is created.  Use events.new_toplevel to get prior
    // behavoir.

      // The new xdg_surface.
    struct wlr_xdg_surface *wlr_xdg_surface = data;

      // TWC only oversees external xdg_surfaces having the role of
      // xdg_toplevel.  We assume that wlroots manages popups
      // internally as nested surfaces belonging to some toplevel.
    if (wlr_xdg_surface->role != WLR_XDG_SURFACE_ROLE_TOPLEVEL) {
        return;
    }

    //LOG_INFO("wcf_wlr_xdg_shell_listener_new_surface New Toplevel");

      // The wlr_xdg_toplevel and the wlr_surface are wrapped within
      // the wlr_xdg_surface.  We need to set listeners for all three.
    struct wlr_surface      *wlr_surface      = wlr_xdg_surface->surface;
    struct wlr_xdg_toplevel *wlr_xdg_toplevel = wlr_xdg_surface->toplevel;

    // At this point, the new wlr_xdg_surface has both a wlr_surface
    // and a wlr_xdg_toplevel, ie, everything needed for a nascent
    // window.  This listener should be invoked after the xdg_toplevel
    // get its role, but before attaching a buffer.  So, the
    // dimensions should be zero.  Next, inform the wcf_client of
    // these new surfaces.

    struct wcf_external_surface *wcf_external_surface = (
        wcf_external_surface_create(
            wlr_xdg_surface,
            ET_XDG_SURFACE
        )
    );
    if ( wcf_external_surface == NULL ) { return; }

    wlr_xdg_surface->data = wcf_external_surface;

    wcf_get_xdg_surface( wcf_external_surface);
    wcf_get_xdg_toplevel(wcf_external_surface);

    // Setup wlroots infra-structure for each of the surfaces

      // Initialize some of the wcf_external_surface members.
    wcf_external_surface->wlr_xdg_toplevel_decoration = NULL;
    wcf_external_surface->xdg_decoration              = NULL;

      // Specify listener routines for each wlr_surface event of interest.
    wcf_external_surface->wcf_xdg_surface.client_commit      .notify = wlr_surface_listener_client_commit;
    wcf_external_surface->wcf_xdg_surface.precommit          .notify = wlr_surface_listener_precommit;
    wcf_external_surface->wcf_xdg_surface.commit             .notify = wlr_surface_listener_commit;
    wcf_external_surface->wcf_xdg_surface.map                .notify = wlr_surface_listener_map;
    wcf_external_surface->wcf_xdg_surface.unmap              .notify = wlr_surface_listener_unmap;
    wcf_external_surface->wcf_xdg_surface.new_subsurface     .notify = wlr_surface_listener_new_subsurface;
    wcf_external_surface->wcf_xdg_surface.destroy_wlr_surface.notify = wlr_surface_listener_destroy;
      // Specify listener routines for each wlr_xdg_surface event of interest.
    wcf_external_surface->wcf_xdg_surface.destroy_xdg_surface.notify = xdg_surface_listener_destroy;
    wcf_external_surface->wcf_xdg_surface.ping_timeout       .notify = xdg_surface_listener_ping_timeout;
    wcf_external_surface->wcf_xdg_surface.new_popup          .notify = xdg_surface_listener_new_popup;
    wcf_external_surface->wcf_xdg_surface.configure          .notify = xdg_surface_listener_configure;
    wcf_external_surface->wcf_xdg_surface.ack_configure      .notify = xdg_surface_listener_ack_configure;
      // Specify listener routines for each wlr_xdg_toplevel event of interest.
    wcf_external_surface->wcf_xdg_surface.request_maximize        .notify = xdg_toplevel_listener_request_maximize;
    wcf_external_surface->wcf_xdg_surface.request_fullscreen      .notify = xdg_toplevel_listener_request_fullscreen;
    wcf_external_surface->wcf_xdg_surface.request_minimize        .notify = xdg_toplevel_listener_request_minimize;
    wcf_external_surface->wcf_xdg_surface.request_move            .notify = xdg_toplevel_listener_request_move;
    wcf_external_surface->wcf_xdg_surface.request_resize          .notify = xdg_toplevel_listener_request_resize;
    wcf_external_surface->wcf_xdg_surface.request_show_window_menu.notify = xdg_toplevel_listener_request_show_window_menu;
    wcf_external_surface->wcf_xdg_surface.set_parent              .notify = xdg_toplevel_listener_set_parent;
    wcf_external_surface->wcf_xdg_surface.set_title               .notify = xdg_toplevel_listener_set_title;
    wcf_external_surface->wcf_xdg_surface.set_app_id              .notify = xdg_toplevel_listener_set_app_id;

      // Register the wlr_surface listeners
    wl_signal_add(&wlr_surface->events.client_commit,  &wcf_external_surface->wcf_xdg_surface.client_commit);
    wl_signal_add(&wlr_surface->events.precommit,      &wcf_external_surface->wcf_xdg_surface.precommit);
    wl_signal_add(&wlr_surface->events.commit,         &wcf_external_surface->wcf_xdg_surface.commit);
    wl_signal_add(&wlr_surface->events.map,            &wcf_external_surface->wcf_xdg_surface.map);
    wl_signal_add(&wlr_surface->events.unmap,          &wcf_external_surface->wcf_xdg_surface.unmap);
    wl_signal_add(&wlr_surface->events.new_subsurface, &wcf_external_surface->wcf_xdg_surface.new_subsurface);
    wl_signal_add(&wlr_surface->events.destroy,        &wcf_external_surface->wcf_xdg_surface.destroy_wlr_surface);

      // Register the wlr_xdg_surface listeners
    wl_signal_add(&wlr_xdg_surface->events.destroy,       &wcf_external_surface->wcf_xdg_surface.destroy_xdg_surface);
    wl_signal_add(&wlr_xdg_surface->events.ping_timeout,  &wcf_external_surface->wcf_xdg_surface.ping_timeout);
    wl_signal_add(&wlr_xdg_surface->events.new_popup,     &wcf_external_surface->wcf_xdg_surface.new_popup);
    wl_signal_add(&wlr_xdg_surface->events.configure,     &wcf_external_surface->wcf_xdg_surface.configure);
    wl_signal_add(&wlr_xdg_surface->events.ack_configure, &wcf_external_surface->wcf_xdg_surface.ack_configure);

      // Register the xdg_toplevel listeners
    wl_signal_add(&wlr_xdg_toplevel->events.request_maximize,   &wcf_external_surface->wcf_xdg_surface.request_maximize);
    wl_signal_add(&wlr_xdg_toplevel->events.request_fullscreen, &wcf_external_surface->wcf_xdg_surface.request_fullscreen);
    wl_signal_add(&wlr_xdg_toplevel->events.request_minimize,   &wcf_external_surface->wcf_xdg_surface.request_minimize);
    wl_signal_add(&wlr_xdg_toplevel->events.request_move,       &wcf_external_surface->wcf_xdg_surface.request_move);
    wl_signal_add(&wlr_xdg_toplevel->events.request_resize,     &wcf_external_surface->wcf_xdg_surface.request_resize);
    wl_signal_add(&wlr_xdg_toplevel->events.request_show_window_menu,  &wcf_external_surface->wcf_xdg_surface.request_show_window_menu);
    wl_signal_add(&wlr_xdg_toplevel->events.set_parent,         &wcf_external_surface->wcf_xdg_surface.set_parent);
    wl_signal_add(&wlr_xdg_toplevel->events.set_title,          &wcf_external_surface->wcf_xdg_surface.set_title);
    wl_signal_add(&wlr_xdg_toplevel->events.set_app_id,         &wcf_external_surface->wcf_xdg_surface.set_app_id);

    return;
}
