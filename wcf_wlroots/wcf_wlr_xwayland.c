#define _POSIX_C_SOURCE 200112L     // required for setenv()

  // System
#include <stddef.h>     // NULL
#include <stdlib.h>     // setenv()

  // Wayland
#include <wayland-server-core.h>    // wl_listener, wl_signal

  // Wlroots
#include <wlr/util/box.h>
#include <wlr/xwayland.h>

  // TWC
#include <twc/twc_log.h>

  // APIs
#include "wcf_wlr_init.h"       // Our API to ensure consistancy

  // Imported Data Types
#include "wcf_wlr.h"
#include "wcf_wlr_listeners.h"  // wcf_wlr_xwayland_listener_new_surface

 /*
 ** WLR xwayland
 **
 ** wlr_xwayland_create() places the global singleton wlr_xwayland
 ** object into the server registry for use by clients.
 **
 ** A wlr_xwayland is a factory for wlr_xwayland_surface objects.
 **
 ** wlr_xwayland events can be found here:
 **
 **     <wlroots_src>/include/wlr/xwayland/xwayland.h
 **
 ** wlr_xwayland listeners are part of the WLR state.
 */


 // ------------------------------
 // wlr_xwayland listener routines
 //
 // Since each wlr_xwayland_surface is a complex object with its own listeners
 // etc, the listener routine
 //
 //     wcf_wlr_xwayland_listener_new_surface()
 //
 // is defined in
 //
 //    wcf_wlr_xwayland_surface.c

static
void
xwayland_listener_ready(
    struct wl_listener *listener,
    void               *data
) {
    LOG_INFO("xwayland_listener_ready");

    struct wlr_state *wlr = (
        wl_container_of(listener, wlr, xwayland_ready)
    );

    struct wlr_box X_net_workarea = {0,0,1920,1200};
    wlr_xwayland_set_workareas(
        wlr->wlr_xwayland,
       &X_net_workarea,
        1
    );

    return;
}

static
void
xwayland_listener_remove_startup_info(
    struct wl_listener *listener,
    void               *data
) {
    LOG_INFO("xwayland_listener_remove_startup_info");
    return;
}


 // ------------------------
 // WLR xdg_shell initialize
 //
 // Only one wlr_xdg_shell object can be created.

extern
error_t
wcf_wlr_xwayland(
    struct wlr_state *wlr
) {
#   define WLR_XWAYLAND_LAZY_MODE true

    wlr->wlr_xwayland = (
        wlr_xwayland_create(
            wlr->wl_display,
            wlr->compositor,
            WLR_XWAYLAND_LAZY_MODE  // Delay Xwayland statup until an
                                    // X client shows up.
        )
    );
    if ( wlr->wlr_xwayland == NULL ) {
        LOG_ERROR("WCF Xwayland failed.");
        return ERROR;
    }

    wlr_xwayland_set_seat(wlr->wlr_xwayland, wlr->wlr_seat);

    LOG_INFO("Created WLR xwayland.  DISPLAY=%s", wlr->wlr_xwayland->display_name);

      // Set DISPLAY environment variable for the Xwayland server
    setenv("DISPLAY", wlr->wlr_xwayland->display_name, true);


      // Specify listener routines for each wlr_xwayland event.
    wlr->xwayland_ready              .notify = xwayland_listener_ready;
    wlr->xwayland_new_surface        .notify = wcf_wlr_xwayland_listener_new_surface;
    wlr->xwayland_remove_startup_info.notify = xwayland_listener_remove_startup_info;

      // Register the wlr_xwayland listeners
    wl_signal_add(&wlr->wlr_xwayland->events.ready,               &wlr->xwayland_ready                );
    wl_signal_add(&wlr->wlr_xwayland->events.new_surface,         &wlr->xwayland_new_surface          );
    wl_signal_add(&wlr->wlr_xwayland->events.remove_startup_info, &wlr->xwayland_remove_startup_info  );

    return OK;
}
