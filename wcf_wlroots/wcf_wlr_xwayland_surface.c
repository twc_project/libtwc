
  // Wayland
#include <wayland-server-core.h>    // wl_listener, wl_signal

  // Wlroots
#include <wlr/xwayland.h>

  // TWC
#include <twc/wayland_types.h>      // wld_<type>_t
#include <twc/twc_log.h>

#include <external_buffer.h>

  // APIs
#include "wcf_wlr_listeners.h"  // wcf_wlr_xwayland_listener_new_surface
#include "wcf_client.h"

  // Imported Data Types
#include "wcf_external_surface.h"

 // --------------------------------------
 // wlr_xwayland_surface listener routines

static
void
wlr_xwayland_surface_listener_destroy(
    struct wl_listener *listener,
    void               *data
) {
    struct wcf_external_surface *wcf_external_surface = (
        wl_container_of(listener, wcf_external_surface, wcf_xwayland_surface.destroy)
    );

    //LOG_INFO("wlr_xwayland_surface_listener_destroy %p", wcf_external_surface->wcf_xwayland_surface.wlr_xwayland_surface);

    wcf_xdg_toplevel_destroy    ( wcf_external_surface );
    wcf_xdg_surface_destroy     ( wcf_external_surface );
    wcf_external_surface_destroy( wcf_external_surface );

    return;
}

static
void
wlr_xwayland_surface_listener_request_configure(
    struct wl_listener *listener,
    void               *data
) {
    //LOG_INFO("wlr_xwayland_surface_listener_request_configure");

    struct wlr_xwayland_surface_configure_event *wlr_xwayland_surface_configure_event = data;

    struct wcf_external_surface *wcf_external_surface = (
        wl_container_of(listener, wcf_external_surface, wcf_xwayland_surface.request_configure)
    );

      // Give the xwayland_surface what it wants.
    wlr_xwayland_surface_configure(
        wcf_external_surface->wcf_xwayland_surface.wlr_xwayland_surface,
        wlr_xwayland_surface_configure_event->x,
        wlr_xwayland_surface_configure_event->y,
        wlr_xwayland_surface_configure_event->width,
        wlr_xwayland_surface_configure_event->height
    );
}

static
void
wlr_xwayland_surface_listener_request_move(
    struct wl_listener *listener,
    void               *data
) {
    //LOG_INFO("wlr_xwayland_surface_listener_request_move");

    struct wcf_external_surface *wcf_external_surface = (
        wl_container_of(listener, wcf_external_surface, wcf_xwayland_surface.request_move)
    );

    wcf_xdg_toplevel_move(wcf_external_surface);

    return;
}

static
void
wlr_xwayland_surface_listener_request_resize(
    struct wl_listener *listener,
    void               *data
) {
    //LOG_INFO("wlr_xwayland_surface_listener_request_resize");

    struct wlr_xwayland_resize_event *resize_event = data;

    struct wcf_external_surface  *wcf_external_surface = (
        wl_container_of(listener, wcf_external_surface, wcf_xwayland_surface.request_resize)
    );

    wcf_xdg_toplevel_resize(wcf_external_surface, resize_event->edges);

    return;
}

static
void
wlr_xwayland_surface_listener_request_minimize(
    struct wl_listener *listener,
    void               *data
) {
    //LOG_INFO("wlr_xwayland_surface_listener_request_minimize");

    return;
}

static
void
wlr_xwayland_surface_listener_request_maximize(
    struct wl_listener *listener,
    void               *data
) {
    //LOG_INFO("wlr_xwayland_surface_listener_request_maximize");

    return;
}

static
void
wlr_xwayland_surface_listener_request_fullscreen(
    struct wl_listener *listener,
    void               *data
) {
    //LOG_INFO("wlr_xwayland_surface_listener_request_fullscreen");

    return;
}

static
void
wlr_xwayland_surface_listener_request_activate(
    struct wl_listener *listener,
    void               *data
) {
    //LOG_INFO("wlr_xwayland_surface_listener_request_activate");

    return;
}

static
struct wcf_external_surface*
wcf_xwayland_find_top_level_window(
    struct wcf_external_surface *wcf_external_surface
) {
    struct wlr_xwayland_surface *wlr_xwayland_surface = (
        wcf_external_surface->wcf_xwayland_surface.wlr_xwayland_surface
    );

    while ( wlr_xwayland_surface->parent != NULL ) {
        wlr_xwayland_surface = wlr_xwayland_surface->parent;
    }

    return wlr_xwayland_surface->data;
}

static
void
wlr_xwayland_surface_listener_associate(
     struct wl_listener *listener,
     void               *data
 ) {
    // Wlroots emits this event when the wlr_xwayland_surface is
    // "associated" with a wlr_surface.  Before this the "surface"
    // member of the wlr_xwayland_surface is invalid.

    struct wcf_external_surface  *wcf_external_surface = (
        wl_container_of(listener, wcf_external_surface, wcf_xwayland_surface.associate)
    );

    struct wlr_xwayland_surface *wlr_xwayland_surface = (
        wcf_external_surface->wcf_xwayland_surface.wlr_xwayland_surface
    );

    /*
    LOG_INFO("wlr_xwayland_surface_listener_associate %d %d %d %d",
        wlr_xwayland_surface->x,
        wlr_xwayland_surface->y,
        wlr_xwayland_surface->width,
        wlr_xwayland_surface->height
    );
    */

    if ( wlr_xwayland_surface->width  == 0 ||
         wlr_xwayland_surface->height == 0
    ) { return; }

      // Find the top-level surface of the X window hierarchy
    struct wcf_external_surface *wcf_external_top_level_surface = (
        wcf_xwayland_find_top_level_window( wcf_external_surface )
    );

    struct wl_surface *child_surface = wcf_external_surface ->wl_surface;

    if ( wcf_external_top_level_surface != wcf_external_surface ) {

        struct wl_surface *top_level_surface = wcf_external_top_level_surface->wl_surface;

        external_buffer_xwayland_child_update_location(
            child_surface,
            top_level_surface,
            wlr_xwayland_surface->x,
            wlr_xwayland_surface->y
        );
    } else {

        // Sometimes a child surface is not part of an X window
        // hierarchy, but wants relative placement to something.
        // Check for non-zero x or y coordinates.

        if ( wlr_xwayland_surface->x != 0 ||
             wlr_xwayland_surface->y != 0
        ) {
            external_buffer_xwayland_child_update_location(
                child_surface,
                NULL,       // no parent, use surface with pointer
                wlr_xwayland_surface->x,
                wlr_xwayland_surface->y
            );
        }
    }

      // xwayland surface is fully fledged, map it.
    wld_geometry_t surface_geometry = {
        wlr_xwayland_surface->x,
        wlr_xwayland_surface->y,
        wlr_xwayland_surface->width,
        wlr_xwayland_surface->height
    };
    wcf_external_surface_commit(
        wcf_external_surface,
        surface_geometry
    );

    return;
}

static
void
wlr_xwayland_surface_listener_dissociate(
    struct wl_listener *listener,
    void               *data
) {
    struct wcf_external_surface  *wcf_external_surface = (
        wl_container_of(listener, wcf_external_surface, wcf_xwayland_surface.dissociate)
    );

    //LOG_INFO("wlr_xwayland_surface_listener_dissociate");

      // Unmap
    wld_geometry_t zero_geometry = {0,0,0,0};
    wcf_external_surface_commit(
        wcf_external_surface,
        zero_geometry
    );

    return;
}

static
void
wlr_xwayland_surface_listener_set_title(
    struct wl_listener *listener,
    void               *data
) {
    struct wcf_external_surface  *wcf_external_surface = (
        wl_container_of(listener, wcf_external_surface, wcf_xwayland_surface.set_title)
    );

    struct wlr_xwayland_surface *wlr_xwayland_surface = (
        wcf_external_surface->wcf_xwayland_surface.wlr_xwayland_surface
    );

    //LOG_INFO("wlr_xwayland_surface_listener_set_title %s", wlr_xwayland_surface->title);

    if ( wlr_xwayland_surface->title == NULL ) { return; }

    wcf_xdg_toplevel_set_title(
        wcf_external_surface,
        wlr_xwayland_surface->title
    );

    return;
}

static
void
wlr_xwayland_surface_listener_set_class(
    struct wl_listener *listener,
    void               *data
) {
    struct wcf_external_surface  *wcf_external_surface = (
        wl_container_of(listener, wcf_external_surface, wcf_xwayland_surface.set_class)
    );

    struct wlr_xwayland_surface *wlr_xwayland_surface = (
        wcf_external_surface->wcf_xwayland_surface.wlr_xwayland_surface
    );

    //LOG_INFO("wlr_xwayland_surface_listener_set_class %s", wlr_xwayland_surface->class);

    wcf_external_surface->app_id_set = true;

      // We use class as the app_id
    wcf_xdg_toplevel_set_app_id(
        wcf_external_surface,
        wlr_xwayland_surface->class
    );

    return;
}

static
void
wlr_xwayland_surface_listener_set_role(
    struct wl_listener *listener,
    void               *data
) {
    //LOG_INFO("wlr_xwayland_surface_listener_set_role");

    return;
}

static
void
wlr_xwayland_surface_listener_set_parent(
    struct wl_listener *listener,
    void               *data
) {
   /*
    struct wcf_external_surface  *wcf_external_surface = (
        wl_container_of(listener, wcf_external_surface, wcf_xwayland_surface.set_parent)
    );

    struct wlr_xwayland_surface  *wlr_xwayland_surface = (
        wcf_external_surface->wcf_xwayland_surface.wlr_xwayland_surface
    );

    LOG_INFO("wlr_xwayland_surface_listener_set_parent %p %p", wlr_xwayland_surface, wlr_xwayland_surface->parent);
    */

    return;
}

static
void
wlr_xwayland_surface_listener_set_startup_id(
    struct wl_listener *listener,
    void               *data
) {
    //LOG_INFO("wlr_xwayland_surface_listener_set_startup_id");

    return;
}

static
void
wlr_xwayland_surface_listener_set_window_type(
    struct wl_listener *listener,
    void               *data
) {
    //LOG_INFO("wlr_xwayland_surface_listener_set_window_type");

    /*
    struct wcf_external_surface  *wcf_external_surface = (
        wl_container_of(listener, wcf_external_surface, wcf_xwayland_surface.set_window_type)
    );

    struct wlr_xwayland_surface  *wlr_xwayland_surface = (
        wcf_external_surface->wcf_xwayland_surface.wlr_xwayland_surface
    );

    #include <xcb/xcb.h>
    xcb_atom_t *atoms = wlr_xwayland_surface->window_type;
    size_t atoms_len  = wlr_xwayland_surface->window_type_len;

    LOG_INFO("wlr_xwayland_surface_listener_set_window_type %zu", sizeof(xcb_atom_t));
    LOG_INFO("wlr_xwayland_surface_listener_set_window_type %d", atoms[0]);
    */

    return;
}

static
void
wlr_xwayland_surface_listener_set_hints(
    struct wl_listener *listener,
    void               *data
) {
    //LOG_INFO("wlr_xwayland_surface_listener_set_hints");

    struct wcf_external_surface  *wcf_external_surface = (
        wl_container_of(listener, wcf_external_surface, wcf_xwayland_surface.set_hints)
    );

    //struct wlr_xwayland_surface  *wlr_xwayland_surface = (
    //    wcf_external_surface->wcf_xwayland_surface.wlr_xwayland_surface
    //);
    //LOG_INFO("wlr_xwayland_surface_listener_set_hints %d", wlr_xwayland_surface->hints->flags);

    return;
}

static
void
wlr_xwayland_surface_listener_set_decorations(
    struct wl_listener *listener,
    void               *data
) {
    //LOG_INFO("wlr_xwayland_surface_listener_set_decorations");

    /*
    struct wcf_external_surface  *wcf_external_surface = (
        wl_container_of(listener, wcf_external_surface, wcf_xwayland_surface.set_decorations)
    );

    struct wlr_xwayland_surface  *wlr_xwayland_surface = (
        wcf_external_surface->wcf_xwayland_surface.wlr_xwayland_surface
    );

    LOG_INFO("wlr_xwayland_surface_listener_set_decorations %d", wlr_xwayland_surface->decorations);
    */

    return;
}

static
void
wlr_xwayland_surface_listener_set_override_redirect(
    struct wl_listener *listener,
    void               *data
) {
    //LOG_INFO("wlr_xwayland_surface_listener_set_override_redirect");

    return;
}

static
void
wlr_xwayland_surface_listener_set_geometry(
    struct wl_listener *listener,
    void               *data
) {
    //LOG_INFO("wlr_xwayland_surface_listener_set_geometry");

    /*
    struct wcf_external_surface  *wcf_external_surface = (
        wl_container_of(listener, wcf_external_surface, wcf_xwayland_surface.set_geometry)
    );

     struct wlr_xwayland_surface  *wlr_xwayland_surface = (
        wcf_external_surface->wcf_xwayland_surface.wlr_xwayland_surface
    );

    LOG_INFO("wlr_xwayland_surface_listener_set_geometry %d %d %d %d %p",
        wlr_xwayland_surface->x,
        wlr_xwayland_surface->y,
        wlr_xwayland_surface->width,
        wlr_xwayland_surface->height,
        wlr_xwayland_surface
    );
    */

    return;
}

static
void
wlr_xwayland_surface_listener_ping_timeout(
    struct wl_listener *listener,
    void               *data
) {
    //LOG_INFO("wlr_xwayland_surface_listener_ping_timeout");

    return;
}


 // -------------------------------
 // WLR xwayland_surface initialize
 //
 // wlr_xwayland_surface objects are created by wlr_xwayland.

extern
void
wcf_wlr_xwayland_listener_new_surface(
    struct wl_listener *listener,
    void               *data
) {
    // This listener registered in ./wcf_wlr_xwayland.c

      // The new xwayland_surface.
    struct wlr_xwayland_surface *wlr_xwayland_surface = data;

    //LOG_INFO("wcf_wlr_xwayland_listener_new_surface %p", wlr_xwayland_surface);

    struct wcf_external_surface *wcf_external_surface = (
        wcf_external_surface_create(
            wlr_xwayland_surface,
            ET_XWAYLAND_SURFACE
        )
    );
    if ( wcf_external_surface == NULL ) { return; }

    wlr_xwayland_surface->data = wcf_external_surface;

      // Initialize some of the wcf_external_surface members.
    wcf_external_surface->wlr_xdg_toplevel_decoration = NULL;
    wcf_external_surface->xdg_decoration              = NULL;

      // Specify listener routines for each wcf_xwayland_surface event of interest.
    wcf_external_surface->wcf_xwayland_surface.destroy              .notify = wlr_xwayland_surface_listener_destroy;
    wcf_external_surface->wcf_xwayland_surface.request_configure    .notify = wlr_xwayland_surface_listener_request_configure;
    wcf_external_surface->wcf_xwayland_surface.request_move         .notify = wlr_xwayland_surface_listener_request_move;
    wcf_external_surface->wcf_xwayland_surface.request_resize       .notify = wlr_xwayland_surface_listener_request_resize;
    wcf_external_surface->wcf_xwayland_surface.request_minimize     .notify = wlr_xwayland_surface_listener_request_minimize;
    wcf_external_surface->wcf_xwayland_surface.request_maximize     .notify = wlr_xwayland_surface_listener_request_maximize;
    wcf_external_surface->wcf_xwayland_surface.request_fullscreen   .notify = wlr_xwayland_surface_listener_request_fullscreen;
    wcf_external_surface->wcf_xwayland_surface.request_activate     .notify = wlr_xwayland_surface_listener_request_activate;
    wcf_external_surface->wcf_xwayland_surface.associate            .notify = wlr_xwayland_surface_listener_associate;
    wcf_external_surface->wcf_xwayland_surface.dissociate           .notify = wlr_xwayland_surface_listener_dissociate;
    wcf_external_surface->wcf_xwayland_surface.set_title            .notify = wlr_xwayland_surface_listener_set_title;
    wcf_external_surface->wcf_xwayland_surface.set_class            .notify = wlr_xwayland_surface_listener_set_class;
    wcf_external_surface->wcf_xwayland_surface.set_role             .notify = wlr_xwayland_surface_listener_set_role;
    wcf_external_surface->wcf_xwayland_surface.set_parent           .notify = wlr_xwayland_surface_listener_set_parent;
    wcf_external_surface->wcf_xwayland_surface.set_startup_id       .notify = wlr_xwayland_surface_listener_set_startup_id;
    wcf_external_surface->wcf_xwayland_surface.set_window_type      .notify = wlr_xwayland_surface_listener_set_window_type;
    wcf_external_surface->wcf_xwayland_surface.set_hints            .notify = wlr_xwayland_surface_listener_set_hints;
    wcf_external_surface->wcf_xwayland_surface.set_decorations      .notify = wlr_xwayland_surface_listener_set_decorations;
    wcf_external_surface->wcf_xwayland_surface.set_override_redirect.notify = wlr_xwayland_surface_listener_set_override_redirect;
    wcf_external_surface->wcf_xwayland_surface.set_geometry         .notify = wlr_xwayland_surface_listener_set_geometry;
    wcf_external_surface->wcf_xwayland_surface.ping_timeout         .notify = wlr_xwayland_surface_listener_ping_timeout;

      // Register the wcf_xwayland_surface listeners
    wl_signal_add(&wlr_xwayland_surface->events.destroy,               &wcf_external_surface->wcf_xwayland_surface.destroy              );
    wl_signal_add(&wlr_xwayland_surface->events.request_configure,     &wcf_external_surface->wcf_xwayland_surface.request_configure    );
    wl_signal_add(&wlr_xwayland_surface->events.request_move,          &wcf_external_surface->wcf_xwayland_surface.request_move         );
    wl_signal_add(&wlr_xwayland_surface->events.request_resize,        &wcf_external_surface->wcf_xwayland_surface.request_resize       );
    wl_signal_add(&wlr_xwayland_surface->events.request_minimize,      &wcf_external_surface->wcf_xwayland_surface.request_minimize     );
    wl_signal_add(&wlr_xwayland_surface->events.request_maximize,      &wcf_external_surface->wcf_xwayland_surface.request_maximize     );
    wl_signal_add(&wlr_xwayland_surface->events.request_fullscreen,    &wcf_external_surface->wcf_xwayland_surface.request_fullscreen   );
    wl_signal_add(&wlr_xwayland_surface->events.request_activate,      &wcf_external_surface->wcf_xwayland_surface.request_activate     );
    wl_signal_add(&wlr_xwayland_surface->events.associate,             &wcf_external_surface->wcf_xwayland_surface.associate            );
    wl_signal_add(&wlr_xwayland_surface->events.dissociate,            &wcf_external_surface->wcf_xwayland_surface.dissociate           );
    wl_signal_add(&wlr_xwayland_surface->events.set_title,             &wcf_external_surface->wcf_xwayland_surface.set_title            );
    wl_signal_add(&wlr_xwayland_surface->events.set_class,             &wcf_external_surface->wcf_xwayland_surface.set_class            );
    wl_signal_add(&wlr_xwayland_surface->events.set_role,              &wcf_external_surface->wcf_xwayland_surface.set_role             );
    wl_signal_add(&wlr_xwayland_surface->events.set_parent,            &wcf_external_surface->wcf_xwayland_surface.set_parent           );
    wl_signal_add(&wlr_xwayland_surface->events.set_startup_id,        &wcf_external_surface->wcf_xwayland_surface.set_startup_id       );
    wl_signal_add(&wlr_xwayland_surface->events.set_window_type,       &wcf_external_surface->wcf_xwayland_surface.set_window_type      );
    wl_signal_add(&wlr_xwayland_surface->events.set_hints,             &wcf_external_surface->wcf_xwayland_surface.set_hints            );
    wl_signal_add(&wlr_xwayland_surface->events.set_decorations,       &wcf_external_surface->wcf_xwayland_surface.set_decorations      );
    wl_signal_add(&wlr_xwayland_surface->events.set_override_redirect, &wcf_external_surface->wcf_xwayland_surface.set_override_redirect);
    wl_signal_add(&wlr_xwayland_surface->events.set_geometry,          &wcf_external_surface->wcf_xwayland_surface.set_geometry         );
    wl_signal_add(&wlr_xwayland_surface->events.ping_timeout,          &wcf_external_surface->wcf_xwayland_surface.ping_timeout         );

    wcf_get_xdg_surface( wcf_external_surface);
    wcf_get_xdg_toplevel(wcf_external_surface);

      // Initial commit
    wld_geometry_t zero_geometry = {0,0,0,0};
    wcf_external_surface_commit(
        wcf_external_surface,
        zero_geometry
    );

    return;
}
