#ifndef TWC_DEVICE_H
#define TWC_DEVICE_H

  // System
#include <stdint.h>     // uint32_t

#include <xkbcommon/xkbcommon.h>

  // Wayland
#include <wayland-util.h>  // wl_fixed_t

  // Protocols
#include <wayland-client-protocol.h>    // enum wl_keyboard_keymap_format

  // APIs
#include "twc_seat.h"
#include "srv_pointer.h"
#include "srv_keyboard.h"
#include "srv_output.h"
#include <twc/wayland_types.h>  // wld_<type>_t

  // Imported data types
#include "wcf_output.h"
#include "wcf_keyboard.h"

  // Opaque types
struct wcf_keyboard;
struct wcf_pointer;
struct srv_keyboard;

static inline
struct srv_pointer*
twc_pointer_announce(
    struct wcf_pointer *wcf_pointer
) {
    // Fix: add srv_pointer_create()
/*
    return srv_pointer_create(
        wcf_pointer,
        ...
    );
*/

    return NULL;
};

static inline
struct srv_keyboard*
twc_keyboard_announce(
    struct wcf_keyboard           *wcf_keyboard,
    char                          *name,
    enum wl_keyboard_keymap_format wl_keyboard_keymap_format,
    int32_t                        keymap_fd
) {
    return srv_keyboard_create(
        wcf_keyboard,
        name,
        wl_keyboard_keymap_format,
        keymap_fd
    );
};

static inline
void
twc_keyboard_destroy(
    struct wcf_keyboard *wcf_keyboard
) {
    srv_keyboard_destroy( wcf_keyboard->srv_keyboard );
}

extern
void
twc_seat_capabilities(
    void
//    struct srv_seat *srv_seat,
//    uint32_t         capabilities
);

static inline
void
twc_deliver_key(
    struct srv_keyboard   *srv_keyboard,
    uint32_t               key,
    enum xkb_key_direction key_direction,
    wld_millisecond_t      time_millisec
) {
   twc_seat_keyboard_key(
        srv_keyboard,
        key,
        key_direction,
        time_millisec
    );

    return;
}

#if 0
  // twc_seat.c runs a XKB state machine for keyboard shortcuts so it
  // knows the modifiers.  This routine is not needed.
static inline
void
twc_deliver_modifiers(
    struct wl_keyboard *wl_keyboard,
    xkb_mod_mask_t      depressed,
    xkb_mod_mask_t      latched,
    xkb_mod_mask_t      locked,
    xkb_mod_mask_t      keyboard_layout
) {
   twc_seat_keyboard_modifiers(
        wl_keyboard,
        depressed,
        latched,
        locked,
        keyboard_layout
    );

    return;
}
#endif

static inline
void
twc_deliver_button(
    uint32_t                   button,
    enum libinput_button_state button_direction,
    wld_millisecond_t          time_millisec
) {
    twc_seat_pointer_button(
        button,
        button_direction,
        time_millisec
    );

    return;
}

static inline
void
twc_deliver_pointer_motion(
    wl_fixed_t        ptr_loc_fix_lx,   // fixed format, layout coord
    wl_fixed_t        ptr_loc_fix_ly,
    wld_millisecond_t time_millisec
) {
    twc_seat_update_pointer_position(
        ptr_loc_fix_lx,
        ptr_loc_fix_ly,
        time_millisec
    );

    return;
}

static inline
void
twc_deliver_pointer_motion_abs(
    wl_fixed_t        ptr_loc_fix_lx,   // fixed format, layout coord
    wl_fixed_t        ptr_loc_fix_ly,
    wld_millisecond_t time_millisec
) {
    twc_seat_update_pointer_position(
        ptr_loc_fix_lx,
        ptr_loc_fix_ly,
        time_millisec
    );

    return;
}

static inline
void
twc_deliver_pointer_axis(
    uint32_t           time,
    uint32_t           axis,
    wl_fixed_t         value
) {
    twc_seat_pointer_axis(
        time,
        axis,
        value
    );

    return;
}

static inline
struct srv_output*
twc_output_announce(
    struct wcf_output *wcf_output,
    char              *output_name,
    wld_dimension_t    width,
    wld_dimension_t    height
) {
    return srv_output_create(
        wcf_output,
        output_name,
        width,
        height
    );
}

static inline
void
twc_output_remove(
    struct wcf_output *wcf_output
) {
    srv_output_destroy(wcf_output->srv_output);

    return;
}

static inline
void
twc_output_compose(
    struct wcf_output *wcf_output
) {
    srv_output_compose(wcf_output->srv_output);

    return;
};

#endif // TWC_DEVICE_H
