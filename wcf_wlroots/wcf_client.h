#ifndef WCF_CLIENT_H
#define WCF_CLIENT_H

  // System
#include <stdint.h>     // uint32_t

  // Protocols
#include <xdg-shell-client-protocol.h>
#include <xdg-decoration-unstable-v1-client-protocol.h>

// wlroots
#include <wlr/util/box.h>

  // TWC
#include <twc/wayland_types.h>      // wld_<type>_t

  // Imported Data Types
#include "wcf_external_surface.h"

extern
struct wcf_external_surface*
wcf_external_surface_create(
    void                      *wlr_surface_ptr,
    enum external_surface_type external_surface_type
);

extern
void
wcf_external_surface_destroy(
    struct wcf_external_surface *wcf_external_surface
);

extern
void
wcf_get_xdg_surface(
    struct wcf_external_surface *wcf_external_surface
);

extern
void
wcf_xdg_surface_ack_configure(
    struct wcf_external_surface *wcf_external_surface,
    uint32_t                     serial
);

extern
void
wcf_xdg_surface_destroy(
    struct wcf_external_surface *wcf_external_surface
);

extern
void
wcf_get_xdg_toplevel(
    struct wcf_external_surface *wcf_external_surface
);

extern
void
wcf_xdg_toplevel_destroy(
    struct wcf_external_surface *wcf_external_surface
);

extern
void
wcf_external_surface_commit(
    struct wcf_external_surface *wcf_external_surface,
    wld_geometry_t               visible_geometry
);

static inline
void
wcf_xdg_toplevel_set_title(
    struct wcf_external_surface *wcf_external_surface,
    char                        *title
) {
    xdg_toplevel_set_title(
        wcf_external_surface->xdg_toplevel,
        title
    );

    return;
}

static inline
void
wcf_xdg_toplevel_set_app_id(
    struct wcf_external_surface *wcf_external_surface,
    char                        *app_id
) {
    xdg_toplevel_set_app_id(
        wcf_external_surface->xdg_toplevel,
        app_id
    );

    return;
}

static inline
void
wcf_xdg_toplevel_set_minimized(
    struct wcf_external_surface *wcf_external_surface
) {
    xdg_toplevel_set_minimized(
        wcf_external_surface->xdg_toplevel
    );

    return;
}

static inline
void
wcf_xdg_toplevel_move(
    struct wcf_external_surface *wcf_external_surface
) {
    xdg_toplevel_move(
        wcf_external_surface->xdg_toplevel,
        NULL,
        0
    );

    return;
}

static inline
void
wcf_xdg_toplevel_resize(
    struct wcf_external_surface *wcf_external_surface,
    uint32_t edges
) {
    // Fix: seat?? serial??
    xdg_toplevel_resize(
        wcf_external_surface->xdg_toplevel,
        NULL,
        0,
        edges
    );

    return;
}

extern
void
wcf_decoration_get_toplevel_decoration(
    struct wcf_external_surface *wcf_external_surface
);

extern
void
wcf_decoration_destroy(
    struct wcf_external_surface *wcf_external_surface
);

static inline
void
wcf_decoration_set_mode(
    struct wcf_external_surface *wcf_external_surface,
    uint32_t                     mode
) {
    zxdg_toplevel_decoration_v1_set_mode(
        wcf_external_surface->xdg_decoration,
        mode
    );

    return;
};

static inline
void
wcf_decoration_unset_mode(
    struct wcf_external_surface *wcf_external_surface
) {
    zxdg_toplevel_decoration_v1_unset_mode(
        wcf_external_surface->xdg_decoration
    );

    return;
};

#if 0  // Below are requests currently unused by TWC.

  // Does wlroots handle this?
struct wcf_surface;
extern
void
wcf_surface_damage_buffer(
    struct wcf_surface *wcf_surface
);

  // Does wlroots handle this?
struct wcf_data_device;
extern
void
wcf_data_device_start_drag(
    struct wcf_data_device *wcf_data_device
);

    // let the wlroots shim handle this as in tinywl
extern
void
wcf_data_device_set_selection(
    struct wcf_data_device *wcf_data_device
);

#endif // Currently unused.

#endif // WCF_CLIENT_H
