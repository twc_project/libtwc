
  // System
#include <stddef.h>    // NULL

  // Wayland
#include <wayland-server-core.h>    // wl_listener, wl_signal

  // Wlroots
#include <wlr/types/wlr_compositor.h>
#include <wlr/types/wlr_subcompositor.h>

  //TWC
#include <twc/twc_log.h>

  // APIs
#include "wcf_wlr_init.h"    // Our API to ensure consistancy

  // Imported Data Types
#include "wcf_wlr.h"

 /*
 ** WLR compositor
 **
 ** wlr_compositor_create() places the Wayland global singleton
 ** wl_compositor into the server registry for use by clients.  A
 ** wl_compositor is a factory for wl_surface objects.  See
 **
 **   <wayland_src>/wayland/protocol/wayland.xml
 **
 ** Similaryly, wlr_compositor is a factory for wlr_surface objects.
 ** WLR compositor events can be found here:
 **
 **     <wlroots_src>/include/wlr/types/wlr_compositor.h
 **
 ** Wayland provides for operations on surfaces (dragged, resized,
 ** maximize), but only if the surface has been given a role, in
 ** particular a "shell" role.  See
 **
 **     <wayland_src>/wayland/protocol/wayland.xml
 **
 ** Presently, the only stable shell is the xdg_shell.  See
 **
 **     <wayland_src>/wayland-protocols/stable/xdg-shell/xdg-shell.xml
 **
 ** The only surfaces that TWC oversees are xdg_surface objects from
 ** the xdg_wm_base factory.  And then only those which have the
 ** xdg_toplevel role.  All other surfaces are left to be managed
 ** internally by wlroots.
 **
 ** Compositor listeners are part of the WLR state.
 */


 // ----------------------------
 // Compositor listener routines

static
void
compositor_listener_new_wl_surface(
    struct wl_listener *listener,
    void               *data
) {
    //LOG_INFO("compositor_listener_new_surface");
    return;
}

static
void
compositor_listener_wl_destroy(
    struct wl_listener *listener,
    void               *data
) {
    LOG_INFO("compositor_listener_destroy");
    return;
}


 // -------------------------
 // WLR compositor initialize
 //
 // Since compositor is a singleton, only one is created.

extern
error_t
wcf_wlr_compositor(
    struct wlr_state *wlr
) {
      // The examples in the wlroots 0.17.4 code used 5
#   define WLR_COMPOSITOR_VERSION 5

    wlr->compositor = (
        wlr_compositor_create(
            wlr->wl_display,
            WLR_COMPOSITOR_VERSION,
            wlr->renderer
        )
    );

    if ( wlr->compositor == NULL ) {
        LOG_ERROR("WLR compositor create failed.");
        return ERROR;
    }

      // Specify listener routines for each wlr_compositor.
    wlr->new_wl_surface    .notify = compositor_listener_new_wl_surface;
    wlr->destroy_compositor.notify = compositor_listener_wl_destroy;

    // The following listeners are no-op's, so don't bother
    // registering them.  We only care about xdg_surfaces.

      // Register the compositor listeners.
    wl_signal_add(&wlr->compositor->events.new_surface, &wlr->new_wl_surface    );
    wl_signal_add(&wlr->compositor->events.destroy,     &wlr->destroy_compositor);

    LOG_INFO("Created WLR compositor.");
    return OK;
}

static
void
subcompositor_listener_destroy(
    struct wl_listener *listener,
    void               *data
) {
    LOG_INFO("subcompositor_listener_destroy");
    return;
}

extern
error_t
wcf_wlr_subcompositor(
    struct wlr_state *wlr
) {
    wlr->subcompositor = (
        wlr_subcompositor_create(wlr->wl_display)
    );

    if ( wlr->subcompositor == NULL ) {
        LOG_ERROR("WLR subcompositor create failed.");
        return ERROR;
    }

      // Specify listener routines for each wlr_subcompositor.
    wlr->destroy_subcompositor.notify = subcompositor_listener_destroy;

      // Register the subcompositor listeners.
//  wl_signal_add(&wlr->compositor->events.destroy,     &wlr->destroy_compositor);

    LOG_INFO("Created WLR subcompositor.");
    return OK;
}
