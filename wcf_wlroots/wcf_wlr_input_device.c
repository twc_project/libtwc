
  // System
#include <stdint.h>                 // uint32_t
#include <stdlib.h>                 // NULL, calloc, free
#include <xkbcommon/xkbcommon.h>

  // Wayland
#include <wayland-util.h>           // wl_container_of, wl_list
#include <wayland-server-core.h>    // wl_listener, wl_signal

  // Wlroots
#include <wlr/types/wlr_data_device.h>  // wlr_seat_set_keyboard()
#include <wlr/types/wlr_input_device.h>
#include <wlr/types/wlr_keyboard.h>
#include <wlr/types/wlr_pointer.h>
#include <wlr/types/wlr_cursor.h>

  // TWC
#include <twc/twc_log.h>

  // APIs
#include "wcf_wlr_init.h"       // Our API to ensure consistancy
#include "wcf_wlr_listeners.h"  // new_input listener routine
#include "twc_device.h"

  // Imported Data Types
#include "wcf_wlr.h"
#include "wcf_pointer.h"
#include "wcf_keyboard.h"

 /*
 ** Wayland uses libinput.  The libinput event codes are identical to
 ** the Linux Kernel input event codes.  Compare
 **
 **   https://gitlab.freedesktop.org/libinput/libinput/-/blob/main/include/linux/linux/input-event-codes.h
 **   https://github.com/torvalds/linux/blob/master/include/uapi/linux/input-event-codes.h
 **
 ** Each input event code represents the press/release of a physical
 ** key.  This includes modifier keys.  Clients usually employ a
 ** system called X Keyboard Extension (XKB) to track keyboard state
 ** (xkb_state) and to convert input event codes into XKB keysyms.
 ** XKB keysyms carry more semantic information, eg, XKB keysyms
 ** distinguish between lower case 'a' and upper case 'A'.
 */

 /*
 ** Notes for xkb
 **
 ** https://wayland-book.com/seat/xkb.html
 ** https://github.com/xkbcommon/libxkbcommon/blob/master/doc/quick-guide.md
 **
 ** XKB modifiers are complicated.  They are configurable.  Each has an
 ** index (starting at 0) and must have a unique name.
 **   https://github.com/xkbcommon/libxkbcommon/blob/master/include/xkbcommon/xkbcommon-names.h
 **
 ** <wlroots_src>/include/wlr/types/wlr_keyboard.h
 ** <wlroots_src>/types/wlr_keyboard.c
 */

 /*
 ** WLR input device
 **
 ** wlr_input_device objects are created by the wlr_backend.  An
 ** input device may be a keyboard or a pointer.  The backend
 ** listener for new_input is defined in this file by the routine
 **
 **     wcf_wlr_backend_listener_new_input()
 **
 ** A wlr_keyboard or wlr_pointer object is created as required.
 **
 ** A wlr_keyboard object has its own listeners which must be
 ** dynamically created, initialized and registered.
 **
 ** Wlroots combines all keyboards into a single wlr_seat.  Of all
 ** the keyboards, only one at-a-time can be 'active' within the
 ** seat.  The routine wlr_seat_set_keyboard(seat, keyboard) changes
 ** which keyboard is active.
 **
 ** Since wlroots exposes pointer activity through the wlr_cursor
 ** object, there's no need to create a wlr_pointer object.
 **
 ** The WLR keyboard and pointer events can be found here:
 **
 **     <wlroots_src>/include/wlr/types/wlr_keyboard.h
 **     <wlroots_src>/include/wlr/types/wlr_pointer.h
 */


 // --------------------------
 // keyboard listener routines

static
void
keyboard_listener_key(
    struct wl_listener *listener,
    void               *data
) {
    const struct wcf_keyboard *wcf_keyboard = (
        wl_container_of(listener, wcf_keyboard, key)
    );

    struct wlr_keyboard_key_event *keyboard_key_data = data;

    struct wlr_keyboard *wlr_keyboard = wcf_keyboard->wlr_keyboard;

    const enum xkb_key_direction key_direction = (
        keyboard_key_data->state == WL_KEYBOARD_KEY_STATE_PRESSED
        ? XKB_KEY_DOWN
        : XKB_KEY_UP
    );

      // Assign this keyboard to be the active keyboard for the seat.
    wlr_seat_set_keyboard(
        wcf_keyboard->wlr->wlr_seat,
        wlr_keyboard
    );

    twc_deliver_key(
        wcf_keyboard->srv_keyboard,
        keyboard_key_data->keycode,
        key_direction,
        keyboard_key_data->time_msec
    );

    return;
}

static
void
keyboard_listener_modifier(
    struct wl_listener *listener,
    void               *data
) {
    // twc_seat.c runs a XKB state machine for keyboard shortcuts so
    // it knows the modifiers.

    return;

#   if 0
    struct wcf_keyboard *wcf_keyboard = (
        wl_container_of(listener, wcf_keyboard, modifiers)
    );

    struct wlr_keyboard *wlr_keyboard = wcf_keyboard->wlr_keyboard;

      // Assign this keyboard to be the active keyboard for the seat.
    wlr_seat_set_keyboard(
        wcf_keyboard->wlr->wlr_seat,
        wlr_keyboard
    );

    twc_deliver_modifiers(
        NULL,   // keyboard
        wlr_keyboard->modifiers.depressed,
        wlr_keyboard->modifiers.latched,
        wlr_keyboard->modifiers.locked,
        wlr_keyboard->modifiers.group
    );

    return;
#   endif
}

static
void
keyboard_listener_keymap(
    struct wl_listener *listener,
    void               *data
) {
    //LOG_INFO("keyboard_listener_keymap");
   return;
}

static
void
keyboard_listener_repeat_info(
    struct wl_listener *listener,
    void               *data
) {
    //LOG_INFO("keyboard_listener_repeat_info");
   return;
}

static
void
keyboard_listener_destroy(
    struct wl_listener *listener,
    void               *data
) {
    struct wcf_keyboard *wcf_keyboard = (
        wl_container_of(listener, wcf_keyboard, destroy)
    );

    twc_keyboard_destroy(wcf_keyboard);

      // Remove keyboard from the keyboard list.
    wl_list_remove(&wcf_keyboard->link);

    free(wcf_keyboard);

    return;
}


  // ------------------------------
  // input_device creation routines

static
void
input_device_new_keyboard(
    struct wlr_state        *wlr,
    struct wlr_input_device *wlr_input_device
) {
    /*
    LOG_INFO("input_device_new_keyboard %s %d %d %d",
        wlr_input_device->name,
        wlr_input_device->vendor,
        wlr_input_device->product,
        wlr_input_device->type
    );
    */

    // For each new keyboard, wlroots expects the handler to supply an
    // xkb_keymap.  This keymap is then delivered to wlroots so it can
    // create/run an XKB state machine based on that keymap.  The
    // state machine allows wlroots to interpret keyboard events
    // before invoking the key.notify and modifiers.notify callbacks.

    struct wcf_keyboard *wcf_keyboard = (
        calloc( 1, sizeof(struct wcf_keyboard) )
    );

    struct wlr_keyboard *wlr_keyboard = (
        wlr_keyboard_from_input_device( wlr_input_device )
    );

    wcf_keyboard->wlr              = wlr;
    wcf_keyboard->wlr_input_device = wlr_input_device;
    wcf_keyboard->wlr_keyboard     = wlr_keyboard;

      // For consistancy, use the same keymap for all xkb_state machines
    struct xkb_keymap *keymap = twc_seat_get_xkb_keymap();

      // The xkb keymap is delivered to the keyboard member of the
      // wlr_input_device.  Wlroots manages the keymap and xkb state
      // within that keyboard member.
    wlr_keyboard_set_keymap(wlr_keyboard, keymap);

    wlr_keyboard_set_repeat_info(wlr_keyboard, 25, 600);

      // Specify listener routines for each wlr_keyboard event.
    wcf_keyboard->key        .notify = keyboard_listener_key;
    wcf_keyboard->modifiers  .notify = keyboard_listener_modifier;
    wcf_keyboard->keymap     .notify = keyboard_listener_keymap;
    wcf_keyboard->repeat_info.notify = keyboard_listener_repeat_info;
    wcf_keyboard->destroy    .notify = keyboard_listener_destroy;

      // Register keyboard listeners.
    wl_signal_add(&wlr_keyboard->events.key,         &wcf_keyboard->key);
    wl_signal_add(&wlr_keyboard->events.modifiers,   &wcf_keyboard->modifiers);
    wl_signal_add(&wlr_keyboard->events.keymap,      &wcf_keyboard->keymap);
    wl_signal_add(&wlr_keyboard->events.repeat_info, &wcf_keyboard->repeat_info);
    wl_signal_add(&wlr_input_device->events.destroy, &wcf_keyboard->destroy);

    wlr_seat_set_keyboard(wlr->wlr_seat, wlr_keyboard);

      // Add keyboard to the keyboard list.
    wl_list_insert(&wlr->keyboard_list, &wcf_keyboard  ->link);

    wcf_keyboard->srv_keyboard = (
        twc_keyboard_announce(
            wcf_keyboard,
            wlr_input_device->name,
            WL_KEYBOARD_KEYMAP_FORMAT_XKB_V1,
            wlr_keyboard->keymap_fd
        )
    );

   return;
}

static
struct wcf_pointer*
input_device_new_pointer(
    struct wlr_state        *wlr,
    struct wlr_input_device *input_device
) {
    // input_device must refer to a pointer.
    //
    // Recall there is only one wlr_cursor, but possibly many pointer
    // devices.  The wlr_cusor can managed them all.  See
    //
    //     <wlroots_src>/include/wlr/types/wlr_cursor.h
    //
    // Fix: wlr_cursor is simpler than wlr_pointer, so we use that for
    // now.  What about mouse acceleration??

    struct wcf_pointer *wcf_pointer = (
        calloc( 1, sizeof(struct wcf_pointer) )
    );

      // Add pointer to the pointer list.
    wl_list_insert(&wlr->pointer_list, &wcf_pointer->link);

    wlr->pointer_count += 1;

      // Associate this pointer with the cursor.
    wlr_cursor_attach_input_device(wlr->wlr_cursor, input_device);

    wcf_pointer->srv_pointer = (
        twc_pointer_announce(
            wcf_pointer
        )
    );

   return wcf_pointer;
}


  // ------------------------------
  // input_device listener routines

static
void
input_device_listener_destroy(
    struct wl_listener *listener,
    void               *data
) {
    struct wlr_input_device     *wlr_input_device     = data;
    struct wcf_wlr_input_device *wcf_wlr_input_device = (
        wl_container_of(listener, wcf_wlr_input_device, destroy)
    );

    struct wlr_state *wlr = wcf_wlr_input_device->wlr;

    if (wlr_input_device->type == WLR_INPUT_DEVICE_POINTER) {

        struct wcf_pointer *wcf_pointer = (
            wcf_wlr_input_device->wcf_pointer
        );

        if ( wcf_pointer != NULL ) {
              // Remove pointer from the pointer list.
            wl_list_remove(&wcf_pointer->link);

            wlr->pointer_count -= 1;

            free(wcf_pointer);
        }
    }

    if (wlr_input_device->type == WLR_INPUT_DEVICE_KEYBOARD) {

          // wlr_keyboards have their own destroy listener.  Will this
          // be called in addtion???  Or should each destroy listener
          // do the work for both, just in case.
        LOG_INFO("input_device_listener_destroy KEYBOARD");
    }

    free(wcf_wlr_input_device);

   return;
};


  // ---------------------------
  // WLR input_device initialize
  //
  // wlr_input_device objects are created by the wlr_backend.
  //
  // An input device may be a keyboard or a pointer.

extern
void
wcf_wlr_backend_listener_new_input(
    struct wl_listener *listener,
    void               *data
) {
    // Registered in ./wcf_wlr_backend.c

    struct wlr_state *wlr = (
        wl_container_of(listener, wlr, new_input)
    );
    struct wlr_input_device *wlr_input_device = data;

    // We have a new input device, either a keyboard or a pointer.

      // Create a wcf_wlr_input_device
    struct wcf_wlr_input_device *wcf_wlr_input_device = (
        calloc( 1, sizeof(struct wcf_wlr_input_device) )
    );

    wcf_wlr_input_device->wlr              = wlr;
    wcf_wlr_input_device->wlr_input_device = wlr_input_device;

      // Specify listener routines for each wlr_input_device event.
    wcf_wlr_input_device->destroy.notify = input_device_listener_destroy;

      // Register the input_device listeners.
    wl_signal_add(&wlr_input_device->events.destroy, &wcf_wlr_input_device->destroy);

    LOG_INFO(
        "wcf_wlr_backend_listener_new_input Type: %x Name: %s",
        wlr_input_device->type,
        wlr_input_device->name
    );

    switch (wlr_input_device->type) {

        case WLR_INPUT_DEVICE_KEYBOARD:

            LOG_INFO("wcf_wlr_backend_listener_new_input Keyboard");
            input_device_new_keyboard(wlr, wlr_input_device);

        break;

        case WLR_INPUT_DEVICE_POINTER:

            LOG_INFO("wcf_wlr_backend_listener_new_input Pointer");

            wcf_wlr_input_device->wcf_pointer = (
                input_device_new_pointer(wlr, wlr_input_device)
            );

        break;

        default:
            LOG_INFO("wcf_wlr_backend_listener_new_input Other");
        break;
    }

    // Record changes to the seat_capabilities and report the new
    // state to the wlr_seat.

    uint32_t seat_capabilities = wlr->seat_capabilities;

    if ( wl_list_empty(&wlr->keyboard_list) ) {
        seat_capabilities &= ~WL_SEAT_CAPABILITY_KEYBOARD;
    } else {
        seat_capabilities |= WL_SEAT_CAPABILITY_KEYBOARD;
    }

    if ( wlr->pointer_count == 0 ) {
        seat_capabilities &= ~WL_SEAT_CAPABILITY_POINTER;
    } else {
        seat_capabilities |= WL_SEAT_CAPABILITY_POINTER;
    }

    wlr_seat_set_capabilities(wlr->wlr_seat, seat_capabilities);

    return;
}
