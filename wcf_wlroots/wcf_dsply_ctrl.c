
  // Add the nsec member to struct timespec in time.h
#define _POSIX_C_SOURCE 200112L

  // System
#include <stdint.h>     // int32_t
#include <stdlib.h>     // NULL, calloc, free
#include <time.h>       // clock_gettime()

  // Wayland
#include <wayland-server-protocol.h>    // wl_output_transform

  // wlroots
#include <wlr/render/wlr_renderer.h>
#include <wlr/types/wlr_output.h>
#include <wlr/types/wlr_cursor.h>
#include <wlr/types/wlr_output_layout.h>
#include <wlr/types/wlr_compositor.h>   // wlr_surface_get_texture()
#include <wlr/types/wlr_xdg_shell.h>    // wlr_xdg_surface_for_each_surface()
#include <wlr/interfaces/wlr_buffer.h>  // wlr_buffer_init(), wlr_buffer_impl
#include <wlr/types/wlr_buffer.h>       // wlr_buffer_drop()
#include <wlr/xwayland.h>

  //TWC
#include <twc/wayland_types.h>    // wld_coordinate_t

  // bic_server APIs
#include "bic_server/wayland_bic.h"

  // APIs
#include "wcf_shim.h"       // Our API to ensure consistancy
#include <twc/twc_log.h>

  // Imported Data Types
#include "wcf_wlr.h"
#include "wcf_external_surface.h"
#include "wcf_output.h"
#include "srv_output-impl.h"

extern struct wlr_state WLR;

 /*
 ** Rendering nested surfaces
 **
 ** wlroots provides a "for_each_surface" operation which cycles
 ** though all the nested surfaces of an xdg_toplevel.  Its signature
 ** is
 **     <wlroots-src>/include/wlr/types/wlr_xdg_shell.h
 **
 **     void
 **     wlr_xdg_surface_for_each_surface(
 **         struct wlr_xdg_surface      *surface,
 **         wlr_surface_iterator_func_t  iterator,
 **         void                        *user_data
 **     );
 **
 ** The first argument is the toplevel xdg_surface.  The second is a
 ** pointer to an "iterator" function to be called for every nested
 ** surface.  The third argument is a pointer to a set of values to be
 ** passed to the iterator function on each invocation.  This
 ** "user_data" could contain any number of values depending on the
 ** needs of the iterator function.
 **
 ** The purpose of the iterator function is to render each nested
 ** surface.  The nested surfaces include the toplevel surface itself,
 ** so it needs no special case.  The signature of the iterator
 ** function is
 **
 **    <wlroots-src>/include/wlr/types/wlr_compositor.h
 **
 **    void wlr_surface_iterator_func(
 **        struct wlr_surface *surface,
 **        int                 sx,
 **        int                 sy,
 **        void               *data
 **    );
 **
 ** The argument surface is the nested surface, sx/sy are surface
 ** coordinates relative to the xdg_toplevel, and data is the
 ** user_data pointer from above.
 */

 /*
 ** Surface textures
 **
 ** wlroots based compositors paint surfaces by rendering the
 ** surface's texture using wlr_render_texture().
 **
 ** For xdg_surfaces, a texture pointer can be retreived from the
 ** underlying wlr_xdg_surface using wlr_surface_get_texture().
 **
 ** For BT_internal surfaces, a texture pointer is stored right in
 ** surface struct.
 */

  // Custom parameters for the iterator function.
struct custom_parameters {
    struct wlr_output        *output;
    struct wlr_renderer      *renderer;
    struct wlr_output_layout *output_layout;
    wld_coordinate_t          toplevel_layout_x;
    wld_coordinate_t          toplevel_layout_y;
    struct timespec          *when;
};

 // The iterator function.
static
void
render_nested_surface(
    struct wlr_surface *surface,
    int                 surface_x,
    int                 surface_y,
    void               *data
) {
    // This function is called for every nested wlr_surface that needs
    // to be rendered.  Each wlr_surface has a wlr_texture.  It is
    // this texture that is passed to the renderer to do its work.
    //
    // This routine does not support HiDPI or any fancy compositing
    // with a custom matrix.  See tinywl.c (ver 0.13.0) for a slightly
    // more complete description.

    struct custom_parameters *parms  = data;
    struct wlr_output        *output = parms->output;

    struct wlr_texture *texture = (
        wlr_surface_get_texture(surface)
    );

    if (texture == NULL) { return; }

      // Render the nested surface.  The renderer takes layout
      // coordinates
    wlr_render_texture(
        parms->renderer,
        texture,
        output->transform_matrix,
        parms->toplevel_layout_x + surface_x,
        parms->toplevel_layout_y + surface_y,
        1.0f  // alpha channel, 1.0 = opaque
    );

      // Notify the client that its surface has been displayed
    wlr_surface_send_frame_done(surface, parms->when);

    return;
}

extern
void
wcf_dsply_ctrl_display_surface(
    struct srv_output *srv_output,
    struct wl_surface *wl_surface,
    wld_coordinate_t   layout_x,
    wld_coordinate_t   layout_y
) {
    struct wcf_output        *wcf_output        = srv_output->wcf_output;
    struct wlr_output        *wlr_output        = wcf_output->wlr_output;
    struct wlr_renderer      *wlr_renderer      = wcf_output->wlr->renderer;
    struct wlr_output_layout *wlr_output_layout = wcf_output->wlr->output_layout;
    struct timespec now;

    struct custom_parameters parms = {
        .output            = wlr_output,
        .output_layout     = wlr_output_layout,
        .renderer          = wlr_renderer,
        .toplevel_layout_x = layout_x,
        .toplevel_layout_y = layout_y,
        .when              = &now,
    };

    struct wcf_external_surface *wcf_external_surface;

    switch (wl_surface->committed_image_type) {

        case BT_internal:

              // Render the surface texture.  The renderer takes
              // layout coordinates.
            wlr_render_texture(
                wlr_renderer,
                wl_surface->wcf_pixel_texture,
                wlr_output->transform_matrix,
                layout_x,
                layout_y,
                1.0f
            );

        break;

        case BT_external:

            wcf_external_surface = wl_surface->wcf_external_surface;

            clock_gettime(CLOCK_MONOTONIC, &now);

            struct wlr_xdg_surface *wlr_xdg_surface = (
                wcf_external_surface->wcf_xdg_surface.wlr_xdg_surface
            );

              // A wlr_xdg_surface may be composed of many nested surfaces
            wlr_xdg_surface_for_each_surface(
                wlr_xdg_surface,
                render_nested_surface,
                &parms
            );

        break;

        case BT_xwayland:

            wcf_external_surface = wl_surface->wcf_external_surface;

            clock_gettime(CLOCK_MONOTONIC, &now);

            struct wlr_surface *wlr_surface = (
                wcf_external_surface->wcf_xwayland_surface.wlr_xwayland_surface->surface
            );

              // A wlr_surface may be composed of many nested surfaces
            wlr_surface_for_each_surface(
                wlr_surface,
                render_nested_surface,
                &parms
            );

        break;

        default:
        break;
    }

    return;
};

  // To set a cursor image, wlroots uses wlr_cursor_set_buffer().  It
  // requires a wlr_buffer, so we must create one from the pixel
  // memory containing our desired image.  A cursor_buffer is a
  // meta-data wrapper for this purpose.  See
  //   https://github.com/swaywm/sway/blob/master/sway/sway_text_node.c#L13
  // for a cairo example.
struct cursor_buffer {

      // The wlr_buffer we'll hand off to wlr_cursor_set_buffer()
    struct wlr_buffer wlr_buffer;

      // Meta-data
    void    *pixel_memeory;
    uint32_t format;
    size_t   stride;
};

 // A wlr_buffer requires a wlr_buffer implementation containing
 // specific callbacks.  The next three routines are the callbacks for
 // our cursor_buffer implementation.

static
void
cursor_buffer_handle_destroy(
    struct wlr_buffer *wlr_buffer
) {
    struct cursor_buffer *cursor_buffer = wl_container_of(wlr_buffer, cursor_buffer, wlr_buffer);

    //LOG_INFO("cursor_buffer_handle_destroy %p", cursor_buffer);

      // destroy the wlr_buffer and its meta-data.  Note that pixel
      // memory is still allocated.
    free(cursor_buffer);

    return;
}

static
bool
cursor_buffer_handle_begin_data_ptr_access(
    struct wlr_buffer *wlr_buffer,
    uint32_t           flags,
    void             **data,
    uint32_t          *format,
    size_t            *stride
) {
    struct cursor_buffer *cursor_buffer = wl_container_of(wlr_buffer, cursor_buffer, wlr_buffer);

    *data   = cursor_buffer->pixel_memeory;
    *stride = cursor_buffer->stride;
    *format = cursor_buffer->format;

    return true;
}

static
void
cursor_buffer_handle_end_data_ptr_access(struct wlr_buffer *wlr_buffer) {
    // No-op
    return;
}

static const struct wlr_buffer_impl cursor_buffer_impl = {
    .destroy               = cursor_buffer_handle_destroy,
    .begin_data_ptr_access = cursor_buffer_handle_begin_data_ptr_access,
    .end_data_ptr_access   = cursor_buffer_handle_end_data_ptr_access,
};

extern
void
wcf_dsply_ctrl_set_cursor(
    struct srv_output *srv_output,
    struct wl_surface *wl_surface,  // cursor image
    int32_t            hotspot_x,
    int32_t            hotspot_y
) {
    // <wlroots_src>/include/wlr/types/wlr_cursor.h
    // <wlroots_src>/include/wlr/types/wlr_xcursor_manager.h
    // <wlroots_src>/types/wlr_xcursor_manager.c
    //
    // The .c file shows that wlr_xcursor_manager_set_cursor_image()
    // simply picks a cursor image from a theme and then calls
    // wlr_cursor_set_image().  So the latter is just as powerful.

    switch ( wl_surface->committed_image_type ) {

        case BT_internal:

              // We rely on wlroots to use the destroy callback in the
              // cursor_buffer_impl to deallocate this memory.
            struct cursor_buffer *cursor_buffer = (
                calloc( 1, sizeof(struct cursor_buffer) )
            );

            //LOG_INFO("cursor_buffer_handle_create %p", cursor_buffer);

            cursor_buffer->pixel_memeory = wl_surface->pix_data;
            cursor_buffer->format        = wl_surface->current_wl_buffer->format;
            cursor_buffer->stride        = wl_surface->stride;

            struct wlr_buffer *wlr_buffer =  &cursor_buffer->wlr_buffer;

              // Submit the wlr_buffer containing the cursor image.
              // The cursor_buffer_impl contains a "destroy callback"
              // so wlroots can free the above calloc().
            wlr_buffer_init(
                wlr_buffer,
               &cursor_buffer_impl,
                wl_surface->width,
                wl_surface->height
            );

            wlr_cursor_set_buffer(
                WLR.wlr_cursor,
                wlr_buffer,
                hotspot_x,
                hotspot_y,
                1.0
            );

              // Tell wlroots that we're done with the wlr_buffer.
              // Presumably it won't invoke the "destroy callback"
              // until after this.
            wlr_buffer_drop(wlr_buffer);

        break;

        case BT_external:
        case BT_xwayland:

            // Fix: ToDo. TWC currently configures wlroots to use
            // wlr_cursor.  So, for external clients, wlroots will not
            // deliver wl_pointer_set_cursor requests.  Those requests are
            // handled internally by wloots.  That means the following
            // code is never invoked.  Note that TWC currently only
            // accepts external toplevels, which cannot be a cursor image.
            // If TWC ever takes over and handles set_cursor for external
            // clients, it will have to deal with all surfaces not just
            // toplevels.

            struct wcf_external_surface *wcf_external_surface = (
                wl_surface->wcf_external_surface
            );

            struct wlr_surface *wlr_surface = (
                wcf_external_surface->external_surface_type == ET_XDG_SURFACE
                ? wcf_external_surface->wcf_xdg_surface.wlr_xdg_surface->surface
                : wcf_external_surface->wcf_xwayland_surface.wlr_xwayland_surface->surface
            );

            wlr_cursor_set_surface(
                WLR.wlr_cursor,
                wlr_surface,
                hotspot_x,
                hotspot_y
            );

        break;

        default:
        break;
    }

    return;
};

extern
void
wcf_dsply_ctrl_output_placement(
    struct srv_output        *srv_output,
    enum wl_output_transform  transform,
    wld_coordinate_t          layout_x,
    wld_coordinate_t          layout_y
) {
    struct wcf_output *wcf_output = srv_output->wcf_output;

    struct wlr_output *wlr_output = (
        wcf_output->wlr_output
    );

    struct wlr_output_layout *wlr_output_layout = (
         wcf_output->wlr->output_layout
    );

    srv_output->transform = transform;

    wlr_output_set_transform( wlr_output, transform );

      // Read back the transformed resolution.
    int width, height;
    wlr_output_transformed_resolution(
        wlr_output,
        &width,
        &height
    );

      // Add this output to the output layout.
    wlr_output_layout_add(
        wlr_output_layout,
        wlr_output,
        layout_x,
        layout_y
    );

      // wlr transform state is double buffered, so must be committed.
    wlr_output_commit( wlr_output );

    srv_output->layout_x = layout_x;
    srv_output->layout_y = layout_y;
    srv_output->width    = width;
    srv_output->height   = height;

    return;
};
