
 // System
#include <stddef.h>      // NULL
#include <libinput.h>    // libinput_button_state

  // Wayland
#include <wayland-util.h>           // wl_container_of, wl_fixe_t
#include <wayland-server-core.h>    // wl_listener, wl_signal

  // Protocols
#include <wayland-client-protocol.h>    // enum wl_pointer_axis

  // Wlroots
#include <wlr/types/wlr_cursor.h>
#include <wlr/types/wlr_data_device.h>
#include <wlr/types/wlr_pointer.h>

  //TWC
#include <twc/twc_log.h>

  // APIs
#include "wcf_wlr_init.h"    // Our API to ensure consistancy
#include "twc_device.h"

  // Imported Data Types
#include "wcf_wlr.h"


 /*
 ** WLR cursor
 **
 ** Wayland does not have a wl_cursor object.  A wlroots wlr_cursor
 ** is a combination of several types of input devices including
 ** pointer, touch pad and tablet tool (stylus).
 **
 ** A Wayland server discerns pointer motion by interacting with the
 ** operating system.  The server sends wl_pointer events to clients.
 ** A wl_pointer.motion message to a client contains a surface
 ** parameter and an x,y coordinate.  The surface parameter is the
 ** surface that has pointer focus as determined by the server.  The
 ** coordinates are surface-relative coordinates.  See
 **
 **     <wayland_src>/wayland/protocol/wayland.xml
 **
 ** The wlroots library provides pointer updates from the operating
 ** system using the wlr_cursor object.  This object emits two kinds
 ** of cursor events wlr_cursor.motion_absolute and
 ** wlr_cursor.motion.  They both report only coordinate information.
 ** The listener must determine which surface, if any, is relevant.
 **
 ** The two kinds wlr_cursor motion event differ in how the
 ** coordinates are reported.  The wlr_cursor.motion event provides a
 ** delta change in coordinates since the last update.  The change is
 ** relative to the layout coordinate system.  The
 ** wlr_cursor.motion_absolute provides the actual layout coordinate
 ** values.  The latter is useful for running the server within some
 ** "host window" belonging to some external window system.  In this
 ** case the pointer may enter and exit the host window in different
 ** places.
 **
 ** (Note that Wayland wl_pointer.set_cusor requests to the server
 ** are exposed by wlroots using the request_set_cursor event of the
 ** wlr_seat object.)
 **
 ** For more information including the WLR cursor events see:
 **
 **     <wlroots_src>/include/wlr/types/wlr_cursor.h
 **
 ** Many of the wlr_cursor events are currently unused by TWC.
 **
 ** Cursor listeners are part of the WLR state.
 **
 ** Note that all
 **
 **     wlr_event_pointer_... objects
 **
 ** have a time_msec memeber.  It reports milliseconds and uses
 ** uint32_t as the base type.
 */

 // ------------------------
 // Cursor listener routines

static
void
cursor_listener_motion(
    struct wl_listener *listener,
    void               *data
) {
    struct wlr_state *wlr = (
        wl_container_of(listener, wlr, motion)
    );

    struct wlr_pointer_motion_event *pointer_motion_data = data;

      // Enlist wlroots to track pointer motion.  It moves the cursor
      // around on the screen, but won't automatically send
      // wl_pointer.motion to a client.  The bic_server does that.
    wlr_cursor_move(
        wlr->wlr_cursor,
       &pointer_motion_data->pointer->base,
        pointer_motion_data->delta_x,
        pointer_motion_data->delta_y
    );

      // In wlr_cursor, the x & y members are of type double. TWC uses
      // fixed, since that's what Wayland wl_pointers use.
    twc_deliver_pointer_motion(
        wl_fixed_from_double(wlr->wlr_cursor->x),
        wl_fixed_from_double(wlr->wlr_cursor->y),
        pointer_motion_data->time_msec
    );

    return;
}

static
void
cursor_listener_motion_absolute(
    struct wl_listener *listener,
    void               *data
) {
    struct wlr_state *wlr = (
        wl_container_of(listener, wlr, motion_absolute)
    );
    struct wlr_pointer_motion_absolute_event *pointer_motion_absolute_data = data;

    wlr_cursor_warp_absolute(
        wlr->wlr_cursor,
       &pointer_motion_absolute_data->pointer->base,
        pointer_motion_absolute_data->x,
        pointer_motion_absolute_data->y
    );

    twc_deliver_pointer_motion(
        wl_fixed_from_double(wlr->wlr_cursor->x),
        wl_fixed_from_double(wlr->wlr_cursor->y),
        pointer_motion_absolute_data->time_msec
    );

    return;
}

static
void
cursor_listener_button(
    struct wl_listener *listener,
    void               *data
) {
    // Mouse buttons.

    struct wlr_state *wlr = (
        wl_container_of(listener, wlr, button)
    );
    struct wlr_pointer_button_event *pointer_button_data = data;

    const enum libinput_button_state button_direction = (
        pointer_button_data->state == WLR_BUTTON_PRESSED
        ? LIBINPUT_BUTTON_STATE_PRESSED
        : LIBINPUT_BUTTON_STATE_RELEASED
    );

    //LOG_INFO("cursor_listener_button %x", pointer_button_data->button);

    twc_deliver_button(
        pointer_button_data->button,
        button_direction,
        pointer_button_data->time_msec
    );

    return;
}

static
void
cursor_listener_axis(
    struct wl_listener *listener,
    void               *data
) {
    //
    // Using this listener routine, wlroots provides mouse wheel
    // information.  Later, the compositor will deliver this info
    // using events of the wl_pointer interface.  The interface has
    // been upgraded over time with additional events.  In particular,
    // for Hi-Res mice.  We are only interested in collecting
    // information for the original event wl_pointer::axis.  It takes
    // three parameters:
    //
    //   time
    //   axis       vertical         for spin; or horizontal for tilt
    //   value      backward/forward for spin; or right/left for tilt
    //

    struct wlr_state *wlr = (
        wl_container_of(listener, wlr, axis)
    );

      // <wlroots>/include/wlr/types/wlr_pointer.h
    struct wlr_pointer_axis_event *pointer_axis_data = data;

    /*  for wl_pointer Version 5
      enum wlr_axis_source source = pointer_axis_data->source;
      double               delta  = pointer_axis_data->delta;

      LOG_INFO("cursor_listener_axis sr %d", pointer_axis_data->source);
      LOG_INFO("cursor_listener_axis d  %f", pointer_axis_data->delta);
    */

    LOG_INFO("cursor_listener_axis axis value %d %d", pointer_axis_data->orientation, pointer_axis_data->delta_discrete);

    uint32_t                  time        = pointer_axis_data->time_msec;
    enum wlr_axis_orientation orientation = pointer_axis_data->orientation;
    int32_t delta_discrete                = pointer_axis_data->delta_discrete;

      // Convert from wlroots type to Wayland type.  See assert() in
      // wcf_client_initialize().
    enum wl_pointer_axis axis  = (enum wl_pointer_axis)orientation;
    wl_fixed_t           value = wl_fixed_from_int(delta_discrete);

    twc_deliver_pointer_axis(
        time,
        axis,
        value
    );

    return;
}

static
void
cursor_listener_frame(
    struct wl_listener *listener,
    void               *data
) {
    // Some pointer activity by the user needs to be "grouped" by the
    // server.  The Wayland wl_pointer.frame event is sent to a client
    // to the mark the end of a group.
    //
    // The wlr_cursor frame event alerts the compositor to the end of
    // a group.  This fact needs to be forwarded on to the client with
    // pointer focus.
    //
    // See the wl_pointer.frame event at
    //   <wayland_src>/wayland/protocol/wayland.xml

    struct wlr_state *wlr = (
        wl_container_of(listener, wlr, frame)
    );

      // Send a wl_pointer.frame event to the client with pointer
      // foucs.
    wlr_seat_pointer_notify_frame(wlr->wlr_seat);

    // Fix: Do the internal client care about frame events?

    return;
}

#if 0  // Below are wlr_cursor events currently unused by TWC.

static
void
cursor_listener_swipe_begin(
    struct wl_listener *listener,
    void               *data
) { return; }

static
void
cursor_listener_swipe_update(
    struct wl_listener *listener,
    void               *data
) { return; }

static
void
cursor_listener_swipe_end(
    struct wl_listener *listener,
    void               *data
) { return; }

static
void
cursor_listener_pinch_begin(
    struct wl_listener *listener,
    void               *data
) { return; }

static
void
cursor_listener_pinch_update(
    struct wl_listener *listener,
    void               *data
) { return; }

static
void
cursor_listener_pinch_end(
    struct wl_listener *listener,
    void               *data
) { return; }

static
void
cursor_listener_touch_up(
    struct wl_listener *listener,
    void               *data
) { return; }

static
void
cursor_listener_touch_down(
    struct wl_listener *listener,
    void               *data
) { return; }

static
void
cursor_listener_touch_motion(
    struct wl_listener *listener,
    void               *data
) { return; }

static
void
cursor_listener_touch_cancel(
    struct wl_listener *listener,
    void               *data
) { return; }

static
void
cursor_listener_touch_frame(
    struct wl_listener *listener,
    void               *data
) { return; }

static
void

cursor_listener_tablet_tool_axis(
    struct wl_listener *listener,
    void               *data
) { return; }

static
void
cursor_listener_tablet_tool_proximity(
    struct wl_listener *listener,
    void               *data
) { return; }

static
void
cursor_listener_tablet_tool_tip(
    struct wl_listener *listener,
    void               *data
) { return; }

static
void
cursor_listener_tablet_tool_button(
    struct wl_listener *listener,
    void               *data
) { return; }

#endif // Currently unused.

  // ---------------------
  // WLR cursor initialize
  //
  // Only one cursor is created.

extern
error_t
wcf_wlr_cursor(
    struct wlr_state *wlr
) {
      // Create the wlr_cursor.  There's only one.
    wlr->wlr_cursor = wlr_cursor_create();

    if ( wlr->wlr_cursor == NULL ) {
        LOG_ERROR("WLR cursor create failed.");
        return ERROR;
    }

    LOG_INFO("wcf_wlr_cursor.");

      // Specify listener routines for each wlr_cursor event.
    wlr->motion         .notify = cursor_listener_motion;
    wlr->motion_absolute.notify = cursor_listener_motion_absolute;
    wlr->axis           .notify = cursor_listener_axis;
    wlr->button         .notify = cursor_listener_button;
    wlr->frame          .notify = cursor_listener_frame;

      // Register the wlr_cursor listeners.
    wl_signal_add(&wlr->wlr_cursor->events.motion,          &wlr->motion);
    wl_signal_add(&wlr->wlr_cursor->events.motion_absolute, &wlr->motion_absolute);
    wl_signal_add(&wlr->wlr_cursor->events.axis,            &wlr->axis);
    wl_signal_add(&wlr->wlr_cursor->events.button,          &wlr->button);
    wl_signal_add(&wlr->wlr_cursor->events.frame,           &wlr->frame);

    // Attaching the cursor to the output layout must be delayed until
    // both are created.  This happens in wcf_shim.c

    return OK;
}
