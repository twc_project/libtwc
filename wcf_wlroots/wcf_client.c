
  // System
#include <stdlib.h>  // NULL, calloc, free
#include <stdbool.h>

  // Wayland
#include <wayland-util.h>   // wl_array

  // wlroots
#include <wlr/types/wlr_seat.h>
#include <wlr/types/wlr_xdg_shell.h>
#include <wlr/types/wlr_xdg_decoration_v1.h>
#include <wlr/types/wlr_keyboard.h>
#include <wlr/xwayland.h>

  // Protocols
#include <wayland-client-protocol.h>
#include <xdg-shell-client-protocol.h>
#include <xdg-decoration-unstable-v1-client-protocol.h>

  //TWC
#include <twc/wayland_types.h>          // wld_<type>_t
#include <twc/wayland_inert.h>
#include <twc/xdg_shell_inert.h>
#include <twc/twc_helper.h>             // twc_helper_connect()
#include <twc/twc_log.h>

  // APIs
#include "wcf_client.h"             // Our API to ensure consistancy
#include "wcf_client_initialize.h"
#include "external_buffer.h"

  // Imported data types
#include "wcf_wlr.h"
#include "wcf_external_surface.h"
#include "bic_server/xdg_decoration_bic.h"
#include "bic_server/wayland_bic.h"
#include "bic_server/xdg_shell_bic.h"

extern struct wlr_state WLR;

static
struct wl_singletons wl_singletons;


 // -------------------
 // wl_buffer listeners
 //
 // Use inert_wl_buffer_listener


 // --------------------
 // wl_surface listeners
 //
 // Use inert_wl_surface_listener


 // --------------------
 // wl_pointer listeners
 //

static
void
wcf_client_pointer_event_enter(
    void              *data,
    struct wl_pointer *wl_pointer,
    uint32_t           serial,
    struct wl_surface *wl_surface,
    wl_fixed_t         surface_x,
    wl_fixed_t         surface_y
) {

    //LOG_INFO("wcf_client_pointer_event_enter %d %d", wl_fixed_to_int(surface_x), wl_fixed_to_int(surface_y));
    wlr_seat_pointer_notify_enter(
        WLR.wlr_seat,
        wl_surface->nested_surface_with_ptr_focus,
        wl_fixed_to_double( surface_x ),
        wl_fixed_to_double( surface_y )
    );

    return;
}

static
void
wcf_client_pointer_event_leave(
    void              *data,
    struct wl_pointer *wl_pointer,
    uint32_t           serial,
    struct wl_surface *surface
) {
    //LOG_INFO("wcf_client_pointer_event_leave");
    wlr_seat_pointer_notify_clear_focus(
        WLR.wlr_seat
    );

    return;
}

static
void
wcf_client_pointer_event_motion(
    void              *data,
    struct wl_pointer *wl_pointer,
    uint32_t           time,
    wl_fixed_t         surface_x,
    wl_fixed_t         surface_y
) {
    wlr_seat_pointer_notify_motion(
        WLR.wlr_seat,
        time,
        wl_fixed_to_double( surface_x ),
        wl_fixed_to_double( surface_y )
    );

    return;
}

static
void
wcf_client_pointer_event_button(
    void              *data,
    struct wl_pointer *wl_pointer,
    uint32_t           serial,
    uint32_t           time,
    uint32_t           button,
    uint32_t           state
) {
    //LOG_INFO("wcf_client_pointer_event_button %d", button);
    wlr_seat_pointer_notify_button(
        WLR.wlr_seat,
        time,
        button,
        state
    );

    return;
}

static
void
wcf_client_pointer_event_axis(
        void              *data,
        struct wl_pointer *wl_pointer,
        uint32_t           time,
        uint32_t           axis,
        wl_fixed_t         value
) {
      // Tell wlroots to send wl_pointer.axis events to the client
      // with pointer focus.
    wlr_seat_pointer_notify_axis(
        WLR.wlr_seat,
        time,
        axis,
        15.0 * value, //pointer_axis_data->delta,
        value,        //pointer_axis_data->delta_discrete,
        0             //pointer_axis_data->source
    );

    return;
}

static
void
wcf_client_pointer_event_frame(
    void              *data,
    struct wl_pointer *wl_pointer
) {
    // Fix: ToDo
    return;
}

static
void
wcf_client_pointer_event_axis_source(
    void              *data,
    struct wl_pointer *wl_pointer,
    uint32_t           axis_source
) {
    // Fix: ToDo
    return;
}

static
void
wcf_client_pointer_event_axis_stop(
    void              *data,
    struct wl_pointer *wl_pointer,
    uint32_t           time,
    uint32_t           axis
) {
    // Fix: ToDo
    return;
}

static
void
wcf_client_pointer_event_axis_discrete(
    void              *data,
    struct wl_pointer *wl_pointer,
    uint32_t           axis,
    int32_t            discrete
) {
    // Fix: ToDo
    return;
}

static
const
struct wl_pointer_listener wcf_client_pointer_listener = {
    .enter         = wcf_client_pointer_event_enter,
    .leave         = wcf_client_pointer_event_leave,
    .motion        = wcf_client_pointer_event_motion,
    .button        = wcf_client_pointer_event_button,
    .axis          = wcf_client_pointer_event_axis,
    .frame         = wcf_client_pointer_event_frame,
    .axis_source   = wcf_client_pointer_event_axis_source,
    .axis_stop     = wcf_client_pointer_event_axis_stop,
    .axis_discrete = wcf_client_pointer_event_axis_discrete,
};

 // ---------------------
 // wl_keyborad listeners
 //

static
void
wcf_client_keyboard_event_keymap(
    void               *data,
    struct wl_keyboard *wl_keyboard,
    uint32_t            format,
    int32_t             fd,
    uint32_t            size
) {
    // Fix: ToDo
    return;
}

static
void
wcf_client_keyboard_event_enter(
    void               *data,
    struct wl_keyboard *wl_keyboard,
    uint32_t            serial,
    struct wl_surface  *surface,
    struct wl_array     *keys
) {
    struct wlr_surface *wlr_surface;

    struct wcf_external_surface *wcf_external_surface = (
        surface->wcf_external_surface
    );

    if ( wcf_external_surface->external_surface_type == ET_XDG_SURFACE ) {
        wlr_surface = wcf_external_surface->wcf_xdg_surface.wlr_xdg_surface->surface;
        wlr_xdg_toplevel_set_activated(
            wcf_external_surface->wcf_xdg_surface.wlr_xdg_surface->toplevel,
            true
        );
    }

    if ( wcf_external_surface->external_surface_type == ET_XWAYLAND_SURFACE ) {
        wlr_surface = wcf_external_surface->wcf_xwayland_surface.wlr_xwayland_surface->surface;
        wlr_xwayland_surface_activate(
            wcf_external_surface->wcf_xwayland_surface.wlr_xwayland_surface,
            true
        );
    }

    struct wlr_keyboard *wlr_keyboard = (
        wlr_seat_get_keyboard(WLR.wlr_seat)
    );

    wlr_seat_keyboard_notify_enter(
         WLR.wlr_seat,
         wlr_surface,
         wlr_keyboard->keycodes,
         wlr_keyboard->num_keycodes,
        &wlr_keyboard->modifiers
    );

    return;
}

static
void
wcf_client_keyboard_event_leave(
    void               *data,
    struct wl_keyboard *wl_keyboard,
    uint32_t            serial,
    struct wl_surface  *surface
) {
    // When the seat calculates a new surface-with-keyboard-focus, it
    // emits a leave event which is handled by the wcf_client through
    // this routine.  The action is to tell wlroots to remove focus
    // from the old surface.
    //
    // Consider the case where a client terminates.  First, wlroots
    // reports a new buffer with no dimension causing the surface to
    // become unmapped.  Second, it reports wlr_xdg_surface destroy.
    // And, finally, it reports wlr_surface destroy.  All as expected.
    //
    // Note that this routine is invoked after the unmap, but before
    // the xdg_surface is reported destroyed.
    //
    // In Wlroots 0.17.4, the call to remove focus results in wlroots
    // attempting to "schedule a configure".  (See wlr_surface.c line
    // 169.)  An error message
    //
    //     "A configure is scheduled for an uninitialized xdg_surface"
    //
    // It seems that wlroots has already decommissioned the
    // wlr_surface even before signaling the wlr_xdg_surface destroy,
    // let alone the wlr_surface_destroy.
    //
    // The subject surface of this event is a wl_surface, so should
    // work as long as this surface is active.

    // Fix: Test is this is still broken in Wlroots 0.18.0.

      // For Wlroots 0.17.4, don't remove focus from unmapped surfaces.
    if ( SURFACE_IS_MAPPED( surface) ) {

        struct wcf_external_surface *wcf_external_surface = (
            surface->wcf_external_surface
        );

        if ( wcf_external_surface->external_surface_type == ET_XDG_SURFACE ) {
            wlr_xdg_toplevel_set_activated(
                wcf_external_surface->wcf_xdg_surface.wlr_xdg_surface->toplevel,
                false
            );
        }

        if ( wcf_external_surface->external_surface_type == ET_XWAYLAND_SURFACE ) {
            wlr_xwayland_surface_activate(
                wcf_external_surface->wcf_xwayland_surface.wlr_xwayland_surface,
                false
            );
        }
    }

    wlr_seat_keyboard_notify_clear_focus( WLR.wlr_seat );

    return;
}

static
void
wcf_client_keyboard_event_key(
    void               *data,
    struct wl_keyboard *wl_keyboard,
    uint32_t            serial,
    uint32_t            time,
    uint32_t            key,
    uint32_t            state
) {
    // For wlroots clients, the wlr_seat will handle the key events

    const struct wl_seat *wl_seat = (
        wl_container_of(wl_keyboard, wl_seat, wl_keyboard)
    );

    wlr_seat_keyboard_notify_key(
        WLR.wlr_seat,
        time,
        key,
        state
    );

    return;
}

static
void
wcf_client_keyboard_event_modifiers(
    void               *data,
    struct wl_keyboard *wl_keyboard,
    uint32_t            serial,
    uint32_t            mods_depressed,
    uint32_t            mods_latched,
    uint32_t            mods_locked,
    uint32_t            group
) {
    struct wlr_keyboard_modifiers wlr_keyboard_modifiers = {
        .depressed = mods_depressed,
        .latched   = mods_latched,
        .locked    = mods_locked,
        .group     = group,
    };

      // Pass the modifiers thru to external client.
    wlr_seat_keyboard_notify_modifiers(
        WLR.wlr_seat,
        &wlr_keyboard_modifiers
    );

    return;
}

static
void
wcf_client_keyboard_event_repeat_info(
    void               *data,
    struct wl_keyboard *wl_keyboard,
    int32_t             rate,
    int32_t             delay
) {
    // Fix: ToDo
    return;
}

static
const
struct wl_keyboard_listener wcf_client_keyboard_listener = {
    .keymap      = wcf_client_keyboard_event_keymap,
    .enter       = wcf_client_keyboard_event_enter,
    .leave       = wcf_client_keyboard_event_leave,
    .key         = wcf_client_keyboard_event_key,
    .modifiers   = wcf_client_keyboard_event_modifiers,
    .repeat_info = wcf_client_keyboard_event_repeat_info,
};

 // ----------------------
 // xdg_surface listeners
 //

static
void
wcf_client_xdg_surface_event_configure(
    void               *data,
    struct xdg_surface *xdg_surface,
    uint32_t            serial
) {
    // A response to this event is mandatory.

    struct wl_surface *wl_surface = (
        wl_container_of(xdg_surface, wl_surface, xdg_surface)
    );

    if ( wl_surface->committed_image_type == BT_xwayland ) {
          // Xwayland clients don't ack_configure, so we do it.
        xdg_surface_ack_configure(xdg_surface, serial);
    } else {
        // For native wayland apps, wlroots automatically sends a
        // configure event.

        //xdg_surface_ack_configure(xdg_surface, serial);
    }

    return;
}

const struct xdg_surface_listener wcf_client_xdg_surface_listener = {
    .configure = wcf_client_xdg_surface_event_configure,
};

 // ----------------------
 // xdg_toplevel listeners
 //

static
void
wcf_client_xdg_toplevel_event_configure(
    void                *data,
    struct xdg_toplevel *xdg_toplevel,
    int32_t              width,
    int32_t              height,
    struct wl_array     *states
) {
    // Fix: handle states

    struct wcf_external_surface *wcf_external_surface = data;

    if ( wcf_external_surface->external_surface_type == ET_XDG_SURFACE ) {
          // Ask the xdg_toplevel to use these dimensions.
        wlr_xdg_toplevel_set_size(
            wcf_external_surface->wcf_xdg_surface.wlr_xdg_surface->toplevel,
            width,
            height
        );
    }

    if ( wcf_external_surface->external_surface_type == ET_XWAYLAND_SURFACE ) {

        struct wlr_xwayland_surface *wlr_xwayland_surface = (
            wcf_external_surface->wcf_xwayland_surface.wlr_xwayland_surface
        );

          // Ask the xwayland_surface to use these dimensions.
        wlr_xwayland_surface_configure(
            wlr_xwayland_surface,
            wlr_xwayland_surface->x,
            wlr_xwayland_surface->y,
            width,
            height
        );

        // Xwayland surfaces never notify wlroots when they've changed
        // their size.  So we assume they do and simulate receiving a
        // "new size" event.

        wld_geometry_t surface_geometry = {
            wlr_xwayland_surface->x,
            wlr_xwayland_surface->y,
            wlr_xwayland_surface->width,
            wlr_xwayland_surface->height
        };

        wcf_external_surface_commit(
            wcf_external_surface,
            surface_geometry
        );
    }

    return;
}

static
void
wcf_client_xdg_toplevel_event_close(
    void                *data,
    struct xdg_toplevel *xdg_toplevel
) {
    struct wcf_external_surface *wcf_external_surface = data;

    if ( wcf_external_surface->external_surface_type == ET_XDG_SURFACE ) {

        // Fix: Newer versions of wlroots uses this parameter for
        // wlr_xdg_toplevel_send_close().
        //
        // wlr_xdg_toplevel_send_close(
        //    wcf_external_surface->wlr_xdg_surface->toplevel
        // );

        wlr_xdg_toplevel_send_close(
            wcf_external_surface->wcf_xdg_surface.wlr_xdg_surface->toplevel
        );
    }

    if ( wcf_external_surface->external_surface_type == ET_XWAYLAND_SURFACE ) {

        wlr_xwayland_surface_close(
            wcf_external_surface->wcf_xwayland_surface.wlr_xwayland_surface
        );
    }

    return;
}

static
void
wcf_client_xdg_toplevel_event_configure_bounds(
    void                *data,
    struct xdg_toplevel *xdg_toplevel,
    int32_t              width,
    int32_t              height
) {
    return;
}

static
void
wcf_client_xdg_toplevel_event_wm_capabilities(
    void                *data,
    struct xdg_toplevel *xdg_toplevel,
    struct wl_array     *capabilities
) {
    struct wcf_external_surface *wcf_external_surface = data;

    enum xdg_toplevel_wm_capabilities bit_map = 0;

    enum xdg_toplevel_wm_capabilities *capability;
    wl_array_for_each( capability, capabilities) {
        bit_map |= *capability;
    }

    //LOG_INFO("wcf_client_xdg_toplevel_event_wm_capabilities %x", bit_map);

    // Fix: The early return is to prevent this error which leades
    // later to a seg fault.
    //
    // [ERROR] [types/xdg_shell/wlr_xdg_surface.c:169] A configure is scheduled for an uninitialized xdg_surface

    return;

    wlr_xdg_toplevel_set_wm_capabilities(
        wcf_external_surface->wcf_xdg_surface.wlr_xdg_surface->toplevel,
        bit_map
    );

    return;
}

static
const
struct xdg_toplevel_listener wcf_client_xdg_toplevel_listener = {
    .configure        = wcf_client_xdg_toplevel_event_configure,
    .close            = wcf_client_xdg_toplevel_event_close,
    .configure_bounds =wcf_client_xdg_toplevel_event_configure_bounds,
    .wm_capabilities  = wcf_client_xdg_toplevel_event_wm_capabilities,
};

 // ------------------------
 // xdg_decoration listeners
 //

static
void
wcf_client_xdg_decoration_event_configure(
    void                               *data,
    struct zxdg_toplevel_decoration_v1 *xdg_decoration,
    uint32_t                            mode
) {
    //LOG_INFO("wcf_client_xdg_decoration_event_configure %d", mode);

      // We left the wcf_external_surface as a cookie in the listener.
    struct wcf_external_surface *wcf_external_surface = data;

    // For the configure event, the mode must always be one of CLIENT
    // or SERVER side.

      // Convert mode to wlroots encoding
    enum wlr_xdg_toplevel_decoration_v1_mode wlr_mode = (
        mode == DM_Client_Side
        ? WLR_XDG_TOPLEVEL_DECORATION_V1_MODE_CLIENT_SIDE
        : WLR_XDG_TOPLEVEL_DECORATION_V1_MODE_SERVER_SIDE
    );

    wlr_xdg_toplevel_decoration_v1_set_mode(
        wcf_external_surface->wlr_xdg_toplevel_decoration,
        wlr_mode
    );

    return;
}

static
const
struct zxdg_toplevel_decoration_v1_listener wcf_client_xdg_decoration_listener = {
    .configure = wcf_client_xdg_decoration_event_configure,
};


 // ----------
 // wcf_client

static
void
wcf_client_new_seat(
    struct wl_seat *wl_seat
) {
    void *data = NULL;

    struct wl_pointer  *wl_pointer  = wl_seat_get_pointer (wl_seat);
    struct wl_keyboard *wl_keyboard = wl_seat_get_keyboard(wl_seat);

    wl_pointer_add_listener( wl_pointer,  &wcf_client_pointer_listener,  data);
    wl_keyboard_add_listener(wl_keyboard, &wcf_client_keyboard_listener, data);

    return;
}

extern
struct wl_display*
wcf_client_initialize(
    bool                   *running,
    struct twc_config_data *twc_config_data
) {
    LOG_INFO("wcf_client_initialize");

    *running = false;

    wl_singletons.new_wl_seat_routine   = wcf_client_new_seat;
    wl_singletons.new_wl_output_routine = twc_helper_output_not_used;

    bool connected = (
        twc_helper_connect( NULL, &wl_singletons )
    );
    if ( ! connected ) { return NULL; }

    struct wl_display *wl_display = (
        wl_singletons.wl_display
    );

    *running = true;

    return wl_display;
}

 /*
 ** The following routines are a subset of the API
 **
 **   wcf_shim --> wcf_client
 **
 ** The remaining routines in the API are defined as static inline.
 ** See
 **   ./wcf_client.h
 */

extern
struct wcf_external_surface*
wcf_external_surface_create(
    void                      *wlr_surface,
    enum external_surface_type external_surface_type
) {
    //LOG_INFO("wcf_external_surface_create type %d", external_surface_type);

    // For each toplevel, the wcf_client creates a surface and
    // attaches a special wl_buffer.  Instead of an offset into a
    // shm_pool, the special buffer contains a wcf_external_surface
    // pointer.

      // wcf_external_surface is freed in wcf_external_surface_destroy() below.
    struct wcf_external_surface *wcf_external_surface = (
        calloc( 1, sizeof(struct wcf_external_surface) )
    );

    wcf_external_surface->external_surface_type = external_surface_type;

    if ( external_surface_type == ET_XDG_SURFACE ) {
        wcf_external_surface->wcf_xdg_surface.wlr_xdg_surface = wlr_surface;
    }

    if ( external_surface_type == ET_XWAYLAND_SURFACE ) {
        wcf_external_surface->wcf_xwayland_surface.wlr_xwayland_surface = wlr_surface;
    }

    wcf_external_surface->is_mapped             = false;
    wcf_external_surface->xdg_surface_destroyed = false;
    wcf_external_surface->app_id_set            = false;
    wcf_external_surface->title_set             = false;

    for (int i = 0; i < 2; i++) {
        wcf_external_surface->wl_buffer[i] = (
            external_buffer_create(
                wcf_external_surface,
                external_surface_type
            )
        );

        wl_buffer_add_listener(
            wcf_external_surface->wl_buffer[i],
            INERT_WL_BUFFER_LISTENER,
            NULL
        );
    }
    wcf_external_surface->next_buffer = 0;

      // wl_surface
    struct wl_surface *wl_surface = (
        wl_compositor_create_surface(
            wl_singletons.wl_compositor
        )
    );
    wl_surface_add_listener(
        wl_surface,
        INERT_WL_SURFACE_LISTENER,
        NULL
    );
    wcf_external_surface->wl_surface = wl_surface;

    if ( external_surface_type == ET_XDG_SURFACE ) {
        wl_surface->committed_image_type = BT_external;
    }

    if ( external_surface_type == ET_XWAYLAND_SURFACE ) {
        wl_surface->committed_image_type = BT_xwayland;
    }

    return wcf_external_surface;
}

extern
void
wcf_external_surface_destroy(
    struct wcf_external_surface *wcf_external_surface
) {
    wl_surface_destroy(wcf_external_surface->wl_surface  );
    wl_buffer_destroy (wcf_external_surface->wl_buffer[0]);
    wl_buffer_destroy (wcf_external_surface->wl_buffer[1]);

    free(wcf_external_surface);

    return;
}

extern
void
wcf_get_xdg_surface(
    struct wcf_external_surface *wcf_external_surface
){
      // xdg_surface
    struct xdg_surface *xdg_surface = (
        xdg_wm_base_get_xdg_surface(
            wl_singletons.xdg_wm_base,
            wcf_external_surface->wl_surface
        )
    );

    xdg_surface_add_listener(
        xdg_surface,
       &wcf_client_xdg_surface_listener,
        NULL
    );
    wcf_external_surface->xdg_surface = xdg_surface;

    return;
}

extern
void
wcf_xdg_surface_ack_configure(
    struct wcf_external_surface *wcf_external_surface,
    uint32_t                     serial
) {
    struct xdg_surface *xdg_surface = wcf_external_surface->xdg_surface;

    xdg_surface_ack_configure(xdg_surface, serial);
}

extern
void
wcf_xdg_surface_destroy(
    struct wcf_external_surface *wcf_external_surface
) {
    //LOG_INFO("wcf_xdg_surface_destroy %p", wcf_external_surface->wcf_xdg_surface.wlr_xdg_surface);

    wcf_external_surface->xdg_surface_destroyed = true;

    xdg_surface_destroy(wcf_external_surface->xdg_surface);
}

extern
void
wcf_get_xdg_toplevel(
    struct wcf_external_surface *wcf_external_surface
){
    //LOG_INFO("wcf_get_xdg_toplevel %p", wcf_external_surface->wcf_xdg_surface.wlr_xdg_surface);

      // xdg_toplevel
    struct xdg_toplevel *xdg_toplevel = (
        xdg_surface_get_toplevel(
            wcf_external_surface->xdg_surface
        )
    );
    xdg_toplevel_add_listener(
         xdg_toplevel,
        &wcf_client_xdg_toplevel_listener,
         wcf_external_surface
    );
    wcf_external_surface->xdg_toplevel = xdg_toplevel;

    return;
}

extern
void
wcf_xdg_toplevel_destroy(
    struct wcf_external_surface *wcf_external_surface
) {
    xdg_toplevel_destroy(wcf_external_surface->xdg_toplevel);
}

extern
void
wcf_external_surface_commit(
    struct wcf_external_surface *wcf_external_surface,
    wld_geometry_t               visible_geometry
) {
    // If a new geometry matches the current one, there is no change.

    //LOG_INFO(
    //  "wcf_external_surface_commit type %d",
    //  wcf_external_surface->external_surface_type
    //);

    struct wl_surface *wl_surface = (
        wcf_external_surface->wl_surface
    );

    if ( visible_geometry.width <= 0 || visible_geometry.height <= 0) {
        // Surface has no pixels.  It either becomes or remains
        // unmapped.

        if ( wcf_external_surface->is_mapped ) {
            // Surface becomes unmapped

            wcf_external_surface->is_mapped = false;

            wl_surface_attach(wl_surface, NULL, 0,0);
            wl_surface_commit(wl_surface);
        } else {
              // The surface is already umapped.  This may be an
              // "Initial Commit."
            wl_surface_commit(wl_surface);
        }

        return;
    }

      // Surface has a new buffer or becomes mapped.  In either case:
    wcf_external_surface->is_mapped = true;

    struct wl_buffer *wl_buffer = (
        wcf_external_surface->wl_buffer[ wcf_external_surface->next_buffer ]
    );
    wcf_external_surface->next_buffer++;
    wcf_external_surface->next_buffer %= 2;

    wl_buffer->width  = visible_geometry.width;
    wl_buffer->height = visible_geometry.height;

    wl_buffer->enclosure_sx = -visible_geometry.x;
    wl_buffer->enclosure_sy = -visible_geometry.y;

    wl_surface_attach(wl_surface, wl_buffer, 0,0);

    wl_surface_commit(wl_surface);

    return;
}

extern
void
wcf_decoration_get_toplevel_decoration(
    struct wcf_external_surface *wcf_external_surface
) {
    struct zxdg_toplevel_decoration_v1 *xdg_decoration = (
        zxdg_decoration_manager_v1_get_toplevel_decoration(
            wl_singletons.xdg_decoration_manager,
            wcf_external_surface->xdg_toplevel
        )
    );

    wcf_external_surface->xdg_decoration = xdg_decoration;

      // Place the wcf_external_surface as a cookie in the listener.

    zxdg_toplevel_decoration_v1_add_listener(
         xdg_decoration,
        &wcf_client_xdg_decoration_listener,
         wcf_external_surface
    );

    return;
}

extern
void
wcf_decoration_destroy(
    struct wcf_external_surface *wcf_external_surface
) {
    zxdg_toplevel_decoration_v1_destroy(
        wcf_external_surface->xdg_decoration
    );

    wcf_external_surface->xdg_decoration = NULL;
}
