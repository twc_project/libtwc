#ifndef WCF_WLR_H
#define WCF_WLR_H

  // System
#include <stdint.h>  // uint32_t

  // Wayland
#include <wayland-server-core.h>  // wl_listener, wl_list

  /*
  ** In "Should You Write a Wayland Compositor?", Tudor Roman writes(*)
  **
  **   "wlroots, which is an exceptional Wayland compositor library
  **    (apart from its lack of documentation)."
  **
  ** (*) https://tudorr.ro/blog/technical/2021/01/26/the-wayland-experience/
  */

  // Opaque types
struct wl_display;
struct wlr_backend;
struct wlr_allocator;
struct wlr_compositor;
struct wlr_subcompositor;
struct wlr_cursor;
struct wlr_data_device_manager;
struct wlr_input_device;
struct wlr_output_layout;
struct wlr_primary_selection_device_manager;
struct wlr_renderer;
struct wlr_seat;
struct wlr_text_input_manager;
struct wlr_xcursor_manager;
struct wlr_xdg_decoration_manager_v1;
struct wlr_xdg_shell;
struct wlr_xwayland;

 /*
 ** The wcf_client uses the wlroots library for I/O devices and to
 ** mangage external clients.
 **
 ** Wlroots makes use of wl_listeners to announce the activities of
 ** various WLR objects, e.g., the wlr_compositor, wlr_cursor, and
 ** wlr_output objects.  The core feature of a listener is a callback
 ** routine.
 **
 ** When a wlr_object corresponds to a Wayland wl_object, wlroots
 ** will announce Wayland client requests by invoking the callback
 ** routine of the listener for the wlr_object which corresponds to
 ** the wl_object request.
 **
 ** Some WLR objects are standard, one-of-kind objects that form the
 ** basis of every wlroots based compositor.  These objects are
 ** created by the compositor by invoking a wlroots library routine
 ** of the form wlr_<name>_create(), eg, wlr_compositor_create().
 ** These WLR objects manage the Wayland Global Singletons as defined
 ** in the Wayland protocol.  These singletons are discoverable by
 ** client programs.
 **
 ** Besides the compositor created objects, there are other WLR
 ** objects.  These are dynamic, created on-demand by clients, and
 ** may have multiple instances.  For example, wlr_xdg_surface
 ** objects.  These objects are announced by wlroots via a
 ** wl_listener.
 **
 ** Standard, one-of-kind WLR objects
 **
 **   wl_display                       technically a wayland, not wlroots, object
 **
 **   wlr_xcursor_manager              no listeners
 **   wlr_backend                      have listener(s)
 **   wlr_compositor                       |
 **   wlr_subcompositor                    |
 **   wlr_output_layout                    |
 **   wlr_cursor                           |
 **   wlr_data_device_manager              |
 **   wlr_primary_selection_device_manager |
 **   wlr_text_input_manager               |
 **   wlr_seat                             |
 **   wlr_xdg_shell                        |
 **   wlr_xdg_decoration_manager_v1        v
 **
 ** Dynamic WLR objects
 **
 **   wlr_output                       each instance has listeners
 **   wlr_keyboard                      |
 **   wlr_pointer                       |
 **   wlr_surface                       |
 **   wlr_xdg_surface                   v  For v0.17.4, only reported when client has assigned a role
 **                                        For v0.18,   each role is reported separetely
 **
 ** We need storage to contain wl_listeners (and other state) for
 ** each of these WLR objects.  Since there is exactly one each of
 ** the standard objects, their state can be combined into a single
 ** data structure.
 */


 // ---------
 // wlr_state
 //

  // This struct contains data for the standard WLR objects described above.
  // There is exactly one instance of this struct.
struct wlr_state {

    const char *socket;

      // Some WLR standard objects require no addtional state.

    struct wl_display          *wl_display;      // wl_display_create()
    struct wlr_xcursor_manager *xcursor_manager; // wlr_xcursor_manager_create()

      // State for wlr_backend

    struct wlr_backend   *backend;      // wlr_backend_autocreate()
    struct wlr_session   *session;      // wlr_backend_autocreate()
    struct wlr_renderer  *renderer;     // wlr_renderer_autocreate()
    struct wlr_allocator *allocator;    // wlr_allocator_autocreate()
//  struct wlr_scene     *scene;        // wlr_scene_create()

    struct wl_listener new_output;      // new CRTC
    struct wl_listener new_input;       // new keyboards and pointers
    struct wl_listener destroy_backend;

    struct wl_list   output_list;       // Unused
    struct wl_list keyboard_list;
    int             pointer_count;
    struct wl_list  pointer_list;       // ??? We use wlr_cursor instead of wlr_pointer.
                                        // maybe wlr_pointer to get libinput support for mouse accel.

      // State for wlr_compositor

    struct wlr_compositor *compositor;  // wlr_compositor_create()

    struct wl_listener new_wl_surface;
    struct wl_listener destroy_compositor;

      // State for wlr_subcompositor

    struct wlr_subcompositor *subcompositor;    // wlr_subcompositor_create()

    struct wl_listener destroy_subcompositor;

      // State for wlr_output_layout

    struct wlr_output_layout *output_layout;    // wlr_output_layout_create()

    struct wl_listener add;
    struct wl_listener change;
    struct wl_listener destroy_output_layout;


      // State for wlr_cursor

    struct wlr_cursor *wlr_cursor;  // wlr_cursor_create()

    struct wl_listener motion;
    struct wl_listener motion_absolute;
    struct wl_listener axis;
    struct wl_listener button;
    struct wl_listener frame;

      // State for wlr_data_device_manager

    struct wlr_data_device_manager *data_device_manager; // wlr_data_device_manager_create()

    struct wl_listener destroy_data_device_manager;

      // State for wlr_primary_selection_device_manager

    struct wlr_primary_selection_v1_device_manager *primary_selection_device_manager; // wlr_primary_selection_v1_device_manager_create()

    struct wl_listener destroy_primary_selection_device_manager;

      // State for wlr_text_input_manager

    struct wlr_text_input_manager_v3 *text_input_manager; // wlr_text_input_v3_device_create()

    struct wl_listener destroy_text_input_manager;

      // State for wlr_seat

    struct wlr_seat *wlr_seat;          // wlr_seat_create()
    uint32_t         seat_capabilities;

    struct wl_listener pointer_grab_begin;
    struct wl_listener pointer_grab_end;
    struct wl_listener keyboard_grab_begin;
    struct wl_listener keyboard_grab_end;
    struct wl_listener touch_grab_begin;
    struct wl_listener touch_grab_end;
    struct wl_listener request_set_cursor;
    struct wl_listener request_set_selection;
    struct wl_listener set_selection;
    struct wl_listener request_set_primary_selection;
    struct wl_listener set_primary_selection;

    struct wl_listener request_start_drag;
    struct wl_listener start_drag;
    struct wl_listener destroy_seat;

      // State for wlr_xdg_shell

    struct wlr_xdg_shell *xdg_shell;    // wlr_xdg_shell_create()

    struct wl_listener new_xdg_surface;
    struct wl_listener destroy_xdg_shell;

      // State for wlr_decoration_manager

    struct wlr_xdg_decoration_manager_v1 *xdg_decoration_manager; // wlr_xdg_decoration_manager_v1_create()

    struct wl_listener new_toplevel_decoration;
    struct wl_listener destroy_xdg_decoration_manager;

      // State for wlr_xwayland
    struct wlr_xwayland *wlr_xwayland;  // wlr_xwayland_create()

    struct wl_listener xwayland_ready;
    struct wl_listener xwayland_new_surface;
    struct wl_listener xwayland_remove_startup_info;

      // output and screenshot managers
    struct wlr_output_manager_v1     *wlr_output_manager_v1;
    struct wlr_screencopy_manager_v1 *wlr_screencopy_manager_v1;
};


  //
  // Below are structs to contain state for dynmaic WLR objects.
  // There are likely multiple instances of these objects.
  //

  // --------------------
  // wcf_wlr_input_device
  //

struct wcf_pointer;

struct wcf_wlr_input_device {
    // Announced by wlr_backend listener new_input.

    // There are several types of input device, eg keyboard, pointer,
    // etc.  An input device has exactly one listener, a destroy
    // listener.
    //
    // Depending on the type, a wlr_input_device points to an
    // auxilliary, type specific object.  E.g.,
    //
    // wlr_input_device->keyboard is a struct wlr_keyboard
    // wlr_input_device->pointer  is a struct wlr_pointer

    struct wl_list           link;
    struct wlr_state        *wlr;
    struct wlr_input_device *wlr_input_device;

    struct wl_listener destroy;

    struct wcf_pointer *wcf_pointer;
};

#endif // WCF_WLR_H
