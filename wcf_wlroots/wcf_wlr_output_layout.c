
  // System
#include <stddef.h>    // NULL

  // Wayland
#include <wayland-server-core.h>    // wl_listener, wl_signal

  // Wlroots
#include <wlr/types/wlr_output_layout.h>
#include <wlr/types/wlr_scene.h>

  // TWC
#include <twc/twc_log.h>

  // APIs
#include "wcf_wlr_init.h"           // Our API to ensure consistancy

  // Imported Data Types
#include "wcf_wlr.h"

 /*
 ** WLR output_layout
 **
 ** There is only one wlr_output_layout object.  It maintains the
 ** spacial arrangement of all output displays relative to one
 ** another.
 **
 ** The WLR output_layout events can be found here:
 **
 **   <wlroots_src>/include/wlr/types/wlr_output_layotu.h
 **
 ** By using wlr_output_layout_add_auto() for new outputs, there's no
 ** need to register listeners.
 */


  // -------------------------------
  // output_layout listener routines

static
void
output_layout_listener_add(
    struct wl_listener *listener,
    void               *data
) {
    LOG_INFO("output_layout_listener_add %p", data);

    return;
}

static
void
output_layout_listener_change(
    struct wl_listener *listener,
    void               *data
) {
    LOG_INFO("output_layout_listener_change %p", data);

    return;
}

static
void
output_layout_listener_destroy(
    struct wl_listener *listener,
    void               *data
) {
    LOG_INFO("output_layout_listener_destroy %p", data);

    struct wlr_output_layout *wlr_output_layout = data;

    wlr_output_layout_destroy(wlr_output_layout);

    return;
}

  // ----------------------------
  // WLR output layout initialize
  //
  // There can only be one.

extern
error_t
wcf_wlr_output_layout(
    struct wlr_state *wlr
) {
    wlr->output_layout = wlr_output_layout_create();

    if ( wlr->output_layout == NULL ) {
        LOG_ERROR("WLR output_layout create failed.");
        return ERROR;
    }

    LOG_INFO("Created WLR output_layout. %p", wlr->output_layout);

    // Attaching the cursor to the output layout must be delayed until
    // both are created.  This happens in wcf_shim.c


      // Specify listener routines for each wlr_output_layout event.
    wlr->add                  .notify = output_layout_listener_add;
    wlr->change               .notify = output_layout_listener_change;
    wlr->destroy_output_layout.notify = output_layout_listener_destroy;

      // Register the output_layout listeners.
    wl_signal_add(&wlr->output_layout->events.add,     &wlr->add);
    wl_signal_add(&wlr->output_layout->events.change,  &wlr->change);
    wl_signal_add(&wlr->output_layout->events.destroy, &wlr->destroy_backend);

    return OK;
}
