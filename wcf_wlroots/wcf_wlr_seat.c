
 // System
#include <stddef.h> // NULL

  // Wayland
#include <wayland-util.h>           // wl_container_of
#include <wayland-server-core.h>    // wl_listener, wl_signal

  // Wlroots
#include <wlr/types/wlr_cursor.h>
#include <wlr/types/wlr_data_device.h>  // wlr_seat_set_selection
#include <wlr/types/wlr_seat.h>
#include <wlr/types/wlr_primary_selection.h>    // wlr_seat_set_primary_selection()

  // TWC
#include <twc/twc_log.h>

  // APIs
#include "wcf_wlr_init.h" // Our API to ensure consistancy

  // Imported Data Types
#include "wcf_wlr.h"

 /*
 ** WLR seat
 **
 ** wlr_seat_create() places the Wayland global singleton wl_seat
 ** into the server registry for use by clients.  A wl_seat is a
 ** factory for wl_pointer, wl_keyboard and wl_touch.  See
 **
 **   <wayland_src>/wayland/protocol/wayland.xml
 **   https://emersion.fr/blog/2020/wayland-clipboard-drag-and-drop/
 **
 ** The wl_seat is closely tied to the Wayland inter-client data
 ** transfer mechanisms such as wl_data_source.  See
 ** seat_listener_request_set_selection() below.
 **
 **   https://emersion.fr/blog/2020/wayland-clipboard-drag-and-drop/
 **
 ** In wlroots, most wl_pointer activity is delivered through the
 ** wlr_cursor object.  However, wl_pointer::set_cursor requests from
 ** clients are delivered through wlr_seat::request_set_cursor.
 **
 ** The WLR seat events can be found here:
 **
 **   <wlroots_src>/include/wlr/types/wlr_seat.h
 **
 ** Seat listeners are part of the WLR state.
 */

 // ----------------------
 // Seat listener routines

static
void
seat_listener_pointer_grab_begin(
    struct wl_listener *listener,
    void               *data
) { return; }

static
void
seat_listener_pointer_grab_end(
    struct wl_listener *listener,
    void               *data
) { return; }

static
void
seat_listener_keyboard_grab_begin(
    struct wl_listener *listener,
    void               *data
) { return; }

static
void
seat_listener_keyboard_grab_end(
    struct wl_listener *listener,
    void               *data
) { return; }

static
void
seat_listener_touch_grab_begin(
    struct wl_listener *listener,
    void               *data
) { return; }

static
void
seat_listener_touch_grab_end(
    struct wl_listener *listener,
    void               *data
) { return; }

static
void
seat_listener_request_set_cursor(
    struct wl_listener *listener,
    void               *data
) {
    // A client has sent a Wayland wl_pointer.set_cursor request and
    // provided a cursor image.  Let wlroots handle it internally.
    //
    // Any client could make this request at any time.  However, the
    // requesting client and the seat_focus client must match.  The
    // seat_focus client is the one that the seat says has current
    // pointer focus.

    //LOG_INFO("seat_listener_request_set_cursor");

    struct wlr_state *wlr = (
        wl_container_of(listener, wlr, request_set_cursor)
    );

      // Get a reference to the set_cursor_data.
    struct wlr_seat_pointer_request_set_cursor_event *set_cursor_data = data;

      // Get a reference to the requesting_client.
    struct wlr_seat_client *requesting_client = set_cursor_data->seat_client;

      // Get a reference to the current seat_focus_client.
    struct wlr_seat_client *seat_focus_client = wlr->wlr_seat->pointer_state.focused_client;

    if (requesting_client == seat_focus_client) {
        wlr_cursor_set_surface(
            wlr->wlr_cursor,
            set_cursor_data->surface,
            set_cursor_data->hotspot_x,
            set_cursor_data->hotspot_y
        );
    }

    return;
}

static
void
seat_listener_request_set_selection(
    struct wl_listener *listener,
    void               *data
) {
    // A client has sent a Wayland wl_data_source.set_selection
    // request.  Let wlroots handle it internally.

    //LOG_INFO("seat_listener_request_set_selection %p", (void *) data);

    struct wlr_state *wlr = (
        wl_container_of(listener, wlr, request_set_selection)
    );

    struct wlr_seat_request_set_selection_event *set_selection_data = data;

      // Always allow the request.
    wlr_seat_set_selection(
        wlr->wlr_seat,
        set_selection_data->source,
        set_selection_data->serial
    );

    return;
}

static
void
seat_listener_set_selection(
    struct wl_listener *listener,
    void               *data
) {
    return;
}

static
void
seat_listener_request_set_primary_selection(
    struct wl_listener *listener,
    void               *data
) {
    // A client has sent a
    // zwp_primary_selection_device_v1::set_selection request.

    LOG_INFO("seat_listener_request_primary_selection %p", (void *) data);

    struct wlr_state *wlr = (
        wl_container_of(listener, wlr, request_set_primary_selection)
    );

    struct wlr_seat_request_set_primary_selection_event *primary_selection_event = data;

      // Always allow the request.
    wlr_seat_set_primary_selection(
        wlr->wlr_seat,
        primary_selection_event->source,
        primary_selection_event->serial
    );

    return;
}

static
void
seat_listener_set_primary_selection(
    struct wl_listener *listener,
    void               *data
) {
    return;
}

static
void
seat_listener_request_start_drag(
    struct wl_listener *listener,
    void               *data
) { return; }

static
void
seat_listener_start_drag(
    struct wl_listener *listener,
    void               *data
) { return; }

static
void
seat_listener_destroy(
    struct wl_listener *listener,
    void               *data
) {
    LOG_INFO("seat_listener_destroy.");
    return;
}

 // -------------------
 // WLR seat initialize
 //
 // Only one seat is created.  Its listeners are part of WLR state.

extern
error_t
wcf_wlr_seat(
    struct wlr_state *wlr
) {
    // Create a wl_seat object and make it visible to clients.  How do
    // you specify which keyboard/mouse if there are many?

    wlr->wlr_seat = wlr_seat_create(wlr->wl_display, "seat0");

LOG_INFO("wcf_wlr_seat %p", wlr_seat_get_keyboard(wlr->wlr_seat) );

    if ( wlr->wlr_seat == NULL ) {
        LOG_ERROR("WLR seat create failed.");
        return ERROR;
    }

    wlr->seat_capabilities = 0;

      // Specify listener routines for each seat event.
    wlr->pointer_grab_begin           .notify = seat_listener_pointer_grab_begin;
    wlr->pointer_grab_end             .notify = seat_listener_pointer_grab_end;
    wlr->keyboard_grab_begin          .notify = seat_listener_keyboard_grab_begin;
    wlr->keyboard_grab_end            .notify = seat_listener_keyboard_grab_end;
    wlr->touch_grab_begin             .notify = seat_listener_touch_grab_begin;
    wlr->touch_grab_end               .notify = seat_listener_touch_grab_end;
    wlr->request_set_cursor           .notify = seat_listener_request_set_cursor;
    wlr->request_set_selection        .notify = seat_listener_request_set_selection;
    wlr->set_selection                .notify = seat_listener_set_selection;
    wlr->request_set_primary_selection.notify = seat_listener_request_set_primary_selection;
    wlr->set_primary_selection        .notify = seat_listener_set_primary_selection;
    wlr->request_start_drag           .notify = seat_listener_request_start_drag;
    wlr->start_drag                   .notify = seat_listener_start_drag;
    wlr->destroy_seat                 .notify = seat_listener_destroy;

      // Register the seat listeners.
    wl_signal_add(&wlr->wlr_seat->events.pointer_grab_begin,            &wlr->pointer_grab_begin);
    wl_signal_add(&wlr->wlr_seat->events.pointer_grab_end,              &wlr->pointer_grab_end);
    wl_signal_add(&wlr->wlr_seat->events.keyboard_grab_begin,           &wlr->keyboard_grab_begin);
    wl_signal_add(&wlr->wlr_seat->events.keyboard_grab_end,             &wlr->keyboard_grab_end);
    wl_signal_add(&wlr->wlr_seat->events.touch_grab_begin,              &wlr->touch_grab_begin);
    wl_signal_add(&wlr->wlr_seat->events.touch_grab_end,                &wlr->touch_grab_end);
    wl_signal_add(&wlr->wlr_seat->events.request_set_cursor,            &wlr->request_set_cursor);
    wl_signal_add(&wlr->wlr_seat->events.request_set_selection,         &wlr->request_set_selection);
    wl_signal_add(&wlr->wlr_seat->events.set_selection,                 &wlr->set_selection);
    wl_signal_add(&wlr->wlr_seat->events.request_set_primary_selection, &wlr->request_set_primary_selection);
    wl_signal_add(&wlr->wlr_seat->events.set_primary_selection,         &wlr->set_primary_selection);
    wl_signal_add(&wlr->wlr_seat->events.request_start_drag,            &wlr->request_start_drag);
    wl_signal_add(&wlr->wlr_seat->events.start_drag,                    &wlr->start_drag);
    wl_signal_add(&wlr->wlr_seat->events.destroy,                       &wlr->destroy_seat);

    return OK;
}
