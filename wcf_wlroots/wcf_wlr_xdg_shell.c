
  // System
#include <stddef.h>    // NULL

  // Wayland
#include <wayland-server-core.h>    // wl_listener, wl_signal

  // Wlroots
#include <wlr/types/wlr_xdg_shell.h>

  // TWC
#include <twc/twc_log.h>

  // APIs
#include "wcf_wlr_init.h"       // Our API to ensure consistancy
#include "wcf_wlr_listeners.h"  // wcf_wlr_xdg_shell_listener_new_surface routine

  // Imported Data Types
#include "wcf_wlr.h"

 /*
 ** WLR xdg_shell
 **
 ** wlr_xdg_shell_create() places the global singleton xdg_shell object
 ** into the server registry for use by clients.
 **
 ** An xdg_shell is a factory for xdg_surface objects.  See
 **
 **     <wayland_src>/wayland-protocols/stable/xdg-shell/xdg-shell.xml
 **
 ** WLR xdg_shell events can be found here:
 **
 **     <wlroots_src>/include/wlr/types/wlr_xdg_shell.h
 **
 ** xdg_shell listeners are part of the WLR state.
 */


 // ---------------------------
 // xdg_shell listener routines
 //
 // Since each new surface is a complex object with its own listeners
 // etc, the listener routine
 //
 //     wcf_wlr_xdg_shell_listener_new_surface()
 //
 // is defined in
 //
 //    wcf_wlr_xdg_surface.c

static
void
xdg_shell_listener_destroy(
    struct wl_listener *listener,
    void               *data
) {
    return;
}


 // ------------------------
 // WLR xdg_shell initialize
 //
 // Only one wlr_xdg_shell object can be created.

extern
error_t
wcf_wlr_xdg_shell(
    struct wlr_state *wlr
) {
#   define XDG_SHELL_VERSION 5

    wlr->xdg_shell = wlr_xdg_shell_create( wlr->wl_display, XDG_SHELL_VERSION );

    if ( wlr->xdg_shell == NULL ) {
        LOG_ERROR("WLR xdg_shell create failed.");
        return ERROR;
    }

    LOG_INFO("Created WLR xdg_shell.");

      // Specify listener routines for each xdg_shell event.
    wlr->new_xdg_surface  .notify = wcf_wlr_xdg_shell_listener_new_surface;
    wlr->destroy_xdg_shell.notify = xdg_shell_listener_destroy;

      // Register the xdg_shell listeners
    wl_signal_add(&wlr->xdg_shell->events.new_surface, &wlr->new_xdg_surface  );
    wl_signal_add(&wlr->xdg_shell->events.destroy,     &wlr->destroy_xdg_shell);

    return OK;
}
