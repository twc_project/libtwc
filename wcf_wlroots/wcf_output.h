#ifndef WCF_OUTPUT_H
#define WCF_OUTPUT_H

  // Wayland
#include <wayland-server-core.h>  // wl_listener, wl_list

  // Opaque types
struct wlr_output;
struct wlr_state;
struct srv_output;
struct wlr_output_damage;

struct wcf_output {
    // Announced by wlr_backend listener new_output.

    // A single graphics device may have several outputs.  There may
    // also be more than one graphics device.

    struct wl_list     link;
    struct wlr_output *wlr_output;
    struct wlr_state  *wlr;

    struct wl_listener frame;   // Please repaint output framebuffer.
    struct wl_listener destroy;

    struct srv_output *srv_output;

      // Fix: ToDo
    struct wlr_output_damage *wlr_output_damage;
    struct wl_listener        damage_frame;
};

#endif // WCF_OUTPUT_H
