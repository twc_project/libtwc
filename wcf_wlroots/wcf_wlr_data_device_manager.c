
  // System
#include <stddef.h>    // NULL

  // Wayland
#include <wayland-server-core.h>    // wl_listener, wl_signal

  // Wlroots
#include <wlr/types/wlr_data_device.h>

  //TWC
#include <twc/twc_log.h>

  // APIs
#include "wcf_wlr_init.h"    // Our API to ensure consistancy

  // Imported Data Types
#include "wcf_wlr.h"


 /*
 ** WLR data device manager
 **
 ** wlr_data_device_manager_create() places the Wayland global
 ** singleton wl_data_device_manager into the server registry for use
 ** by clients.
 **
 ** A wl_data_device_manager is a factory for wl_data_source and
 ** wl_data_device objects.  These are inter-client data transfer
 ** mechanisms, e.g., cut and paste.  They are closedly tied to the
 ** singleton wl_seat object.  See
 **
 **   <wayland_src>/wayland/protocol/wayland.xml
 **   https://emersion.fr/blog/2020/wayland-clipboard-drag-and-drop/
 **
 ** In wlroots, the inter-client transfer mechanism are exposed in
 ** the wlr_seat object.
 **
 ** WLR data device manager events can be found here:
 **
 **   <wlroots_src>/include/wlr/types/wlr_data_device.h
 **
 ** Data device manager listeners are part of the WLR state.
 */


  // -------------------------------------
  // Data device manager listener routines

static
void
data_device_manager_listener_destroy(
    struct wl_listener *listener,
    void               *data
) {
    LOG_INFO("data_device_manager_listener_destroy.");

    return;
}


  // ----------------------------------
  // WLR data device manager initialize
  //
  // Since data_device_manager is a singleton, only one is created.

extern
error_t
wcf_wlr_data_device_manager(
    struct wlr_state *wlr
) {
    wlr->data_device_manager = (
        wlr_data_device_manager_create(wlr->wl_display)
    );

    if ( wlr->data_device_manager == NULL ) {
        LOG_ERROR("WLR data_device_manager create failed.");
        return ERROR;
    }

      // Specify listener routines for each data_device_manager event.
    wlr->destroy_data_device_manager.notify = data_device_manager_listener_destroy;

      // Register the data_device_manager listeners.
    wl_signal_add(&wlr->data_device_manager->events.destroy, &wlr->destroy_data_device_manager);

    LOG_INFO("Created WLR data_device_manager.");

    return OK;
}
