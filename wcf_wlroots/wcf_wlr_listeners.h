#ifndef WCF_WLR_LISTENERS_H
#define WCF_WLR_LISTENERS_H

 /*
 ** This file contains function prototypes for listener routines
 ** which are defined in one code file and are registered to a
 ** listener in another code file.  This occurs when new objects are
 ** produced by some factory.  See the discussion at the end of this
 ** file.
 */

struct wl_listener;

typedef
void
wlr_listener_routine_f(
    struct wl_listener *listener,
    void               *data
);

extern wlr_listener_routine_f wcf_wlr_backend_listener_new_input;
extern wlr_listener_routine_f wcf_wlr_backend_listener_new_output;

extern wlr_listener_routine_f wcf_wlr_xdg_shell_listener_new_surface;

extern wlr_listener_routine_f wcf_wlr_xdg_decoration_manager_v1_listener_new_toplevel_decoration;

extern wlr_listener_routine_f wcf_wlr_xwayland_listener_new_surface;


 /*
 ** Discussion:
 **
 ** Suppose some object, say xdg_surface, is produced by a factory,
 ** say xdg_shell.  The listener routine for new objects is
 ** registered in the code file for the factory.  However, code for
 ** the listener routine body is defined in the code file for the
 ** object.
 **
 ** Why not just define the new object listener routine in the code
 ** file for the factory?
 **
 ** Note that code for the new object will need to allocate memory.
 ** This memory will be free'd in the "object_destroy" listener
 ** that's part of the newly allocated memory.  The destroy routine
 ** belongs in the code file for the object.  Since it is good
 ** practice to keep alloc/free combinations within a single file,
 ** the body of the new object routine should also be in the code
 ** file for the object.
 */

#endif // WCF_WLR_LISTENERS_H
