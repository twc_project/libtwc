#ifndef WCF_POINTER_H
#define WCF_POINTER_H

  // Wayland
#include <wayland-server-core.h>  // wl_listener, wl_list

  // Opaque types
struct wlr_state;
struct wlr_input_device;
struct srv_pointer;

struct wcf_pointer {
    // Announced (indirectly) by wlr_backend listener new_input.

    // There may be many pointers.  A wlr_pointer has many listeners,
    // but no destroy listener.  TWC uses wlr_cursor instead of
    // wlr_pointer for mouse input, so no listeners are defined.

    struct wl_list           link;
    struct wlr_state        *wlr;
    struct wlr_input_device *wlr_input_device;

    // We use wlr_cursor as a simpler way to manage pointer events.
    // So it's not necessary to define pointer  listeners.
    //
    // struct wl_listener motion;
    // struct wl_listener button;
    // . . .

    struct srv_pointer *srv_pointer;
};

#endif // WCF_POINTER_H
