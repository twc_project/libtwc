
  // Protocols
#include <wm-agent-v1-client-protocol.h>

  // APIs
#include "wm_agent_pre-defined.h"  // Our API to ensure consistancy (server)
#include "wayland_pre-defined.h"
#include <twc/wm_agent_inert.h>

 /* -------------------
 ** Pre-defined Objects
 **
 ** Instead of a NULL pointer, we define special wm_agent_<object>s to
 ** serve as an indicator of a non-existent wm_agent_<object>.  They
 ** are defined with the name inert_wm_agent_<object>.  The listener
 ** routines for each inert_wm_agent_<object> are no-ops.  Therefore,
 ** code may safely dereference inert_wm_agent_<object>s and even
 ** invoke any listener routine.  There is no need to test for valid
 ** function pointers.  This simplifies the logic of many code
 ** sequences.
 */

 // ----------------------------
 // null_xx_wm_interactive_agent

struct xx_wm_interactive_agent null_xx_wm_interactive_agent = {
    .data = NULL,

    .xx_wm_interactive_agent_listener = {
        .dismiss       = inert_xx_wm_interactive_agent_event_dismiss,
        .annex_revoked = inert_xx_wm_interactive_agent_event_annex_revoked,
        .select        = inert_xx_wm_interactive_agent_event_select,
        .track         = inert_xx_wm_interactive_agent_event_track,
        .stop          = inert_xx_wm_interactive_agent_event_stop,
    },

    .proxy_surface = INERT_WL_SURFACE,
};

 // -------------------
 // null_xx_wm_menu_agent

struct xx_wm_menu_agent null_xx_wm_menu_agent = {
    .data = NULL,

    .xx_wm_menu_agent_listener = {
        .dismiss           = inert_xx_wm_menu_agent_event_dismiss,
        .annex_revoked     = inert_xx_wm_menu_agent_event_annex_revoked,
        .keyboard_shortcut = inert_xx_wm_menu_agent_event_keyboard_shortcut,
        .new_service       = inert_xx_wm_menu_agent_event_new_service,
    },

    .is_requested  = false,
    .proxy_surface = INERT_WL_SURFACE,
};

 // ------------------------
 // null_xx_wm_workspace_agent

struct xx_wm_workspace_agent null_xx_wm_workspace_agent = {
    .data = NULL,

    .xx_wm_workspace_agent_listener = {
        .dismiss               = inert_xx_wm_workspace_agent_event_dismiss,
        .configure_output_mode = inert_xx_wm_workspace_agent_event_configure_output_mode,
    },

    .is_requested = false,
};

 /* ---------------------------
 ** Special pre-defined objects
 **
 ** The "initial" version of a wm_agent_base_v1 object is used as a
 ** template during new object creation.
 */

 // ---------------------
 // initial_wm_agent_base

struct wm_agent_base_v1 initial_wm_agent_base_v1 = {
    .data = NULL,

    .wm_agent_base_v1_listener = {
        .new_agent_action = inert_wm_agent_base_v1_event_new_agent_action,
        .new_agent        = inert_wm_agent_base_v1_event_new_agent,
        .restart          = inert_wm_agent_base_v1_event_restart,
    },

    .xx_wm_window_registry.is_listener_set    = false,
    .xx_wm_workspace_registry.is_listener_set = false,
};
