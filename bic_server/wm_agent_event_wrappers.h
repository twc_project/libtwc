#ifndef WM_AGENT_EVENT_WRAPPERS_H
#define WM_AGENT_EVENT_WRAPPERS_H

  // System
#include <stdint.h>     // uint32_t

  // Protocols
#include <wm-agent-v1-client-protocol.h>

  // Opaque types
struct twc_window;
struct twc_workspace;
struct srv_output;

  // -----------------
  // xx_wm_interactive

extern
void
xx_wm_interactive_select(
    void
);

extern
void
xx_wm_interactive_track(
    void
);

extern
void
xx_wm_interactive_stop(
    void
);

  // ---------------------
  // xx_wm_window_registry

extern
void
xx_wm_window_new(
    struct twc_window *twc_window
);

  // ------------
  // xx_wm_window

extern
void
xx_wm_window_update_iconify_state(
    struct twc_window *twc_window
);

extern
void
xx_wm_window_update_display_state(
    struct twc_window                *twc_window,
    enum   xx_wm_window_display_state display_state
);

extern
void
xx_wm_window_update_app_id(
    struct twc_window *twc_window
);

extern
void
xx_wm_window_update_title(
    struct twc_window *twc_window
);

extern
void
xx_wm_window_update_location(
    struct twc_window *twc_window
);

extern
void
xx_wm_window_update_icon_location(
    struct twc_window *twc_window
);

extern
void
xx_wm_window_update_dimension(
    struct twc_window *twc_window
);

extern
void
xx_wm_window_update_icon_dimension(
    struct twc_window *twc_window
);

extern
void
xx_wm_window_update_occupancy(
    struct twc_window    *twc_window
);

extern
void
xx_wm_window_update_enter_output(
    struct twc_window *twc_window,
    struct srv_output *srv_output
);

extern
void
xx_wm_window_update_pointer_focus(
    struct twc_window        *twc_window,
    enum   xx_wm_window_focus focus
);

extern
void
xx_wm_window_update_keyboard_focus(
    struct twc_window        *twc_window,
    enum   xx_wm_window_focus focus
);

extern
void
xx_wm_window_update_selection_focus(
    struct twc_window        *twc_window,
    enum   xx_wm_window_focus focus
);

extern
void
xx_wm_window_thumbnail_max_size(
    struct twc_window *twc_window,
    uint32_t           width,
    uint32_t           height
);

extern
void
xx_wm_window_action(
    struct twc_window             *twc_window,
    enum wm_agent_base_v1_agent_id agent_id,
    uint32_t                       action_id
);

extern
void
xx_wm_window_expired(
    struct twc_window *twc_window
);

  // ------------------------
  // xx_wm_workspace_registry

extern
void
xx_wm_workspace_new(
    struct twc_workspace *twc_workspace
);

#endif  // WM_AGENT_EVENT_WRAPPERS_H
