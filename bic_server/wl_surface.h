#ifndef WL_SURFACE_H
#define wl_SURFACE_H

  // System
#include <stdlib.h>  // NULL

  // Wayland
#include <wayland-util.h>       // wl_fixed_t

  // APIs
#include "wcf_shim.h"
#include <twc/wayland_types.h>      // wld_coordinate_t

extern
bool
wl_simple_surface_at(
    struct wl_surface *wl_surface,
    wld_coordinate_t   ptr_loc_sx,      // int   format, surface coord
    wld_coordinate_t   ptr_loc_sy,
    wl_fixed_t        *ptr_loc_fix_sx,  // fixed format, surface coord
    wl_fixed_t        *ptr_loc_fix_sy
);

extern
nested_surface_t*
wl_surface_at(
    struct wl_surface *wl_surface,
    wld_coordinate_t  ptr_loc_sx,       // int   format, Surface coord
    wld_coordinate_t  ptr_loc_sy,
    wl_fixed_t       *ptr_loc_fix_sx,   // fixed format, Surface coord
    wl_fixed_t       *ptr_loc_fix_sy
);

#endif  // WL_SURFACE_H
