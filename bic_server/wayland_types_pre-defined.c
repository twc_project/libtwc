
  // Imported data types
#include <twc/wayland_types.h>

 // --------------------
 // invalid_wld_geometry

const wld_geometry_t invalid_wld_geometry = {
    WLD_INVALID_COORD,  WLD_INVALID_COORD,
    0,                  0
};
