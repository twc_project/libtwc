
  // System
#include <stdlib.h>  // calloc, free

  // TWC
#include <twc_window.h>
#include <twc_window-impl.h>
#include <twc/wayland_inert.h>
#include <twc_seat.h>
#include <twc/twc_log.h>

  // APIs
#include "external_buffer.h"     // Our API to ensure consistancy

  // Imported data types
#include "wayland_bic.h"

 /*
 ** This request is used exclusively by the wcf_client.
 **
 ** For the wcf_client, a buffer does not describe a chunk of shared
 ** memory.  Instead, a buffer represents a reference to a
 ** wcf_external_surface.  In the BIC Server, a wl_buffer has two
 ** variations - one for internal buffers used by BiCs and one for
 ** external surfaces used by the wcf_client.
 **
 ** When an external client creates and maps a surface, the wcf_client
 ** creates a buffer of the external variation.  The wcf_client then
 ** creates and attaches that buffer in the normal way to a built-in
 ** wl_surface.  This wl_surface corresponds to the external surface.
 **
 ** There is never any need for the wcf_client to interact with a
 ** shm_pool.  The actual image data is contained in the
 ** wcf_external_surface and is handled by the WCF.
 */

extern
struct wl_buffer*
external_buffer_create(
    struct wcf_external_surface *wcf_external_surface,
    enum   external_surface_type external_surface_type
) {
    enum buffer_type buffer_type;

    switch ( external_surface_type ) {
        case ET_XDG_SURFACE:
            buffer_type = BT_external;
        break;

        case ET_XWAYLAND_SURFACE:
            buffer_type = BT_xwayland;
        break;

        default:
            return NULL;
        break;

    }

    struct wl_buffer *wl_buffer = (
        calloc( 1, sizeof(struct wl_buffer) )
    );

    wl_buffer->data               = NULL;
    wl_buffer->wl_buffer_listener = inert_wl_buffer_listener;
    wl_buffer->wl_shm_pool        = NULL;

    wl_buffer->buffer_type = buffer_type;
    wl_buffer->width       = 0;
    wl_buffer->height      = 0;
    wl_buffer->stride      = 0;     // only valid for BT_internal
    wl_buffer->format      = 0;     // only valid for BT_internal

    wl_buffer->wcf_external_surface = wcf_external_surface;

    return wl_buffer;
}

extern
void
external_buffer_destroy(
    struct wl_buffer *wl_buffer
) {
    free(wl_buffer);

    return;
}

extern
void
external_buffer_xwayland_child_update_location(
    struct wl_surface *child_surface,
    struct wl_surface *top_level_surface,
    wld_coordinate_t   surface_x,
    wld_coordinate_t   surface_y
) {
    // Child windows are positioned relative to the root window of the
    // hierarchy.  If the top_level_surface is unknown, use the
    // focused surface as the root.

    if ( top_level_surface == NULL ) {

          // Child hierarchy in unknown.  Assume the the top_level is
          // the current surface.
        top_level_surface = (
            twc_seat_view_with_keyboard_focus()->view_surface
        );
    }

    struct twc_view *child_view     = child_surface    ->twc_view;
    struct twc_view *top_level_view = top_level_surface->twc_view;

    /*
    LOG_INFO("child_update_location %d %d %d %d",
        top_level_view->layout_x + top_level_view->view_surface_vx + surface_x,
        top_level_view->layout_y + top_level_view->view_surface_vy + surface_y,
        surface_x,
        surface_y
    );
    */

    twc_view_update_location(
        child_view,
        top_level_view->layout_x + top_level_view->view_surface_vx + surface_x,
        top_level_view->layout_y + top_level_view->view_surface_vy + surface_y
    );

    return;
}
