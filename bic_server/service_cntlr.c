
  // Wayland
#include <wayland-util.h>   // wl_list

  // APIs
#include "service_cntlr.h"  // Our API to ensure consistancy

  // Imported data types
#include "wm_service_bic.h"     // xx_zwm_service_controller
#include "server_state-impl.h"  // BIC_SERVER_unbound_controller_list

extern
void
unbound_controller_list_insert(
    struct xx_zwm_service_controller *xx_zwm_service_controller
) {
    //
    // When an xx_wm_desktop_service is created, it has an embedded
    // xx_zwm_service_controller.  The menu agent is notified of this
    // controller with the expectation that the menu agent will bind
    // the controller.
    //
    // This list is searched by numeric_name when the menu agent binds
    // a controller.
    //

    xx_zwm_service_controller->is_in_unbound_controller_list = true;

    wl_list_insert( BIC_SERVER_unbound_controller_list, &xx_zwm_service_controller->link );

    return;
}

extern
void
unbound_controller_list_remove(
    struct xx_zwm_service_controller *xx_zwm_service_controller
) {
    if ( xx_zwm_service_controller->is_in_unbound_controller_list ) {

        wl_list_remove( &xx_zwm_service_controller->link );

        xx_zwm_service_controller->is_in_unbound_controller_list = false;
    }

    return;
}
