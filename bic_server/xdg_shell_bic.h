#ifndef XDG_SHELL_BIC_H
#define XDG_SHELL_BIC_H

  // System
#include <stdbool.h>
#include <stdint.h>     // uint32_t

  // Protocols
#include <xdg-shell-client-protocol.h>

  // Imported data types
#include <twc/wayland_types.h>      // wld_coordinate_t
#include "xdg_decoration_bic.h"

 // -----------------------------------------
 // xdg_shell interfaces for Built-in Clients
 //
 //   Since Built-in Clients have modest needs, the bic_server does
 //   not support every interface object.

struct xdg_wm_base {
    // A xdg_wm_base has a ping event
};

#define SAME_XDG_GEOMETRY(g1, g2) ( \
    g1.x      == g2.x      && \
    g1.y      == g2.y      && \
    g1.width  == g2.width  && \
    g1.height == g2.height    \
)

#define IS_XDG_ROLE(t) (    \
    t == ST_XDG_TOPLEVEL || \
    t == ST_XDG_POPUP       \
)

struct xdg_surface {
    void                       *data;
    struct xdg_surface_listener xdg_surface_listener;
    // ping/pong

    bool has_initial_commit;
    bool ack_configure_outstanding;

      // xdg_surface.set_winoow_geometry
    wld_geometry_t current_enclosure_geometry,
                   pending_enclosure_geometry;

      // xdg-shell roles can be reinstated after being destroyed.  At
      // most one can be true.
    bool is_toplevel;   // permanent once set
    bool is_popup;      // permanent once set

    uint32_t configure_serial;
};

#define  LOWEST_XDG_TOPLEVEL_STATE XDG_TOPLEVEL_STATE_MAXIMIZED
#define HIGHEST_XDG_TOPLEVEL_STATE XDG_TOPLEVEL_STATE_SUSPENDED
  // State values start at 1
#define XDG_TOPLEVEL_STATE_RANGE   (HIGHEST_XDG_TOPLEVEL_STATE + 1)

struct xdg_toplevel {
    void                        *data;
    struct xdg_toplevel_listener xdg_toplevel_listener;

      // Size properties which a client can request.  Zero means
      // unrestricted.
    wld_dimension_t maxsize_width,
                    maxsize_height,
                    minsize_width,
                    minsize_height,
                    pending_maxsize_width,
                    pending_maxsize_heigh,
                    pending_minsize_width,
                    pending_minsize_height;

      // Status of xdg_toplevel_states.
      // Indexed by enum xdg_toplevel_state.
    bool xdg_toplevel_states[XDG_TOPLEVEL_STATE_RANGE];

      // These are the xdg_toplevel_states which a client can request.
    bool pending_state_maximized;
    bool pending_state_minimized;
    bool pending_state_fullscreen;

      // Supplied in an xdg_toplevel.move or xdg_toplevel.resize
      // request.  The client sends their serial from the
      // pointer/keyboard/touch event which triggers the move or
      // resize.
    uint32_t user_event_serial;

    struct zxdg_toplevel_decoration_v1 xdg_toplevel_decoration;
};

struct xdg_popup {
    void                     *data;
    struct xdg_popup_listener xdg_popup_listener;
};

#endif  // XDG_SHELL_BIC_H
