
  // System
#include <stdint.h>     // uint32_t
#include <stdio.h>      // size_t, sizeof, NULL
#include <stdlib.h>     // calloc, free
#include <string.h>     // strncpy

  // Wayland
#include <wayland-util.h>  // wl_container_of, wl_list, wl_fixed_t, struct wl_interface

  // Protocols
#include <wm-agent-v1-client-protocol.h>       // Our API to ensure consistancy
#include <desktop-cues-v1-client-protocol.h>

  // TWC
#include <twc/wayland_types.h>      // ADD_LISTENER_SUCCESS
#include <twc/wm_agent_inert.h>
#include <twc/twc_log.h>

  // APIs
#include "wm_agent_event.h"
#include "desktop_cues_event.h"
#include "wm_service_event.h"
#include "wm_agent_pre-defined.h"
#include "wayland_pre-defined.h"
#include "twc_window.h"
#include "twc_seat.h"
#include "twc_service.h"
#include "wayland_event.h"
#include "twc_op.h"
#include "srv_output.h"
#include "wm_agent_event_wrappers.h"   // xx_wm_window_wl_output_at()
#include "service_cntlr.h"

  // Imported data types
#include "bic_connection.h"
#include "wayland_bic.h"
#include "wm_agent_bic.h"
#include "wm_service_bic.h"
#include "twc_window-impl.h"
#include "twc_workspace-impl.h"
#include "twc_state-impl.h"
#include "server_state-impl.h"

  // Record some agents as they're granted.  Until then, these values
  // are harmless.
struct xx_wm_interactive_agent *granted_xx_wm_interactive_agent = NULL_XX_WM_INTERACTIVE_AGENT;
struct xx_wm_menu_agent        *granted_xx_wm_menu_agent        = NULL_XX_WM_MENU_AGENT;
struct xx_wm_workspace_agent   *granted_xx_wm_workspace_agent   = NULL_XX_WM_WORKSPACE_AGENT;

static bool default_surface_annexed = false;
static bool overlay_surface_annexed = false;

static bool  interactive_agent_granted = false;
static bool decoration_stylist_granted = false;
static bool       icon_stylist_granted = false;
static bool    workspace_agent_granted = false;
static bool         task_agent_granted = false;
static bool         menu_agent_granted = false;

 // --------------
 // wm_agent_base

extern
void
wm_agent_base_v1_destroy(
    struct wm_agent_base_v1 *wm_agent_base_v1
) {
    // Fix: ToDo

    return;
}

extern
void
wm_agent_base_v1_seek_agency(
    struct wm_agent_base_v1         *wm_agent_base_v1,
    enum   wm_agent_base_v1_agent_id agent_id
) {
    /*
    ** Each agent object is an exclusive singleton and may only be
    ** bound once..  Note that a client may seek an agency, but then
    ** fail to bind the object.  Therefore there is a test to prevent
    ** a client from requesting the same agency twice.
    */

    struct bic_connection *bic_connection = (
        wl_container_of(wm_agent_base_v1, bic_connection, wm_agent_base_v1)
    );

    const struct wl_interface *wl_interface;

    switch (agent_id) {

        case WM_AGENT_BASE_V1_AGENT_ID_INTERACTIVE:

            if ( interactive_agent_granted ) { return; }

            struct xx_wm_interactive_agent *xx_wm_interactive_agent = (
                &bic_connection->wm_agent_base_v1.xx_wm_interactive_agent
            );

            if ( xx_wm_interactive_agent->is_requested ) { /* Protocol Error */ return; }

            xx_wm_interactive_agent->is_requested = true;

            wl_interface = &xx_wm_interactive_agent_interface;

        break;

        case WM_AGENT_BASE_V1_AGENT_ID_MENU:

            if ( menu_agent_granted ) { return; }

            struct xx_wm_menu_agent *xx_wm_menu_agent = (
                &bic_connection->wm_agent_base_v1.xx_wm_menu_agent
            );

            if ( xx_wm_menu_agent->is_requested ) { /* Protocol Error */ return; }

            xx_wm_menu_agent->is_requested = true;

            wl_interface = &xx_wm_menu_agent_interface;

        break;

        case WM_AGENT_BASE_V1_AGENT_ID_DECORATION:

            if ( decoration_stylist_granted ) { return; }

            struct xx_wm_decoration_stylist *xx_wm_decoration_stylist = (
                &bic_connection->wm_agent_base_v1.xx_wm_decoration_stylist
            );

            if ( xx_wm_decoration_stylist->is_requested ) { /* Protocol Error */ return; }

            xx_wm_decoration_stylist->is_requested = true;

            wl_interface = &xx_wm_decoration_stylist_interface;

        break;

        case WM_AGENT_BASE_V1_AGENT_ID_ICON:

            if ( icon_stylist_granted ) { return; }

            struct xx_wm_icon_stylist *xx_wm_icon_stylist = (
                &bic_connection->wm_agent_base_v1.xx_wm_icon_stylist
            );

            if ( xx_wm_icon_stylist->is_requested ) { /* Protocol Error */ return; }

            xx_wm_icon_stylist->is_requested = true;

            wl_interface = &xx_wm_icon_stylist_interface;

        break;

        case WM_AGENT_BASE_V1_AGENT_ID_WORKSPACE:

            if ( workspace_agent_granted ) { return; }

            struct xx_wm_workspace_agent *xx_wm_workspace_agent = (
                &bic_connection->wm_agent_base_v1.xx_wm_workspace_agent
            );

            if ( xx_wm_workspace_agent->is_requested ) { /* Protocol Error */ return; }

            xx_wm_workspace_agent->is_requested = true;

            wl_interface = &xx_wm_workspace_agent_interface;

        break;

        case WM_AGENT_BASE_V1_AGENT_ID_TASK:

            if ( task_agent_granted ) { return; }

            struct xx_wm_task_agent *xx_wm_task_agent = (
                &bic_connection->wm_agent_base_v1.xx_wm_task_agent
            );

            if ( xx_wm_task_agent->is_requested ) { /* Protocol Error */ return; }

            xx_wm_task_agent->is_requested = true;

            wl_interface = &xx_wm_task_agent_interface;

        break;

        default:
            return;
        break;
    }

    ADD_AGENT(bic_connection, agent_id);

    wm_agent_base_v1_emit_new_agent(
        wm_agent_base_v1,
        wl_interface
    );

    return;
}

extern
void*
wm_agent_base_v1_bind(
    struct wm_agent_base_v1   *wm_agent_base_v1,
    uint32_t                   name,
    const struct wl_interface *wl_interface,
    uint32_t                   version
) {
    void *new_id = NULL;

    struct bic_connection *bic_connection = (
        wl_container_of(wm_agent_base_v1, bic_connection, wm_agent_base_v1)
    );

      // Non-agents may not bind an agent object
    if (  IS_NOT_AGENT(bic_connection) ) { /* Protocol Error */ return NULL; }

    enum wm_agent_base_v1_agent_id agent_id = WM_AGENT_BASE_V1_AGENT_ID_NONE;

    if ( strcmp(wl_interface->name, xx_wm_interactive_agent_interface.name) == 0 ) {
        // xx_wm_interactive_agent
        struct xx_wm_interactive_agent *xx_wm_interactive_agent = (
            &bic_connection->wm_agent_base_v1.xx_wm_interactive_agent
        );

        if ( interactive_agent_granted ) { /* Protocol Error */ return NULL; }

        granted_xx_wm_interactive_agent = xx_wm_interactive_agent;

        interactive_agent_granted = true;

        new_id = xx_wm_interactive_agent;

        agent_id = WM_AGENT_BASE_V1_AGENT_ID_INTERACTIVE;

    } else if ( strcmp(wl_interface->name, xx_wm_menu_agent_interface.name) == 0 ) {
        // xx_wm_menu_agent
        struct xx_wm_menu_agent *xx_wm_menu_agent = (
            &bic_connection->wm_agent_base_v1.xx_wm_menu_agent
        );

        if ( menu_agent_granted ) { /* Protocol Error */ return NULL; }

        xx_wm_menu_agent->is_listener_set = false;

        granted_xx_wm_menu_agent = xx_wm_menu_agent;

        menu_agent_granted = true;

        new_id = xx_wm_menu_agent;

        agent_id = WM_AGENT_BASE_V1_AGENT_ID_MENU;

    } else if ( strcmp(wl_interface->name, xx_wm_decoration_stylist_interface.name) == 0 ) {
        // xx_wm_decoration_stylist
        struct xx_wm_decoration_stylist *xx_wm_decoration_stylist = (
             &bic_connection->wm_agent_base_v1.xx_wm_decoration_stylist
        );

        if ( decoration_stylist_granted ) { /* Protocol Error */ return NULL; }

        decoration_stylist_granted = true;

        new_id = xx_wm_decoration_stylist;

        agent_id = WM_AGENT_BASE_V1_AGENT_ID_DECORATION;

    } else if ( strcmp(wl_interface->name, xx_wm_icon_stylist_interface.name) == 0 ) {
        // xx_wm_icon_stylist
        struct xx_wm_icon_stylist *xx_wm_icon_stylist = (
             &bic_connection->wm_agent_base_v1.xx_wm_icon_stylist
        );

        if ( icon_stylist_granted ) { /* Protocol Error */ return NULL; }

        icon_stylist_granted = true;

        new_id = xx_wm_icon_stylist;

        agent_id = WM_AGENT_BASE_V1_AGENT_ID_ICON;

     } else if ( strcmp(wl_interface->name, xx_wm_workspace_agent_interface.name) == 0 ) {
        // xx_wm_workspace_agent
        struct xx_wm_workspace_agent *xx_wm_workspace_agent = (
            &bic_connection->wm_agent_base_v1.xx_wm_workspace_agent
        );

        if ( workspace_agent_granted ) { /* Protocol Error */ return NULL; }

        granted_xx_wm_workspace_agent = xx_wm_workspace_agent;

        workspace_agent_granted = true;

        new_id = xx_wm_workspace_agent;

        agent_id = WM_AGENT_BASE_V1_AGENT_ID_WORKSPACE;

    } else if ( strcmp(wl_interface->name, xx_wm_task_agent_interface.name) == 0 ) {
        // xx_wm_task_agent
        struct xx_wm_task_agent *xx_wm_task_agent = (
            &bic_connection->wm_agent_base_v1.xx_wm_task_agent
        );

        if ( task_agent_granted ) { /* Protocol Error */ return NULL; }

        task_agent_granted = true;

        new_id = xx_wm_task_agent;

        agent_id = WM_AGENT_BASE_V1_AGENT_ID_TASK;
    }

    //LOG_INFO("wm_agent_base_v1_bind success");

    bool new_agent = IS_NOT_AGENT( bic_connection );

    ADD_AGENT(bic_connection, agent_id);

    if ( new_agent ) {

        struct agent_action *agent_action;
        twc_agent_action_list_for_each( agent_action ) {
            wm_agent_base_v1->wm_agent_base_v1_listener.new_agent_action(
                wm_agent_base_v1->data,
                wm_agent_base_v1,
                agent_action->agent_id,
                agent_action->name,
                agent_action->action_id
            );
        }
    }

    return new_id;
}

extern
struct xx_wm_window_registry*
wm_agent_base_v1_get_window_registry(
    struct wm_agent_base_v1 *wm_agent_base_v1
) {
    struct bic_connection *bic_connection = (
        wl_container_of(wm_agent_base_v1, bic_connection, wm_agent_base_v1)
    );

    if ( IS_NOT_AGENT(bic_connection) ) { /* Protocol Error */ return NULL; }

    struct xx_wm_window_registry *xx_wm_window_registry = (
        &wm_agent_base_v1->xx_wm_window_registry
    );

    xx_wm_window_registry->xx_wm_window_registry_listener = inert_xx_wm_window_registry_listener;
    xx_wm_window_registry->is_listener_set = false;

    return xx_wm_window_registry;
}

extern
struct xx_wm_workspace_registry*
wm_agent_base_v1_get_workspace_registry(
    struct wm_agent_base_v1 *wm_agent_base_v1
) {
    struct bic_connection *bic_connection = (
        wl_container_of(wm_agent_base_v1, bic_connection, wm_agent_base_v1)
    );

    if ( IS_NOT_AGENT(bic_connection) ) { /* Protocol Error */ return NULL; }

    struct xx_wm_workspace_registry *xx_wm_workspace_registry = (
        &wm_agent_base_v1->xx_wm_workspace_registry
    );

    xx_wm_workspace_registry->xx_wm_workspace_registry_listener = inert_xx_wm_workspace_registry_listener;
    xx_wm_workspace_registry->is_listener_set = false;

    return xx_wm_workspace_registry;
}

extern
void
wm_agent_base_v1_register_agent_action(
    struct wm_agent_base_v1       *wm_agent_base_v1,
    enum wm_agent_base_v1_agent_id agent_id,
    const  char                   *name,
    uint32_t                       action_id,
    struct xx_zwm_service         *xx_zwm_service
) {
    struct bic_connection *bic_connection = (
        wl_container_of(wm_agent_base_v1, bic_connection, wm_agent_base_v1)
    );

    if ( ! IS_AGENT_THIS(bic_connection, agent_id) ) { return; }

    struct agent_action *agent_action = (
        calloc( 1, sizeof(struct agent_action) )
    );

    strncpy(agent_action->name, name, MAX_AGENT_ACTION_NAME_LENGTH);
    agent_action->name[MAX_AGENT_ACTION_NAME_LENGTH - 1] = '\0';

    agent_action-> agent_id      =  agent_id;
    agent_action->action_id      = action_id;
    agent_action->xx_zwm_service = xx_zwm_service;

    wl_list_insert( BIC_SERVER_agent_action_list, &agent_action->link );

    wm_agent_base_v1_emit_new_agent_action(
        agent_id,
        name,
        action_id
    );

    return;

    // Fix: When an agent resigns, free all its' agent actions.
}

extern
int
wm_agent_base_v1_add_listener(
    struct wm_agent_base_v1                *wm_agent_base_v1,
    const struct wm_agent_base_v1_listener *wm_agent_base_v1_listener,
    void                                   *data
) {
    wm_agent_base_v1->data                      = data;
    wm_agent_base_v1->wm_agent_base_v1_listener = *wm_agent_base_v1_listener;

    // The client depends on the listener to be granted and agency.
    // When binding its first agency, the existing agent_actions will
    // be emitted.

    return ADD_LISTENER_SUCCESS;
}

 // ---------------------
 // xx_wm_interactive_agent

extern
void
xx_wm_interactive_agent_resign(
    struct xx_wm_interactive_agent *xx_wm_interactive_agent
) {
    if ( overlay_surface_annexed ) {
        // Protocol Error: the overlay surface must be released prior
        // to this request.
        return;
    }

    granted_xx_wm_interactive_agent = NULL_XX_WM_INTERACTIVE_AGENT;

    interactive_agent_granted = false;

    return;
}

extern
struct wl_surface*
xx_wm_interactive_agent_annex_overlay_surface(
    struct xx_wm_interactive_agent *xx_wm_interactive_agent
) {
    //LOG_INFO("xx_wm_interactive_agent_annex_overlay_surface");

    // 1) The OVERLAY_WL_SURFACE is assigned to the client "as-if" the
    //    client had created it.
    //
    // 2) Since the OVERLAY_WL_SURFACE is a global resource, a new
    //    client-specific wl_surface is created to serve as a proxy
    //    for the client.  The proxy surface does not have a role.
    //    Therefore, the proxy is not eligible to be on the view list.
    //    So, the proxy will never become the surface with pointer or
    //    with keyboard focus.  For events involving the
    //    OVERLAY_WL_SURFACE, the proxy is used instead.  See
    //    wayland_event.c
    //
    //  From the client perspective, the proxy surface IS the overlay
    //  surface.  It is already created, attached and committed.  No
    //  requests, other than destroy, are required of the client.  If
    //  the client does make requests, they will be performed on the
    //  proxy.  But, since it does not have a role, there will be no
    //  noticeable effect.  The state of the global OVERLAY_WL_SURFACE
    //  will not be affected.

    // Fix: is this test needed???
    if ( granted_xx_wm_interactive_agent != xx_wm_interactive_agent ) {
        // Protocol Error: Can't annex without a proper claim
        return NULL;
    }

    // Fix: must throw an error with an event.  wl_display::error??
    if ( overlay_surface_annexed ) {
        // Protocol Error: Can't annex twice
        return NULL;
    }

    overlay_surface_annexed = true;

    struct wm_agent_base_v1 *wm_agent_base_v1 = (
        wl_container_of(xx_wm_interactive_agent, wm_agent_base_v1, xx_wm_interactive_agent)
    );

    struct bic_connection *bic_connection = (
        wl_container_of(wm_agent_base_v1, bic_connection, wm_agent_base_v1)
    );

    OVERLAY_WL_SURFACE->bic_connection = bic_connection;

    struct wl_surface *proxy_wl_surface = (
        wl_compositor_create_surface(
            &bic_connection->wl_compositor
        )
    );

    proxy_wl_surface->type = ST_PROXY;
      // Record the client's proxy_wl_surface.
    xx_wm_interactive_agent->proxy_surface = proxy_wl_surface;

    twc_op_overlay_surface_annexed(true);

    return proxy_wl_surface;
}

extern
void
xx_wm_interactive_agent_release (
    struct xx_wm_interactive_agent *xx_wm_interactive_agent
) {
    overlay_surface_annexed = false;

    twc_op_overlay_surface_annexed(false);

    OVERLAY_WL_SURFACE->bic_connection = INERT_BIC_CONNECTION;

    wl_surface_destroy(xx_wm_interactive_agent->proxy_surface);

    return;
}

extern
void
xx_wm_interactive_agent_select_at(
    struct xx_wm_interactive_agent *xx_wm_interactive_agent,
    wl_fixed_t                      layout_x,
    wl_fixed_t                      layout_y
) {
    twc_op_select_at(
        layout_x,
        layout_y
    );

    return;
}

extern
void
xx_wm_interactive_agent_tracking_at(
    struct xx_wm_interactive_agent *xx_wm_interactive_agent,
    wl_fixed_t                      layout_x,
    wl_fixed_t                      layout_y
) {
    twc_op_tracking_at(
        layout_x,
        layout_y
    );

    return;
}

extern
void
xx_wm_interactive_agent_drop(
    struct xx_wm_interactive_agent *xx_wm_interactive_agent
) {
    twc_op_drop();

    return;
}

extern
int
xx_wm_interactive_agent_add_listener(
    struct xx_wm_interactive_agent                *xx_wm_interactive_agent,
    const struct xx_wm_interactive_agent_listener *xx_wm_interactive_agent_listener,
    void                                          *data
) {
    xx_wm_interactive_agent->data                             = data;
    xx_wm_interactive_agent->xx_wm_interactive_agent_listener = *xx_wm_interactive_agent_listener;

    return ADD_LISTENER_SUCCESS;
}

 // ----------------------
 // xx_wm_decoration_stylist

extern
void
xx_wm_decoration_stylist_resign(
    struct xx_wm_decoration_stylist *xx_wm_decoration_stylist
) {
    // Fix: ToDo
    return;
}

extern
struct xx_wm_decoration*
xx_wm_decoration_stylist_get_decoration(
    struct xx_wm_decoration_stylist *xx_wm_decoration_stylist,
    enum   xx_wm_decoration_for      xx_wm_decoration_for,
    struct xx_wm_window             *xx_wm_window
) {
    struct xx_wm_decoration *xx_wm_decoration = (
        xx_wm_decoration_for == XX_WM_DECORATION_FOR_CLIENT
        ? &xx_wm_window->twc_window->client_view->xx_wm_decoration
        : &xx_wm_window->twc_window->  icon_view->xx_wm_decoration
    );

    if ( xx_wm_decoration->is_active ) {
        // Protocol error
        return NULL;
    }
    xx_wm_decoration->is_active = true;

    xx_wm_decoration->xx_wm_decoration_for = xx_wm_decoration_for;

    xx_wm_decoration->top_surface = INERT_WL_SURFACE;
    xx_wm_decoration->top_sx      = 0;
    xx_wm_decoration->top_sy      = 0;

    xx_wm_decoration->bottom_surface = INERT_WL_SURFACE;
    xx_wm_decoration->bottom_sx      = 0;
    xx_wm_decoration->bottom_sy      = 0;

    xx_wm_decoration->left_surface = INERT_WL_SURFACE;
    xx_wm_decoration->left_sx      = 0;
    xx_wm_decoration->left_sy      = 0;

    xx_wm_decoration->right_surface = INERT_WL_SURFACE;
    xx_wm_decoration->right_sx      = 0;
    xx_wm_decoration->right_sy      = 0;

    xx_wm_decoration->is_top_pending    = false;
    xx_wm_decoration->is_bottom_pending = false;
    xx_wm_decoration->is_left_pending   = false;
    xx_wm_decoration->is_right_pending  = false;

    xx_wm_decoration->is_top_offset_pending    = false;
    xx_wm_decoration->is_bottom_offset_pending = false;
    xx_wm_decoration->is_left_offset_pending   = false;
    xx_wm_decoration->is_right_offset_pending  = false;

    xx_wm_decoration->pending_top_surface = INERT_WL_SURFACE;
    xx_wm_decoration->pending_top_sx      = 0;
    xx_wm_decoration->pending_top_sy      = 0;

    xx_wm_decoration->pending_bottom_surface = INERT_WL_SURFACE;
    xx_wm_decoration->pending_bottom_sx      = 0;
    xx_wm_decoration->pending_bottom_sy      = 0;

    xx_wm_decoration->pending_left_surface = INERT_WL_SURFACE;
    xx_wm_decoration->pending_left_sx      = 0;
    xx_wm_decoration->pending_left_sy      = 0;

    xx_wm_decoration->pending_right_surface = INERT_WL_SURFACE;
    xx_wm_decoration->pending_right_sx      = 0;
    xx_wm_decoration->pending_right_sy      = 0;

    xx_wm_decoration->has_auto_relocate     = false;

    struct wm_agent_base_v1 *wm_agent_base_v1 = (
        wl_container_of(xx_wm_decoration_stylist, wm_agent_base_v1, xx_wm_decoration_stylist)
    );

    struct bic_connection *bic_connection = (
        wl_container_of(wm_agent_base_v1, bic_connection, wm_agent_base_v1)
    );

    xx_wm_decoration->bic_connection = bic_connection;

    return xx_wm_decoration;
}

extern
void
xx_wm_decoration_stylist_declare_cueing(
    struct xx_wm_decoration_stylist             *xx_wm_decoration_stylist,
    enum   xx_wm_decoration_stylist_cueing_state cueing_state
) {
    struct wm_agent_base_v1 *wm_agent_base_v1 = (
        wl_container_of(xx_wm_decoration_stylist, wm_agent_base_v1, xx_wm_decoration_stylist)
    );

    enum xx_wm_decoration_stylist_cueing_state current_state = (
        wm_agent_base_v1->cueing_state
    );

    if ( cueing_state == current_state ) { return; }

      // Record new cueing state
    wm_agent_base_v1->cueing_state = cueing_state;

    switch (cueing_state) {
        case XX_WM_DECORATION_STYLIST_CUEING_STATE_ENABLED:
              // Announce the xx_desktop_cues_base_v1 singleton
            wl_registry_global(
               &xx_desktop_cues_base_v1_interface,
                XX_DESKTOP_CUES_BASE_V1_NUMERIC_NAME
            );
        break;

        case XX_WM_DECORATION_STYLIST_CUEING_STATE_DISABLED:

            struct bic_connection *bic_connection = (
                wl_container_of(wm_agent_base_v1, bic_connection, wm_agent_base_v1)
            );

            xx_desktop_cues_base_v1_event_revoked(
                &bic_connection->xx_desktop_cues_base_v1
            );

        break;
    }

    return;
}

extern
int
xx_wm_decoration_stylist_add_listener(
    struct xx_wm_decoration_stylist                *xx_wm_decoration_stylist,
    const struct xx_wm_decoration_stylist_listener *xx_wm_decoration_stylist_listener,
    void                                         *data
) {
    xx_wm_decoration_stylist->data = data;

    xx_wm_decoration_stylist->xx_wm_decoration_stylist_listener = (
        *xx_wm_decoration_stylist_listener
    );

    return ADD_LISTENER_SUCCESS;
}

 // ----------------
 // xx_wm_decoration

extern
void
xx_wm_decoration_destroy(
    struct xx_wm_decoration *xx_wm_decoration
) {
    struct twc_view *twc_view = (
        wl_container_of(xx_wm_decoration, twc_view, xx_wm_decoration)
    );

    if ( twc_view->xx_wm_decoration.is_active == false ) { return; }

    // Assume Decoration agent didn't commit NULL surfaces before
    // calling destory.

    xx_wm_decoration_attach(xx_wm_decoration, NULL, XX_WM_DECORATION_EDGE_TOP   , 0, 0);
    xx_wm_decoration_attach(xx_wm_decoration, NULL, XX_WM_DECORATION_EDGE_BOTTOM, 0, 0);
    xx_wm_decoration_attach(xx_wm_decoration, NULL, XX_WM_DECORATION_EDGE_LEFT  , 0, 0);
    xx_wm_decoration_attach(xx_wm_decoration, NULL, XX_WM_DECORATION_EDGE_RIGHT , 0, 0);

    xx_wm_decoration_commit(xx_wm_decoration);

    twc_view->xx_wm_decoration.is_active = false;

    twc_view_recalc_geometry(twc_view);

    return;
}

extern
void
xx_wm_decoration_attach(
    struct xx_wm_decoration   *xx_wm_decoration,
    struct wl_surface         *wl_surface,
    enum xx_wm_decoration_edge xx_wm_decoration_edge,
    int32_t                    sx,
    int32_t                    sy
) {
    if ( wl_surface == NULL) {

        wl_surface = INERT_WL_SURFACE;

        sx = 0;
        sy = 0;

    } else {

        if ( wl_surface->type != ST_NONE       &&
             wl_surface->type != ST_XX_WM_DECO
        ) {
            // Protocol error

            return;;
        }

        wl_surface->type                  = ST_XX_WM_DECO;
        wl_surface->xx_wm_decoration_edge = xx_wm_decoration_edge;
    }

    switch (xx_wm_decoration_edge) {

        case XX_WM_DECORATION_EDGE_TOP:
            xx_wm_decoration->pending_top_surface   = wl_surface;
            xx_wm_decoration->pending_top_sx        = sx;
            xx_wm_decoration->pending_top_sy        = sy;
            xx_wm_decoration->is_top_pending        = true;
            xx_wm_decoration->is_top_offset_pending = true;
        break;

        case XX_WM_DECORATION_EDGE_BOTTOM:
            xx_wm_decoration->pending_bottom_surface   = wl_surface;
            xx_wm_decoration->pending_bottom_sx        = sx;
            xx_wm_decoration->pending_bottom_sy        = sy;
            xx_wm_decoration->is_bottom_pending        = true;
            xx_wm_decoration->is_bottom_offset_pending = true;
        break;

        case XX_WM_DECORATION_EDGE_LEFT:
            xx_wm_decoration->pending_left_surface   = wl_surface;
            xx_wm_decoration->pending_left_sx        = sx;
            xx_wm_decoration->pending_left_sy        = sy;
            xx_wm_decoration->is_left_pending        = true;
            xx_wm_decoration->is_left_offset_pending = true;
        break;

        case XX_WM_DECORATION_EDGE_RIGHT:
            xx_wm_decoration->pending_right_surface   = wl_surface;
            xx_wm_decoration->pending_right_sx        = sx;
            xx_wm_decoration->pending_right_sy        = sy;
            xx_wm_decoration->is_right_pending        = true;
            xx_wm_decoration->is_right_offset_pending = true;
        break;

        default:
            // Error!

            return;
        break;
    }

    return;
}

extern
void
xx_wm_decoration_offset(
    struct xx_wm_decoration   *xx_wm_decoration,
    enum xx_wm_decoration_edge xx_wm_decoration_edge,
    int32_t                    sx,
    int32_t                    sy
) {
    switch (xx_wm_decoration_edge) {

        case XX_WM_DECORATION_EDGE_TOP:
            xx_wm_decoration->pending_top_sx = sx;
            xx_wm_decoration->pending_top_sy = sy;
            xx_wm_decoration->is_top_offset_pending = true;
        break;

        case XX_WM_DECORATION_EDGE_BOTTOM:
            xx_wm_decoration->pending_bottom_sx = sx;
            xx_wm_decoration->pending_bottom_sy = sy;
            xx_wm_decoration->is_bottom_offset_pending = true;
        break;

        case XX_WM_DECORATION_EDGE_LEFT:
            xx_wm_decoration->pending_left_sx = sx;
            xx_wm_decoration->pending_left_sy = sy;
            xx_wm_decoration->is_left_offset_pending = true;
        break;

        case XX_WM_DECORATION_EDGE_RIGHT:
            xx_wm_decoration->pending_right_sx = sx;
            xx_wm_decoration->pending_right_sy = sy;
            xx_wm_decoration->is_right_offset_pending = true;
        break;

        default:
            // Error!

            return;
        break;
    }

    return;
}

extern
void
xx_wm_decoration_commit(
    struct xx_wm_decoration *xx_wm_decoration
) {
    twc_view_commit_decoration(xx_wm_decoration);

    if ( xx_wm_decoration->is_top_offset_pending ) {

        xx_wm_decoration->top_sx = xx_wm_decoration->pending_top_sx;
        xx_wm_decoration->top_sy = xx_wm_decoration->pending_top_sy;

        xx_wm_decoration->pending_top_sx = 0;
        xx_wm_decoration->pending_top_sy = 0;

        xx_wm_decoration->is_top_offset_pending = false;
    }

    if ( xx_wm_decoration->is_top_pending ) {

        xx_wm_decoration->top_surface = xx_wm_decoration->pending_top_surface;
        xx_wm_decoration->pending_top_surface = INERT_WL_SURFACE;

        xx_wm_decoration->is_top_pending = false;
    }

    if ( xx_wm_decoration->is_bottom_offset_pending ) {

        xx_wm_decoration->bottom_sx = xx_wm_decoration->pending_bottom_sx;
        xx_wm_decoration->bottom_sy = xx_wm_decoration->pending_bottom_sy;

        xx_wm_decoration->pending_bottom_sx = 0;
        xx_wm_decoration->pending_bottom_sy = 0;

        xx_wm_decoration->is_bottom_offset_pending = false;
    }

    if ( xx_wm_decoration->is_bottom_pending ) {

        xx_wm_decoration->bottom_surface = xx_wm_decoration->pending_bottom_surface;
        xx_wm_decoration->pending_bottom_surface = INERT_WL_SURFACE;

        xx_wm_decoration->is_bottom_pending = false;
    }

    if ( xx_wm_decoration->is_left_offset_pending ) {

        xx_wm_decoration->left_sx = xx_wm_decoration->pending_left_sx;
        xx_wm_decoration->left_sy = xx_wm_decoration->pending_left_sy;

        xx_wm_decoration->pending_left_sx = 0;
        xx_wm_decoration->pending_left_sy = 0;

        xx_wm_decoration->is_left_offset_pending = false;
    }

    if ( xx_wm_decoration->is_left_pending ) {

        xx_wm_decoration->left_surface = xx_wm_decoration->pending_left_surface;
        xx_wm_decoration->pending_left_surface = INERT_WL_SURFACE;

        xx_wm_decoration->is_left_pending = false;
    }

    if ( xx_wm_decoration->is_right_offset_pending ) {

        xx_wm_decoration->right_sx = xx_wm_decoration->pending_right_sx;
        xx_wm_decoration->right_sy = xx_wm_decoration->pending_right_sy;

        xx_wm_decoration->pending_right_sx = 0;
        xx_wm_decoration->pending_right_sy = 0;

        xx_wm_decoration->is_right_offset_pending = false;
    }

    if ( xx_wm_decoration->is_right_pending ) {

        xx_wm_decoration->right_surface = xx_wm_decoration->pending_right_surface;
        xx_wm_decoration->pending_right_surface = INERT_WL_SURFACE;

        xx_wm_decoration->is_right_pending = false;
    }

    struct twc_view *twc_view = (
        wl_container_of(xx_wm_decoration, twc_view, xx_wm_decoration)
    );

    twc_view_recalc_geometry(twc_view);

    return;
}

extern
void
xx_wm_decoration_auto_relocate(
    struct xx_wm_decoration *xx_wm_decoration
) {
    xx_wm_decoration->has_auto_relocate = true;

    return;
}

extern
void
xx_wm_decoration_set_titlebar(
    struct xx_wm_decoration   *xx_wm_decoration,
    enum xx_wm_decoration_edge xx_wm_decoration_edge
) {
    xx_wm_decoration->titlebar_surface = xx_wm_decoration_edge;

    return;
}

extern
struct xx_desktop_cue_controller*
xx_wm_decoration_bind_cue_controller(
    struct xx_wm_decoration *xx_wm_decoration,
    uint32_t                 name
) {
    struct xx_desktop_cue_controller *xx_desktop_cue_controller = NULL;

    struct xx_desktop_cue *xx_desktop_cue;
    wl_list_for_each(      xx_desktop_cue, &xx_wm_decoration->desktop_cue_list, link ) {

        if ( xx_desktop_cue->numeric_name == name ) {

            xx_desktop_cue_controller = &xx_desktop_cue->xx_desktop_cue_controller;

            xx_desktop_cue_controller->xx_desktop_cue_controller_listener = (
                inert_xx_desktop_cue_controller_listener
            );

            break;
        }
    }

    return xx_desktop_cue_controller;
}

extern
int
xx_wm_decoration_add_listener(
    struct xx_wm_decoration                *xx_wm_decoration,
    const struct xx_wm_decoration_listener *xx_wm_decoration_listener,
    void                                   *data
) {
    return ADD_LISTENER_SUCCESS;
}

 // -------------------------
 // xx_desktop_cue_controller
 //

extern
void
xx_desktop_cue_controller_destroy(
    struct xx_desktop_cue_controller *xx_desktop_cue_controller
) {
    // Fix: A xx_desktop_cue_controller is intergated into a xx_desktop_cue_controller, so
    // nothing to free.

    return;
}

extern
void
xx_desktop_cue_controller_send(
    struct xx_desktop_cue_controller *xx_desktop_cue_controller
) {
    struct xx_desktop_cue *xx_desktop_cue = (
        wl_container_of(xx_desktop_cue_controller, xx_desktop_cue, xx_desktop_cue_controller)
    );

    xx_desktop_cue_event_take(
        xx_desktop_cue
    );

    return;
}

 // ------------------
 // xx_wm_icon_stylist

extern
void
xx_wm_icon_stylist_resign(
    struct xx_wm_icon_stylist *xx_wm_icon_stylist
) {
    // Fix: ToDo

    return;
}

extern
struct xx_wm_icon*
xx_wm_icon_stylist_get_icon(
    struct xx_wm_icon_stylist *xx_wm_icon_stylist,
    struct wl_surface         *wl_surface
) {
    if ( wl_surface->type != ST_NONE     &&
         wl_surface->type != ST_XX_WM_ICON
    ) {
        // Protocol error
        return NULL;
    }

    // Initialize wl_surface->xx_wm_icon here.
    // It is currently an emtpy struct.

    wl_surface->type = ST_XX_WM_ICON;

    struct xx_wm_icon *xx_wm_icon = (
        &wl_surface->xx_wm_icon
    );

    xx_wm_icon->data = NULL;
    xx_wm_icon->xx_wm_icon_listener = inert_xx_wm_icon_listener;

    return xx_wm_icon;
}

extern
void
xx_wm_icon_stylist_set_preferred_size(
    struct xx_wm_icon_stylist *xx_wm_icon_stylist,
    int32_t                    size
) {
    // Fix: ToDo

    return;
}

extern
int
xx_wm_icon_stylist_add_listener(
    struct xx_wm_icon_stylist                *xx_wm_icon_stylist,
    const struct xx_wm_icon_stylist_listener *xx_wm_icon_stylist_listener,
    void                                     *data
) {
    return ADD_LISTENER_SUCCESS;
}

 // ----------
 // xx_wm_icon

extern
void
xx_wm_icon_destroy(
    struct xx_wm_icon *xx_wm_icon
) {
    struct wl_surface *wl_surface = (
        wl_container_of(xx_wm_icon, wl_surface, xx_wm_icon)
    );

    // A wl_surface role cannot be removed without destroying the
    // wl_surface.

    return;
}

static
void
helper_attach_icon(
    struct twc_view   *icon_view,
    struct wl_surface *wl_surface
) {
    // A pre-requisite condition is (wl_surface != INERT_WL_SURFACE)

      // Mutually link the icon_view and the wl_surface
    wl_surface->twc_view    = icon_view;
    icon_view->view_surface = wl_surface;

    icon_view->width  = wl_surface->width;
    icon_view->height = wl_surface->height;

    return;
}

static
void
helper_detach_icon(
    struct twc_view   *icon_view,
    struct wl_surface *wl_surface
) {
    // A pre-requisite condition is (wl_surface != INERT_WL_SURFACE)

      // The surface and the view are no longer mutually linked.
    wl_surface->twc_view                    = NULL_TWC_VIEW;
    icon_view->xx_wm_decoration.top_surface = INERT_WL_SURFACE;

    return;
}

extern
void
xx_wm_icon_for_window(
    struct xx_wm_icon   *new_xx_wm_icon,
    struct xx_wm_window *xx_wm_window
) {
    // Attach an icon to a window.  There may be an old icon or not.
    // The new icon may be NULL or not.  This makes 4 possible cases.
    // They are mutually exclusive.  Only one will fire.

    bool               changes_made     = true;
    struct twc_view   *icon_view        = xx_wm_window->twc_window->icon_view;
    struct wl_surface *old_icon_surface = icon_view->view_surface;

    // The first two conditionals involve new_xx_wm_icon == NULL (*).

    if ( ( new_xx_wm_icon   == NULL             ) &&
         ( old_icon_surface == INERT_WL_SURFACE )
    ) {
        // case 1.  Both the new and old icons are NULL.
        return;
    }

    if ( ( new_xx_wm_icon   == NULL             ) &&
         ( old_icon_surface != INERT_WL_SURFACE )
    ) {
        // case 2.  New icon is NULL, old icon is non-NULL.
        helper_detach_icon(icon_view, old_icon_surface);

    } else {

        // (*) At this point, new_xx_wm_icon cannot be NULL.

            struct wl_surface *new_icon_surface = (
            wl_container_of(new_xx_wm_icon, new_icon_surface, xx_wm_icon)
        );

        if ( old_icon_surface == INERT_WL_SURFACE ) {
            // case 3.  New icon is non-NUll, old icon is NULL.
            helper_attach_icon(icon_view, new_icon_surface);

        } else {
            // case 4.  Both new and old icons are non-NULL.
            if ( new_icon_surface->type == ST_XX_WM_ICON ) {

                helper_attach_icon(icon_view, new_icon_surface);

            } else {
                // new surface is not an icon surface, so it is
                // effectively NULL
                changes_made = false;
            }
        }
    }

    if ( changes_made ) {
        xx_wm_window_update_icon_dimension(
            xx_wm_window->twc_window
        );
    }

    return;
}

static
struct wl_buffer*
lookup_xdg_toplevel_icon_buffer(
    struct xx_wm_icon *xx_wm_icon,
    int32_t            size,
    int32_t            buffer
) {
    // Fix: ToDo

    // Fix: Add checks to make sure buffer is not otherwise in use.
    // These checks may already be part of xdg_toplevel_icon_request.c

    return NULL;
}

extern
void
xx_wm_icon_attach_application_buffer(
    struct xx_wm_icon *xx_wm_icon,
    int32_t            size,
    int32_t            scale
) {

    struct wl_surface *wl_surface = (
        wl_container_of(xx_wm_icon, wl_surface, xx_wm_icon)
    );

    struct wl_buffer *wl_buffer = (
        lookup_xdg_toplevel_icon_buffer(xx_wm_icon, size, scale)
    );

    wl_surface_attach(
        wl_surface,
        wl_buffer,
        0,0
    );

    // The icon stylist must commit the surface.

    return;
}

extern
int
xx_wm_icon_add_listener(
    struct xx_wm_icon                *xx_wm_icon,
    const struct xx_wm_icon_listener *xx_wm_icon_listener,
    void                             *data
) {
    xx_wm_icon->data                =  data;
    xx_wm_icon->xx_wm_icon_listener = *xx_wm_icon_listener;

    return ADD_LISTENER_SUCCESS;
}

 // ----------------
 // xx_wm_menu_agent

extern
void
xx_wm_menu_agent_resign(
    struct xx_wm_menu_agent *xx_wm_menu_agent
) {
    // Fix: ToDo

    granted_xx_wm_menu_agent = NULL_XX_WM_MENU_AGENT;

    if ( default_surface_annexed ) {
        // Protocol Error:
        return;
    }

    return;
}

extern
struct wl_surface*
xx_wm_menu_agent_annex_default_surface(
    struct xx_wm_menu_agent *xx_wm_menu_agent
) {
    // This routine is similar to
    // xx_wm_interactive_agent_annex_overlay_surface().  See that
    // routine for comments.

    if ( default_surface_annexed ) {
        // Protocol Error:
        return NULL;
    }

    default_surface_annexed = true;

    struct wm_agent_base_v1 *wm_agent_base_v1 = (
        wl_container_of(xx_wm_menu_agent, wm_agent_base_v1, xx_wm_menu_agent)
    );

    struct bic_connection *bic_connection = (
        wl_container_of(wm_agent_base_v1, bic_connection, wm_agent_base_v1)
    );

    DEFAULT_WL_SURFACE->bic_connection = bic_connection;

    struct wl_surface *proxy_wl_surface = (
        wl_compositor_create_surface(
            &bic_connection->wl_compositor
        )
    );

    proxy_wl_surface->type = ST_PROXY;
      // Record the client's proxy_wl_surface.
    xx_wm_menu_agent->proxy_surface = proxy_wl_surface;

    return proxy_wl_surface;
}

extern
void
xx_wm_menu_agent_release(
    struct xx_wm_menu_agent *xx_wm_menu_agent
) {
    default_surface_annexed = false;

    DEFAULT_WL_SURFACE->bic_connection = INERT_BIC_CONNECTION;

    wl_surface_destroy(xx_wm_menu_agent->proxy_surface);

    return;
}

extern
void
xx_wm_menu_agent_select_window_intrinsic(
    struct xx_wm_menu_agent    *xx_wm_menu_agent,
    enum xx_wm_window_intrinsic window_intrinsic
) {
    twc_action_select_window_intrinsic( window_intrinsic );

    return;
}

extern
void
xx_wm_menu_agent_apply_window_intrinsic(
    struct xx_wm_menu_agent    *xx_wm_menu_agent,
    enum xx_wm_window_intrinsic window_intrinsic
) {
    twc_action_apply_window_intrinsic( window_intrinsic );

    return;
}

extern
void
xx_wm_menu_agent_select_agent_action(
    struct xx_wm_menu_agent       *xx_wm_menu_agent,
    enum wm_agent_base_v1_agent_id agent_id,
    uint32_t                       action_id
) {
    twc_action_select_agent_action(
        agent_id,
        action_id
    );

    return;
}

extern
void
xx_wm_menu_agent_apply_agent_action(
    struct xx_wm_menu_agent       *xx_wm_menu_agent,
    enum wm_agent_base_v1_agent_id agent_id,
    uint32_t                       action_id
) {
    if ( xx_wm_menu_agent != granted_xx_wm_menu_agent ) { return; }

    struct twc_view *twc_view = twc_view_with_pointer();

    xx_wm_window_action (
        twc_view->twc_window,
        agent_id,
        action_id
    );

    return;
}

extern
void
xx_wm_menu_agent_compositor_op(
    struct xx_wm_menu_agent              *xx_wm_menu_agent,
    enum   xx_wm_menu_agent_compositor_op compositor_op
) {
    twc_op_compositor_op( compositor_op );

    return;
}

extern
void
xx_wm_menu_agent_load_plug_in(
    struct xx_wm_menu_agent *xx_wm_menu_agent,
    const  char             *name
) {
    // Fix: ToDo

    return;
}

extern
void
xx_wm_menu_agent_unload_plug_in(
    struct xx_wm_menu_agent *xx_wm_menu_agent,
    const  char             *name
) {
    // Fix: ToDo

    return;
}

extern
struct xx_zwm_service_controller*
xx_wm_menu_agent_bind_service_controller(
    struct xx_wm_menu_agent *xx_wm_menu_agent,
    uint32_t                 numeric_name
) {
    struct xx_zwm_service_controller *xx_zwm_service_controller, *tmp;
    wl_list_for_each_safe(            xx_zwm_service_controller,  tmp, BIC_SERVER_unbound_controller_list, link ) {

        if ( xx_zwm_service_controller->numeric_name == numeric_name ) {

            unbound_controller_list_remove( xx_zwm_service_controller );

              // The controller becomes active
            xx_zwm_service_controller->is_active = true;

            return xx_zwm_service_controller;
        }
    }

    return NULL;
}

extern
void
xx_wm_menu_agent_dismiss_agent(
    struct xx_wm_menu_agent         *xx_wm_menu_agent,
    enum   wm_agent_base_v1_agent_id agent_id
) {
    // Fix: ToDo

    return;
}

extern
int
xx_wm_menu_agent_add_listener(
    struct       xx_wm_menu_agent          *xx_wm_menu_agent,
    const struct xx_wm_menu_agent_listener *xx_wm_menu_agent_listener,
    void                                   *data
) {
    xx_wm_menu_agent->data                      =  data;
    xx_wm_menu_agent->xx_wm_menu_agent_listener = *xx_wm_menu_agent_listener;

    if ( xx_wm_menu_agent->is_listener_set ) { return ADD_LISTENER_SUCCESS; }

    // This is the first time a listener has been added.  Emit
    // new_service events for all desktop_services.

    xx_wm_menu_agent->is_listener_set = true;

    struct xx_zwm_service_controller *xx_zwm_service_controller, *tmp;
    wl_list_for_each_safe(            xx_zwm_service_controller,  tmp, BIC_SERVER_unbound_controller_list, link ) {

        // For each unbound controller emit new_service.  When the
        // menu agent binds the controller, the controller will be
        // removed from the list, so use the 'safe' iterator.

        struct xx_zwm_service *xx_zwm_service = (
            wl_container_of(xx_zwm_service_controller, xx_zwm_service, xx_zwm_service_controller)
        );

        xx_wm_menu_agent_emit_new_service(
            granted_xx_wm_menu_agent,
            xx_zwm_service_controller->numeric_name,
            0,  // version num
            xx_zwm_service->provider_name,
            xx_zwm_service->service_name
        );
    }

    return ADD_LISTENER_SUCCESS;
}

 // ---------------------
 // xx_wm_workspace_agent

extern
void
xx_wm_workspace_agent_resign(
    struct xx_wm_workspace_agent *xx_wm_workspace_agent
) {

    granted_xx_wm_workspace_agent = NULL_XX_WM_WORKSPACE_AGENT;

    // Fix: ToDo

    return;
}

extern
void
xx_wm_workspace_agent_activate(
    struct xx_wm_workspace_agent *xx_wm_workspace_agent,
    struct xx_wm_workspace       *xx_wm_workspace,
    struct wl_output             *wl_output
) {
    srv_output_set_active_workspace(wl_output->srv_output, xx_wm_workspace->twc_workspace);

      // The subject twc_workspace
    struct twc_workspace *twc_workspace = (
        xx_wm_workspace->twc_workspace
    );

    // Fix: add a routine xx_wm_workspace_active() to wm_wrappers.c which
    // contains this loop.

      // Notify every client which has an xx_wm_workspace corresponding
      // to twc_workspace.
    struct xx_wm_workspace *xx_wm_workspace_entry;
    wl_list_for_each(       xx_wm_workspace_entry, &twc_workspace->xx_wm_workspace_list, link) {

        xx_wm_workspace_emit_active(xx_wm_workspace_entry, wl_output);
    }

    return;
}

extern
void
xx_wm_workspace_agent_set_wallpaper(
    struct xx_wm_workspace_agent *xx_wm_workspace_agent,
    struct xx_wm_workspace       *xx_wm_workspace,
    struct wl_output             *wl_output,
    struct wl_surface            *wallpaper_surface
) {
    if ( wallpaper_surface != NULL ) {
        if ( wallpaper_surface->type != ST_NONE            &&
             wallpaper_surface->type != ST_XX_WM_WALLPAPER
        ) {
            // Protocol error
            return;
        }
        wallpaper_surface->type = ST_XX_WM_WALLPAPER;
    }
    srv_output_set_wallpaper(
        wl_output->srv_output,
        xx_wm_workspace->twc_workspace,
        wallpaper_surface
    );

    return;
}

extern
void
xx_wm_workspace_agent_reoccupy_window(
    struct xx_wm_workspace_agent *xx_wm_workspace_agent,
    struct xx_wm_window          *xx_wm_window,
    enum   xx_wm_workspace_id     wksp_occupancy
) {
    twc_window_reoccupy(
        xx_wm_window->twc_window,
        wksp_occupancy
    );

    return;
}

extern
void
xx_wm_workspace_agent_create_workspace(
    struct xx_wm_workspace_agent *xx_wm_workspace_agent,
    const  char                  *name
) {
    // Fix: ToDo

    return;
}

extern
int
xx_wm_workspace_agent_add_listener(
    struct       xx_wm_workspace_agent          *xx_wm_workspace_agent,
    const struct xx_wm_workspace_agent_listener *xx_wm_workspace_agent_listener,
    void                                        *data
) {
    xx_wm_workspace_agent->data                           =  data;
    xx_wm_workspace_agent->xx_wm_workspace_agent_listener = *xx_wm_workspace_agent_listener;

    return ADD_LISTENER_SUCCESS;
}

 // ----------------
 // xx_wm_task_agent

extern
void
xx_wm_task_agent_resign(
    struct xx_wm_task_agent *xx_wm_task_agent
) {
    // Fix: ToDo

    return;
}

extern
int
xx_wm_task_agent_add_listener(
    struct       xx_wm_task_agent          *xx_wm_task_agent,
    const struct xx_wm_task_agent_listener *xx_wm_task_agent_listener,
    void                                   *data
) {
    xx_wm_task_agent->data                      =  data;
    xx_wm_task_agent->xx_wm_task_agent_listener = *xx_wm_task_agent_listener;

    return ADD_LISTENER_SUCCESS;
}

 // ---------------------
 // xx_wm_window_registry

  // List insertion occurs at 'add_listener' time, not at 'bind' time.
#define twc_window_interest_list_insert(b) ( \
    wl_list_insert(                  \
        TWC_window_interest_list,    \
        &((b)->window_interest_link) \
    )                                \
)

  // List removal occurs on destroy.  There might not have been an
  // insert.  See insert above.
#define twc_window_interest_list_remove(b) ( \
    (b)->window_interest_link.next != 0            \
    ? wl_list_remove(&((b)->window_interest_link)) \
    : 1                                            \
)

extern
void
xx_wm_window_registry_destroy(
    struct xx_wm_window_registry *xx_wm_window_registry
) {
    struct wm_agent_base_v1 *wm_agent_base_v1 = (
        wl_container_of(
            xx_wm_window_registry,
            wm_agent_base_v1,
            xx_wm_window_registry
        )
    );
    struct bic_connection *bic_connection = (
        wl_container_of(wm_agent_base_v1, bic_connection, wm_agent_base_v1)
    );

    twc_window_interest_list_remove(bic_connection);

      // Reset the xx_wm_window_registry within the bic_connection.
    xx_wm_window_registry->xx_wm_window_registry_listener = (
        inert_xx_wm_window_registry_listener
    );
    xx_wm_window_registry->is_listener_set = false;

    return;
}

extern
struct xx_wm_window*
xx_wm_window_registry_bind_window(
    struct xx_wm_window_registry *xx_wm_window_registry,
    uint32_t                      name
) {
    struct twc_window *twc_window = (
        twc_window_numeric_name_lookup(name)
    );

    if (twc_window == NULL) { return NULL; }

    struct xx_wm_window *xx_wm_window = (
        calloc( 1, sizeof(struct xx_wm_window) )
    );

    xx_wm_window->xx_wm_window_listener = inert_xx_wm_window_listener;
    xx_wm_window->is_listener_set       = false;

    struct wm_agent_base_v1 *wm_agent_base_v1 = (
        wl_container_of(
            xx_wm_window_registry,
            wm_agent_base_v1,
            xx_wm_window_registry
        )
    );

    struct bic_connection *bic_connection = (
        wl_container_of( wm_agent_base_v1, bic_connection, wm_agent_base_v1 )
    );

    xx_wm_window->bic_connection = bic_connection;
    xx_wm_window->twc_window     = twc_window;

    wl_list_insert( &twc_window->xx_wm_window_list, &xx_wm_window->link );

    return xx_wm_window;
}

extern
int
xx_wm_window_registry_add_listener(
    struct       xx_wm_window_registry          *xx_wm_window_registry,
    const struct xx_wm_window_registry_listener *xx_wm_window_registry_listener,
    void                                        *data
) {
    // A client is not interested in new windows until it registers a
    // listener.

    xx_wm_window_registry->data = data;
    xx_wm_window_registry->xx_wm_window_registry_listener = (
        *xx_wm_window_registry_listener
    );

    if ( xx_wm_window_registry->is_listener_set ) { return ADD_LISTENER_SUCCESS; }

    xx_wm_window_registry->is_listener_set = true;

      // Add client to the new_window interest_list.
    struct wm_agent_base_v1 *wm_agent_base_v1 = (
        wl_container_of( xx_wm_window_registry, wm_agent_base_v1, xx_wm_window_registry )
    );
    struct bic_connection *bic_connection = (
        wl_container_of( wm_agent_base_v1, bic_connection, wm_agent_base_v1 )
    );
    twc_window_interest_list_insert(bic_connection);

      // Now that there's a listener, announce existing windows to the
      // client.
    struct twc_window        *twc_window;
    twc_window_list_for_each( twc_window ) {
        xx_wm_window_registry_emit_new(
            xx_wm_window_registry,
            twc_window->numeric_name,
            0
        );
    }

    return ADD_LISTENER_SUCCESS;
}

 // ------------
 // xx_wm_window

extern
void
xx_wm_window_destroy(
    struct xx_wm_window *xx_wm_window
) {
    wl_list_remove( &xx_wm_window->link );

    free( xx_wm_window );
}

extern
void
xx_wm_window_intrinsic(
    struct xx_wm_window        *xx_wm_window,
    enum xx_wm_window_intrinsic window_intrinsic
) {
    struct twc_window *twc_window = xx_wm_window->twc_window;

    struct twc_view *twc_view = (
          twc_window->is_iconified
        ? xx_wm_window->twc_window->icon_view
        : xx_wm_window->twc_window->client_view
    );

    twc_action_window_intrinsic(
        twc_view,
        window_intrinsic
    );

    return;
}

extern
void
xx_wm_window_agent_action(
    struct xx_wm_window           *xx_wm_window,
    enum wm_agent_base_v1_agent_id agent_id,
    uint32_t                       action_id
) {
    if ( xx_wm_window == NULL ) { return; }

    struct twc_window *twc_window = xx_wm_window->twc_window;

    xx_wm_window_action(
        twc_window,
        agent_id,
        action_id
    );

    return;
}

extern
void
xx_wm_window_deposit_thumbnail(
    struct xx_wm_window *xx_wm_window,
    struct wl_buffer    *wl_buffer
) {

    // Fix: ToDo

    return;
}

extern
struct wl_buffer*
xx_wm_window_get_thumbnail(
    struct xx_wm_window *xx_wm_window
) {

    // Fix: ToDo

    return NULL;
}

extern
int
xx_wm_window_add_listener(
    struct xx_wm_window                *xx_wm_window,
    const struct xx_wm_window_listener *xx_wm_window_listener,
    void                               *data
) {
    xx_wm_window->data                  =  data;
    xx_wm_window->xx_wm_window_listener = *xx_wm_window_listener;

    if ( xx_wm_window->is_listener_set ) { return ADD_LISTENER_SUCCESS; }

    xx_wm_window->is_listener_set = true;

    // Client added its first listener.  Emit window properties.

    struct twc_window *twc_window  = xx_wm_window->twc_window;
    struct twc_view   *client_view = twc_window->client_view;
    struct twc_view   *  icon_view = twc_window->icon_view;

    enum xx_wm_window_iconify_state iconify_state;
    enum xx_wm_window_display_state display_state;

    if ( twc_window->is_iconified ) {

        iconify_state = XX_WM_WINDOW_ICONIFY_STATE_ICONIFIED;

        if ( twc_window->iconify_by_masking ) {
            display_state = XX_WM_WINDOW_DISPLAY_STATE_MASKED;
        } else {
            display_state = XX_WM_WINDOW_DISPLAY_STATE_ICON;
        }

    } else {

        iconify_state = XX_WM_WINDOW_ICONIFY_STATE_NOT_ICONIFIED;

        if ( twc_window->hold_icon ) {
            display_state = XX_WM_WINDOW_DISPLAY_STATE_ALL;
        } else {
            display_state = XX_WM_WINDOW_DISPLAY_STATE_WINDOW;
        }
    }

    xx_wm_window_emit_iconify_state(
        xx_wm_window,
        iconify_state
    );

    xx_wm_window_emit_display_state(
        xx_wm_window,
        display_state
    );

    if ( TWC_WINDOW_HAS_APP_ID( twc_window) ) {
        xx_wm_window_emit_app_id(
            xx_wm_window,
            xx_wm_window->twc_window->app_id
        );
    }

    if ( TWC_WINDOW_HAS_TITLE( twc_window) ) {
        xx_wm_window_emit_title(
            xx_wm_window,
            xx_wm_window->twc_window->title
        );
    }

    if ( client_view->layout_x != WLD_INVALID_COORD &&
         client_view->layout_y != WLD_INVALID_COORD
    ) {
        xx_wm_window_emit_location(
            xx_wm_window,
            wl_fixed_from_int(client_view->layout_x),
            wl_fixed_from_int(client_view->layout_y)
        );
    }

    if ( icon_view->layout_x != WLD_INVALID_COORD &&
         icon_view->layout_y != WLD_INVALID_COORD
    ) {
        xx_wm_window_emit_icon_location(
            xx_wm_window,
            icon_view->layout_x,
            icon_view->layout_y
        );

        struct srv_output *srv_output = (
            srv_output_at(
                client_view->layout_x,
                client_view->layout_y
            )
        );

        struct bic_connection *bic_connection = (
            xx_wm_window->bic_connection
        );

        struct wl_output *wl_output;
        bic_connection_output_list_for_each( bic_connection, wl_output) {

            if ( wl_output->srv_output == srv_output ) {
                xx_wm_window_emit_enter_output(
                    xx_wm_window,
                    wl_output
                );
             }
        }
    }

    xx_wm_window_emit_dimension(
        xx_wm_window,
        client_view->width,
        client_view->height
    );

    xx_wm_window_emit_icon_dimension(
        xx_wm_window,
        icon_view->width,
        icon_view->height
    );

    xx_wm_window_emit_occupancy(
        xx_wm_window,
        twc_window->wksp_occupancy
    );

    if ( twc_seat_view_with_keyboard_focus() == client_view ) {
        xx_wm_window_emit_keyboard_focus(
            xx_wm_window,
            XX_WM_WINDOW_FOCUS_POSSESS
        );
    }

    // Fix: ToDo thumbnail_max_size

    return ADD_LISTENER_SUCCESS;
}

 // ------------------------
 // xx_wm_workspace_registry

  // List insertion occurs at 'add_listener' time, not at 'bind' time.
#define twc_workspace_interest_list_insert(b) ( \
    wl_list_insert(                     \
        TWC_workspace_interest_list,    \
        &((b)->workspace_interest_link) \
    )                                   \
)

  // List removal occurs on destroy.  There might not have been an
  // insert.  See insert above.
#define twc_workspace_interest_list_remove(b) ( \
    (b)->workspace_interest_link.next != 0            \
    ? wl_list_remove(&((b)->workspace_interest_link)) \
    : 1                                               \
)

extern
void
xx_wm_workspace_registry_destroy(
    struct xx_wm_workspace_registry *xx_wm_workspace_registry
) {
    struct wm_agent_base_v1 *wm_agent_base_v1 = (
        wl_container_of(xx_wm_workspace_registry, wm_agent_base_v1, xx_wm_workspace_registry)
    );
    struct bic_connection *bic_connection = (
        wl_container_of(wm_agent_base_v1, bic_connection, wm_agent_base_v1)
    );

    twc_workspace_interest_list_remove(bic_connection);

      // Reset the xx_wm_workspace_registry within the bic_connection.
    xx_wm_workspace_registry->xx_wm_workspace_registry_listener = (
        inert_xx_wm_workspace_registry_listener
    );
    xx_wm_workspace_registry->is_listener_set = false;

    return;
}

extern
struct xx_wm_workspace*
xx_wm_workspace_registry_bind_workspace(
    struct xx_wm_workspace_registry *xx_wm_workspace_registry,
    uint32_t                         name
) {
    struct twc_workspace *twc_workspace = (
        twc_workspace_numeric_name_lookup(name)
    );

    if (twc_workspace == NULL) { return NULL; }

    struct xx_wm_workspace        *xx_wm_workspace = (
        calloc( 1, sizeof(struct xx_wm_workspace) )
    );

    xx_wm_workspace->xx_wm_workspace_listener = inert_xx_wm_workspace_listener;

    struct wm_agent_base_v1 *wm_agent_base_v1 = (
        wl_container_of(xx_wm_workspace_registry, wm_agent_base_v1, xx_wm_workspace_registry)
    );
    struct bic_connection *bic_connection = (
        wl_container_of(wm_agent_base_v1, bic_connection, wm_agent_base_v1)
    );

    xx_wm_workspace->twc_workspace  = twc_workspace;
    xx_wm_workspace->bic_connection = bic_connection;

    wl_list_insert( &twc_workspace->xx_wm_workspace_list, &xx_wm_workspace->link );

    return xx_wm_workspace;
}

extern
int
xx_wm_workspace_registry_add_listener(
    struct       xx_wm_workspace_registry          *xx_wm_workspace_registry,
    const struct xx_wm_workspace_registry_listener *xx_wm_workspace_registry_listener,
    void                                           *data
) {
    // A client is not interested in new workspaces until it registers
    // a listener.

    xx_wm_workspace_registry->data = data;
    xx_wm_workspace_registry->xx_wm_workspace_registry_listener = *xx_wm_workspace_registry_listener;

    if ( xx_wm_workspace_registry->is_listener_set ) { return ADD_LISTENER_SUCCESS; }

    xx_wm_workspace_registry->is_listener_set = true;

      // Add client to the new_workspace interest_list.
    struct wm_agent_base_v1 *wm_agent_base_v1 = (
        wl_container_of( xx_wm_workspace_registry, wm_agent_base_v1, xx_wm_workspace_registry )
    );
    struct bic_connection *bic_connection = (
        wl_container_of( wm_agent_base_v1, bic_connection, wm_agent_base_v1 )
    );
    twc_workspace_interest_list_insert(bic_connection);

      // Now that there's a listener, announce existing workspaces to
      // the client.
    struct twc_workspace        *twc_workspace;
    twc_workspace_list_for_each( twc_workspace ) {
        xx_wm_workspace_registry_emit_new(
            xx_wm_workspace_registry,
            twc_workspace->id,
            0
        );
    }

    return ADD_LISTENER_SUCCESS;
}

 // ---------------
 // xx_wm_workspace

extern
void
xx_wm_workspace_destroy(
    struct xx_wm_workspace *xx_wm_workspace
) {
    wl_list_remove( &xx_wm_workspace->link );

    free( xx_wm_workspace );

    return;
}

extern
void
xx_wm_workspace_remove(
    struct xx_wm_workspace *xx_wm_workspace
) {
    // Fix: ToDo

    return;
}

extern
int
xx_wm_workspace_add_listener(
    struct xx_wm_workspace                *xx_wm_workspace,
    const struct xx_wm_workspace_listener *xx_wm_workspace_listener,
    void                                  *data
) {
    xx_wm_workspace->data                     =  data;
    xx_wm_workspace->xx_wm_workspace_listener = *xx_wm_workspace_listener;

    struct bic_connection *bic_connection = (
        xx_wm_workspace->bic_connection
    );

      // Notify the agent of workspace attributes.
    struct wl_output *wl_output;
    bic_connection_output_list_for_each( bic_connection, wl_output) {

          // workspace name
        xx_wm_workspace_emit_name(
            xx_wm_workspace,
            xx_wm_workspace->twc_workspace->name
        );

          // active output, if any
        if ( srv_output_active_workspace(wl_output->srv_output) ==
             xx_wm_workspace->twc_workspace
        ) {
            xx_wm_workspace_emit_active(
                xx_wm_workspace,
                wl_output
            );
            break;
        }
    }

    return ADD_LISTENER_SUCCESS;
}

 // -------------------------
 // xx_zwm_service_controller

extern
void
xx_zwm_service_controller_destroy(
    struct xx_zwm_service_controller *xx_zwm_service_controller
) {
    if ( xx_zwm_service_controller == reoccupy_service_controller ) {
        reoccupy_service_controller = NULL;
    }

      // Since the controller was available for this request, it
      // shouldn't be in the unbound list.  Check anyway.
    unbound_controller_list_remove( xx_zwm_service_controller );

    xx_zwm_service_controller->xx_zwm_service_controller_listener = inert_xx_zwm_service_controller_listener;
    xx_zwm_service_controller->numeric_name                       = 0;
    xx_zwm_service_controller->is_active                          = false;

      // The service_controller is embedded in an xx_zwm_service.
    struct xx_zwm_service *xx_zwm_service = (
        wl_container_of(xx_zwm_service_controller, xx_zwm_service, xx_zwm_service_controller)
    );

    if ( xx_zwm_service->is_destroyed ) {

        // The desktop_service has been previously destroyed, so
        // memory containning the desktop_service and
        // service_controller can be deallocated.

        free( xx_zwm_service );
    }

    return;
}

extern
void
xx_zwm_service_controller_call_for_service(
    struct xx_zwm_service_controller *xx_zwm_service_controller
) {
    struct xx_zwm_service *xx_zwm_service = (
        wl_container_of(xx_zwm_service_controller, xx_zwm_service, xx_zwm_service_controller)
    );

    struct wl_list *service_window_list = &xx_zwm_service->service_window_list;

    if ( wl_list_empty(service_window_list) ) { return; }

    struct twc_workspace *twc_workspace = (
        twc_seat_workspace_with_pointer_focus()
    );

    struct srv_output *srv_output = (
        twc_seat_output_with_pointer_focus()
    );

    bool context_matching_window_found = false;

    struct twc_window *twc_window;
    struct xx_zwm_service_window *xx_zwm_service_window;
    wl_list_for_each(             xx_zwm_service_window, service_window_list, link) {

        // Are any service_windows associated with the desktop_service
        // relevant to the current output/workspace?

        twc_window = xx_zwm_service_window->twc_window;

        if (WKSP_OCCUPANCY_IS_MEMBER(
                twc_window->wksp_occupancy,
                twc_workspace_id( twc_workspace )
            )
        ) {
            if ( twc_window->preferred_output == NULL ) {
                context_matching_window_found = true;
                break;
            }

            if ( twc_window->preferred_output == srv_output ) {
                context_matching_window_found = true;
                break;
            }
        }
    }

    if ( ! context_matching_window_found ) { return; }

    // An appropriate service_window was found.  Assert it.  That is,
    // make it conspicuous to the user via the service assert_method.

    switch ( xx_zwm_service->twc_service_methods.assert_method ) {

        case XX_ZWM_SERVICE_ASSERT_METHOD_DEICONIFY:

            twc_window_set_iconify_state(twc_window, false);

        break;

        case XX_ZWM_SERVICE_ASSERT_METHOD_RAISE:

            twc_window_raise( twc_window );

        break;

        case XX_ZWM_SERVICE_ASSERT_METHOD_RELOCATE:

            twc_window_relocate_to_pointer( twc_window );

        break;

        case XX_ZWM_SERVICE_ASSERT_METHOD_NOP:
        default:
        break;
    }

    // Fix: Should the event xdg_top_level::configure(state =
    // "activated") be emitted?

    return;
}

extern
void
xx_zwm_service_controller_relay_operation(
    struct xx_zwm_service_controller *xx_zwm_service_controller,
    uint32_t                       operation
) {
    struct xx_zwm_service *xx_zwm_service = (
        wl_container_of(xx_zwm_service_controller, xx_zwm_service, xx_zwm_service_controller)
    );

    xx_zwm_service_emit_operation(
        xx_zwm_service,
        operation
    );

    return;
}

extern
int
xx_zwm_service_controller_add_listener(
    struct xx_zwm_service_controller                *xx_zwm_service_controller,
    const struct xx_zwm_service_controller_listener *xx_zwm_service_controller_listener,
    void                                            *data
) {
    xx_zwm_service_controller->data                               = data;
    xx_zwm_service_controller->xx_zwm_service_controller_listener = *xx_zwm_service_controller_listener;

    return ADD_LISTENER_SUCCESS;
}
