  // System
#include <stdlib.h>  // NULL

  // Wayland
#include <wayland-util.h>       // wl_fixed_t

  // APIs
#include "wl_surface.h"     // Our API to ensure consistancy
#include "wcf_shim.h"       // nested_surface_t
#include <twc/wayland_types.h>  // wld_coordinate_t

  // Imported data types
#include "twc_window-impl.h"
#include "wayland_bic.h"

#if 0
static
nested_surface_t*
wl_internal_surface_at(
    struct wl_surface *wl_surface,
    wld_coordinate_t   ptr_loc_sx,      // int   format, surface coord
    wld_coordinate_t   ptr_loc_sy,
    wl_fixed_t        *ptr_loc_fix_sx,  // fixed format, surface coord
    wl_fixed_t        *ptr_loc_fix_sy
) {
    wld_coordinate_t  right_x = wl_surface->width;
    wld_coordinate_t bottom_y = wl_surface->height;

      // Is the pointer location between 0 and the surface
      // boundaries?
    if ( (0 <= ptr_loc_sx) && (ptr_loc_sx < right_x ) &&
         (0 <= ptr_loc_sy) && (ptr_loc_sy < bottom_y)
    ) {
        *ptr_loc_fix_sx = wl_fixed_from_int( ptr_loc_sx );
        *ptr_loc_fix_sy = wl_fixed_from_int( ptr_loc_sy );

          // Since there are no nested surfaces, the "nested surface"
          // is simply the wl_surface itself.
        return wl_surface;
    }

    return NULL;
}
#endif

extern
bool
wl_simple_surface_at(
    struct wl_surface *wl_surface,
    wld_coordinate_t   ptr_loc_sx,      // int   format, surface coord
    wld_coordinate_t   ptr_loc_sy,
    wl_fixed_t        *ptr_loc_fix_sx,  // fixed format, surface coord
    wl_fixed_t        *ptr_loc_fix_sy
) {
    wld_coordinate_t  right_x = wl_surface->width;
    wld_coordinate_t bottom_y = wl_surface->height;

      // Is the pointer location between 0 and the surface
      // boundaries?
    if ( (0 <= ptr_loc_sx) && (ptr_loc_sx < right_x ) &&
         (0 <= ptr_loc_sy) && (ptr_loc_sy < bottom_y)
    ) {
        *ptr_loc_fix_sx = wl_fixed_from_int( ptr_loc_sx );
        *ptr_loc_fix_sy = wl_fixed_from_int( ptr_loc_sy );

        return true;
    }

    return false;
}

extern
nested_surface_t*
wl_surface_at(
    struct wl_surface *wl_surface,
    wld_coordinate_t   ptr_loc_sx,      // int   format, surface coord
    wld_coordinate_t   ptr_loc_sy,
    wl_fixed_t        *ptr_loc_fix_sx,  // fixed format, surface coord
    wl_fixed_t        *ptr_loc_fix_sy
) {
    nested_surface_t *nested_surface = NULL;

      // pointer location in enclosure-relative coordinates
    wld_coordinate_t ptr_loc_ex;
    wld_coordinate_t ptr_loc_ey;

    switch ( wl_surface->committed_image_type ) {

        case BT_internal:
            if (
                wl_simple_surface_at(
                   wl_surface,
                   ptr_loc_sx,      // int   format, surface coord
                   ptr_loc_sy,
                   ptr_loc_fix_sx,  // fixed format, surface coord
                   ptr_loc_fix_sy
                )
            ) {
                  // Since there are no nested surfaces, the "nested
                  // surface" is simply the wl_surface itself.
                nested_surface = wl_surface;
            }

        break;

        case BT_external:
        case BT_xwayland:

            // See the discussion of Surface Enclosures in
            // twc_window-impl.h.
            //
            // Backgound: When rendering a surface, the wcf compositor
            // places the origin of the surface enclosure at the
            // supplied coordinates.  The (visible) primary surface is
            // allowed to be inside the enclosure, in which case, the
            // primary surface will not appear in the proper place on
            // screen.  To compensate, srv_output_compose() places the
            // surface using enclosure-relative coordinates.

            // The relevance to this routine is that we must also
            // compensate and supply enclosure-relative coordinates.

            struct twc_view *twc_view = wl_surface->twc_view;

            wld_coordinate_t   surface_vx = twc_view->view_surface_vx;
            wld_coordinate_t   surface_vy = twc_view->view_surface_vy;

            wld_coordinate_t enclosure_vx = twc_view->enclosure_vx;
            wld_coordinate_t enclosure_vy = twc_view->enclosure_vy;

             // Convert surface-relative to enclosure-relative
             // coordinates.  See explanation below.
            ptr_loc_ex = ptr_loc_sx + surface_vx - enclosure_vx;
            ptr_loc_ey = ptr_loc_sy + surface_vy - enclosure_vy;

            nested_surface = (
                wcf_external_surface_at(
                    wl_surface->wcf_external_surface,
                    ptr_loc_ex,     // Enclosure-relative
                    ptr_loc_ey,     // coordinates.
                    ptr_loc_fix_sx,
                    ptr_loc_fix_sy
                )
            );

        break;

        default:  // ERROR
        break;
    }

    return nested_surface;

    /*
    ** Derivation of the translation from surface to enclosure
    ** relative coordinatses using view coordinates as an
    ** intermediary.  It's confusing, since one translation uses
    ** addition and the other subtraction.
    **
    ** Let pA0 be the orig point of the A Coord system
    ** Let pB0 be the orig point of the B Coord system
    **
    ** Let C(p,A) denote the coordinate of point p in the A system.
    **
    ** Translating origins from system A to B is given by
    **
    **   F1:  C(pA0,B) = - C(pB0,A)
    **
    ** The tranlation of coordinates from system A to B is
    **
    **   F2:  C(p,B) = C(p,A) - C(pB0,A)
    **               = C(p,A) + C(pA0,B)  by virtue of F1
    **
    ** We have three Coord systems
    **
    **   S      surface-relative coordinates
    **   V         view-relative coordinates
    **   E    enclosure-relative coordinates
    **
    ** We also have known values for
    **
    **   C(p  ,S)    where p is the pointer location
    **   C(pS0,V)
    **   C(pE0,V)    = -C(pV0,E)
    **
    ** We want
    **
    **   C(p,E) from C(p,S)
    **
    **   by F2,
    **
    **     C(p,E) = C(p,V)            - C(pE0,V)
    **
    **   expanding the first term, again using F2
    **
    **     C(p,E) = C(p,S) + C(pS0,V) - C(pE0,V)
    **
    ** All values on the right-hand side are known.
    */
}
