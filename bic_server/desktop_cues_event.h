#ifndef DESKTOP_CUES_EVENT_H
#define DESKTOP_CUES_EVENT_H

  // Opaque types
struct xx_desktop_cues_base_v1;
struct xx_desktop_cue;

 // --------------------
 // xx_desktop_cues_base

extern
void
xx_desktop_cues_base_v1_event_revoked(
    struct xx_desktop_cues_base_v1 *xx_desktop_cues_base_v1
);

 // --------------
 // xx_desktop_cue

extern
void
xx_desktop_cue_event_take(
    struct xx_desktop_cue *xx_desktop_cue
);

#endif  // DESKTOP_CUES_EVENT_H
