
  // System
#include <stdint.h>     // uint32_t

  // Protocols
#include <desktop-cues-v1-client-protocol.h>    // Our API to ensure consistancy

  // APIs
#include "wm_service_event.h"   // Our API to ensure consistancy
#include "wm_service_bic.h"

 // ----------------------
 // xx_zwm_service_base_v1
 //
 //   There are no events

 // --------------
 // xx_zwm_service

extern
void
xx_zwm_service_emit_operation(
    struct xx_zwm_service *xx_zwm_service,
     uint32_t              operation
) {
    xx_zwm_service->xx_zwm_service_listener.operation(
        xx_zwm_service->data,
        xx_zwm_service,
        operation
    );

    return;
}

 // -------------------
 // xx_zwm_service_window
 //
 //   There are no events
