
  // System
#include <stdint.h>     // uint32_t

  // Wayland
#include <wayland-util.h>   // wl_list

  // TWC
#include <twc/twc_log.h>

  // APIs
#include "wm_agent_event_wrappers.h"    // Our API to ensure consistancy
#include "twc_window.h"
#include "wm_agent_event.h"
#include "srv_output.h"

  // Imported data types
#include "twc_state-impl.h"     // twc interest lists
#include "twc_window-impl.h"
#include "twc_workspace-impl.h"
#include "bic_connection.h"
#include "server_state-impl.h"

 /*
 ** This file conatins wrappers for certain xx_wm_<inteface> events.
 ** Wrappers are helpful in the following cases.
 **
 ** 1) The event must be emitted to many interested agents, so a loop
 **    is required.
 **
 ** 2) The protocol object (event source) can be hidden from the
 **    caller in the case where the protocol object is a granted
 **    agent as they are unique.
 */


  // ----------------
  // wm_agent_base_v1

extern
void
wm_agent_base_v1_emit_new_agent_action(
    enum wm_agent_base_v1_agent_id agent_id,
    const  char                   *name,
    uint32_t                       action_id
) {
    // Some menu agents may utilize agent_actions even tho they don't
    // bind the window registry, so loop thru all connections.  Agents
    // should know about their own agent actions.

    struct bic_connection             *bic_connection;
    bic_connection_list_for_each( bic_connection ) {

        if ( IS_AGENT_THIS( bic_connection, agent_id ) ||
             IS_NOT_AGENT(  bic_connection           )
        ) { continue; }

        struct wm_agent_base_v1 *wm_agent_base_v1 = (
            &bic_connection->wm_agent_base_v1
        );

        wm_agent_base_v1->wm_agent_base_v1_listener.new_agent_action(
            wm_agent_base_v1->data,
            wm_agent_base_v1,
            agent_id,
            name,
            action_id
        );
    }
}

  // -----------------
  // xx_wm_interactive

extern
void
xx_wm_interactive_select(
    void
) {
    xx_wm_interactive_agent_emit_select(
        granted_xx_wm_interactive_agent
    );
}

extern
void
xx_wm_interactive_track(
    void
) {
    xx_wm_interactive_agent_emit_track(
        granted_xx_wm_interactive_agent
    );
}

extern
void
xx_wm_interactive_stop(
    void
) {
    xx_wm_interactive_agent_emit_stop(
        granted_xx_wm_interactive_agent
    );
}

  // ---------------------
  // xx_wm_window_registry

extern
void
xx_wm_window_new(
    struct twc_window *twc_window
) {
      // Notify clients that are interested in windows
    struct bic_connection *bic_connection;
    twc_window_interest_list_for_each(bic_connection) {

        xx_wm_window_registry_emit_new(
            &bic_connection->wm_agent_base_v1.xx_wm_window_registry,
            twc_window->numeric_name,
            0
        );
    }

    return;
}

  // ------------
  // xx_wm_window

#define xx_wm_window_list_for_each(x, w) \
    wl_list_for_each( (x), &((w)->xx_wm_window_list), link)

#define xx_wm_window_list_for_each_safe(x, t, w) \
    wl_list_for_each_safe( (x), (t), &((w)->xx_wm_window_list), link)

extern
void
xx_wm_window_update_iconify_state(
    struct twc_window *twc_window
) {
    struct xx_wm_window *xx_wm_window;
    xx_wm_window_list_for_each( xx_wm_window, twc_window ) {

        enum xx_wm_window_iconify_state iconify_state = (
            twc_window->is_iconified
            ? XX_WM_WINDOW_ICONIFY_STATE_ICONIFIED
            : XX_WM_WINDOW_ICONIFY_STATE_NOT_ICONIFIED
        );

        xx_wm_window_emit_iconify_state(
            xx_wm_window,
            iconify_state
        );
    }

    return;
}

extern
void
xx_wm_window_update_display_state(
    struct twc_window                *twc_window,
    enum   xx_wm_window_display_state display_state
) {
    struct xx_wm_window *xx_wm_window;
    xx_wm_window_list_for_each( xx_wm_window, twc_window ) {

        xx_wm_window_emit_display_state(
            xx_wm_window,
            display_state
        );
    }

    return;
}

extern
void
xx_wm_window_update_app_id(
    struct twc_window *twc_window
) {
    struct xx_wm_window *xx_wm_window;
    xx_wm_window_list_for_each( xx_wm_window, twc_window ) {

        xx_wm_window_emit_app_id(
            xx_wm_window,
            twc_window->app_id
        );
    }

    return;
}

extern
void
xx_wm_window_update_title(
    struct twc_window *twc_window
) {
    struct xx_wm_window *xx_wm_window;
    xx_wm_window_list_for_each( xx_wm_window, twc_window ) {

        xx_wm_window_emit_title(
            xx_wm_window,
            twc_window->title
        );
    }

    return;
}

extern
void
xx_wm_window_update_location(
    struct twc_window *twc_window
) {
    struct xx_wm_window *xx_wm_window;
    xx_wm_window_list_for_each( xx_wm_window, twc_window ) {

        xx_wm_window_emit_location(
            xx_wm_window,
            wl_fixed_from_int(twc_window->client_view->layout_x),
            wl_fixed_from_int(twc_window->client_view->layout_y)
        );
    }

    return;
}

extern
void
xx_wm_window_update_icon_location(
    struct twc_window *twc_window
) {
    struct xx_wm_window *xx_wm_window;
    xx_wm_window_list_for_each( xx_wm_window, twc_window ) {

        xx_wm_window_emit_icon_location(
            xx_wm_window,
            twc_window->icon_view->layout_x,
            twc_window->icon_view->layout_y
        );
    }

    return;
}

extern
void
xx_wm_window_update_dimension(
    struct twc_window *twc_window
) {
    struct xx_wm_window *xx_wm_window;
    xx_wm_window_list_for_each( xx_wm_window, twc_window ) {

        xx_wm_window_emit_dimension(
            xx_wm_window,
            twc_window->client_view->view_surface->width,
            twc_window->client_view->view_surface->height
        );
    }

    return;
}

extern
void
xx_wm_window_update_icon_dimension(
    struct twc_window *twc_window
) {
    struct xx_wm_window *xx_wm_window;
    xx_wm_window_list_for_each( xx_wm_window, twc_window ) {

        xx_wm_window_emit_icon_dimension(
            xx_wm_window,
            twc_window->icon_view->view_surface->width,
            twc_window->icon_view->view_surface->height
        );
    }

    return;
}

extern
void
xx_wm_window_update_occupancy(
    struct twc_window *twc_window
) {
    struct xx_wm_window *xx_wm_window;
    xx_wm_window_list_for_each( xx_wm_window, twc_window ) {

        xx_wm_window_emit_occupancy(
            xx_wm_window,
            twc_window->wksp_occupancy
        );
    }

    return;
}

extern
void
xx_wm_window_update_enter_output(
    struct twc_window *twc_window,
    struct srv_output *srv_output
) {
    struct xx_wm_window *xx_wm_window;
    xx_wm_window_list_for_each( xx_wm_window, twc_window ) {

        struct bic_connection *bic_connection = (
            xx_wm_window->bic_connection
        );

        struct wl_output *wl_output;
        bic_connection_output_list_for_each(bic_connection, wl_output) {

            if ( wl_output->srv_output == srv_output ) {
                xx_wm_window_emit_enter_output(
                        xx_wm_window,
                    wl_output
                );
            }
        }
    }

    return;
}

extern
void
xx_wm_window_update_pointer_focus(
    struct twc_window      *twc_window,
    enum xx_wm_window_focus focus
) {
    struct xx_wm_window *xx_wm_window;
    xx_wm_window_list_for_each( xx_wm_window, twc_window ) {
        xx_wm_window_emit_pointer_focus(
            xx_wm_window,
            focus
        );
    }

    return;
}

extern
void
xx_wm_window_update_keyboard_focus(
    struct twc_window      *twc_window,
    enum xx_wm_window_focus focus
) {
    struct xx_wm_window *xx_wm_window;
    xx_wm_window_list_for_each( xx_wm_window, twc_window ) {
        xx_wm_window_emit_keyboard_focus(
            xx_wm_window,
            focus
        );
    }

    return;
}

extern
void
xx_wm_window_update_selection_focus(
    struct twc_window      *twc_window,
    enum xx_wm_window_focus focus
) {
    struct xx_wm_window *xx_wm_window;
    xx_wm_window_list_for_each( xx_wm_window, twc_window ) {
        xx_wm_window_emit_selection_focus(
            xx_wm_window,
            focus
        );
    }

    return;
}

extern
void
xx_wm_window_thumbnail_max_size(
    struct twc_window *twc_window,
    uint32_t           width,
    uint32_t           height
) {
    struct xx_wm_window *xx_wm_window;
    xx_wm_window_list_for_each( xx_wm_window, twc_window ) {

        xx_wm_window_emit_thumbnail_max_size(
            xx_wm_window,
            width,
            height
        );
    }

    return;
}

extern
void
xx_wm_window_action(
    struct twc_window             *twc_window,
    enum wm_agent_base_v1_agent_id agent_id,
    uint32_t                       action_id
) {
    struct agent_action *agent_action = (
        lookup_agent_action(
            agent_id,
            action_id
        )
    );
    if ( agent_action == NULL ) { return; }

    bool found_agent_window = false;

    struct xx_wm_window        *client_window;
    xx_wm_window_list_for_each( client_window, twc_window ) {

        enum wm_agent_base_v1_agent_id agent_mask = (
            client_window->bic_connection->agent_mask
        );

        if ( agent_id & agent_mask ) {
            found_agent_window = true;
            break;
        }
    }

    if ( found_agent_window ) {

         xx_wm_window_emit_action(
            client_window,
            agent_id,
            action_id
        );

        if ( agent_action->xx_zwm_service == NULL ) { return; }

        // This agent action has an accompanying service

        struct xx_zwm_service_controller *xx_zwm_service_controller = (
            &agent_action->xx_zwm_service->xx_zwm_service_controller
        );

        xx_zwm_service_controller_call_for_service(
            xx_zwm_service_controller
        );
    }

    return;
}

extern
void
xx_wm_window_expired(
    struct twc_window *twc_window
) {
    struct xx_wm_window             *xx_wm_window,*tmp;
    xx_wm_window_list_for_each_safe( xx_wm_window, tmp, twc_window ) {

        xx_wm_window_emit_expired(
            xx_wm_window
        );
    }

    return;
}

  // ------------------------
  // xx_wm_workspace_registry

extern
void
xx_wm_workspace_new(
    struct twc_workspace *twc_workspace
) {
      // Notify clients that are interested in workspaces
    struct bic_connection *bic_connection;
    twc_workspace_interest_list_for_each(bic_connection) {
        xx_wm_workspace_registry_emit_new(
            &bic_connection->wm_agent_base_v1.xx_wm_workspace_registry,
            twc_workspace->id,
            0
        );
    }

    return;
}
