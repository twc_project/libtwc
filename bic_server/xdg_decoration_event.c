
  // Protocols
#include <xdg-decoration-unstable-v1-client-protocol.h>

  // APIs
#include "xdg_decoration_event.h"   // Our API to ensure consistancy
#include "xdg_decoration_bic.h"

extern
void
xdg_decoration_configure(
    struct zxdg_toplevel_decoration_v1 *xdg_toplevel_decoration,
    uint32_t                            mode
) {
    xdg_toplevel_decoration->xdg_decoration_listener.configure(
         xdg_toplevel_decoration->data,
         xdg_toplevel_decoration,
         mode
    );

    return;
}
