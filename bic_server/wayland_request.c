
  // System
#include <stdbool.h>
#include <stddef.h>     // NULL
#include <stdint.h>     // uint32_t
#include <stdlib.h>     // malloc, calloc, free
#include <string.h>     // strcmp
#include <sys/mman.h>   // mmap(), munmap()

  // Wayland
#include <wayland-util.h>       // wl_container_of, wl_list, wl_fixed_t, struct wl_interface
#include <libdrm/drm_fourcc.h>  // DRM_FORMAT_...

  // Protocols
#include <wayland-client-protocol.h>   // Our API to ensure consistancy

  // TWC
#include <twc/wayland_types.h>  // wld_dimension_t
#include <twc/wayland_inert.h>
#include <twc/twc_log.h>

  // APIs
#include "wayland_event.h"
#include "xdg_shell_event.h"
#include "twc_surface.h"
#include "twc_seat.h"
#include "wcf_shim.h"
#include "srv_output.h"
#include "external_buffer.h"
#include "twc_window.h"         // NULL_TWC_VIEW
#include "wayland_pre-defined.h"

  // Imported data types
#include "wayland_bic.h"
#include "xdg_decoration_bic.h"
#include "bic_connection.h"
#include "server_state-impl.h"

 /*
 ** We're interested in the character string names for each
 ** wl_<object>.  These are given by the "name" member of a struct
 ** wl_interface.  As an example, consider the wl_registry_interface.
 **
 ** The symbol wl_registry_interface is a struct wl_interface object.
 ** A declaration for the wl_registry_interface symbol
 **
 **     extern const struct wl_interface wl_registry_interface;
 **
 ** appears in both of the header files:
 **
 **     wayland-server-protocol.h
 **     wayland-client-protocol.h
 **
 ** The type declaration for
 **
 **     struct wl_interface { . . . }
 **
 **  appears in
 **
 **     wayland-util.h
 **
 ** The symbol name wl_registry_interface also appears in both DLLs
 **
 **     libwayland-server.so
 **     libwayland-client.so
 **
 ** This is true for every wl_<interface>_interface.  We assume that
 ** each of these symbols is defined identically in both DLLs.
 **
 ** So whether we link a window manager client against the server DLL
 ** while building a Built-in Client or against the client DLL while
 ** building a normal external client, the definition for any
 ** wl_<object>_interface will be the same.
 **
 ** In other words, when we link window manager clients with the
 ** server library, it will be 'as-if' they were linked with the
 ** client library (*).
 **
 ** To verify:
 **
 **     readelf -Ws /usr/lib/x86_64-linux-gnu/libwayland-server.so | grep registry
 **          180: 0000000000015be0    40 OBJECT  GLOBAL DEFAULT   19 wl_registry_interface
 **
 **     readelf -Ws /usr/lib/x86_64-linux-gnu/libwayland-client.so | grep registry
 **          180: 0000000000015be0    40 OBJECT  GLOBAL DEFAULT   19 wl_registry_interface
 **
 ** The GLOBAL means exported, so our code will pick up the library
 ** definition.  The DEFAULT means the symbol may be 'preempted'(**).
 ** So, if we define any wl_<object>_interface symbol, ours will
 ** override.  We create and complie <protocol_name>-protocol.c for
 ** each protocol for the BiCs and make use of
 ** libwayland-server/libwayland-client for exteranl clients.  See
 ** ./protocol/build_std/<protocol_name>-protocol.c and
 ** ./stage/lib/x86_64-linux-gnu/pkgconfig/twc-protocol-libs.pc
 **
 ** Of course there's a twist.
 **
 ** The twist is that the corresponging symbols for xdg-shell are not
 ** exported by the wlroots library:
 **
 **    readelf -Ws /usr/local/lib/x86_64-linux-gnu/libwlroots.so | fgrep -i xdg_wm_base
 **      2683: 00000000000ac5e0    40 OBJECT  LOCAL  DEFAULT   19 xdg_wm_base_interface
 **
 ** The word LOCAL means not exported.  This means that if the symbol
 ** is not defined when the DLL is encountered by the linker, it will
 ** remain undefined.  The symbol must be defined elsewhere, eg, in
 ** our code.
 **
 **
 ** (*) Annoyingly, the server header file also uses the same name for
 ** a type decalartion.  Compare these two.
 **
 **     struct wl_interface          { . . . } wl_registry_interface;
 **
 **     struct wl_registry_interface { list-of-funcion-pointers };
 **
 ** (**) The DEFAULT means the symbol may be 'preempted'.  That is, if
 ** the symbol is already defined when the DLL is encountered by the
 ** linker, no "duplicate definition' error will occur and the symbol
 ** value will retain its current value.  This means that IF we were
 ** to define the symbol in our code, it will supercede the definition
 ** in the DLL.  We're content to use the standard definition.
 **
 ** The function pointers are set to server listener routines for
 ** incoming client-to-server requests.  Variables of this type would
 ** be only used by server library code and do not seem to be exported
 ** in the server library.
 */

 // ----------
 // wl_display

extern
struct wl_registry*
wl_display_get_registry(
    struct wl_display  *wl_display
) {
    struct bic_connection *bic_connection = (
        wl_container_of(wl_display, bic_connection, wl_display)
    );

    return &bic_connection->wl_registry;
}

 // ----------
 // wl_registry

extern
void*
wl_registry_bind(
    struct wl_registry        *wl_registry,
    uint32_t                   name,
    const struct wl_interface *wl_interface,
    uint32_t                   version
) {
    void *new_id = NULL;

    struct bic_connection *bic_connection = (
        wl_container_of(wl_registry, bic_connection, wl_registry)
    );

    // Global interfaces are emitted through wl_registry::global
    //
    // wl_display  is bound using wl_display_connect()
    // wl_registry is bound using wl_display_get_registry()
    //
    // The first few interfaces are Singletons, so the (numeric) name
    // is not needed to distinguish which Global object to bind.

    if ( strcmp(wl_interface->name, wl_compositor_interface.name) == 0 ) {
        // wl_compoistor
        new_id = &bic_connection->wl_compositor;

    } else if ( strcmp(wl_interface->name, wl_shm_interface.name) == 0 ) {
        // wl_shm
        new_id = &bic_connection->wl_shm;

    } else if ( strcmp(wl_interface->name, xdg_wm_base_interface.name) == 0 ) {
        // xdg_wm_base
        new_id = &bic_connection->xdg_wm_base;

    } else if ( strcmp(wl_interface->name, zxdg_decoration_manager_v1_interface.name) == 0 ) {
        // xdg_decoration_manager
        new_id = &bic_connection->xdg_decoration_manager;

    } else if ( strcmp(wl_interface->name, wm_agent_base_v1_interface.name) == 0 ) {
        // wm_agent_base_v1
        new_id = &bic_connection->wm_agent_base_v1;

    } else if ( strcmp(wl_interface->name, xx_desktop_cues_base_v1_interface.name) == 0 ) {
        // xx_desktop_cues_base_v1
        new_id = &bic_connection->xx_desktop_cues_base_v1;

    } else if ( strcmp(wl_interface->name, xx_zwm_service_base_v1_interface.name) == 0 ) {
        // xx_zwm_service_base_v1
        new_id = &bic_connection->xx_zwm_service_base_v1;

          // Fix: All clients get positive vetting.  OK for BiCs, but
          // not otherwise.
        bic_connection->xx_zwm_service_base_v1.is_client_vetted = true;

    } else if ( strcmp(wl_interface->name, wl_seat_interface.name) == 0 ) {
        // wl_seat
        // Fix: wl_seat is not a Singleton.  A lookup based on
        // (numeric) name is needed to find the specific Global seat
        // to bind.
        new_id = &bic_connection->wl_seat;

    } else if ( strcmp(wl_interface->name, wl_output_interface.name) == 0 ) {
        // wl_output
        struct srv_output *srv_output = srv_output_numeric_name_lookup(name);

        if ( srv_output != NULL ) {

            struct wl_output *wl_output = (
                calloc( 1, sizeof(struct wl_output) )
            );
            wl_output->srv_output = srv_output;

            bic_connection_output_list_insert(bic_connection, wl_output);

            new_id = wl_output;
        }
    }

    return new_id;
}

  // For Singletons, the object being bound is unambiguous.
  // Singletons are never removed.
#define GLOBAL_SINGLETON_NUMERIC_NAME 0

  // In general, a server could create several wl_seat objects.  The
  // bic_server only employs one seat, so one numeric name is
  // sufficient.
#define SEAT_NUMERIC_NAME 0

static
int
emit_registry_global_objects(
    struct wl_display *wl_display
) {
    int dispatched_event_count = 0;

    struct bic_connection *bic_connection = (
        wl_container_of(wl_display, bic_connection, wl_display)
    );

    if ( bic_connection->wl_registry.is_listener_set ) {
        return dispatched_event_count;
    }

    bic_connection->wl_registry.is_listener_set = true;

    struct wl_registry *wl_registry = &bic_connection->wl_registry;

    wl_registry->wl_registry_listener.global(
        wl_registry->data,
        wl_registry,
        GLOBAL_SINGLETON_NUMERIC_NAME,
        wl_compositor_interface.name,
        wl_compositor_interface.version
    );
    dispatched_event_count++;

    wl_registry->wl_registry_listener.global(
        wl_registry->data,
        wl_registry,
        GLOBAL_SINGLETON_NUMERIC_NAME,
        wl_shm_interface.name,
        wl_shm_interface.version
    );
    dispatched_event_count++;

    // Fix: Loop over all seats.
    //struct srv_seat *srv_seat;
    //wl_list_for_each(srv_seat, &TWC.srv_seat_list, link) {

        wl_registry->wl_registry_listener.global(
            wl_registry->data,
            wl_registry,
            SEAT_NUMERIC_NAME,      // srv_seat->numeric_name
            wl_seat_interface.name,
            wl_seat_interface.version
        );
        dispatched_event_count++;

    //}

    struct srv_output *srv_output;
    srv_output_list_for_each(srv_output) {

        wl_registry->wl_registry_listener.global(
            wl_registry->data,
            wl_registry,
            srv_output_numeric_name(srv_output),
            wl_output_interface.name,
            wl_output_interface.version
        );
        dispatched_event_count++;
    }

    wl_registry->wl_registry_listener.global(
        wl_registry->data,
        wl_registry,
        GLOBAL_SINGLETON_NUMERIC_NAME,
        xdg_wm_base_interface.name,
        xdg_wm_base_interface.version
    );
    dispatched_event_count++;

    wl_registry->wl_registry_listener.global(
        wl_registry->data,
        wl_registry,
        GLOBAL_SINGLETON_NUMERIC_NAME,
        zxdg_toplevel_decoration_v1_interface.name,
        zxdg_toplevel_decoration_v1_interface.version
    );
    dispatched_event_count++;

    wl_registry->wl_registry_listener.global(
        wl_registry->data,
        wl_registry,
        GLOBAL_SINGLETON_NUMERIC_NAME,
        wm_agent_base_v1_interface.name,
        wm_agent_base_v1_interface.version
    );
    dispatched_event_count++;

    wl_registry->wl_registry_listener.global(
        wl_registry->data,
        wl_registry,
        GLOBAL_SINGLETON_NUMERIC_NAME,
        xx_zwm_service_base_v1_interface.name,
        xx_zwm_service_base_v1_interface.version
    );
    dispatched_event_count++;

    wl_registry->wl_registry_listener.global(
        wl_registry->data,
        wl_registry,
        GLOBAL_SINGLETON_NUMERIC_NAME,
        xx_desktop_cues_base_v1_interface.name,
        xx_desktop_cues_base_v1_interface.version
    );
    dispatched_event_count++;

    return dispatched_event_count;
}

extern
int
wl_registry_add_listener(
    struct wl_registry                *wl_registry,
    const struct wl_registry_listener *wl_registry_listener,
    void                              *data
) {
    wl_registry->data = data;
    wl_registry->wl_registry_listener = *wl_registry_listener;

    struct bic_connection *bic_connection = (
        wl_container_of(wl_registry, bic_connection, wl_registry)
    );

    emit_registry_global_objects( &bic_connection->wl_display );

    return ADD_LISTENER_SUCCESS;
}


 // -------------
 // wl_compositor

extern
struct wl_surface*
wl_compositor_create_surface(
    struct wl_compositor *wl_compositor
) {
    struct bic_connection *bic_connection = (
        wl_container_of(wl_compositor, bic_connection, wl_compositor)
    );

    struct wl_surface *wl_surface = (
        calloc( 1, sizeof(struct wl_surface) )
    );

    wl_surface->data = NULL;
    wl_surface->wl_surface_listener = inert_wl_surface_listener;

    wl_surface->type      = ST_NONE;
    wl_surface->twc_view  = NULL_TWC_VIEW;
    wl_surface->is_mapped = false;

    //  wl_surface->xdg_shell initialized by
    //  xdg_wm_base_get_xdg_surface()

    //  wl_surface->xdg_toplevel initialized by
    //  xdg_surface_get_toplevel().  Similarly for xdg_popup.

    //  wl_surface->xx_wm_icon initialized by
    //  xx_wm_icon_stylist_get_icon().

    wl_surface->pending_wl_buffer          = UNDEFINED_WL_BUFFER;
    wl_surface->current_wl_buffer          =     INERT_WL_BUFFER;
    wl_surface->is_current_buffer_released = true;

    wl_surface->committed_image_type = BT_internal;
    wl_surface->width                = 0;
    wl_surface->height               = 0;
    wl_surface->stride               = 0;
    wl_surface->pix_data             = NULL;
    wl_surface->wcf_pixel_texture    = NULL;

    wl_surface->nested_surface_with_ptr_focus = NULL;

    wl_surface->bic_connection = bic_connection;

    return wl_surface;
};

 // ------
 // wl_shm

#define WL_SHM_BY_MALLOC ((struct wl_shm*) (-1))

extern
struct wl_shm_pool*
wl_shm_create_pool(
    struct wl_shm *wl_shm,
    int32_t        fd,
    int32_t        size
) {
    // Fix: there needs to be a list of shm_pools for each client so
    // they can be deallocated if a client ends abruptly.

    struct wl_shm_pool *wl_shm_pool = (
        calloc( 1, sizeof(struct wl_shm_pool) )
    );

    wl_shm_pool->shm_pool_size = size;

    void *shm_pool_base = NULL;

  if ( wl_shm == WL_SHM_BY_MALLOC ) {

       // This case only arises when the caller is the
       // twc_shm_helper_create_pool() routine defined below.  Note
       // that only BiCs can link to this version of
       // twc_shm_helper_create_pool().  External clients will have
       // linked to the version in libtwc-helper-std.a.  They will not
       // by using WL_SHM_BY_MALLOC.

        shm_pool_base = malloc( size );

        if (shm_pool_base == NULL) {
            return NULL;
        }

        wl_shm_pool->shm_pool_type = SPT_MALLOC;

    } else {

        // This is he normal shm_open()/mmap() method of establishing
        // shared memory.  Both standard and built-in clients may fall
        // under this case.  For BiCs, both the client and server will
        // mmap() the memory within the same process/address_space.
        // The segment will appear in multiple places (aliased) in the
        // process.  As long as the data cache is physically tagged,
        // this won't cause cache sync problems.  Only BiCs which use
        // twc_shm_helper_create_pool will fall into the
        // WL_SHM_BY_MALLOC case above.

        shm_pool_base = (
            mmap(
                NULL,
                size,
                PROT_READ | PROT_WRITE, MAP_SHARED,
                fd,
                0
            )
        );
        if (shm_pool_base == MAP_FAILED) {
            return NULL;
        }

        wl_shm_pool->shm_pool_type = SPT_MMAP;
    }

    wl_shm_pool->shm_pool_base = shm_pool_base;
    wl_shm_pool->buffer_count  = 0;
    wl_shm_pool->is_destroyed  = false;

    return wl_shm_pool;
}

static
void
check_wl_shm_pool(
    struct wl_shm_pool *wl_shm_pool
) {
    if (( wl_shm_pool->is_destroyed      ) &&
        ( wl_shm_pool->buffer_count == 0 )
    ) {
        // No buffers are using the shared memory segment.

        if ( wl_shm_pool->shm_pool_type == SPT_MALLOC ) {

            free(
                wl_shm_pool->shm_pool_base
            );

        } else {

            munmap(
                wl_shm_pool->shm_pool_base,
                wl_shm_pool->shm_pool_size
            );
        }

        free(wl_shm_pool);
    }

    return;
}

 // -----------
 // wl_shm_pool

extern
struct wl_buffer*
wl_shm_pool_create_buffer(
    struct wl_shm_pool *wl_shm_pool,
    int32_t             offset,
    int32_t             width,
    int32_t             height,
    int32_t             stride,
    uint32_t            format
) {
    // Buffers can't have zero pixels.  See shm_pool_create_buffer()
    // in <wayland-src>/src/wayland-shm.c

    if ( wl_shm_pool->is_destroyed ) { return NULL; }

    int32_t max_row_cnt = ( INT32_MAX /  stride           );
    int32_t max_offset  = ( offset    + (stride * height) );
    int32_t pool_size   = ( wl_shm_pool->shm_pool_size    );

    if ( offset      <  0      ||
         width       <= 0      ||
         height      <= 0      ||
         stride      <  width  ||
         max_row_cnt <= height ||
         pool_size   < max_offset
    ) { return NULL; }


      // There were endian problems with enum:wl_shm_format and other
      // tools.  They have been resolved except for two basic formats.
      // Since wlroot uses drm_format exclusively, we convert.  See
      //   https://github.com/swaywm/wlroots/pull/2744
      //
      // See also the paragraphs under "wl_shm::format - pixel formats" in
      //   https://wayland.freedesktop.org/docs/html/apa.html#protocol-spec-wl_shm
      //
      // And also
      //   https://lists.freedesktop.org/archives/wayland-devel/2017-March/033492.html
    if ( format == WL_SHM_FORMAT_XRGB8888 ) {
         format =  DRM_FORMAT_XRGB8888;
    } else if
       ( format == WL_SHM_FORMAT_ARGB8888 ) {
         format =  DRM_FORMAT_ARGB8888;
    }

    struct wl_buffer *wl_buffer = (
        calloc( 1, sizeof(struct wl_buffer) )
    );

    wl_buffer->data               = NULL;
    wl_buffer->wl_buffer_listener = inert_wl_buffer_listener;
    wl_buffer->wl_shm_pool        = wl_shm_pool;
    wl_shm_pool->buffer_count++;

    wl_buffer->buffer_type  = BT_internal;
    wl_buffer->width        = width;
    wl_buffer->height       = height;
    wl_buffer->enclosure_sx = 0;
    wl_buffer->enclosure_sy = 0;
    wl_buffer->stride       = stride;
    wl_buffer->format       = format;

    wl_buffer->pix_data = wl_shm_pool->shm_pool_base + offset;

    return wl_buffer;
};

extern
void
wl_shm_pool_destroy(
    struct wl_shm_pool *wl_shm_pool
) {
    // Fix: with the shm_pool_table kludge, only the client
    // should call munmap().

    // The documentation for wl_shm_pool destroy in the Wayland.xml
    // protocol says "The mmapped memory will be released when all
    // buffers that have been created from this pool are gone."

    wl_shm_pool->is_destroyed = true;

    check_wl_shm_pool(wl_shm_pool);

    return;
}

 // ---------
 // wl_buffer

extern
int
wl_buffer_add_listener(
    struct wl_buffer                *wl_buffer,
    const struct wl_buffer_listener *wl_buffer_listener,
    void                            *data
) {
    wl_buffer->wl_buffer_listener = *wl_buffer_listener;
    wl_buffer->data               =  data;

    return ADD_LISTENER_SUCCESS;
}

extern
void
wl_buffer_destroy(
    struct wl_buffer *wl_buffer
) {
    if (wl_buffer == NULL) { return; }

    if (wl_buffer->buffer_type == BT_internal ) {

        wl_buffer->wl_shm_pool->buffer_count--;

        check_wl_shm_pool(wl_buffer->wl_shm_pool);

        free(wl_buffer);

    } else {
        external_buffer_destroy(wl_buffer);
    }

    return;
};

 // ----------
 // wl_surface

extern
void
wl_surface_destroy(
    struct wl_surface *wl_surface
) {
    // Fix: Can you destroy a fully fledged surface?  Should all the
    // extra struff be destroyed automatically as a side effect??
    // What about mapped_roles that are not
//    if ( IS_SURFACE_WINDOW(wl_surface) {

    if ( wl_surface->type != ST_NONE ) {
        // Protocol Error!
        return;
    }

    free(wl_surface);

    return;
};

extern
void
wl_surface_attach(
    struct wl_surface *wl_surface,
    struct wl_buffer  *wl_buffer,
    int32_t            x,
    int32_t            y
) {
    // non-zero x or y is deprecated, use wl_surface:offset instead.

    //LOG_INFO("wl_surface_attach %d", wl_surface->type);

    if ( wl_surface->xdg_surface.ack_configure_outstanding ) {
        // Error: Client must send xdg_surface_ack_configure request
        // before attaching a buffer.

        LOG_ERROR("missing ack_configure");
        return;
    }

    if ( wl_surface->type == ST_PROXY ) { return; }

    if (wl_buffer == NULL) {

        // Fix: For XDG surfaces, clear all xdg state except role.
        // How about xx_wm_decoration and xx_wm_icon roles??

        wl_buffer = INERT_WL_BUFFER;
    }

      // Attaching a buffer simply replaces the pending buffer.  The
      // pending buffer will be instated at commit time.
    wl_surface->pending_wl_buffer = wl_buffer;

    return;
};

static
void
wl_surface_buffer_transition(
    struct wl_surface *wl_surface,
    struct wl_buffer  *old_wl_buffer,
    struct wl_buffer  *new_wl_buffer
) {
      // First, retire the old_wl_buffer.
      // Due to possible early release, the old_wl_buffer cannot be
      // reliably dereferenced, but it can be tested.
    if ( old_wl_buffer != INERT_WL_BUFFER ) {

        if ( wl_surface->committed_image_type == BT_internal ) {

            wl_surface->pix_data = NULL;

            wcf_pixel_texture_destroy(
                wl_surface->wcf_pixel_texture
            );
            wl_surface->wcf_pixel_texture = NULL;
        }

          // Release the old buffer, unless it had an early release.
        if (wl_surface->is_current_buffer_released == false ) {
            // old_wl_buffer pointer is still valid

            wl_surface->is_current_buffer_released = true;
            wl_buffer_release(old_wl_buffer);   // Fix: what if the release listener calls commit?
        }
    }

    // Second, instate the new_wl_buffer.

    wl_surface->pending_wl_buffer   = UNDEFINED_WL_BUFFER;
    wl_surface->current_wl_buffer   = new_wl_buffer;

      // To facilitate early release, promote wl_buffer member data to
      // the wl_surface.
    wl_surface->committed_image_type = new_wl_buffer->buffer_type;
    wl_surface->width                = new_wl_buffer->width;
    wl_surface->height               = new_wl_buffer->height;
    wl_surface->stride               = new_wl_buffer->stride;

    if ( new_wl_buffer == INERT_WL_BUFFER) {
        // The new buffer is empty - the surface will become unmapped.
        //
        // If the old_wl_buffer was external, the wcf_external_surface
        // remains in play.  Do Not Invalidate.  The wcf_external_surface
        // value is used to emit leave events for wl_pointer and
        // wl_keyboard.

        /*
        wl_surface->wcf_external_surface = NULL;
        */

        return;
    }

    bool buffer_busy = true;

    // Promote the new_wl_buffer image data.

    if ( new_wl_buffer->buffer_type == BT_internal ) {

        wl_surface->pix_data = new_wl_buffer->pix_data;

          // The new_wl_buffer contains pix_data.  Convert it to a
          // wcf_pixel_texture and store in the surface, replacing any
          // old texture.  If a copy of the pix_data is made, the
          // buffer will no longer be busy.
        wl_surface->wcf_pixel_texture = (
            wcf_pixel_texture_create(
                new_wl_buffer->width,
                new_wl_buffer->height,
                new_wl_buffer->stride,
                new_wl_buffer->format,
                new_wl_buffer->pix_data,
                &buffer_busy
            )
        );

    } else {
          // new_wl_buffer->wcf_external_surface contains the displayable
          // image for this buffer.  Promote it to the wl_surface.  It
          // will remain busy until a subsequent buffer is committed.

        buffer_busy = true;
        wl_surface->wcf_external_surface = new_wl_buffer->wcf_external_surface;
    }

    wl_surface->is_current_buffer_released = false;
    if ( buffer_busy == false ) {
        // Early release

        wl_surface->is_current_buffer_released = true;
        wl_buffer_release(new_wl_buffer);   // Fix: what if the release listener calls commit?
    }

    return;
}

extern
void
wl_surface_commit(
    struct wl_surface *wl_surface
) {
    // The following is an incomplete list of surface properties.
    // (Some are double-buffered, in which case, any newly requested
    // value is merely pending until commit time.  The Wayland
    // protocol for wl_surface.commit says any new buffer is to be
    // applied first.)
    //
    // Property                                 Applies to
    // -------------------------------          ------------
    //
    //  Properties set by client
    // wl_buffer                                wl_surface
    // set_input_region                         wl_surface
    // set_opaque_region                        wl_surface
    // set_buffer_transformation                wl_surface
    // set_buffer_scale                         wl_surface
    // damage_buffer                            wl_surface
    //
    // set_window_geometry                      xdg_surface
    //
    // set_max_size                             xdg_toplevel
    // set_min_size                             xdg_toplevel
    //
    // (un)set_maximized                        xdg_toplevel
    // (un)set_fullscreen                       xdg_toplevel
    // set_minimize                             xdg_toplevel
    //
    // set_parent                               xdg_toplevel
    // set_app_id                               xdg_toplevel
    // set_title                                xdg_toplevel
    //
    //  Properties set by compositor
    // width                                    xdg_toplevel
    // height                                   xdg_toplevel
    // enum xdg_toplevel.states                 xdg_toplevel
    // bounds                                   xdg_toplevel
    // wm_capabilities                          xdg_toplevel
    // x                                           xdg_popup
    // y                                           xdg_popup
    // width                                       xdg_popup
    // height                                      xdg_popup
    // repositioned                                xdg_popup
    // enum zxdg_toplevel_decoration_v1.mode    xdg_toplevel
    //
    //  Properties that can be requested by a client
    // xdg_toplevel.maximized                   xdg_toplevel
    // xdg_toplevel.fullscreen                  xdg_toplevel
    // enum zxdg_toplevel_decoration_v1.mode    xdg_toplevel
    //
    //  What are the properties requiring an
    //  xdg_surface::configure/ack_configure sequence?
    //
    // The following properties are not double-buffered.
    //
    // drag-and-drop  (simple role)             wl_surface
    // cursor         (simple role)             wl_surface
    // xx_wm_icon                               wl_surface
    // xx_wm_decoration                         wl_surface
    // wl_subsurface  (PO role)                 wl_surface
    //
    // xdg_surface                              wl_surface   ambiguous (1)
    // xdg_toplevel   (xdg-shell role)          xdg_surface
    // xdg_popup      (xdg-shell role)          xdg_surface
    //
    // 1. A role "is set permanently for the whole lifetime of the
    //    wl_surface object".
    //
    // 2. Giving a Protocol Object (PO) role to a wl_surface results
    //    in a new protocol object, while giving a simple role does
    //    not.  The wl_subsurface and xdg-shell roles are PO roles.
    //
    // 3. Some shell roles are said to be windows.  All xdg-shell
    //    roles are windows.
    //
    // 4. The requirement for an "initial commit" is an xdg-shell
    //    concept.  Other shells/roles may or may not follow.
    //
    // 5. There are no xdg-shell properties that can be set by both
    //    client and compositor.
    //
    // 6. xdg_toplevel.state
    //      maximized
    //      fullscreen
    //      resizing
    //      activated
    //      tiled_left
    //      tiled_right
    //      tiled_top
    //      tiled_bottom
    //      suspended
    //
    // Question: In xdg-shell, what can happen after toplevel role is
    // given but before the initial commit?  See
    //   https://gitlab.freedesktop.org/wayland/wayland-protocols/-/issues/109
    //
    // (1) "The client must call wl_surface.commit on the
    //     corresponding wl_surface for the xdg_surface state to take
    //     effect."

    if ( IS_XDG_ROLE(wl_surface->type)                &&
         ! wl_surface->xdg_surface.has_initial_commit
    ) {
        // The "initial commit" for an xdg_shell role.

        if (( wl_surface->pending_wl_buffer != INERT_WL_BUFFER     ) &&
            ( wl_surface->pending_wl_buffer != UNDEFINED_WL_BUFFER )
        ) { return; /* Protocol error */ }

        wl_surface->xdg_surface.has_initial_commit = true;

        twc_window_initial_configure(
            wl_surface
        );

        return;
    }

      // Is there a pending buffer to commit?  INERT_WL_BUFFER is OK.
    if ( wl_surface->pending_wl_buffer == UNDEFINED_WL_BUFFER ) { return; }

      // Surface contents are double-buffered.
    struct wl_buffer *old_wl_buffer = wl_surface->current_wl_buffer;
    struct wl_buffer *new_wl_buffer = wl_surface->pending_wl_buffer;

    if ( old_wl_buffer == INERT_WL_BUFFER &&
         new_wl_buffer == INERT_WL_BUFFER
    ) {
          // The surface remains not mapped
        return;
    }

      // Extract salient data about the old wl_buffer from the
      // wl_surface.
    wld_dimension_t old_wl_buffer_width  = wl_surface->width;
    wld_dimension_t old_wl_buffer_height = wl_surface->height;

      // Perform buffer changeover.
    wl_surface_buffer_transition(
        wl_surface,
        old_wl_buffer,
        new_wl_buffer
    );

      // Consider map changes
    bool surface_has_buffer = new_wl_buffer != INERT_WL_BUFFER;
    bool surface_has_role   = SURFACE_HAS_ROLE(wl_surface);

    if ( wl_surface->is_mapped ) {

        if ( ! surface_has_buffer ||
             ! surface_has_role
        ) {
              // A mapped surface lost its buffer or role
            twc_surface_no_longer_mapped(
                wl_surface
            );
        }

    } else {
          // Surface is not mapped, should it be?
        if ( surface_has_buffer &&
             surface_has_role
        ) {
            twc_surface_becomes_mapped(
                wl_surface
            );
        }
    }

    // Fix: Consider wl_surface pending items like set_opaque_region.

      // Consider xdg_toplevel changes
    if ( wl_surface->type == ST_XDG_TOPLEVEL ) {

        // Fix: Implement addtional xdg_toplevel properties

    }

    if ( IS_XDG_ROLE(wl_surface->type) ) {
        // Fix: this code is suppose to handle commiting
        // xdg_surface.set_window_geometry.

        /*
        bool same_xdg_geometry = true;

        struct xdg_surface *xdg_surface = &wl_surface->xdg_surface;

        same_xdg_geometry = (
            SAME_XDG_GEOMETRY(
                xdg_surface->visible_geometry,
                xdg_surface->pending_geometry
            )
        );
          // Is there pending geometry from a call to
          // xdg_surface.set_window_geometry()
        if ( ! same_xdg_geometry ) {
            xdg_surface->visible_geometry = xdg_surface->pending_geometry;
            xdg_surface->pending_geometry = invalid_wld_geometry;
        }
        */
    }

    if (( old_wl_buffer_width  != new_wl_buffer->width ) ||
        ( old_wl_buffer_height != new_wl_buffer->height)
    ) {
          // See (*) below.
        twc_window_update_surface_size( wl_surface );
    }

    return;

    // (*) Warning: This commit routine is re-entrant.  When
    // twc_window_update_surface_size() is called, all the processing
    // for the subject surface is complete.  If the dimension has
    // changed, one or more agents may be notified.  For example, the
    // deco stylist.  In response, these agents may alter their
    // surface(s).  That would result in subsequent surface commits.
    // Since the BIC server does not enqueue incoming requests from
    // BIC clients, this routine could be callled again before
    // exiting.
};

extern
int
wl_surface_add_listener(
    struct wl_surface                *wl_surface,
    const struct wl_surface_listener *wl_surface_listener,
    void                              *data
) {
    wl_surface->wl_surface_listener = *wl_surface_listener;
    wl_surface->data                =  data;

      // The DEFAULT or OVERLAY surfaces are already mapped and may
      // have pointer location.  If so, emit an enter event.
    if ( SURFACE_IS_PROXY(wl_surface) ) {
        twc_seat_enter_proxy_on_new_listener( wl_surface );
    }

    return ADD_LISTENER_SUCCESS;
}

 // ----------
 // wl_pointer

extern
int
wl_pointer_add_listener(
    struct wl_pointer                *wl_pointer,
    const struct wl_pointer_listener *wl_pointer_listener,
    void                             *data
) {
    wl_pointer->wl_pointer_listener = *wl_pointer_listener;
    wl_pointer->data                =  data;

    return 0;
}

extern
void
wl_pointer_set_cursor(
    struct wl_pointer *wl_pointer,
    uint32_t           serial,
    struct wl_surface *surface,     // parameter name used in protocol header
    int32_t            hotspot_x,
    int32_t            hotspot_y
) {
    // See documentation for in wl_pointer in the Wayland XML file.

      // Promote surface to cursor role if appropriate.
    if (surface->type == ST_NONE    ) {
        surface->type =  ST_CURSOR;
    }

      // If wrong role or no image, do not use surface for cursor.
    if ( SURFACE_IS_EMPTY( surface ) ||
         surface->type != ST_CURSOR
    ) { return; }

    // Fix: The empty surface is supposed to result in a "hidden"
    // cursor, not preserving the existing one.

      // Replace the previous cursor surface.
    wcf_dsply_ctrl_set_cursor(
        // Fix: should first parameter be output_at(current_ptr_loc)?
        NULL,
        surface,
        hotspot_x,
        hotspot_y
    );

    return;
}

 // -----------
 // wl_keyboard

extern
int
wl_keyboard_add_listener(
    struct wl_keyboard                *wl_keyboard,
    const struct wl_keyboard_listener *wl_keyboard_listener,
    void                              *data
) {
    wl_keyboard->wl_keyboard_listener = *wl_keyboard_listener;
    wl_keyboard->data                 =  data;

    return ADD_LISTENER_SUCCESS;
}

 // -------
 // wl_seat

extern
struct wl_pointer*
wl_seat_get_pointer(
    struct wl_seat *wl_seat
) {
    struct bic_connection *bic_connection = (
        wl_container_of(wl_seat, bic_connection, wl_seat)
    );

    bic_connection->wl_seat.wl_pointer. enter_serial = 0;
    bic_connection->wl_seat.wl_pointer. leave_serial = 0;
    bic_connection->wl_seat.wl_pointer.button_serial = 0;

    return &bic_connection->wl_seat.wl_pointer;
}

extern
struct wl_keyboard*
wl_seat_get_keyboard(
    struct wl_seat *wl_seat
) {
    struct bic_connection *bic_connection = (
        wl_container_of(wl_seat, bic_connection, wl_seat)
    );

    bic_connection->wl_seat.wl_keyboard.    enter_serial = 0;
    bic_connection->wl_seat.wl_keyboard.    leave_serial = 0;
    bic_connection->wl_seat.wl_keyboard.      key_serial = 0;
    bic_connection->wl_seat.wl_keyboard.modifiers_serial = 0;

    bic_connection->wl_seat.default_seat = twc_seat_get_default_seat();

    return &bic_connection->wl_seat.wl_keyboard;
}

extern
int
wl_seat_add_listener(
    struct wl_seat                *wl_seat,
    const struct wl_seat_listener *wl_seat_listener,
    void                          *data
) {
    // No-op

    return ADD_LISTENER_SUCCESS;
}

 // ---------
 // wl_output

extern
int
wl_output_add_listener(
    struct wl_output                *wl_output,
    const struct wl_output_listener *wl_output_listener,
    void                            *data
) {
    wl_output->wl_output_listener = *wl_output_listener;
    wl_output->data               =  data;

    return ADD_LISTENER_SUCCESS;
}

extern
void
wl_output_destroy(
    struct wl_output *wl_output
) {
    // Fix: Is there more ToDo?

    bic_connection_output_list_remove( wl_output );

    free(wl_output);

    return;
}

 // --------------
 // twc_shm_helper
 //
 // These twc_shm_helper routines are intended to aid clients with shm
 // pool management.  These routines have two versions, one for
 // external clients and one for BiCs.  The external version uses
 // shm_open() and mmap() to establish shared memory.  The internal
 // BiC version (below) uses malloc() for shared memory.  See also
 // twc_helper.h.  The routines below are purposely defined in this
 // file so that the use of WL_SHM_BY_MALLOC is restricted to only one
 // file.

#include <twc/twc_helper.h>     // Our API to ensure consistancy

extern
void
twc_shm_helper_pool_create(
    struct wl_shm              *wl_shm,
    int32_t                     shm_pool_size,
    struct twc_shm_helper_pool *twc_shm_helper_pool
) {
    int shm_pool_fd = -1;

    struct wl_shm_pool *wl_shm_pool = (
        wl_shm_create_pool(
            WL_SHM_BY_MALLOC,   // shm pool will be type SPT_MALLOC
            shm_pool_fd,        // the fd parameter is ignored
            shm_pool_size
        )
    );

    twc_shm_helper_pool->shm_pool_fd   = shm_pool_fd;
    twc_shm_helper_pool->shm_pool_base = wl_shm_pool->shm_pool_base;
    twc_shm_helper_pool->shm_pool_size = shm_pool_size;
    twc_shm_helper_pool->wl_shm_pool   = wl_shm_pool;

    return;
}

extern
void
twc_shm_helper_pool_unmap(
    struct twc_shm_helper_pool *twc_shm_helper_pool
) {
    // The shm_pool type can only be SPT_MALLOC.  The shared memory
    // will be freed when the last buffer is destroyed.  See
    // check_wl_shm_pool().

    return;
}
