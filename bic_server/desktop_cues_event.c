
  // Protocols
#include <desktop-cues-v1-client-protocol.h>    // Our API to ensure consistancy

  // APIs
#include "desktop_cues_event.h"   // Our API to ensure consistancy
#include "desktop_cues_bic.h"

 // --------------------
 // xx_desktop_cues_base

extern
void
xx_desktop_cues_base_v1_event_revoked(
    struct xx_desktop_cues_base_v1 *xx_desktop_cues_base_v1
) {
    xx_desktop_cues_base_v1->xx_desktop_cues_base_v1_listener.revoked(
         xx_desktop_cues_base_v1->data,
         xx_desktop_cues_base_v1
    );

    return;
}

 // --------------
 // xx_desktop_cue

extern
void
xx_desktop_cue_event_take(
    struct xx_desktop_cue *xx_desktop_cue
) {
    xx_desktop_cue->xx_desktop_cue_listener.take(
         xx_desktop_cue->data,
         xx_desktop_cue
    );

    return;
}
