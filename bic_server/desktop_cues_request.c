
  // System
#include <stdlib.h>     // calloc, free
#include <string.h>     // strncpy

  // Wayland
#include <wayland-util.h>   // wl_list

  // Protocols
#include <desktop-cues-v1-client-protocol.h>    // Our API to ensure consistancy

  // TWC
#include <twc/desktop_cues_inert.h>

  // APIs
#include "wm_agent_event.h"

  // Imported data types
#include "desktop_cues_bic.h"
#include "wayland_bic.h"
#include "twc_window-impl.h"

 // -----------------------
 // xx_desktop_cues_base_v1

extern
void
xx_desktop_cues_base_v1_destroy(
    struct xx_desktop_cues_base_v1 *xx_desktop_cues_base_v1
) {
    // Fix: ToDo

    return;
}

extern
struct xx_desktop_cue*
xx_desktop_cues_base_v1_create_cue(
    struct xx_desktop_cues_base_v1 *xx_desktop_cues_base_v1,
    struct xdg_toplevel            *xdg_toplevel,
    const  char                    *cue_name
) {
    struct wl_surface *wl_surface = (
        wl_container_of(xdg_toplevel, wl_surface, xdg_toplevel)
    );

    struct xx_wm_decoration *xx_wm_decoration = (
        &wl_surface->twc_view->xx_wm_decoration
    );

    struct xx_desktop_cue *xx_desktop_cue = (
        calloc( 1, sizeof(struct  xx_desktop_cue) )
    );

      // Initialized by compiler.  Incremented for each new desktop_cue.
    static uint32_t numeric_name = 0;
    numeric_name++;
    xx_desktop_cue->numeric_name = numeric_name;

    strncpy(xx_desktop_cue->cue_name, cue_name, MAX_CUE_NAME_LENGTH);
    xx_desktop_cue->cue_name[MAX_CUE_NAME_LENGTH - 1] = '\0';

    xx_desktop_cue->xx_desktop_cue_listener = inert_xx_desktop_cue_listener;

    wl_list_insert( &xx_wm_decoration->desktop_cue_list, &xx_desktop_cue->link );

    xx_wm_decoration_emit_new_cue(
        xx_wm_decoration,
        numeric_name,
        cue_name
    );

    return xx_desktop_cue;
}

extern
int
xx_desktop_cues_base_v1_add_listener(
    struct       xx_desktop_cues_base_v1          *xx_desktop_cues_base_v1,
    const struct xx_desktop_cues_base_v1_listener *xx_desktop_cues_base_v1_listener,
    void                                           *data
) {
    xx_desktop_cues_base_v1->data = data;

    xx_desktop_cues_base_v1->xx_desktop_cues_base_v1_listener = (
        *xx_desktop_cues_base_v1_listener
    );

    return ADD_LISTENER_SUCCESS;
}

 // --------------
 // xx_desktop_cue

extern
void
xx_desktop_cue_destroy(
    struct xx_desktop_cue *xx_desktop_cue
) {
    wl_list_remove( &xx_desktop_cue->link );

    free(xx_desktop_cue);

    return;
}

extern
int
xx_desktop_cue_add_listener(
    struct       xx_desktop_cue          *xx_desktop_cue,
    const struct xx_desktop_cue_listener *xx_desktop_cue_listener,
    void                                 *data
) {
    xx_desktop_cue->data = data;

    xx_desktop_cue->xx_desktop_cue_listener = (
        *xx_desktop_cue_listener
    );

    return ADD_LISTENER_SUCCESS;
}
