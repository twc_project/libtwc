#ifndef XDG_DECORATION_BIC_H
#define XDG_DECORATION_BIC_H

  // System
#include <stdbool.h>

  // Protocols
#include <xdg-decoration-unstable-v1-client-protocol.h>

  // Opaque types
struct wcf_xdg_surface;

struct zxdg_decoration_manager_v1 {
    // A zxdg_decoration_manager_v1 has no events
};

  // The enum named zxdg_toplevel_decoration_v1_mode has only two
  // values.
enum decoration_mode {
    DM_None        = 0,     // No client preference
    DM_Client_Side = ZXDG_TOPLEVEL_DECORATION_V1_MODE_CLIENT_SIDE,  // 1
    DM_Server_Side = ZXDG_TOPLEVEL_DECORATION_V1_MODE_SERVER_SIDE,  // 2
};

struct zxdg_toplevel_decoration_v1 {
    void *data;
    struct zxdg_toplevel_decoration_v1_listener xdg_decoration_listener;

    bool is_listener_set;

    enum decoration_mode client_preference;
    enum decoration_mode response_to_client;
};

#endif // XDG_DECORATION_BIC_H
