#ifndef XDG_DECORATION_EVENT_H
#define XDG_DECORATION_EVENT_H

  // System
#include <stdint.h>  // uint32_t ...NULL

  // Opaque types
struct zxdg_toplevel_decoration_v1;


 // --------------
 // xdg_decoration

extern
void
xdg_decoration_configure(
    struct zxdg_toplevel_decoration_v1 *xdg_toplevel_decoration,
    uint32_t                            mode
);

#endif  // XDG_DECORATION_EVENT_H
