#ifndef WM_SERVICE_EVENT_H
#define WM_SERVICE_EVENT_H

  // System
#include <stdint.h>     // uint32_t

  // Opaque types
struct xx_zwm_service;

 // ----------------------
 // xx_zwm_service_base_v1
 //
 //   There are no events

 // --------------
 // xx_zwm_service

extern
void
xx_zwm_service_emit_operation(
    struct xx_zwm_service *xx_zwm_service,
    uint32_t               operation
);

 // -------------------
 // xx_zwm_service_window
 //
 //   There are no events

#endif  // WM_SERVICE_EVENT_H
