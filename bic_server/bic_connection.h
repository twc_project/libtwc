#ifndef BIC_CONNECTION_H
#define BIC_CONNECTION_H

  // Wayland
#include <wayland-util.h>   // wl_list

  // Protocols
#include <wm-agent-v1-client-protocol.h>   // agent_id bitmask

 /*
 ** In "Should You Write a Wayland Compositor?", Tudor Roman writes(*)
 **
 **   "... writing a compositor is somewhat complex"
 **
 ** since
 **
 **   "... everything is related to everything"
 **
 ** (*) https://tudorr.ro/blog/technical/2021/01/26/the-wayland-experience/
 */

  // Imported data types
#include "wayland_bic.h"
#include "xdg_shell_bic.h"
#include "xdg_decoration_bic.h"
#include "wm_agent_bic.h"
#include "wm_service_bic.h"
#include "desktop_cues_bic.h"

#define ADD_AGENT(         c,a) ( (c)->agent_mask |= (a) )
#define IS_AGENT_THIS(     c,a) ( (c)->agent_mask &  (a) )
#define IS_EXACTLY_AGENT(  c,a) ( (c)->agent_mask == (a) )
#define IS_NOT_AGENT(      c  ) ( (c)->agent_mask == WM_AGENT_BASE_V1_AGENT_ID_NONE     )
#define IS_AGENT_ANY(      c  ) ( (c)->agent_mask != WM_AGENT_BASE_V1_AGENT_ID_NONE     )
#define IS_AGENT_MENU(     c  ) ( (c)->agent_mask &  WM_AGENT_BASE_V1_AGENT_ID_MENU     )
#define IS_AGENT_WKSP(     c  ) ( (c)->agent_mask &  WM_AGENT_BASE_V1_AGENT_ID_WORKSPACE)
#define IS_AGENT_TASK(     c  ) ( (c)->agent_mask &  WM_AGENT_BASE_V1_AGENT_ID_TASK     )

#define bic_connection_output_list_insert(b,w) \
    wl_list_insert(&(b)->wl_output_list, &((w)->link))

#define bic_connection_output_list_remove(w) \
    wl_list_remove(&((w)->link))

#define bic_connection_output_list_for_each(b,w) \
    wl_list_for_each( (w), &(b)->wl_output_list, link)

  // Built-in Client connection
struct bic_connection {
    struct wl_list link;

    // This struct represents a connection between a Built-in Client
    // and the server.  It contains storage for global interfaces
    // bound by the client.

    // ----------------
    // wayland protocol

    struct wl_display    wl_display;    // client visible handle
    struct wl_registry   wl_registry;
    struct wl_compositor wl_compositor;
    struct wl_shm        wl_shm;

    struct wl_list       wl_output_list;    // multiple outputs are allowed
    struct wl_seat       wl_seat;           // Fix: multiple seats are
                                            // allowed, should be a
                                            // list.
    // ------------------
    // xdg_shell protocol

    struct xdg_wm_base xdg_wm_base;

    // -----------------------
    // xdg_decoration protocol

    struct zxdg_decoration_manager_v1 xdg_decoration_manager;

    // ------------------
    // wm_agent protocol

    struct wm_agent_base_v1 wm_agent_base_v1;

    enum wm_agent_base_v1_agent_id agent_mask;     // bitmask of agents
                                                    // granted to connection

    // -------------------
    // mw_service protocol

    struct xx_zwm_service_base_v1 xx_zwm_service_base_v1;

    // ------------------------
    // xx_desktop_cues protocol

    struct xx_desktop_cues_base_v1 xx_desktop_cues_base_v1;

      // Each window/workspace maintains a list of interested clients,
      // ie, connections.
    struct wl_list    window_interest_link;
    struct wl_list workspace_interest_link;

      // Each connection maintains a list of desktop services.
    struct wl_list desktop_service_list;
};

#endif  // BIC_CONNECTION_H
