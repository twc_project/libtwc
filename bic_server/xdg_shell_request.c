  // System
#include <string.h>  // strncpy

  // Wayland
#include <wayland-util.h>  // wl_container_of, wl_list

  // Protocols
#include <xdg-shell-client-protocol.h>  // Our API to ensure consistancy

  // TWC
#include <twc/xdg_shell_inert.h>
#include <twc/xdg_decoration_inert.h>
#include <twc/twc_log.h>

  // APIs
#include "wayland_pre-defined.h"
#include "twc_surface.h"
#include "twc_op.h"
#include "wm_agent_event_wrappers.h"
#include "twc_window.h"
#include "xdg_decoration_event.h"

  // Imported data types
#include "wayland_bic.h"
#include "twc_window-impl.h"
#include "bic_connection.h"

 // -----------
 // xdg_wm_base

extern
void
xdg_wm_base_destroy(
    struct xdg_wm_base *xdg_wm_base
) {
    return;
}

extern
struct xdg_positioner*
xdg_wm_base_create_positioner(
    struct xdg_wm_base *xdg_wm_base
) {
    return NULL;
}

extern
struct xdg_surface*
xdg_wm_base_get_xdg_surface(
    struct xdg_wm_base *xdg_wm_base,
    struct wl_surface  *wl_surface
) {
    if ( wl_surface->type != ST_NONE ) {
        // Protocol error
        return NULL;
    }

    wl_surface->type = ST_XDG_SURFACE;

    struct xdg_surface *xdg_surface = &wl_surface->xdg_surface;

    xdg_surface->xdg_surface_listener = inert_xdg_surface_listener;

    xdg_surface->has_initial_commit        = false;
    xdg_surface->ack_configure_outstanding = false;

    xdg_surface->is_toplevel = false;
    xdg_surface->is_popup    = false;

    return xdg_surface;
}

extern
void
xdg_wm_base_pong(
    struct xdg_wm_base *xdg_wm_base,
    uint32_t            serial
) {
    return;
}

 // -----------
 // xdg_surface

extern
void
xdg_surface_destroy(
    struct xdg_surface *xdg_surface
) {
    struct wl_surface *wl_surface = (
        wl_container_of(xdg_surface, wl_surface, xdg_surface)
    );

      // According to the description for wl_surface in the Wayland
      // Protocol, roles are permanent.
    if ( SURFACE_HAS_ROLE(wl_surface) ) {
        // Error: defunct_role_object
        return;
    }

    wl_surface->type = ST_NONE;

    return;
}

extern
struct xdg_toplevel*
xdg_surface_get_toplevel(
    struct xdg_surface *xdg_surface
) {
    struct wl_surface *wl_surface = (
        wl_container_of(xdg_surface, wl_surface, xdg_surface)
    );

    if ( wl_surface->type != ST_XDG_SURFACE &&
         ! xdg_surface->is_toplevel
    ) {
        // Protocol error
        return NULL;
    }

    wl_surface->type         = ST_XDG_TOPLEVEL;
    xdg_surface->is_toplevel = true;

    xdg_surface->has_initial_commit = false;

    struct xdg_toplevel *xdg_toplevel = &wl_surface->xdg_toplevel;

    xdg_toplevel->data = NULL;

    xdg_toplevel->xdg_toplevel_listener = inert_xdg_toplevel_listener;

    xdg_toplevel->maxsize_width          = 0;
    xdg_toplevel->maxsize_height         = 0;
    xdg_toplevel->minsize_width          = 0;
    xdg_toplevel->minsize_height         = 0;
    xdg_toplevel->pending_maxsize_width  = 0;
    xdg_toplevel->pending_maxsize_heigh  = 0;
    xdg_toplevel->pending_minsize_width  = 0;
    xdg_toplevel->pending_minsize_height = 0;

    for (int i =  XDG_TOPLEVEL_STATE_MAXIMIZED;
             i <= XDG_TOPLEVEL_STATE_TILED_BOTTOM;
             i++
    ) {
        xdg_toplevel->xdg_toplevel_states[i] = false;
    }

    xdg_toplevel->xdg_toplevel_decoration.data                    = NULL;
    xdg_toplevel->xdg_toplevel_decoration.xdg_decoration_listener = inert_xdg_decoration_listener;

    twc_window_create(wl_surface);

    return &wl_surface->xdg_toplevel;
}

extern
void
xdg_surface_ack_configure(
    struct xdg_surface *xdg_surface,
    uint32_t            serial
) {
    struct wl_surface *wl_surface = (
        wl_container_of(xdg_surface, wl_surface, xdg_surface)
    );

    xdg_surface->ack_configure_outstanding = false;

    return;
}

extern
int
xdg_surface_add_listener(
    struct xdg_surface                *xdg_surface,
    const struct xdg_surface_listener *xdg_surface_listener,
    void                              *data
) {
    xdg_surface->data                 =  data;
    xdg_surface->xdg_surface_listener = *xdg_surface_listener;

    return 0;
}

 // ------------
 // xdg_toplevel

extern
void
xdg_toplevel_destroy(
    struct xdg_toplevel *xdg_toplevel
) {
    struct wl_surface *wl_surface = (
        wl_container_of(xdg_toplevel, wl_surface, xdg_toplevel)
    );

    if (wl_surface->type != ST_XDG_TOPLEVEL) {
        return;
    }

    twc_surface_no_longer_mapped(wl_surface);

    twc_window_destroy(wl_surface->twc_view->twc_window);

    wl_surface->type = ST_XDG_SURFACE;
      // xdg_surface->is_toplevel remains true

    xdg_toplevel->xdg_toplevel_listener = inert_xdg_toplevel_listener;
    xdg_toplevel->data                  = NULL;

    xdg_toplevel->xdg_toplevel_decoration.data                    = NULL;
    xdg_toplevel->xdg_toplevel_decoration.xdg_decoration_listener = inert_xdg_decoration_listener;

    xdg_toplevel->xdg_toplevel_decoration.client_preference = DM_None;

    return;
}

extern
void
xdg_toplevel_set_title(
    struct xdg_toplevel *xdg_toplevel,
    const char          *title
) {
    struct wl_surface *wl_surface = (
        wl_container_of(xdg_toplevel, wl_surface, xdg_toplevel)
    );

    struct twc_window *twc_window = (
        wl_surface->twc_view->twc_window
    );

    strncpy(
        twc_window->title,
        title,
        MAX_TITLE_LENGTH
    );
    twc_window->title[MAX_TITLE_LENGTH - 1] = '\0';

    twc_window_apply_user_config(twc_window);

    xx_wm_window_update_title(twc_window);

    return;
};

extern
void
xdg_toplevel_set_app_id(
    struct xdg_toplevel *xdg_toplevel,
    const char          *app_id
) {
    struct wl_surface *wl_surface = (
        wl_container_of(xdg_toplevel, wl_surface, xdg_toplevel)
    );

    struct twc_window *twc_window = (
        wl_surface->twc_view->twc_window
    );

    strncpy(
        twc_window->app_id,
        app_id,
        MAX_APP_ID_LENGTH
    );
    twc_window->app_id[MAX_APP_ID_LENGTH - 1] = '\0';

    twc_window_apply_user_config(twc_window);

    xx_wm_window_update_app_id(twc_window);

    return;
};

extern
void
xdg_toplevel_move(
    struct xdg_toplevel *xdg_toplevel,
    struct wl_seat      *seat,
    uint32_t            serial
) {
    struct wl_surface *wl_surface = (
        wl_container_of(xdg_toplevel, wl_surface, xdg_toplevel)
    );

    struct twc_view *twc_view = (
        wl_surface->twc_view
    );

    twc_action_window_intrinsic(
        twc_view,
        XX_WM_WINDOW_INTRINSIC_MOVE
    );
};

extern
void
xdg_toplevel_resize(
    struct xdg_toplevel *xdg_toplevel,
    struct wl_seat      *seat,
    uint32_t            serial,
    uint32_t            edges
) {
    struct wl_surface *wl_surface = (
        wl_container_of(xdg_toplevel, wl_surface, xdg_toplevel)
    );

    twc_op_resize_view(
        wl_surface->twc_view,
        edges
    );
};

extern
void
xdg_toplevel_set_minimized(
    struct xdg_toplevel *xdg_toplevel
) {
    // Every compositor is completely free to interpret the meaning of
    // minimize.  We choose it to mean iconify.

    struct wl_surface *wl_surface = (
        wl_container_of(xdg_toplevel, wl_surface, xdg_toplevel)
    );

    twc_window_set_iconify_state(
        wl_surface->twc_view->twc_window,
        true
    );

    return;
};

extern
int
xdg_toplevel_add_listener(
    struct xdg_toplevel                *xdg_toplevel,
    const struct xdg_toplevel_listener *xdg_toplevel_listener,
    void                               *data
) {
    xdg_toplevel->data                  =  data;
    xdg_toplevel->xdg_toplevel_listener = *xdg_toplevel_listener;

    return 0;
}
