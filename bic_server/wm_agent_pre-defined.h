#ifndef WM_AGENT_PRE_DEFINED_H
#define WM_AGENT_PRE_DEFINED_H

  // Imported data types
#include "wm_agent_bic.h"

extern struct xx_wm_interactive_agent   null_xx_wm_interactive_agent;
#define  NULL_XX_WM_INTERACTIVE_AGENT (&null_xx_wm_interactive_agent)

extern struct xx_wm_menu_agent   null_xx_wm_menu_agent;
#define  NULL_XX_WM_MENU_AGENT (&null_xx_wm_menu_agent)

extern struct xx_wm_workspace_agent   null_xx_wm_workspace_agent;
#define  NULL_XX_WM_WORKSPACE_AGENT (&null_xx_wm_workspace_agent)

extern struct wm_agent_base_v1 initial_wm_agent_base_v1;

#endif // WM_AGENT_PRE_DEFINED_H
