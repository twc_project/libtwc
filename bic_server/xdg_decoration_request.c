
  // System
#include <stdlib.h>  // NULL, calloc, free

  // Protocols
#include <xdg-decoration-unstable-v1-client-protocol.h>

  // TWC
#include <twc/xdg_decoration_inert.h>
#include <twc/twc_log.h>

  // APIs
#include "xdg_decoration_event.h"
#include "twc_decoration.h"
#include "wayland_pre-defined.h"

  // Imported data types
#include "twc_window-impl.h"
#include "wayland_bic.h"
#include "xdg_shell_bic.h"
#include "xdg_decoration_bic.h"

extern
void
zxdg_decoration_manager_v1_destroy(
    struct zxdg_decoration_manager_v1 *xdg_decoration_manager
) {
    // Fix: ToDo

    return;
}

extern
struct zxdg_toplevel_decoration_v1*
zxdg_decoration_manager_v1_get_toplevel_decoration(
    struct zxdg_decoration_manager_v1 *xdg_decoration_manager,
    struct xdg_toplevel               *xdg_toplevel
) {
    //LOG_INFO("zxdg_decoration_manager_v1_get_toplevel_decoration");
    struct zxdg_toplevel_decoration_v1 *xdg_toplevel_decoration = (
        &xdg_toplevel->xdg_toplevel_decoration
    );

    xdg_toplevel_decoration->data                    = NULL;
    xdg_toplevel_decoration->xdg_decoration_listener = inert_xdg_decoration_listener;

    xdg_toplevel_decoration->is_listener_set   = false;
    xdg_toplevel_decoration->client_preference = DM_None;

    return xdg_toplevel_decoration;
}

extern
void
zxdg_toplevel_decoration_v1_destroy(
    struct zxdg_toplevel_decoration_v1 *xdg_toplevel_decoration
) {
    xdg_toplevel_decoration->data = NULL;
    xdg_toplevel_decoration->xdg_decoration_listener = inert_xdg_decoration_listener;

    xdg_toplevel_decoration->is_listener_set   = false;
    xdg_toplevel_decoration->client_preference = DM_None;

    return;
}

static
void
xdg_toplevel_decoration_set_mode(
    struct zxdg_toplevel_decoration_v1 *xdg_toplevel_decoration,
    enum   decoration_mode              mode
) {
    if ( xdg_toplevel_decoration->client_preference == mode ) { return; }

    xdg_toplevel_decoration->client_preference = mode;

    struct xdg_toplevel *xdg_toplevel = (
        wl_container_of(xdg_toplevel_decoration, xdg_toplevel, xdg_toplevel_decoration)
    );

    struct wl_surface *wl_surface = (
        wl_container_of(xdg_toplevel, wl_surface, xdg_toplevel)
    );

    struct twc_view *twc_view = wl_surface->twc_view;

    enum decoration_mode response_to_client = (
        twc_update_decoration_mode(
            twc_view,
            mode
        )
    );

    xdg_toplevel_decoration->response_to_client = response_to_client;

      // Tell the client how it should decorate
    xdg_decoration_configure(
        xdg_toplevel_decoration,
        response_to_client
    );

    return;
}

extern
void
zxdg_toplevel_decoration_v1_set_mode(
    struct zxdg_toplevel_decoration_v1 *xdg_toplevel_decoration,
    uint32_t                            mode
) {
    //LOG_INFO("zxdg_toplevel_decoration_v1_set_mode %d", mode);

    xdg_toplevel_decoration_set_mode(
        xdg_toplevel_decoration,
        mode
    );

    return;
}

extern
void
zxdg_toplevel_decoration_v1_unset_mode(
    struct zxdg_toplevel_decoration_v1 *xdg_toplevel_decoration
) {
    xdg_toplevel_decoration_set_mode(
        xdg_toplevel_decoration,
         DM_None
    );

    return;
}

extern
int
zxdg_toplevel_decoration_v1_add_listener(
    struct zxdg_toplevel_decoration_v1                *xdg_toplevel_decoration,
    const struct zxdg_toplevel_decoration_v1_listener *xdg_decoration_listener,
    void                                              *data
) {
    /*
    ** According to the description for get_toplevel_decoration in the
    ** xdg_decoration_unstable_v1 protocol, the client must get the
    ** decoration object BEFORE a buffer is 'attached or committed'.
    ** The client must also wait for an
    ** xdg_toplevel_decoration.configure event from the compositor
    ** BEFORE buffer attaching/commiting a buffer.
    */

    //LOG_INFO("zxdg_toplevel_decoration_v1_add_listener");

    xdg_toplevel_decoration->data                    =  data;
    xdg_toplevel_decoration->xdg_decoration_listener = *xdg_decoration_listener;

    if ( xdg_toplevel_decoration->is_listener_set ) { return ADD_LISTENER_SUCCESS; }

    xdg_toplevel_decoration->is_listener_set = true;

    struct xdg_toplevel *xdg_toplevel = (
        wl_container_of(xdg_toplevel_decoration, xdg_toplevel, xdg_toplevel_decoration)
    );

    struct wl_surface *wl_surface = (
        wl_container_of(xdg_toplevel, wl_surface, xdg_toplevel)
    );

    struct twc_view *twc_view = wl_surface->twc_view;

    enum decoration_mode response_to_client = (
        twc_update_decoration_mode(
            twc_view,
            DM_None
        )
    );

    xdg_decoration_configure(
        xdg_toplevel_decoration,
        response_to_client
    );

    return ADD_LISTENER_SUCCESS;
}
