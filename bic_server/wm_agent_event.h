#ifndef WM_AGENT_EVENT_H
#define WM_AGENT_EVENT_H

  // System
#include <stdbool.h>
#include <stdint.h>     // uint32_t

  // Wayland
#include <wayland-util.h>   // wl_fixed_t

  // Protocols
#include <wm-agent-v1-client-protocol.h>    // enum xx_wm_window_iconify_state
#include <desktop-cues-v1-client-protocol.h>

  // Imported data types
#include "wm_agent_bic.h"
#include "wm_service_bic.h"
#include "desktop_cues_bic.h"

  // Opaque types
struct wl_interface;
struct wl_output;

 // -----------------
 // wm_agent_base_v1

extern
void
wm_agent_base_v1_emit_new_agent_action(
    enum wm_agent_base_v1_agent_id agent_id,
    const  char                   *name,
    uint32_t                       action_id
);

static inline
void
wm_agent_base_v1_emit_new_agent(
    struct wm_agent_base_v1   *wm_agent_base_v1,
    const struct wl_interface *wl_interface
) {
  // For Exclusive Singletons, the object being bound is unambiguous.
  // Singletons are never removed.
#   define EXCLUSICE_SINGLETON_NUMERIC_NAME 0

    wm_agent_base_v1->wm_agent_base_v1_listener.new_agent(
        wm_agent_base_v1->data,
        wm_agent_base_v1,
        EXCLUSICE_SINGLETON_NUMERIC_NAME,
        wl_interface->name,
        wl_interface->version
    );
}

static inline
void
wm_agent_base_v1_emit_restart(
    struct wm_agent_base_v1 *wm_agent_base_v1
) {
    wm_agent_base_v1->wm_agent_base_v1_listener.restart(
        wm_agent_base_v1->data,
        wm_agent_base_v1
    );
}

 // ------------------------
 // xx_wm_interactive_agent

static inline
void
xx_wm_interactive_agent_emit_dismiss(
    struct xx_wm_interactive_agent *xx_wm_interactive_agent
) {
    xx_wm_interactive_agent->xx_wm_interactive_agent_listener.dismiss(
        xx_wm_interactive_agent->data,
        xx_wm_interactive_agent
    );

    return;
}

static inline
void
xx_wm_interactive_agent_emit_annex_revoked(
    struct xx_wm_interactive_agent *xx_wm_interactive_agent
) {
    xx_wm_interactive_agent->xx_wm_interactive_agent_listener.annex_revoked(
        xx_wm_interactive_agent->data,
        xx_wm_interactive_agent
    );

    return;
}

static inline
void
xx_wm_interactive_agent_emit_select(
    struct xx_wm_interactive_agent *xx_wm_interactive_agent
) {
    xx_wm_interactive_agent->xx_wm_interactive_agent_listener.select(
        xx_wm_interactive_agent->data,
        xx_wm_interactive_agent
    );

    return;
}

static inline
void
xx_wm_interactive_agent_emit_track(
    struct xx_wm_interactive_agent *xx_wm_interactive_agent
) {
    xx_wm_interactive_agent->xx_wm_interactive_agent_listener.track(
        xx_wm_interactive_agent->data,
        xx_wm_interactive_agent
    );

    return;
}

static inline
void
xx_wm_interactive_agent_emit_stop(
    struct xx_wm_interactive_agent *xx_wm_interactive_agent
) {
    xx_wm_interactive_agent->xx_wm_interactive_agent_listener.stop(
        xx_wm_interactive_agent->data,
        xx_wm_interactive_agent
    );

    return;
}

 // ------------------------
 // xx_wm_decoration_stylist

static inline
void
xx_wm_decoration_stylist_emit_dismiss(
    struct xx_wm_decoration_stylist *xx_wm_decoration_stylist
) {
    xx_wm_decoration_stylist->xx_wm_decoration_stylist_listener.dismiss(
        xx_wm_decoration_stylist->data,
        xx_wm_decoration_stylist
    );

    return;
}

 // ----------------
 // xx_wm_decoration
 //

static inline
void
xx_wm_decoration_emit_new_cue(
    struct xx_wm_decoration *xx_wm_decoration,
    uint32_t                 numeric_name,
    const  char             *cue_name
) {
    xx_wm_decoration->xx_wm_decoration_listener.new_cue(
        xx_wm_decoration->data,
        xx_wm_decoration,
        numeric_name,
        cue_name
    );

    return;
}

 // -------------------------
 // xx_desktop_cue_controller
 //

static inline
void
xx_desktop_cue_controller_emit_abandon(
    struct xx_desktop_cue_controller *xx_desktop_cue_controller
) {
    xx_desktop_cue_controller->xx_desktop_cue_controller_listener.abandon(
        xx_desktop_cue_controller->data,
        xx_desktop_cue_controller
    );

    return;
}

 // ------------------
 // xx_wm_icon_stylist

static inline
void
xx_wm_icon_stylist_emit_dismiss(
    struct xx_wm_icon_stylist *xx_wm_icon_stylist
) {
    xx_wm_icon_stylist->xx_wm_icon_stylist_listener.dismiss(
        xx_wm_icon_stylist->data,
        xx_wm_icon_stylist
    );

    return;
}

 // ----------
 // xx_wm_icon

static inline
void
xx_wm_icon_emit_xdg_toplevel_icon(
    struct xx_wm_icon *xx_wm_icon,
    const  char       *icon_name
) {
    xx_wm_icon->xx_wm_icon_listener.xdg_toplevel_icon(
        xx_wm_icon->data,
        xx_wm_icon,
        icon_name
    );

    return;
}

static inline
void
xx_wm_icon_emit_xdg_toplevel_icon_buffer(
    struct xx_wm_icon *xx_wm_icon,
    int32_t            size,
    int32_t            scale
) {
    xx_wm_icon->xx_wm_icon_listener.xdg_toplevel_icon_buffer(
        xx_wm_icon->data,
        xx_wm_icon,
        size,
        scale
    );

    return;
}

 // ----------------
 // xx_wm_menu_agent

static inline
void
xx_wm_menu_agent_emit_dismiss(
    struct xx_wm_menu_agent *xx_wm_menu_agent
) {
    xx_wm_menu_agent->xx_wm_menu_agent_listener.dismiss(
        xx_wm_menu_agent->data,
        xx_wm_menu_agent
    );

    return;
}

static inline
void
xx_wm_menu_agent_emit_annex_revoked(
    struct xx_wm_menu_agent *xx_wm_menu_agent
) {
    xx_wm_menu_agent->xx_wm_menu_agent_listener.annex_revoked(
        xx_wm_menu_agent->data,
        xx_wm_menu_agent
    );

    return;
}

static inline
void
xx_wm_menu_agent_emit_new_service(
    struct xx_wm_menu_agent *xx_wm_menu_agent,
    uint32_t                 numeric_name,
    uint32_t                 version,
    const char              *provider_name,
    const char              *service_name
) {
    xx_wm_menu_agent->xx_wm_menu_agent_listener.new_service(
        xx_wm_menu_agent->data,
        xx_wm_menu_agent,
        numeric_name,
        version,
        provider_name,
        service_name
    );

    return;
}

static inline
void
xx_wm_menu_agent_emit_keyboard_shortcut(
    struct xx_wm_menu_agent *xx_wm_menu_agent,
    uint32_t                 shortcut_key,
    uint32_t                 shortcut_key_state,
    uint32_t                 mods_depressed,
    uint32_t                 shortcut_mod_state
) {
    xx_wm_menu_agent->xx_wm_menu_agent_listener.keyboard_shortcut(
        xx_wm_menu_agent->data,
        xx_wm_menu_agent,
        shortcut_key,
        shortcut_key_state,
        mods_depressed,
        shortcut_mod_state
    );

    return;
}

 // ---------------------
 // xx_wm_workspace_agent

static inline
void
xx_wm_workspace_agent_emit_dismiss(
    struct xx_wm_workspace_agent *xx_wm_workspace_agent
) {
    xx_wm_workspace_agent->xx_wm_workspace_agent_listener.dismiss(
        xx_wm_workspace_agent->data,
        xx_wm_workspace_agent
    );

    return;
}

 // ----------------
 // xx_wm_task_agent

static inline
void
xx_wm_task_agent_emit_dismiss(
    struct xx_wm_task_agent *xx_wm_task_agent
) {
    xx_wm_task_agent->xx_wm_task_agent_listener.dismiss(
        xx_wm_task_agent->data,
        xx_wm_task_agent
    );

    return;
}

 // -------------------
 // xx_wm_window_registry

static inline
void
xx_wm_window_registry_emit_new(
    struct xx_wm_window_registry *xx_wm_window_registry,
    uint32_t                      name,
    uint32_t                      version
) {
    xx_wm_window_registry->xx_wm_window_registry_listener.new(
        xx_wm_window_registry->data,
        xx_wm_window_registry,
        name,
        version
    );

    return;
}

 // ------------
 // xx_wm_window

static inline
void
xx_wm_window_emit_iconify_state(
    struct xx_wm_window            *xx_wm_window,
    enum xx_wm_window_iconify_state iconify_state
) {
    xx_wm_window->xx_wm_window_listener.iconify_state(
        xx_wm_window->data,
        xx_wm_window,
        iconify_state
    );

    return;
}

static inline
void
xx_wm_window_emit_display_state(
    struct xx_wm_window            *xx_wm_window,
    enum xx_wm_window_display_state display_state
) {
    xx_wm_window->xx_wm_window_listener.display_state(
        xx_wm_window->data,
        xx_wm_window,
        display_state
    );

    return;
}

static inline
void
xx_wm_window_emit_app_id(
    struct xx_wm_window *xx_wm_window,
    char                *app_id
) {
    xx_wm_window->xx_wm_window_listener.app_id(
        xx_wm_window->data,
        xx_wm_window,
        app_id
    );

    return;
}

static inline
void
xx_wm_window_emit_title(
    struct xx_wm_window *xx_wm_window,
    char                *title
) {
    xx_wm_window->xx_wm_window_listener.title(
        xx_wm_window->data,
        xx_wm_window,
        title
    );

    return;
}

static inline
void
xx_wm_window_emit_location(
    struct xx_wm_window *xx_wm_window,
    wl_fixed_t           layout_x,
    wl_fixed_t           layout_y
) {
    xx_wm_window->xx_wm_window_listener.location(
        xx_wm_window->data,
        xx_wm_window,
        layout_x,
        layout_y
    );

    return;
}

static inline
void
xx_wm_window_emit_icon_location(
    struct xx_wm_window *xx_wm_window,
    wl_fixed_t           layout_x,
    wl_fixed_t           layout_y
) {
    xx_wm_window->xx_wm_window_listener.icon_location(
        xx_wm_window->data,
        xx_wm_window,
        layout_x,
        layout_y
    );

    return;
}

static inline
void
xx_wm_window_emit_dimension(
    struct xx_wm_window *xx_wm_window,
    uint32_t             width,
    uint32_t             height
) {
    xx_wm_window->xx_wm_window_listener.dimension(
        xx_wm_window->data,
        xx_wm_window,
        width,
        height
    );

    return;
}

static inline
void
xx_wm_window_emit_icon_dimension(
    struct xx_wm_window *xx_wm_window,
    uint32_t             width,
    uint32_t             height
) {
    // Fix: Is this necessary??  Only for switcher map mode.  Will the
    // wksp mgr ever care about icon location or dimension??
    return;
}

static inline
void
xx_wm_window_emit_occupancy(
    struct xx_wm_window *xx_wm_window,
    uint32_t             occupation
) {
    xx_wm_window->xx_wm_window_listener.occupancy(
        xx_wm_window->data,
        xx_wm_window,
        occupation
    );

    return;
}

static inline
void
xx_wm_window_emit_enter_output(
    struct xx_wm_window *xx_wm_window,
    struct wl_output    *wl_output
) {
    xx_wm_window->xx_wm_window_listener.enter_output(
        xx_wm_window->data,
        xx_wm_window,
        wl_output
    );

    return;
}

static inline
void
xx_wm_window_emit_pointer_focus(
    struct xx_wm_window      *xx_wm_window,
    enum   xx_wm_window_focus focus
) {
    xx_wm_window->xx_wm_window_listener.pointer_focus(
        xx_wm_window->data,
        xx_wm_window,
        focus
    );

    return;
}

static inline
void
xx_wm_window_emit_keyboard_focus(
    struct xx_wm_window      *xx_wm_window,
    enum   xx_wm_window_focus focus
) {
    xx_wm_window->xx_wm_window_listener.keyboard_focus(
        xx_wm_window->data,
        xx_wm_window,
        focus
    );

    return;
}

static inline
void
xx_wm_window_emit_selection_focus(
    struct xx_wm_window      *xx_wm_window,
    enum   xx_wm_window_focus focus
) {
    xx_wm_window->xx_wm_window_listener.selection_focus(
        xx_wm_window->data,
        xx_wm_window,
        focus
    );

    return;
}

static inline
void
xx_wm_window_emit_thumbnail_max_size(
    struct xx_wm_window *xx_wm_window,
    uint32_t             width,
    uint32_t             height
) {
    xx_wm_window->xx_wm_window_listener.thumbnail_max_size(
        xx_wm_window->data,
        xx_wm_window,
        width,
        height
    );

    return;
}

static inline
void
xx_wm_window_emit_action(
    struct xx_wm_window           *xx_wm_window,
    enum wm_agent_base_v1_agent_id agent_id,
    uint32_t                       action_id
) {
    xx_wm_window->xx_wm_window_listener.action(
        xx_wm_window->data,
        xx_wm_window,
        agent_id,
        action_id
    );

    return;
}

static inline
void
xx_wm_window_emit_expired(
    struct xx_wm_window *xx_wm_window
) {
    xx_wm_window->xx_wm_window_listener.expired(
        xx_wm_window->data,
        xx_wm_window
    );

    return;
}

 // ------------------------
 // xx_wm_workspace_registry

static inline
void
xx_wm_workspace_registry_emit_new(
    struct xx_wm_workspace_registry *xx_wm_workspace_registry,
    uint32_t                         name,
    uint32_t                         version
) {
    xx_wm_workspace_registry->xx_wm_workspace_registry_listener.new(
        xx_wm_workspace_registry->data,
        xx_wm_workspace_registry,
        name,
        version
    );
    return;
}

 // ---------------
 // xx_wm_workspace

static inline
void
xx_wm_workspace_emit_name(
    struct xx_wm_workspace *xx_wm_workspace,
    char                   *name
) {
    xx_wm_workspace->xx_wm_workspace_listener.name(
        xx_wm_workspace->data,
        xx_wm_workspace,
        name
    );

    return;
}

static inline
void
xx_wm_workspace_emit_expired(
    struct xx_wm_workspace *xx_wm_workspace
) {
    xx_wm_workspace->xx_wm_workspace_listener.expired(
        xx_wm_workspace->data,
        xx_wm_workspace
    );

    return;
}

static inline
void
xx_wm_workspace_emit_active(
    struct xx_wm_workspace *xx_wm_workspace,
    struct wl_output     *wl_output
) {
    xx_wm_workspace->xx_wm_workspace_listener.active(
        xx_wm_workspace->data,
        xx_wm_workspace,
        wl_output
    );

    return;
}

 // -------------------------
 // xx_zwm_service_controller

static inline
void
xx_zwm_service_controller_emit_new_service_operation(
    struct xx_zwm_service_controller *xx_zwm_service_controller,
    const  char                      *name,
    uint32_t                          operation
) {
    xx_zwm_service_controller->xx_zwm_service_controller_listener.new_service_operation(
        xx_zwm_service_controller->data,
        xx_zwm_service_controller,
        name,
        operation
    );

    return;
}

static inline
void
xx_zwm_service_controller_emit_abandon(
    struct xx_zwm_service_controller *xx_zwm_service_controller
) {
    xx_zwm_service_controller->xx_zwm_service_controller_listener.abandon(
        xx_zwm_service_controller->data,
        xx_zwm_service_controller
    );

    return;
}

#endif  // WM_AGENT_EVENT_H
