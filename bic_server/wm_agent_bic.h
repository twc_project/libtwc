#ifndef XM_WM_AGENT_BIC_H
#define XM_WM_AGENT_BIC_H

  // System
#include <stdbool.h>

  // Wayland
#include <wayland-util.h>  // wl_list

  // Protocols
#include <wm-agent-v1-client-protocol.h>
#include <desktop-cues-v1-client-protocol.h>

  // APIs
#include <twc/wayland_types.h>  // wld_<type>_t
#include <twc_service.h>        // twc_service_methods

  // Opaque types
struct wl_surface;

extern struct xx_wm_interactive_agent *granted_xx_wm_interactive_agent;
extern struct xx_wm_menu_agent        *granted_xx_wm_menu_agent;
extern struct xx_wm_workspace_agent   *granted_xx_wm_workspace_agent;

 // --------------------
 // xdg agent interfaces
 //

struct xx_wm_interactive_agent {
    void *data;
    struct xx_wm_interactive_agent_listener xx_wm_interactive_agent_listener;

    bool is_requested;

      // When a client annexes the global pre-defined Overlay surface,
      // a wl_surface is returned as the client-side object.  This
      // member records that proxy to the Overlay surface.
    struct wl_surface *proxy_surface;
};

struct xx_wm_decoration_stylist {
    void *data;
    struct xx_wm_decoration_stylist_listener xx_wm_decoration_stylist_listener;

    bool is_requested;
};

enum deco_frame_style {
    FS_NOT_SPECIFIED,
    FS_TITLEBAR_ONLY,
    FS_BORDERS_ONLY,
    FS_ALL_BORDERS,
};

struct xx_wm_decoration {
    void *data;
    struct xx_wm_decoration_listener xx_wm_decoration_listener;

    bool is_active;

    enum xx_wm_decoration_for xx_wm_decoration_for;

    struct wl_surface *top_surface;
    struct wl_surface *bottom_surface;
    struct wl_surface *left_surface;
    struct wl_surface *right_surface;

      // surface-relative coordinates for deco edges
    wld_coordinate_t    top_sx,    top_sy;
    wld_coordinate_t bottom_sx, bottom_sy;
    wld_coordinate_t   left_sx,   left_sy;
    wld_coordinate_t  right_sx,  right_sy;

    bool    is_top_pending;
    bool is_bottom_pending;
    bool   is_left_pending;
    bool  is_right_pending;

    bool    is_top_offset_pending;
    bool is_bottom_offset_pending;
    bool   is_left_offset_pending;
    bool  is_right_offset_pending;

    struct wl_surface *pending_top_surface;
    struct wl_surface *pending_bottom_surface;
    struct wl_surface *pending_left_surface;
    struct wl_surface *pending_right_surface;

    wld_coordinate_t pending_top_sx,    pending_top_sy;
    wld_coordinate_t pending_bottom_sx, pending_bottom_sy;
    wld_coordinate_t pending_left_sx,   pending_left_sy;
    wld_coordinate_t pending_right_sx,  pending_right_sy;

    enum deco_frame_style deco_frame_style;

    enum xx_wm_decoration_edge titlebar_surface;

    bool has_auto_relocate;

    struct wl_list desktop_cue_list;

      // The client which created the decoration.
    struct bic_connection *bic_connection;
};

struct xx_wm_icon_stylist {
    void *data;
    struct xx_wm_icon_stylist_listener xx_wm_icon_stylist_listener;

    bool is_requested;
};

struct xx_wm_icon {
    void *data;
    struct xx_wm_icon_listener xx_wm_icon_listener;
};

struct xx_wm_menu_agent {
    void *data;
    struct xx_wm_menu_agent_listener xx_wm_menu_agent_listener;

    bool is_requested;

      // See xx_wm_interactive_agent.proxy_surface above
    struct wl_surface *proxy_surface;

      // Announce xx_zwm_service_controlers the first time a listener
      // is set.
    bool is_listener_set;
};

struct xx_wm_workspace_agent {
    void *data;
    struct xx_wm_workspace_agent_listener xx_wm_workspace_agent_listener;

    bool is_requested;
};

struct xx_wm_task_agent {
    void *data;
    struct xx_wm_task_agent_listener xx_wm_task_agent_listener;

    bool is_requested;
};

struct xx_wm_window_registry {
    void *data;
    struct xx_wm_window_registry_listener xx_wm_window_registry_listener;

      // Announce windows the first time a listener is set.
    bool is_listener_set;
};

struct xx_wm_window {
    void *data;
    struct xx_wm_window_listener xx_wm_window_listener;

    struct wl_list link;

    struct twc_window *twc_window;

      // The client which created the xx_wm_window.
    struct bic_connection *bic_connection;

      // Emit window properties the first time a listener is set.
    bool is_listener_set;
};

struct xx_wm_workspace_registry {
    void *data;
    struct xx_wm_workspace_registry_listener xx_wm_workspace_registry_listener;

      // Announce workspaces the first time a listener is set.
    bool is_listener_set;
};

struct xx_wm_workspace {
    void *data;
    struct xx_wm_workspace_listener xx_wm_workspace_listener;

    struct wl_list link;

    struct twc_workspace *twc_workspace;

      // The client which created the decoration.
    struct bic_connection *bic_connection;
};

struct wm_agent_base_v1 {
    void *data;
    struct wm_agent_base_v1_listener wm_agent_base_v1_listener;

    struct xx_wm_interactive_agent xx_wm_interactive_agent;
    struct xx_wm_menu_agent        xx_wm_menu_agent;

    struct xx_wm_window_registry    xx_wm_window_registry;
    struct xx_wm_workspace_registry xx_wm_workspace_registry;

    struct xx_wm_decoration_stylist xx_wm_decoration_stylist;
    struct xx_wm_icon_stylist       xx_wm_icon_stylist;

    struct xx_wm_workspace_agent    xx_wm_workspace_agent;
    struct xx_wm_task_agent         xx_wm_task_agent;

    enum xx_wm_decoration_stylist_cueing_state cueing_state;
};

#endif  // XM_WM_AGENT_BIC_H
