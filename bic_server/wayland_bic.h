#ifndef WAYLAND_BIC_H
#define WAYLAND_BIC_H

  // System
#include <stdbool.h>
#include <stdint.h>     // uint32_t

  // Wayland
#include <wayland-util.h>       // wl_fixed_t, wl_list

  // Protocols
#include <wayland-client-protocol.h>        // struct wl_<object>
#include <wm-agent-v1-client-protocol.h>    // enum xx_wm_decoration_edge

  // Imported Data Types
#include "xdg_shell_bic.h"
#include "xdg_decoration_bic.h"
#include "wm_agent_bic.h"

  // Opaque types
struct wl_display;
struct wl_registry;
struct wl_compositor;
struct wl_shm;
struct wl_shm_pool;
struct wl_buffer;
struct wl_surface;
struct wl_seat;
struct wl_pointer;
struct wl_keyboard;
struct wl_output;

struct bic_connection;
struct srv_seat;
struct srv_output;

 /*
 ** Protocol object interfaces for Built-in Clients
 **
 ** Since Built-in Clients have modest needs, the bic_server does not
 ** support every Wayland interface object.
 */

enum buffer_type {
    BT_internal,    // result of wl_shm_pool_create_buffer()
    BT_external,    // result of    external_create_buffer()
    BT_xwayland,    // result of    external_create_buffer()
};

struct wl_buffer {
    void                     *data;
    struct wl_buffer_listener wl_buffer_listener;
    struct wl_shm_pool       *wl_shm_pool;    // Source pool

    // A wl_buffer may be eligible for an early release, ie, when
    // committed.  The client may then reuse/destroy the wl_buffer.
    // To facilitate early release, the following buffer data will be
    // promoted to the wl_surface.

    enum buffer_type buffer_type;
    wld_dimension_t  width;
    wld_dimension_t  height;

    wld_coordinate_t enclosure_sx;
    wld_coordinate_t enclosure_sy;

     int32_t         stride;    // Valid only
    uint32_t         format;    // for BT_internal.

    union {
          // For BT_internal, a buffer references a shared memory
          // segment containing pixel data.
        uint8_t *pix_data;      // From a client shm_pool

          // For BT_external and BT_xwayland, a buffer is simply
          // references a wcf_external_surface from the wcf_client.
        void *wcf_external_surface;
    };
};

  // According to the wl_surface interface description in the Wayland
  // Core Protocol, a surface without a Role is "useless."  Roles are
  // given via a request thru some interface.  The BIC server assigns
  // a type to each surface.  Most types correspond to some Role.
  // Once a surface has a role, it cannot change.
enum surface_type {
                         // Protocol   Interface              Request                new obj
                         // ---------  ---------------------  ---------------------  -------
    ST_begin_mapped_roles,

    ST_DND_ICON,         // wayland    wl_data_device         start_drag
    ST_CURSOR,           // wayland    wl_pointer             set_cursor
    ST_SUB_SURFACE,      // wayland    wl_subcompositor       get_subsurface         new_id

    ST_begin_window_roles,

    ST_XDG_POPUP,        // xdg_shell  xdg_surface            get_popup              new_id
    ST_XDG_TOPLEVEL,     // xdg_shell  xdg_surface            get_toplevel           new_id

    ST_end_window_roles,

    ST_XX_WM_ICON,       // xdg_agent  xx_wm_icon_stylist       get_icon
    ST_XX_WM_DECO,       // xdg_agent  xx_wm_decoration_stylist get_decoration
    ST_XX_WM_WALLPAPER,  // xdg_agent  xx_wm_workspace_agent    set_wallpaper

    ST_end_mapped_roles,

    ST_XDG_SURFACE,      // xdg_shell  xdg_wm_base            get_xdg_surface        new_id
    ST_OVERLAY,          // xdg_agent  xx_wm_interactive_agent  "pre-defined"
    ST_DEFAULT,          // xdg_agent  xx_wm_menu_agent         "pre-defined"

    ST_INERT,
    ST_NONE,

      // Clients annex, not allocate, the pre-defined surfaces Default
      // and Overlay.  When a client annexes such a surface, it is
      // given a "proxy" surface.
    ST_PROXY,           // xdg_agent  xx_wm_interactive_agent  annex_default_surface  new_id
                        // xdg_agent  xx_wm_menu_agent         annex_orpah_surface    new_id
};

#define SURFACE_IS_WINDOW(s) ( \
    ( (ST_begin_window_roles) < (s)->type             ) && \
    ( (s)->type               < (ST_end_window_roles) )    \
)

#define SURFACE_HAS_ROLE(s) ( \
    ( (ST_begin_mapped_roles) < (s)->type             ) && \
    ( (s)->type               < (ST_end_mapped_roles) )    \
)

#define SURFACE_HAS_THIS_ROLE(s,r) ( \
    (s)->type == (r) \
)

#define SURFACE_IS_PROXY(s) ( \
    ( (s)->type == ST_PROXY )   \
)

 /*
 ** Note: The term "map", when applied to a window, has slightly
 ** different meaning in the Wayland world as opposed to the X Window
 ** world.
 **
 ** For X Windows, mapping a window typically means choosing to make
 ** it visible.
 **
 ** In the context of Wayland's xdg-shell protocol, a surface is said
 ** to be mapped, if it is merely ready to be displayed.  The
 ** compositor separately decides whether to actually make it visible.
 */

#define SURFACE_IS_MAPPED(s) ( (s)->is_mapped  )
#define SURFACE_IS_EMPTY(s)  ( (s)->width == 0 )

struct wl_surface {
    void                       *data;
    struct wl_surface_listener  wl_surface_listener;

      // Not all types represent a role.  Not all roles represent a
      // Window.
    enum surface_type type;

    struct twc_view *twc_view;

      // A wl_surface is not displayable unitl it has a role and a
      // non-null buffer. An xdg_surface has some complex additional
      // criteria imposed by the xdg_shell involving an "initial
      // commit".  A displayable xdg_shell surface is said to be
      // "mapped".
    bool is_mapped;

      // The pending_wl_buffer value is either UNDEFINED_WL_BUFFER or
      // a valid pointer to a wl_buffer including the INERT_WL_BUFFER.
      //
      // The current_wl_buffer value is either
      //   INERT_WL_BUFFER,
      //   valid   wl_buffer pointer (is_current_buffer_released == false),
      //   invalid wl_buffer pointer (is_current_buffer_released == true).
      // The invalid wl_buffer case can never point to INERT_WL_BUFFER.
    struct wl_buffer *pending_wl_buffer;
    struct wl_buffer *current_wl_buffer;    // not valid once released
    bool              is_current_buffer_released;

      // Protocol objects for shell surfaces
    union {
        struct xdg_surface xdg_surface;  // xdg shell
      //struct xx_wm_surface xx_wm_surface;  // acc shell
    };

      // Protocol objects for surface roles
    union {
        struct xdg_toplevel xdg_toplevel;   // xdg-shell role
        struct xdg_popup    xdg_popup;      // xdg-shell role
        struct xx_wm_icon   xx_wm_icon;     // accessory-shell role

          // Since windows are decorated, not surfaces, the
          // xx_wm_decoration is stored with window data in the
          // twc_view.  However, this member documents which edge.
        enum xx_wm_decoration_edge xx_wm_decoration_edge;
    };

      // The following annonymous struct contains data which is
      // promoted from the wl_buffer at commit time.  Since a commit
      // may result in the early release of the buffer object, this
      // buffer info may not be available after commit.
    struct {
        enum buffer_type committed_image_type;
        wld_dimension_t  width;
        wld_dimension_t  height;
        int32_t          stride;

        union {
              // BT_internal
            struct {
                uint8_t *pix_data;          // promoted from client wl_buffer
                void    *wcf_pixel_texture; // calculated by Compositor
            };
              // BT_external, BT_xwayland
            void *wcf_external_surface;
        };
    };

      // When pointer focus is assigned, this is the nested
      // surface which actually has the pointer.
    void *nested_surface_with_ptr_focus;

      // The client connection which created this surface.
    struct bic_connection *bic_connection;
};

struct wl_display  {
    void                       *data;
    struct wl_display_listener  wl_display_listener;
};

struct wl_registry {
    void                       *data;
    struct wl_registry_listener wl_registry_listener;

      // Announce global singletons the first time a listener is set.
    bool is_listener_set;
};

struct wl_compositor {
    // A wl_compositor has no events
};

struct wl_shm {
    void                   *data;
    struct wl_shm_listener  wl_shm_listener;
};

enum shm_pool_type {
    SPT_MMAP,
    SPT_MALLOC,
};

struct wl_shm_pool {
    // A wl_shm_pool is an interface for allocating memory from a
    // shared region.  A pool may be destroyed even if some buffers
    // are still using the shared memory created through the pool.
    //
    // For Built-in-Clients, the shared memory region is created using
    // malloc().  These regions can't be automatically free'd when the
    // pool is destroyed.
    //
    // This struct serves two purposes.
    //
    //   1) It denotes an active wl_shm_pool.
    //
    //   2) It records information about the shared memory region.
    //
    // When the wl_shm_pool is destroyed, only the second purpose
    // remains.
    //
    // After a wl_shm_pool is destroyed, further allocations are not
    // possible.  However, the wl_shm_pool persists for purpose 2
    // until all buffers which use the shared memeory are destroyed.
    // In the meantime, invocations of wl_shm_pool_create_buffer() are
    // rejected with an error as if the pool did not exist.

    int32_t  shm_pool_size;
    uint8_t *shm_pool_base;    // address of the malloc'd shared memory region

    uint32_t buffer_count;
    bool     is_destroyed;

    enum shm_pool_type shm_pool_type;
};

struct wl_pointer {
    void                       *data;
    struct wl_pointer_listener  wl_pointer_listener;

    uint32_t  enter_serial;
    uint32_t  leave_serial;
    uint32_t button_serial;
};

struct wl_keyboard {
    void                        *data;
    struct wl_keyboard_listener  wl_keyboard_listener;

    uint32_t     enter_serial;
    uint32_t     leave_serial;
    uint32_t       key_serial;
    uint32_t modifiers_serial;
};

struct wl_seat {
    void                    *data;
    struct wl_seat_listener  wl_seat_listener;
    uint32_t                 numeric_name;

    struct wl_pointer  wl_pointer;
    struct wl_keyboard wl_keyboard;

    void *default_seat;
};

struct wl_output {
    void                      *data;
    struct wl_output_listener  wl_output_listener;
    uint32_t                   numeric_name;

    struct wl_list link;

    struct srv_output *srv_output;
};

#endif  // WAYLAND_BIC_H

#if 0
    // ---------------------------
    // Wayland Surface Role Ritual

      // Create a surface.
    wl_surface = wl_compositor_create_surface(compositor);

      // Create an xdg_surface extension interface for the surface.
    xdg_surface = xdg_wm_base_get_xdg_surface(xdg_wm_base, wl_surface);

    // The xdg_shell xml description says a commit is required in
    // order "for the xdg_surface state to take effect."  Does that
    // mean that, no role can be added unitl a "pre-role commit" is
    // performed?
    //
    // Note that adding an interface listener only alters client local
    // state, so this may be done before any commit.  Besides, the
    // server may emit events as a result of a commit.  So, the
    // listeners must be readied beforehand.  Presumably, the
    // xdg_surface listener must be added before the toplevel role is
    // assigned instead of after.
    //
    //    xdg_surface_add_listener(xdg_surface,  &xdg_surface_listener,  NULL);
    //    wl_surface_commit(surface);   // pre-role commit??

    // From the xdg-shell protocol descrition, "A role must be
    // assigned before any other requests are made to the xdg_surface
    // object."

      // Create an xdg_toplevel extension interface for the
      // xdg_surface.  This is a role.
    xdg_toplevel = xdg_surface_get_toplevel(xdg_surface);

    xdg_surface_add_listener(xdg_surface,  &xdg_surface_listener,  NULL);
    xdg_surface_add_listener(xdg_toplevel, &xdg_toplevel_listener, NULL);

    // Since the pre-role commit has been done, can we set attributes
    // here, before the initial commit??  That would make sense so the
    // server would know what configuration to emit.

      // The esteemed "Initial Commit."  After a role, before buffer.
    wl_surface_commit(surface);

      // A roundtrip waits while the server processes all outstanding
      // requests.  During this time, the server may emit events.
    wl_display_roundtrip(display);

        // . . .
        //
        // Eventually, the server emits an xdg_surface::configure
        // event.

              // Within the xdg_surface listener, handle the configure event.
            xdg_surface_event_configure(data, xdg_surface, serial) {...}

        // . . .
        //
        // The roundtrip returns when the server has processed all
        // outstanding requests and is finished emitting events for
        // this client.

      // The serial is from the configure event.
    xdg_surface_ack_configure(xdg_surface, serial);

    // Surface attributes may be set after this point . . .

    // Wouldn't it be more convenient to set attributes before the
    // initial commit??

#endif

#if 0
    // --------------------------
    // Setting Surface Attributes
    //
    // Attribute changes are requested (set) using client requests.
    // The server is not required to honor the request.
    //
    // 1 The setting of some attributes is not double-buffered.
    //
    // 2 The setting of some attributes is double-buffered.
    //
    // 3 The setting of some attributes causes the server to emit
    //   configure events.


    // 1) Non double-buffered attributes

        xdg_toplevel_set_app_id(xdg_toplevel, "...");
        xdg_toplevel_set_title( xdg_toplevel, "...");

    // 2) Double-buffered attributes which do not generate an event
    //    response from the server.

        xdg_surface_set_window_geometry(xdg_surface, x, y, width, height);

        xdg_toplevel_set_max_size(xdg_toplevel, width, height);
        xdg_toplevel_set_min_size(xdg_toplevel, width, height);

        wl_surface_attach(wl_surface, wl_buffer, 0, 0);

    // 3) Double-buffered attributes which generate an xdg_toplevel
    //    event response from the server.  The last event is always
    //    xdg_surface_event_configure().

        xdg_toplevel_set_<state>(  xdg_toplevel);
        xdg_toplevel_unset_<state>(xdg_toplevel);


      // ?? commit
    wl_surface_commit(surface);


    // Server may emit events in batches.
    xdg_toplevel_event_configure(       data, xdg_surface, width, height, states) {...}
    xdg_toplevel_event_configure_bounds(data, xdg_surface, width, height)         {...}
    xdg_toplevel_event_wm_capabilities( data, xdg_surface, capabilities)          {...}

    // After the last event of a batch, server sends this.  Client
    // must reply with ack_configure request.
    xdg_surface_event_configure(data, xdg_surface, serial) {...}

#endif

#if 0
    // ----------------------
    // Xdg_toplevel configure

    // A compositor may choose to provide "hints" about surface
    // dimension (and/or state).
    xdg_toplevel_emit_configure(width, height, states)

    // Even though they are explicitly documented as hints, a client
    // __MUST__ respond with this request.
    xdg_surface_ack_configure(xdg_surface, serial)

    // However, that request requires that a serial has been obtained
    // from the server.  So, the server MUST emit another event BEFORE
    // the client can respond to the first.
    xdg_surface_emit_configure(serial)

    // Note that the client has no way to negatively acknowledge
    // (nack) the hint.  It can't even withhold the ack_configure
    // because then there would be an outstaniding serial which
    // impairs future interactions.

    // If the client intends to follow the hint, it is FORBIDDEN from
    // commiting the changes for the hint until AFTER the
    // ack_configure.

    // If the client intends to ignore hint, it must still
    // ack_configure the hint, leading the server to falsely believe
    // that it will comply.  The server has no way of knowing if the
    // hint will be followed.  Some future commit may possibly
    // confirm, but not necessarily the next one.

#endif
