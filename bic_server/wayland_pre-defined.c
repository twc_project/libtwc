
  // Protocols
#include <wayland-client-protocol.h>

  // APIs
#include "wayland_pre-defined.h"     // Our API to ensure consistancy (server)
#include <twc/wayland_inert.h>
#include "twc_window.h"

  // Imported data types
#include "wayland_bic.h"
#include "bic_connection.h"

 /* -------------------------
 ** Pre-defined Inert Objects
 **
 ** Instead of a NULL pointer, we define special wl_<object>s to serve
 ** as an indicator of a non-existent wl_<object>.  They are defined
 ** with the name inert_wl_<object>.  The listener routines for each
 ** inert_wl_<object> are no-ops.  Therefore, code may safely
 ** dereference inert_wl_<object>s and even invoke any listener
 ** routine.  There is no need to test for valid function pointers.
 ** This simplifies the logic of many code sequences.
 */

 // --------------------
 // inert_bic_connection

struct bic_connection inert_bic_connection = {

    .wl_seat.wl_pointer.wl_pointer_listener = {
        .enter         = inert_wl_pointer_event_enter,
        .leave         = inert_wl_pointer_event_leave,
        .motion        = inert_wl_pointer_event_motion,
        .button        = inert_wl_pointer_event_button,
        .axis          = inert_wl_pointer_event_axis,
        .frame         = inert_wl_pointer_event_frame,
        .axis_source   = inert_wl_pointer_event_axis_source,
        .axis_stop     = inert_wl_pointer_event_axis_stop,
        .axis_discrete = inert_wl_pointer_event_axis_discrete,
    },

    .wl_seat.wl_keyboard.wl_keyboard_listener = {
        .keymap      = inert_wl_keyboard_event_keymap,
        .enter       = inert_wl_keyboard_event_enter,
        .leave       = inert_wl_keyboard_event_leave,
        .key         = inert_wl_keyboard_event_key,
        .modifiers   = inert_wl_keyboard_event_modifiers,
        .repeat_info = inert_wl_keyboard_event_repeat_info,
    },

    .wm_agent_base_v1.xx_wm_interactive_agent.proxy_surface = INERT_WL_SURFACE,
    .wm_agent_base_v1.       xx_wm_menu_agent.proxy_surface = INERT_WL_SURFACE,
};

 // ---------------
 // inert_wl_buffer

struct wl_buffer inert_wl_buffer = {
    .data               = NULL,

    .wl_buffer_listener = {
        .release = inert_wl_buffer_event_release,
    },

    .buffer_type = BT_internal,
    .width       = 0,
    .height      = 0,
    .stride      = 0,
    .format      = 0,
    .pix_data    = NULL,
};

 // -------------------------
 // inert_wl_surface_listener

static
const
struct wl_surface_listener static_wl_surface_listener = {
    .enter = inert_wl_surface_event_enter,
    .leave = inert_wl_surface_event_leave,
};

 // ----------------
 // inert_wl_surface

struct wl_surface inert_wl_surface = {
    .data                 = NULL,
    .wl_surface_listener  = static_wl_surface_listener,

    .pending_wl_buffer    = UNDEFINED_WL_BUFFER,
    .current_wl_buffer    =     INERT_WL_BUFFER,

    .committed_image_type = BT_internal,
    .type                 = ST_INERT,
    .twc_view             = NULL_TWC_VIEW,
    .is_mapped            = false,
    .bic_connection       = INERT_BIC_CONNECTION,
    .wcf_pixel_texture    = NULL,

    .nested_surface_with_ptr_focus = NULL,
};

 /* ----------------------------
 ** Special pre-defined surfaces
 **
 ** In addition to the inert objects above, there are two pre-defined
 ** surfaces used for specific purposes.  They are:
 **
 **   default_wl_surface
 **
 **   overlay_wl_surface
 **
 ** Each is defined using the inert_wl_buffer.
 **
 ** The default_wl_surface is the surface of last resort for
 ** twc_window_surface_at().  An agent may annex this surface and add
 ** its own listeners in order to collect orhpan input.
 **
 ** When active, the overlay_wl_surface is the exclusive result for
 ** twc_window_surface_at().  An agent may annex this surface in order
 ** to hijack all input.
 */

 // ------------------
 // default_wl_surface

struct wl_surface default_wl_surface = {
    .data                 = NULL,
    .wl_surface_listener  = static_wl_surface_listener,

    .pending_wl_buffer    = UNDEFINED_WL_BUFFER,
    .current_wl_buffer    =     INERT_WL_BUFFER,

    .committed_image_type = BT_internal,
    .type                 = ST_DEFAULT,
    .twc_view             = NULL_TWC_VIEW,
    .is_mapped            = false,
    .bic_connection       = INERT_BIC_CONNECTION,
    .wcf_pixel_texture    = NULL,

    .nested_surface_with_ptr_focus = NULL,
};

 // ------------------
 // overlay_wl_surface

struct wl_surface overlay_wl_surface = {
    .data                 = NULL,
    .wl_surface_listener  = static_wl_surface_listener,

    .pending_wl_buffer    = UNDEFINED_WL_BUFFER,
    .current_wl_buffer    =     INERT_WL_BUFFER,

    .committed_image_type = BT_internal,
    .type                 = ST_OVERLAY,
    .twc_view             = NULL_TWC_VIEW,
    .is_mapped            = false,
    .bic_connection       = INERT_BIC_CONNECTION,
    .wcf_pixel_texture    = NULL,

    .nested_surface_with_ptr_focus = NULL,
};
