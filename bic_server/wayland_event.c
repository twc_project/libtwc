
  // System
#include <stdint.h>         // uint32_t, ...

  // Wayland
#include <wayland-util.h>   // wl_fixed_t, struct wl_interface

  // APIs
#include "wayland_event.h"          // Our API to ensure consistancy
#include "wayland_pre-defined.h"

  // Imported data types
#include "bic_connection.h"
#include "wayland_bic.h"
#include "server_state-impl.h"

static inline
struct wl_surface*
client_proxy(
    struct wl_surface *wl_surface
) {
    if ( wl_surface->type != ST_OVERLAY &&
         wl_surface->type != ST_DEFAULT
    ) {
        return wl_surface;
    }

      // Substitute the client's proxy for the pre-defined surface.
    return (
        wl_surface == OVERLAY_WL_SURFACE
        ? wl_surface->bic_connection->wm_agent_base_v1.xx_wm_interactive_agent.proxy_surface
        : wl_surface->bic_connection->wm_agent_base_v1.       xx_wm_menu_agent.proxy_surface
    );
}

  // -----------
  // wl_registry

extern
void
wl_registry_global(
    const struct wl_interface *wl_interface,
    uint32_t                   numeric_name
) {
    struct bic_connection *bic_connection;
    bic_connection_list_for_each(bic_connection) {

        struct wl_registry *wl_registry = &bic_connection->wl_registry;

        wl_registry->wl_registry_listener.global(
            wl_registry->data,
            wl_registry,
            numeric_name,
            wl_interface->name,
            wl_interface->version
        );
    }

    return;
}

extern
void
wl_registry_global_remove(
    uint32_t numeric_name
) {
    struct bic_connection *bic_connection;
    bic_connection_list_for_each(bic_connection) {

        struct wl_registry *wl_registry = &bic_connection->wl_registry;

        wl_registry->wl_registry_listener.global_remove(
            wl_registry->data,
            wl_registry,
            numeric_name
        );
    }

    return;
}

  // ---------
  // wl_buffer

extern
void
wl_buffer_release(
    struct wl_buffer *wl_buffer
) {
    wl_buffer->wl_buffer_listener.release(
        wl_buffer->data,
        wl_buffer
    );

    return;
}


  // ----------
  // wl_pointer

extern
void
wl_pointer_enter(
    struct wl_pointer *wl_pointer,
    struct wl_surface *surface,
    wl_fixed_t         surface_x,
    wl_fixed_t         surface_y
) {
    wl_pointer->wl_pointer_listener.enter(
        wl_pointer->data,
        wl_pointer,
        wl_pointer->enter_serial++,
        client_proxy(surface),
        surface_x,
        surface_y
    );

    return;
}

extern
void
wl_pointer_leave(
    struct wl_pointer *wl_pointer,
    struct wl_surface *surface
) {
    wl_pointer->wl_pointer_listener.leave(
        wl_pointer->data,
        wl_pointer,
        wl_pointer->leave_serial++,
        client_proxy(surface)
    );

    return;
}

extern
void
wl_pointer_motion(
    struct wl_surface *wl_surface,  // surface with ptr focus
    uint32_t           time,
    wl_fixed_t         surface_x,
    wl_fixed_t         surface_y
) {
    struct wl_pointer *wl_pointer = (
        &wl_surface->bic_connection->wl_seat.wl_pointer
    );

    wl_pointer->wl_pointer_listener.motion(
        wl_pointer->data,
        wl_pointer,
        time,
        surface_x,
        surface_y
    );

    return;
}

extern
void
wl_pointer_button(
    struct wl_pointer *wl_pointer,
    uint32_t           time,
    uint32_t           button,
    uint32_t           state
) {
    wl_pointer->wl_pointer_listener.button(
        wl_pointer->data,
        wl_pointer,
        wl_pointer->button_serial++,
        time,
        button,
        state
    );

    return;
}

extern
void
wl_pointer_event_axis(
        struct wl_pointer *wl_pointer,
        uint32_t           time,
        uint32_t           axis,
        wl_fixed_t         value
) {
    wl_pointer->wl_pointer_listener.axis(
        wl_pointer->data,
        wl_pointer,
        time,
        axis,
        value
    );

    return;
}


  // -----------
  // wl_keyboard

extern
void
wl_keyboard_enter(
    struct wl_keyboard *wl_keyboard,
    struct wl_surface  *surface,
    struct wl_array    *keys
) {
    wl_keyboard = (
        &surface->bic_connection->wl_seat.wl_keyboard
    );

    wl_keyboard->wl_keyboard_listener.enter(
        wl_keyboard->data,
        wl_keyboard,
        wl_keyboard->enter_serial++,
        client_proxy(surface),
        keys
    );

    return;
}

extern
void
wl_keyboard_leave(
    struct wl_keyboard *wl_keyboard,
    struct wl_surface  *surface
) {
    wl_keyboard = (
        &surface->bic_connection->wl_seat.wl_keyboard
    );

    wl_keyboard->wl_keyboard_listener.leave(
        wl_keyboard->data,
        wl_keyboard,
        wl_keyboard->leave_serial++,
        client_proxy(surface)
    );

    return;
}

extern
void
wl_keyboard_key(
    struct wl_keyboard *wl_keyboard,
    uint32_t            time,
    uint32_t            key,
    uint32_t            state
) {
    wl_keyboard->wl_keyboard_listener.key(
        wl_keyboard->data,
        wl_keyboard,
        wl_keyboard->key_serial++,
        time,
        key,
        state
    );

    return;
}

extern
void
wl_keyboard_modifiers(
    struct wl_keyboard *wl_keyboard,
    uint32_t            depressed,
    uint32_t            latched,
    uint32_t            locked,
    uint32_t            keyboard_layout
) {
    wl_keyboard->wl_keyboard_listener.modifiers(
        wl_keyboard->data,
        wl_keyboard,
        wl_keyboard->modifiers_serial++,
        depressed,
        latched,
        locked,
        keyboard_layout
    );

    return;
}
