
  // System
#include <stdint.h>     // uint32_t
#include <stdio.h>      // size_t, sizeof, NULL
#include <stdlib.h>     // calloc, free
#include <string.h>     // strncpy

  // Wayland
#include <wayland-util.h>  // wl_container_of, wl_list, wl_fixed_t, struct wl_interface

  // Protocols
#include <wm-service-v1-client-protocol.h>  // Our API to ensure consistancy

  // TWC
#include <twc/wayland_types.h>      // ADD_LISTENER_SUCCESS
#include <twc/wm_service_inert.h>
#include <twc/wm_agent_inert.h>
#include <twc/twc_log.h>

  // APIs
#include "wm_agent_event.h"
#include "service_cntlr.h"
#include "server_state-impl.h"

  // Imported data types
#include "wm_service_bic.h"
#include "bic_connection.h"
#include "twc_window-impl.h"

 // ----------------------
 // xx_zwm_service_base_v1

extern
void
xx_zwm_service_base_v1_destroy(
    struct xx_zwm_service_base_v1 *xx_zwm_service_base_v1
) {
    // Fix: ToDo

    return;
}

extern
struct xx_zwm_service*
xx_zwm_service_base_v1_create_service(
    struct xx_zwm_service_base_v1 *xx_zwm_service_base_v1,
    const  char                   *provider_name,
    const  char                   *service_name
) {
    struct bic_connection *bic_connection = (
        wl_container_of(xx_zwm_service_base_v1, bic_connection, xx_zwm_service_base_v1)
    );

    bool not_a_desktop_assistant = (
        (   IS_NOT_AGENT ( bic_connection )                         ) &&
        ( ! bic_connection->xx_zwm_service_base_v1.is_client_vetted )
    );

    bool bad_service_name = (
        ( service_name    == NULL ) ||
        ( service_name[0] == '\0' )
    );

    if ( not_a_desktop_assistant || bad_service_name ) {
        /* Protocol Error */
         return NULL;
    }

    struct xx_zwm_service  *xx_zwm_service = (
        calloc( 1, sizeof(struct xx_zwm_service) )
    );
    struct xx_zwm_service_controller *xx_zwm_service_controller = (
        &xx_zwm_service->xx_zwm_service_controller
    );

    if ( IS_AGENT_WKSP( bic_connection ) ) {
      // Check for the Workspace Reoccupy Service.
        int cmp = (
            strncmp(
                service_name,
                "Reoccupy",
                MAX_SERVICE_NAME_LENGTH
            )
        );
        if ( cmp == 0 ) {
            reoccupy_service_controller = xx_zwm_service_controller;
        }
    }

    // Init xx_zwm_service

    xx_zwm_service->is_destroyed            = false;
    xx_zwm_service->xx_zwm_service_listener = inert_xx_zwm_service_listener;

    xx_zwm_service->twc_service_methods.assert_method     = XX_ZWM_SERVICE_ASSERT_METHOD_NOP;
    xx_zwm_service->twc_service_methods.stand_down_method = XX_ZWM_SERVICE_STAND_DOWN_METHOD_NOP;

    twc_service_get_methods(
        provider_name,
        service_name,
        &xx_zwm_service->twc_service_methods
    );

    wl_list_init( &xx_zwm_service->service_window_list);
    wl_list_init( &xx_zwm_service->     operation_list);

    strncpy(xx_zwm_service->provider_name, provider_name, MAX_PROVIDER_NAME_LENGTH);
    xx_zwm_service->service_name[MAX_PROVIDER_NAME_LENGTH - 1] = '\0';

    strncpy(xx_zwm_service->service_name, service_name, MAX_SERVICE_NAME_LENGTH);
    xx_zwm_service->service_name[MAX_SERVICE_NAME_LENGTH - 1] = '\0';

    // Init xx_zwm_service_controller

      // Initialized by compiler.  Incremented for each new service.
    static uint32_t service_contoller_name = 0;
    service_contoller_name++;

    xx_zwm_service_controller->is_active = false;   // Starts false, becomes true when bound

    xx_zwm_service_controller->xx_zwm_service_controller_listener = inert_xx_zwm_service_controller_listener;
    xx_zwm_service_controller->is_in_unbound_controller_list      = false;
    xx_zwm_service_controller->numeric_name                       = service_contoller_name;

    unbound_controller_list_insert( &xx_zwm_service->xx_zwm_service_controller );

      // Add the xx_zwm_service to the bic_connection's list
    wl_list_insert(
       &(bic_connection->desktop_service_list),
       &(xx_zwm_service->link)
    );

    xx_wm_menu_agent_emit_new_service(
        granted_xx_wm_menu_agent,
        service_contoller_name++,
        0,  // version num
        provider_name,
        service_name
    );

    return xx_zwm_service;
}

 // --------------
 // xx_zwm_service

extern
void
xx_zwm_service_destroy(
    struct xx_zwm_service *xx_zwm_service
) {
      // Remove from the bic_connection service list
    wl_list_remove( &xx_zwm_service->link );

    xx_zwm_service->xx_zwm_service_listener = inert_xx_zwm_service_listener;
    xx_zwm_service->is_destroyed            = true;

    struct xx_zwm_service_controller *xx_zwm_service_controller = (
        &xx_zwm_service->xx_zwm_service_controller
    );

    unbound_controller_list_remove( xx_zwm_service_controller );

    if ( xx_zwm_service_controller->is_active ) {

          // The service_controller is in use.  Notify its creator.
        xx_zwm_service_controller_emit_abandon(
            xx_zwm_service_controller
        );
    }

    struct xx_zwm_service_window *xx_zwm_service_window, *tmp;
    wl_list_for_each_safe(        xx_zwm_service_window,  tmp, &xx_zwm_service->service_window_list, link ) {

        // Agents should destroy service_windows before destroying the
        // service.  Landing here is an error condition on the agent.

        xx_zwm_service_window_destroy( xx_zwm_service_window );
    }

    struct operation_label *operation_label, *tmp_label;
    wl_list_for_each_safe(  operation_label,  tmp_label, &xx_zwm_service->operation_list, link ) {

          // destroy any registered operations
        free( operation_label );
    }

    if ( xx_zwm_service_controller->is_active == false ) {

        // The embedded service_controller is inactive, so memory can
        // be deallocated.

        free( xx_zwm_service );
    }

    return;
}

extern
struct xx_zwm_service_window*
xx_zwm_service_get_service_window(
    struct xx_zwm_service *xx_zwm_service,
    struct xdg_toplevel   *xdg_toplevel
) {
    if ( xdg_toplevel == NULL ) { return NULL; }

    struct wl_surface *wl_surface = (
        wl_container_of(xdg_toplevel, wl_surface, xdg_toplevel)
    );

    struct twc_window *twc_window = (
        wl_surface->twc_view->twc_window
    );

    struct xx_zwm_service_window   *xx_zwm_service_window = (
        calloc( 1, sizeof(struct xx_zwm_service_window) )
    );

    xx_zwm_service_window->xx_zwm_service = xx_zwm_service;
    xx_zwm_service_window->twc_window     = twc_window;
    twc_window->xx_zwm_service_window     = xx_zwm_service_window;

    wl_list_insert(
       &(xx_zwm_service->service_window_list),
       &(xx_zwm_service_window->link)
     );

    return xx_zwm_service_window;
}

extern
void
xx_zwm_service_register_operation(
    struct xx_zwm_service *xx_zwm_service,
    const  char           *name,
    uint32_t               operation
) {
    //LOG_INFO("xx_zwm_service_register_operation %s %d", name, operation);

    struct operation_label      *operation_label = (
        calloc( 1, sizeof(struct operation_label) )
    );

    operation_label->operation = operation;

    strncpy(operation_label->operation_name, name, MAX_OPERATION_NAME_LENGTH);
    operation_label->operation_name[MAX_OPERATION_NAME_LENGTH - 1] = '\0';

    wl_list_insert( &xx_zwm_service->operation_list, &operation_label->link );

    struct xx_zwm_service_controller *xx_zwm_service_controller = (
        &xx_zwm_service->xx_zwm_service_controller
    );

    xx_zwm_service_controller_emit_new_service_operation(
        xx_zwm_service_controller,
        name,
        operation
    );

    return;
}

extern
void
xx_zwm_service_set_assert_method(
    struct xx_zwm_service            *xx_zwm_service,
    enum xx_zwm_service_assert_method assert_method
) {
    // Fix: Should the client be able to over-ride pre-configured values?

    switch ( assert_method ) {

        case XX_ZWM_SERVICE_ASSERT_METHOD_NOP:
        case XX_ZWM_SERVICE_ASSERT_METHOD_DEICONIFY:
        case XX_ZWM_SERVICE_ASSERT_METHOD_RAISE:
        case XX_ZWM_SERVICE_ASSERT_METHOD_RELOCATE:

            xx_zwm_service->twc_service_methods.assert_method = assert_method;
        break;

        default:
            xx_zwm_service->twc_service_methods.assert_method = XX_ZWM_SERVICE_ASSERT_METHOD_NOP;
        break;
    }

    return;
}

extern
void
xx_zwm_service_set_stand_down_method(
    struct xx_zwm_service                *xx_zwm_service,
    enum xx_zwm_service_stand_down_method stand_down_method
) {
    switch ( stand_down_method ) {

        case XX_ZWM_SERVICE_STAND_DOWN_METHOD_NOP:
        case XX_ZWM_SERVICE_STAND_DOWN_METHOD_LOWER:
        case XX_ZWM_SERVICE_STAND_DOWN_METHOD_ICONIFY:

            xx_zwm_service->twc_service_methods.stand_down_method = stand_down_method;
        break;

        default:
            xx_zwm_service->twc_service_methods.stand_down_method = XX_ZWM_SERVICE_STAND_DOWN_METHOD_NOP;
        break;
    }

    return;
}

extern
int
xx_zwm_service_add_listener(
    struct xx_zwm_service                *xx_zwm_service,
    const struct xx_zwm_service_listener *xx_zwm_service_listener,
    void                                 *data
) {
    xx_zwm_service->data                    = data;
    xx_zwm_service->xx_zwm_service_listener = *xx_zwm_service_listener;

    return ADD_LISTENER_SUCCESS;
}

 // ---------------------
 // xx_zwm_service_window

extern
void
xx_zwm_service_window_destroy(
    struct xx_zwm_service_window *xx_zwm_service_window
) {
      // An xx_wm_service_window should be destroyed before destroying
      // its corresponding twc_window.  This routine is called when
      // destroying a twc_window just to be sure.
    if ( xx_zwm_service_window == NULL ) { return; }

    xx_zwm_service_window->twc_window->xx_zwm_service_window = NULL;

    if ( xx_zwm_service_window->is_in_service_window_list ) {
        wl_list_remove( &xx_zwm_service_window->link );
    }

    free( xx_zwm_service_window );

    return;
}

extern
void
xx_zwm_service_window_set_context(
    struct xx_zwm_service_window *xx_zwm_service_window,
    enum   xx_wm_workspace_id     wksp_occupancy,
    struct wl_output             *wl_output
) {
    twc_window_set_context(
        xx_zwm_service_window->twc_window,
        wksp_occupancy,
        wl_output
    );

    return;
}

extern
void
xx_zwm_service_window_set_hotspot(
    struct xx_zwm_service_window *xx_zwm_service_window,
    int32_t                       hotspot_sx,
    int32_t                       hotspot_sy
) {
    twc_window_set_hotspot(
        xx_zwm_service_window->twc_window,
        hotspot_sx,
        hotspot_sy
    );

    return;
}

extern
void
xx_zwm_service_window_stand_down(
    struct xx_zwm_service_window *xx_zwm_service_window
) {
    struct twc_window *twc_window = (
        xx_zwm_service_window->twc_window
    );

    switch ( xx_zwm_service_window->xx_zwm_service->twc_service_methods.stand_down_method ) {

        case XX_ZWM_SERVICE_STAND_DOWN_METHOD_NOP:
        break;

        case XX_ZWM_SERVICE_STAND_DOWN_METHOD_ICONIFY:
            twc_window_set_iconify_state( twc_window, true );
        break;

        case XX_ZWM_SERVICE_STAND_DOWN_METHOD_LOWER:
            twc_window_lower( twc_window );
        break;
    }

    return;
}

#if 0
//   xx_zwm_service_window_window has no events
extern
int
xx_zwm_service_window_add_listener(
    struct xx_wm_service_window                *xx_wm_service_window,
    const struct xx_wm_service_window_listener *xx_wm_service_window_listener,
    void                                       *data
) {
    xx_wm_service_window->data                          = data;
    xx_wm_service_window->xx_wm_service_window_listener = *xx_wm_service_window_listener;

    return ADD_LISTENER_SUCCESS;
}
#endif
