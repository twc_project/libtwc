#ifndef WAYLAND_PRE_DEFINED_H
#define WAYLAND_PRE_DEFINED_H

  // Imported data types
#include "bic_connection.h"
#include "wayland_bic.h"

 /*
 ** Pre-defined wayland objects are for bic_server use only.
 */

 // -------------------------
 // Pre-defined wayland types

#include <twc/wayland_types.h>
extern const wld_geometry_t invalid_wld_geometry;

 // --------------------------
 // Pre-defined bic_connection

extern struct bic_connection   inert_bic_connection;
#define  INERT_BIC_CONNECTION (&inert_bic_connection)

 // -------------------
 // Pre-defined buffers

extern struct     wl_buffer     inert_wl_buffer;
#define      INERT_WL_BUFFER  ( &inert_wl_buffer)
#define UNDEFINED_WL_BUFFER  (struct wl_buffer*)(-1)

 // --------------------
 // Pre-defined surfaces

extern struct wl_surface   inert_wl_surface;
#define  INERT_WL_SURFACE (&inert_wl_surface)

extern struct   wl_surface   default_wl_surface;
#define DEFAULT_WL_SURFACE (&default_wl_surface)

extern struct   wl_surface   overlay_wl_surface;
#define OVERLAY_WL_SURFACE (&overlay_wl_surface)

#define DEFAULT_NESTED_WL_SURFACE (&default_wl_surface)
#define OVERLAY_NESTED_WL_SURFACE (&overlay_wl_surface)

#endif // WAYLAND_PRE_DEFINED_H
