
  // Wayland
#include <wayland-util.h>  // wl_list

  // APIs
#include "bic_server.h"     // Our API to ensure consistancy

  // Imported data types
#include "server_state-impl.h"
#include "service_cntlr.h"

  //
struct server_state BIC_SERVER;

struct xx_zwm_service_controller *reoccupy_service_controller;

extern
void
bic_server_setup(
    void
) {
    wl_list_init(BIC_SERVER_output_list);
    wl_list_init(BIC_SERVER_seat_list);
    wl_list_init(BIC_SERVER_connection_list);
    wl_list_init(BIC_SERVER_unbound_controller_list);
    wl_list_init(BIC_SERVER_agent_action_list);

    reoccupy_service_controller = NULL;
}

extern
struct agent_action*
lookup_agent_action(
    enum wm_agent_base_v1_agent_id agent_id,
    uint32_t                       action_id
) {
    struct agent_action *agent_action;
    twc_agent_action_list_for_each( agent_action ) {

        if ( agent_action-> agent_id ==  agent_id &&
             agent_action->action_id == action_id
        ) {
            return agent_action;
        }
    }

    return NULL;
}





  // When libjpeg is used only by plug-ins, meson doesn't include a
  // reference for it in libtwc.  The following forces meson to
  // include libjpeg in libjpeg.
#include <jpeglib.h>
extern void jpeg_meson_gratuitous_reference(void);
extern void jpeg_meson_gratuitous_reference(void){jpeg_start_decompress(NULL);}
