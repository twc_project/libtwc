
  // System
#include <stdint.h>     // int32_t

  // Wayland
#include <wayland-util.h>   // wl_array

  // TWC
#include <twc/twc_log.h>

  // APIs
#include "xdg_shell_event.h"    // Our API to ensure consistancy

  // Imported data types
#include "xdg_shell_bic.h"

 // -----------
 // xdg_surface

extern
void
xdg_surface_emit_configure(
    struct xdg_surface *xdg_surface
) {
    xdg_surface->ack_configure_outstanding = true;

    xdg_surface->xdg_surface_listener.configure(
        xdg_surface->data,
        xdg_surface,
        255         // Fix: serial
    );

    return;
}

 // ------------
 // xdg_toplevel

#include <wayland-util.h>

extern
void
xdg_toplevel_emit_configure(
    struct xdg_toplevel *xdg_toplevel,
    int32_t              width,
    int32_t              height,
    struct wl_array     *states
) {
    xdg_toplevel->xdg_toplevel_listener.configure(
        xdg_toplevel->data,
        xdg_toplevel,
        width,
        height,
        states
    );

    return;
}

extern
void
xdg_toplevel_emit_close(
    struct xdg_toplevel *xdg_toplevel
) {
    xdg_toplevel->xdg_toplevel_listener.close(
        xdg_toplevel->data,
        xdg_toplevel
    );

    return;
}

extern
void
xdg_toplevel_emit_configure_bounds(
    struct xdg_toplevel *xdg_toplevel,
    int32_t              width,
    int32_t              height
) {
    return;
}

extern
void
xdg_toplevel_emit_wm_capabilities(
    struct xdg_toplevel *xdg_toplevel,
    struct wl_array     *capabilities
) {
    xdg_toplevel->xdg_toplevel_listener.wm_capabilities(
        xdg_toplevel->data,
        xdg_toplevel,
        capabilities
    );

    return;
}
