#ifndef WAYLAND_EVENT_H
#define WAYLAND_EVENT_H

  // System
#include <stdint.h>         // uint32_t, ...

  // Wayland
#include <wayland-util.h>   // wl_fixed_t

#include "wayland_bic.h"

// To examine official event prototypes:
// grep        "^struct .*_listener"        /usr/include/wayland-client-protocol.h
// grep -A 100 "^struct wl_buffer_listener" /usr/include/wayland-client-protocol.h


 // ----------
 // wl_display
 // wl_registry
 // wl_callback
 // wl_shm
 //
 // Add as needed


  // -----------
  // wl_registry

extern
void
wl_registry_global(
    const struct wl_interface *wl_interface,
    uint32_t                   numeric_name
);

extern
void
wl_registry_global_remove(
    uint32_t numeric_name
);


 // ----------
 // wl_buffer

extern
void
wl_buffer_release(
    struct wl_buffer *wl_buffer
);


 // --------------
 // wl_data_offer
 // wl_data_source
 // wl_data_device
 // wl_surface
 // wl_seat
 //
 // Add as needed


 // -----------
 // wl_pointer

extern
void
wl_pointer_enter(
    struct wl_pointer *wl_pointer,
    struct wl_surface *wl_surface,
    wl_fixed_t         surface_x,
    wl_fixed_t         surface_y
);

extern
void
wl_pointer_leave(
    struct wl_pointer *wl_pointer,
    struct wl_surface *wl_surface
);

extern
void
wl_pointer_motion(
    struct wl_surface *wl_surface,
    uint32_t           time,
    wl_fixed_t         surface_x,
    wl_fixed_t         surface_y
);

extern
void
wl_pointer_button(
    struct wl_pointer *wl_pointer,
    uint32_t           time,
    uint32_t           button,
    uint32_t           state
);

extern
void
wl_pointer_event_axis(
    struct wl_pointer *wl_pointer,
    uint32_t           time,
    uint32_t           axis,
    wl_fixed_t         value
);


 // ------------
 // wl_keyboard

extern
void
wl_keyboard_enter(
    struct wl_keyboard *wl_keyboard,
    struct wl_surface  *wl_surface,
    struct wl_array    *keys
);

extern
void
wl_keyboard_leave(
    struct wl_keyboard *wl_keyboard,
    struct wl_surface  *wl_surface
);

extern
void
wl_keyboard_key(
    struct wl_keyboard *wl_keyboard,
    uint32_t            time,
    uint32_t            key,
    uint32_t            state
);

extern
void
wl_keyboard_modifiers(
    struct wl_keyboard *wl_keyboard,
    uint32_t            depressed,
    uint32_t            latched,
    uint32_t            locked,
    uint32_t            keyboard_layout
);


 // ---------
 // wl_touch
 // wl_output
 //
 // Add as needed

#endif  // WAYLAND_EVENT_H
