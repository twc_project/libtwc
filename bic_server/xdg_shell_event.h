#ifndef XDG_SHELL_EVENT_H
#define XDG_SHELL_EVENT_H

  // System
#include <stdint.h>         // int32_t, ...

  // Wayland
#include <wayland-util.h>   // wl_array

struct xdg_surface;
struct xdg_toplevel;

 // -----------
 // xdg_surface

extern
void
xdg_surface_emit_configure(
    struct xdg_surface *xdg_surface
);

 // ------------
 // xdg_toplevel

extern
void
xdg_toplevel_emit_configure(
    struct xdg_toplevel *xdg_toplevel,
    int32_t              width,
    int32_t              height,
    struct wl_array     *states
);

extern
void
xdg_toplevel_emit_close(
    struct xdg_toplevel *xdg_toplevel
);

extern
void
xdg_toplevel_emit_configure_bounds(
    struct xdg_toplevel *xdg_toplevel,
    int32_t              width,
    int32_t              height
);

extern
void
xdg_toplevel_emit_wm_capabilities(
    struct xdg_toplevel *xdg_toplevel,
    struct wl_array     *capabilities
);

#endif  // XDG_SHELL_EVENT_H
