#ifndef SERVICE_CNTLR_H
#define SERVICE_CNTLR_H

  // Imported data types
#include "wm_service_bic.h"     // xx_zwm_service_controller

extern
void
unbound_controller_list_insert(
    struct xx_zwm_service_controller *xx_zwm_service_controller
);

extern
void
unbound_controller_list_remove(
    struct xx_zwm_service_controller *xx_zwm_service_controller
);

#endif  // SERVICE_CNTLR_H
