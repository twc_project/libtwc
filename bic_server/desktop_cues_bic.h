#ifndef DESKTOP_CUES_EVENT_BIC_H
#define DESKTOP_CUES_EVENT_BIC_H

  // Wayland
#include <wayland-util.h>  // wl_list

  // Protocols
#include <desktop-cues-v1-client-protocol.h>

  // Imported data types
#include "wm_agent_bic.h"

#define XX_DESKTOP_CUES_BASE_V1_NUMERIC_NAME 0

struct xx_desktop_cues_base_v1 {
    void *data;
    struct xx_desktop_cues_base_v1_listener xx_desktop_cues_base_v1_listener;
};

#define MAX_CUE_NAME_LENGTH 16
struct xx_desktop_cue {
    void *data;
    struct xx_desktop_cue_listener xx_desktop_cue_listener;

      // For an xx_wm_decoration.desktop_cue_list
    struct wl_list link;

    char cue_name[MAX_CUE_NAME_LENGTH];

    uint32_t numeric_name;

    // Any client can create a desktop_cue.  But, since only the menu
    // agent can bind its controller, there need be only one
    // controller object per cue.  So, this struct contains an
    // embedded xx_desktop_cue_controller.  Its memory cannot be
    // deallocated until both objects are inactive.

      // embedded
    struct xx_desktop_cue_controller {
        void *data;
        struct xx_desktop_cue_controller_listener xx_desktop_cue_controller_listener;
    } xx_desktop_cue_controller;
};

#endif // DESKTOP_CUES_EVENT_BIC_H
