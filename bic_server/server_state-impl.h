#ifndef SERVER_STATE_IMPL_H
#define SERVER_STATE_IMPL_H

  // System
#include <xkbcommon/xkbcommon.h>

  // Wayland
#include <wayland-util.h>  // wl_list

  // Protocols
#include <wm-agent-v1-client-protocol.h>    // enum wm_agent_base_v1_agent_id

  // Imported data types
#include "srv_output-impl.h"
#include "wm_service_bic.h"

  // Opaque types
struct xx_zwm_service;

 /*
 ** Most Wayland servers built with libwayland-server will manage only
 ** one DRI device at a time, but are not limited in this way by
 ** libwayland.  Note that many individual DRI devices have mulitple
 ** outputs.
 */

extern struct server_state BIC_SERVER;

extern struct xx_zwm_service_controller *reoccupy_service_controller;

 // --------------------------
 // Accessors for server_state

#define BIC_SERVER_output_list             ( &BIC_SERVER.        srv_output_list )
#define BIC_SERVER_seat_list               ( &BIC_SERVER.          bic_seat_list )
#define BIC_SERVER_connection_list         ( &BIC_SERVER.    bic_connection_list )
#define BIC_SERVER_unbound_controller_list ( &BIC_SERVER.unbound_controller_list )
#define BIC_SERVER_agent_action_list       ( &BIC_SERVER.agent_action_list       )

#define MAX_AGENT_ACTION_NAME_LENGTH 32
struct agent_action {
    struct wl_list link;

    enum wm_agent_base_v1_agent_id agent_id;
    char                           name[MAX_AGENT_ACTION_NAME_LENGTH];
    uint32_t                       action_id;
    struct xx_zwm_service         *xx_zwm_service;
};

extern
struct agent_action*
lookup_agent_action(
    enum wm_agent_base_v1_agent_id agent_id,
    uint32_t                       action_id
);


  // List iterators

#define srv_output_list_for_each(o) \
    wl_list_for_each( (o), BIC_SERVER_output_list, link)

#define bic_connection_list_for_each(c) \
    wl_list_for_each( (c), BIC_SERVER_connection_list, link)

#define twc_agent_action_list_for_each(a) \
    wl_list_for_each( (a), BIC_SERVER_agent_action_list, link)

// Fix: this should be in a workspace file
#define twc_workspace_interest_list_for_each(b) \
    wl_list_for_each((b), TWC_workspace_interest_list, workspace_interest_link)

// Fix: this should be in a window file
#define twc_window_interest_list_for_each(b) \
    wl_list_for_each((b), TWC_window_interest_list, window_interest_link)

  // Declaration of the global state for the bic_server.  There is
  // exactly one instance of this struct.  It is defined in
  // server_setup.c
struct server_state {

    struct wl_list         srv_output_list;
    struct wl_list           bic_seat_list;
    struct wl_list     bic_connection_list;
    struct wl_list unbound_controller_list;
    struct wl_list       agent_action_list;
};

#endif  // SERVER_STATE_IMPL_H
