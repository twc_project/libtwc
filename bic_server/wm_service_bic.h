#ifndef WM_SERVICE_BIC_H
#define WM_SERVICE_BIC_H

  // System
#include <stdbool.h>

  // Wayland
#include <wayland-util.h>  // wl_list

  // Protocols
#include <wm-service-v1-client-protocol.h>  // Our API to ensure consistancy
#include <wm-agent-v1-client-protocol.h>   // enum wm_agent_base_v1_agent_id

  // APIs
#include "wm_agent_bic.h"
#include "twc_service.h"

struct operation_label {

      // Each wm_service has a list of service operations
    struct wl_list link;

#   define   MAX_OPERATION_NAME_LENGTH 32
    char     operation_name[MAX_OPERATION_NAME_LENGTH];
    uint32_t operation;
};

struct xx_zwm_service_base_v1 {
    // xx_zwm_service_base_v1 has no events

    bool is_client_vetted;
};

struct xx_zwm_service {

    // Any agent can create a service.  But, since only the menu agent
    // can bind its controller, there need be only one controller
    // object per service.  So, this struct contains an embedded
    // xx_wm_service_controller.  Its memory cannot be deallocated
    // until both objects are inactive.

    void *data;
    struct xx_zwm_service_listener xx_zwm_service_listener;

    bool is_destroyed;  // Only deallocate when both service and controller
                        // are destroyed.

      // Each connection has a list of wm_services.
    struct wl_list link;

//    enum wm_agent_base_v1_agent_id agent_id;
    struct twc_service_methods      twc_service_methods;

      // List of service_windows corresponding to this service.
    struct wl_list service_window_list;

      // list of operations registered to this service.
    struct wl_list operation_list;

#   define MAX_PROVIDER_NAME_LENGTH 32
#   define MAX_SERVICE_NAME_LENGTH  32
    char provider_name[MAX_SERVICE_NAME_LENGTH];
    char  service_name[MAX_SERVICE_NAME_LENGTH];

    struct xx_zwm_service_controller {

        // ------------------------
        // xx_wm_service_controller
        // (embedded)

        void *data;
        struct xx_zwm_service_controller_listener xx_zwm_service_controller_listener;

        bool is_active;  // Only deallocate when both service and
                         // controller are destroyed/inactive.

          // Until the menu agent binds this object, it is a member of
          // a list of unbound controllers.
        struct wl_list link;
        bool           is_in_unbound_controller_list;

        uint32_t numeric_name;

    } xx_zwm_service_controller;
};

 // ---------------------
 // xx_zwm_service_window
 //

struct xx_zwm_service_window {
#   if 0
      // A xx_zwm_service_window has no events
    void *data;
    struct xx_zwm_service_window_listener xx_zwm_service_window_listener;
#   endif

      // Each xx_zwm_service maintains a list of service_windows using
      // this link.
    struct wl_list link;
    bool           is_in_service_window_list;

      // The service window corresponds to this service
    struct xx_zwm_service *xx_zwm_service;

    struct twc_window *twc_window;
};

#endif // WM_SERVICE_BIC_H
