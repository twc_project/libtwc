
  // System
#include <stddef.h>  // NULL
#include <stdlib.h>  // malloc, calloc, free

  // Wayland
#include <wayland-util.h>           // wl_list
#include <wayland-client-core.h>    // Our API to ensure consistancy.
                                    // wl_display_connect, wl_display_get_fd,
                                    // wl_display_connect_to_fd,
                                    // wl_display_roundtrip, wl_display_dispatch

  // Protocols
#include "wayland_bic.h"            // wl_<interface>_interface
#include "xdg_shell_bic.h"          // xdg_wm_base_interface
#include "xdg_decoration_bic.h"     // zxdg_toplevel_decoration_v1_interface
#include "wm_agent_bic.h"           // xdg_agent_base_v1_interface

  // TWC
#include <twc/wayland_inert.h>
#include <twc/wm_agent_inert.h>
#include <twc/desktop_cues_inert.h>
#include <twc/twc_log.h>

  // APIs
#include "wm_agent_pre-defined.h"

  // Imported data types
#include "bic_connection.h"
#include "server_state-impl.h"

 /*
 ** This file contains alternate function definitions for some of the
 ** routines normally defined in <wayland_src>/src/wayland_client.c.
 ** These alternates are used by built-in clients.  They are declared
 ** in wayland-client-core.h
 */

 // ----------
 // wl_display

#define bic_connection_list_insert(b) \
    wl_list_insert( BIC_SERVER_connection_list, &((b)->link) )

#define bic_connection_list_remove(b) \
    wl_list_remove( &((b)->link) )

extern
struct wl_display*
wl_display_connect(
    const char *name
) {
    struct bic_connection *bic_connection = (
        calloc( 1, sizeof(struct bic_connection) )
    );

    wl_list_init(&bic_connection->wl_output_list);
    wl_list_init(&bic_connection->desktop_service_list);

    // Fix: initialize all members of bic_connection

    bic_connection->wl_display.  wl_display_listener    = inert_wl_display_listener;
    bic_connection->wl_registry. wl_registry_listener   = inert_wl_registry_listener;
    //              wl_compositor                       = has no events
    bic_connection->wl_shm.      wl_shm_listener        = inert_wl_shm_listener;

    bic_connection->wm_agent_base_v1 = initial_wm_agent_base_v1;

    struct xx_wm_interactive_agent *xx_wm_interactive_agent   = &bic_connection->wm_agent_base_v1.xx_wm_interactive_agent;
    xx_wm_interactive_agent->xx_wm_interactive_agent_listener = inert_xx_wm_interactive_agent_listener;
    xx_wm_interactive_agent->is_requested                     = false;

    struct xx_wm_menu_agent *xx_wm_menu_agent   = &bic_connection->wm_agent_base_v1.xx_wm_menu_agent;
    xx_wm_menu_agent->xx_wm_menu_agent_listener = inert_xx_wm_menu_agent_listener;
    xx_wm_menu_agent->is_requested              = false;

    struct xx_wm_decoration_stylist *xx_wm_decoration_stylist   = &bic_connection->wm_agent_base_v1.xx_wm_decoration_stylist;
    xx_wm_decoration_stylist->xx_wm_decoration_stylist_listener = inert_xx_wm_decoration_stylist_listener;
    xx_wm_decoration_stylist->is_requested                      = false;

    struct xx_wm_icon_stylist *xx_wm_icon_stylist   = &bic_connection->wm_agent_base_v1.xx_wm_icon_stylist;
    xx_wm_icon_stylist->xx_wm_icon_stylist_listener = inert_xx_wm_icon_stylist_listener;
    xx_wm_icon_stylist->is_requested                = false;

    struct xx_wm_workspace_agent *xx_wm_workspace_agent   = &bic_connection->wm_agent_base_v1.xx_wm_workspace_agent;
    xx_wm_workspace_agent->xx_wm_workspace_agent_listener = inert_xx_wm_workspace_agent_listener;
    xx_wm_workspace_agent->is_requested                   = false;

    struct xx_wm_task_agent *xx_wm_task_agent   = &bic_connection->wm_agent_base_v1.xx_wm_task_agent;
    xx_wm_task_agent->xx_wm_task_agent_listener = inert_xx_wm_task_agent_listener;
    xx_wm_task_agent->is_requested              = false;

    struct xx_zwm_service_base_v1 *xx_zwm_service_base_v1 = &bic_connection->xx_zwm_service_base_v1;
    xx_zwm_service_base_v1->is_client_vetted = false;

    struct xx_desktop_cues_base_v1 *xx_desktop_cues_base_v1   = &bic_connection->xx_desktop_cues_base_v1;
    xx_desktop_cues_base_v1->xx_desktop_cues_base_v1_listener = inert_xx_desktop_cues_base_v1_listener;

    bic_connection->wm_agent_base_v1.wm_agent_base_v1_listener = inert_wm_agent_base_v1_listener;

    bic_connection->agent_mask = WM_AGENT_BASE_V1_AGENT_ID_NONE;   // No agents yet.

    bic_connection->wl_registry.is_listener_set = false;

    bic_connection_list_insert(bic_connection);

    return &bic_connection->wl_display;
}

extern
int
wl_display_roundtrip(
    struct wl_display *wl_display
) {
    /*
    ** Since Built-in clients run in the server, there is only one
    ** thread of execution.  All request/event processing is performed
    ** in a run-to-completion fashion.  This means that when
    ** roundtrip() is invoked, all prior activity is already complete.
    **
    ** Note that <protocol_object>_add_listener() requests will
    ** trigger any necessary events once there are listeners to
    ** actually callback.
    */

    return 0;   // 0 is considered success
}

extern
void
wl_display_disconnect(
    struct wl_display *wl_display
) {
    // Fix: Is there more ToDo?  Check for lingering state and destroy
    // it??

    struct bic_connection *bic_connection = (
        wl_container_of(wl_display, bic_connection, wl_display)
    );

    bic_connection_list_remove(bic_connection);

    free(bic_connection);

    return;
}
