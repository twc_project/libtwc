#ifndef SRV_KEYBOARD_IMPL_H
#define SRV_KEYBOARD_IMPL_H

#define KEYBOARD_NAME_LENGTH 24

struct srv_keyboard {

    struct wl_list link;

    struct wcf_keyboard           *wcf_keyboard;
    char                           keyboard_name[KEYBOARD_NAME_LENGTH];
    enum wl_keyboard_keymap_format wl_keyboard_keymap_format;
    int32_t                        keymap_fd;
};

#endif // SRV_KEYBOARD_IMPL_H
