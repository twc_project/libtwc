#ifndef TWC_DECORATION_H
#define TWC_DECORATION_H

enum twc_decoration_policy {

    DP_Never_Decorate,      // window will not be decorated

    DP_Always_SSD,          // window will get SSD

    DP_Allow_CSD_else_No_Decorate,  // If requested by client, use
                                    // CSD, else DM_None

    DP_Allow_CSD_else_SSD,          // If requested by client, use
                                    // CSD, else use SSD
};

#endif // TWC_DECORATION_H
