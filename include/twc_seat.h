#ifndef TWC_SEAT_H
#define TWC_SEAT_H

  // System
#include <libinput.h>
#include <xkbcommon/xkbcommon.h>

  // Protocols
#include <wayland-client-protocol.h>    // enum wl_pointer_axis

  // Wayland
#include <wayland-util.h>   // wl_fixed_t

  // APIs
#include <twc/wayland_types.h>  // wld_dimension_t, wld_millisecond_t

  // Opaque types
struct wl_surface;
struct wl_pointer;
struct wl_keyboard;
struct twc_workspace;
struct srv_keyboard;
struct srv_pointer;

  // --------
  // Typedefs

typedef uint8_t twc_button_id;

  // Mouse buttons are numbered 1-9.  Zero is not used.
#define NUMBER_OF_BUTTONS     9
#define BUTTON_MAPPING_LIMIT (NUMBER_OF_BUTTONS+1)
typedef twc_button_id button_mapping_array[BUTTON_MAPPING_LIMIT];

enum twc_mouse_button {
    MB_Button_None          = 0,

    MB_Button_Left          = 1,
    MB_Button_Middle        = 2,
    MB_Button_Right         = 3,

    MB_Button_Spin_Forward  = 4,
    MB_Button_Spin_Backward = 5,
    MB_Button_Tilt_Left     = 6,
    MB_Button_Tilt_Right    = 7,

    MB_Button_Thumb_Rear    = 8,
    MB_Button_Thumb_Front   = 9,
};

  // -------------------
  // Function Prototypes

extern
void
twc_seat_initialize(
    void
);

extern
void
twc_seat_new_keyboard(
    struct srv_keyboard *srv_keyboard
);

extern
void*
twc_seat_get_default_seat(
    void
);

extern
struct xkb_keymap*
twc_seat_get_xkb_keymap(
    void
);

extern
void
twc_seat_new_output_dimensions(
    wld_dimension_t width,
    wld_dimension_t height
);

extern
void
twc_seat_enter_proxy_on_new_listener(
    struct wl_surface *wl_surface
);

extern
void
twc_seat_update_pointer_position(
    wl_fixed_t        ptr_loc_fix_lx,  // fixed format, layout coord
    wl_fixed_t        ptr_loc_fix_ly,
    wld_millisecond_t time_millisec
);

extern
void
twc_seat_recalc_pointer_focus(
    void
);

extern
void
twc_seat_reassign_keyboard_focus(
    struct wl_surface *wl_surface
);

extern
void
twc_seat_surface_remove_all_focus(
    struct wl_surface *wl_surface
);

extern
struct srv_output*
twc_seat_output_with_pointer_focus(
    void
);

extern
struct twc_workspace*
twc_seat_workspace_with_pointer_focus(
    void
);

extern
struct twc_view*
twc_seat_view_with_keyboard_focus(
    void
);

extern
void
twc_seat_pointer_location(
    wl_fixed_t *ptr_loc_fix_lx,
    wl_fixed_t *ptr_loc_fix_ly
);

extern
void
twc_seat_keyboard_key(
    struct srv_keyboard   *srv_keyboard,
    uint32_t               key,
    enum xkb_key_direction key_direction,
    wld_millisecond_t      time_millisec
);

extern
void
twc_seat_keyboard_modifiers(
    struct wl_keyboard *wl_keyboard,
    xkb_mod_mask_t      depressed,
    xkb_mod_mask_t      latched,
    xkb_mod_mask_t      locked,
    xkb_mod_mask_t      keyboard_layout
);

extern
void
twc_seat_pointer_button(
    uint32_t                   event_code,
    enum libinput_button_state button_direction,
    wld_millisecond_t          time_millisec
);

extern
void
twc_seat_pointer_axis(
    uint32_t             time,
    enum wl_pointer_axis axis,
    wl_fixed_t           value
);

#endif  // TWC_SEAT_H
