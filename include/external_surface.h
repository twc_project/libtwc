#ifndef EXTERNAL_SURFACE__H
#define EXTERNAL_SURFACE__H

enum external_surface_type {
    ET_XDG_SURFACE,
    ET_XWAYLAND_SURFACE,
};

#endif // EXTERNAL_SURFACE__H
