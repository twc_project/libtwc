#ifndef TWC_SURFACE_H
#define TWC_SURFACE_H

  // Imported data types
#include "bic_server/xdg_decoration_bic.h"

  // Opaque types
struct wl_surface;
struct twc_view;

extern
void
twc_surface_becomes_mapped(
    struct wl_surface *wl_surface
);

extern
void
twc_surface_no_longer_mapped(
    struct wl_surface *wl_surface
);

#endif // TWC_SURFACE_H
