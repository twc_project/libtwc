#ifndef TWC_WORKSPACE_IMPL_H
#define TWC_WORKSPACE_IMPL_H

  // System
#include <stdint.h>   // uint32_t

  // Wayland
#include <wayland-util.h>  // wl_list

  // Protocols
#include <wm-agent-v1-client-protocol.h>    // enum xx_wm_workspace_id

  // TWC
#include <twc/wayland_types.h>      // wld_<type>_t

  // APIs
#include "twc_workspace.h"

 // Opaque types
struct wl_surface;

  // ---------
  // Workspace

struct twc_workspace {
    struct wl_list link;

    uint32_t index;

    enum xx_wm_workspace_id id;   // doubles as numeric name

      // With CLICK_TO_FOCUS, remember who has focus on wksp switch.
    struct wl_surface *wl_surface_with_keyboard_focus;

      // For pointer warping on wksp switch, remember the pointer
      // postion.
    wld_coordinate_t pointer_x, pointer_y;

      // A list of the xx_wm_workspace objects created by xdg agents
    struct wl_list xx_wm_workspace_list;

    char *name;
};

#endif // TWC_WORKSPACE_IMPL_H
