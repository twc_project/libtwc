#ifndef SRV_OUTPUT_H
#define SRV_OUTPUT_H

  // Wayland
#include <wayland-util.h>       // wl_fixed_t

  // TWC
#include <twc/wayland_types.h>  // wld_<type>_t

  // Opaque types
struct wl_surface;
struct srv_output;
struct wcf_output;
struct twc_workspace;

extern
struct srv_output*
srv_output_create(
    struct wcf_output *wcf_output,
    char              *output_name,
    wld_dimension_t    width,
    wld_dimension_t    height
);

extern
void
srv_output_destroy(
    struct srv_output *srv_output
);

extern
void
srv_output_dimensions(
    struct srv_output *srv_output,
    wld_dimension_t   *width,
    wld_dimension_t   *height
);

extern
void
srv_output_set_wallpaper(
    struct srv_output    *srv_output,
    struct twc_workspace *twc_workspace,
    struct wl_surface    *wallpaper_surface
);

extern
void
srv_output_next_window_coord(
    struct srv_output *srv_output,
    wld_coordinate_t  *next_window_x,
    wld_coordinate_t  *next_window_y
);

extern
void
srv_output_set_next_window_coord(
    struct srv_output *srv_output,
    wld_coordinate_t   next_window_x,
    wld_coordinate_t   next_window_y
);

extern
void
srv_output_next_icon_coord(
    struct srv_output *srv_output,
    wld_coordinate_t  *next_icon_x,
    wld_coordinate_t  *next_icon_y
);

extern
void
srv_output_set_next_icon_coord(
    struct srv_output *srv_output,
    wld_coordinate_t   next_icon_x,
    wld_coordinate_t   next_icon_y
);

extern
struct twc_workspace*
srv_output_active_workspace(
    struct srv_output *srv_output
);

extern
void
srv_output_set_active_workspace(
    struct srv_output    *srv_output,
    struct twc_workspace *twc_workspace
);

extern
wld_uint_t
srv_output_numeric_name(
    struct srv_output *srv_output
);

extern
struct srv_output*
srv_output_at(
    wl_fixed_t pointer_location_fix_lx,
    wl_fixed_t pointer_location_fix_ly
);

extern
void
srv_output_compose(
    struct srv_output *srv_output
);

extern
struct srv_output*
srv_output_numeric_name_lookup(
    uint32_t name
);

#endif  // SRV_OUTPUT_H
