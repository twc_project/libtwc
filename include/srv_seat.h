#ifndef SRV_SEAT_H
#define SRV_SEAT_H

  // System
#include <stdint.h>  // int32_t

  // Wayland
#include <wayland-util.h>       // wl_list

  // Protocols
#include <wayland-client-protocol.h>   // enum wl_seat_capability

#define INPUT_NAME_LENGTH 32

struct srv_seat {

      // pointer location
    wl_fixed_t ptr_loc_fix_lx;  // fixed format, layout coord
    wl_fixed_t ptr_loc_fix_ly;

    enum wl_seat_capability wl_seat_capabilities;

    struct wl_surface *    wl_surface_with_ptr_focus;
    void              *nested_surface_with_ptr_focus;

    struct wl_surface *wl_surface_with_kbd_focus;

    char  pointer_name[INPUT_NAME_LENGTH];
    char keyboard_name[INPUT_NAME_LENGTH];

    enum wl_keyboard_keymap_format wl_keyboard_keymap_format;

    int32_t    keymap_fd;     // keymap file descriptor
    wld_uint_t keymap_file_size;

    struct wl_list      srv_seat_keyboard_list;
    struct srv_keyboard *srv_keyboard;

    struct srv_pointer  *srv_pointer;
};

#endif // SRV_SEAT_H
