#ifndef TWC_WINDOW_H
#define TWC_WINDOW_H

  // System
#include <stdbool.h>
#include <stdint.h>     // uint32_t

  // Wayland
#include <wayland-util.h>   // wl_list, wl_fixed_t

  // Protocols
#include <xdg-shell-client-protocol.h>  // enum xdg_toplevel_resize_edge

  // TWC
#include <twc/wayland_types.h>  // wld_<type>_t

  // APIs
#include "twc_workspace.h"

  // Opaque types
struct wl_surface;
struct wl_output;
struct twc_window;
struct twc_view;
struct xx_wm_decoration;
struct xdg_toplevel;
struct xx_wm_workspace;

#define RESIZE_EDGE_NOT_PRE_SPECIFIED (0xfff)

#define MAX_APP_ID_LENGTH 256

#define MAX_TITLE_LENGTH 256

extern struct twc_view  null_twc_view;
#define NULL_TWC_VIEW (&null_twc_view)

  // Window Gravity
enum window_gravity {
    WG_NorthWest,   // (0      , 0       )
    WG_North,       // (width/2, 0       )
    WG_NorthEast,   // (width  , 0       )
    WG_West,        // (0      , height/2)
    WG_Center,      // (width/2, height/2)
    WG_East,        // (width  , height/2)
    WG_SouthWest,   // (0      , height  )
    WG_South,       // (width/2, height  )
    WG_SouthEast,   // (width  , height  )
};

 // ----------
 // twc_window

extern
void
twc_window_initialize(
    void
);

extern
struct twc_window*
twc_window_create(
    struct wl_surface *wl_surface
);

extern
void
twc_window_destroy(
    struct twc_window *twc_window
);

extern
struct twc_window*
twc_window_numeric_name_lookup(
    uint32_t numeric_name
);

extern
void
twc_window_update_display_list(
    struct twc_window *twc_window
);

extern
void
twc_window_update_surface_size(
    struct wl_surface *wl_surface
);

extern
struct wl_surface*
twc_window_surface_at(
      // pointer location coordinates
    wl_fixed_t  ptr_loc_fix_lx, // fixed format, layout  coord
    wl_fixed_t  ptr_loc_fix_ly,
    wl_fixed_t *ptr_loc_fix_sx, // fixed format, surface coord
    wl_fixed_t *ptr_loc_fix_sy,
    void      **nested_surface
);

extern
void
twc_window_activate_overlay(
    void
);

extern
void
twc_window_deactivate_overlay(
    void
);

extern
void
twc_window_raise(
    struct twc_window *twc_window
);

extern
void
twc_window_lower(
    struct twc_window *twc_window
);

extern
void
twc_window_relocate_to_pointer(
    struct twc_window *twc_window
);

extern
void
twc_window_reoccupy(
    struct twc_window *twc_window,
    wksp_occupancy_t   wksp_occupancy
);

extern
void
twc_window_set_context(
    struct twc_window      *twc_window,
    enum xx_wm_workspace_id wksp_occupancy,
    struct wl_output       *wl_output
);

extern
void
twc_window_set_hotspot(
    struct twc_window *twc_window,
    int32_t            hotspot_sx,
    int32_t            hotspot_sy
);

extern
void
twc_window_initial_configure(
    struct wl_surface *wl_surface
);

extern
void
twc_window_apply_user_config(
    struct twc_window *twc_window
);

extern
void
twc_window_set_iconify_state(
    struct twc_window *twc_window,
    bool               iconify_state
);

extern
void
twc_view_toggle_iconify(
    struct twc_view *twc_view
);

 // --------
 // twc_view

extern
void
twc_view_raise_lower(
    struct twc_view *twc_view
);

extern
void
twc_view_vzoom(
    struct twc_view *twc_view
);

extern
void
twc_view_close(
    struct twc_view *twc_view
);

extern
void
twc_view_commit_decoration(
    struct xx_wm_decoration *xx_wm_decoration
);

extern
void
twc_view_recalc_geometry(
    struct twc_view *twc_view
);

extern
void
twc_view_update_location(
    struct twc_view *client_view,
    wld_coordinate_t layout_x,
    wld_coordinate_t layout_y
);

extern
enum decoration_mode
twc_update_decoration_mode(
    struct twc_view       *twc_view,
    enum   decoration_mode request_mode
);

extern
wksp_occupancy_t
twc_view_wksp_occupancy(
    struct twc_view *twc_view
);

extern
struct twc_view*
twc_view_at(
    wl_fixed_t  ptr_loc_fix_lx, // fixed format, layout coord
    wl_fixed_t  ptr_loc_fix_ly
);

extern
wld_coordinate_t
twc_view_horizontal_fit(
    struct twc_view  *twc_view,
    wld_coordinate_t  output_x,
    wld_dimension_t   output_width
);

extern
wld_coordinate_t
twc_view_vertical_fit(
    struct twc_view  *twc_view,
    wld_coordinate_t  output_y,
    wld_dimension_t   output_height
);

extern
struct twc_view*
twc_view_with_pointer(
    void
);

extern
void
twc_view_select(
    struct twc_view *twc_view
);

extern
bool
twc_view_select_for_resize(
    struct twc_view              *twc_view,
    enum xdg_toplevel_resize_edge resize_edges
);

extern
void
twc_view_unselect(
    void
);

extern
void
twc_view_relocate_selected(
    wl_fixed_t ptr_loc_fix_lx,  // fixed format, layout coord
    wl_fixed_t ptr_loc_fix_ly
);

extern
void
twc_view_resize_selected(
    wl_fixed_t ptr_loc_fix_lx,  // fixed format, layout coord
    wl_fixed_t ptr_loc_fix_ly
);

extern
void
twc_view_toggle_iconify_selected(
    void
);

#endif // TWC_WINDOW_H
