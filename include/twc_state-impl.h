#ifndef TWC_STATE_IMPL_H
#define TWC_STATE_IMPL_H

  // System
#include <stdbool.h>
#include <xkbcommon/xkbcommon.h>

  // Wayland
#include <wayland-util.h>  // wl_list

 // TWC
#include <srv_seat.h>
#include <twc/toml.h>

  // APIs
#include "twc_decoration.h"

 /*
 ** Most Wayland servers built with libwayland-server will manage only
 ** one DRI device at a time, but are not limited in this way by
 ** libwayland.  Note that many individual DRI devices have mulitple
 ** outputs.
 **
 ** A wl_display object in libwayland-server represents the set of all
 ** clients currently connected to a server.  It does not refer to any
 ** graphical device or concept.  Using the term display is
 ** unfortunate, but seems to be a holdover from using $DISPLAY by X11
 ** to denote both an X server and its screens.
 **
 ** Many WLR object classes are meant to have exactly one instance per
 ** wl_display.  E.g., wlr_compositor.  These are created with a
 ** wlroots function whose name is of the form wlr_object_create(),
 ** e.g., wlr_compositor_create().
 */

  // Opaque types
struct twc_workspace;

extern struct twc_state TWC;

 // -----------------------
 // Accessors for twc_state

#define TWC_srv_pointer_list     ( &TWC.srv_pointer_list   )
#define TWC_srv_keyboard_list    ( &TWC.srv_output_list    )

#define TWC_workspace_list       ( &TWC.workspace_list     )

#define TWC_window_list          ( &TWC.twc_window_list    )
#define TWC_display_list         ( &TWC.twc_display_list   )

#define TWC_workspace_interest_list ( &TWC.workspace_interest_list )
#define TWC_window_interest_list    ( &TWC.window_interest_list    )

#define TWC_workspace_output_unison ( TWC.workspace_output_unison )

#define TWC_focus_follows_pointer   (   TWC.focus_follows_pointer )
#define TWC_click_to_focus          ( ! TWC.focus_follows_pointer )

  // List iterators

#define twc_workspace_list_for_each(w) \
    wl_list_for_each( (w), TWC_workspace_list, link)

#define twc_window_list_for_each(w) \
    wl_list_for_each( (w), TWC_window_list, link)

#define twc_display_list_for_each(v) \
    wl_list_for_each( (v), TWC_display_list, link)

#define twc_display_list_for_each_reverse(v) \
    wl_list_for_each_reverse((v), TWC_display_list, link)   // Note reverse

#define twc_workspace_interest_list_for_each(b) \
    wl_list_for_each((b), TWC_workspace_interest_list, workspace_interest_link)

#define twc_window_interest_list_for_each(b) \
    wl_list_for_each((b), TWC_window_interest_list, window_interest_link)

  // Declaration of the global state for TWC.  There is exactly one
  // instance of this struct.  It is defined in twc_setup.c
struct twc_state {

    struct wl_list workspace_list;
    struct wl_list twc_window_list;
    struct wl_list twc_display_list;    // The twc_views currently
                                        // eligible for display.

      // Fix: Support multiple seats
    struct srv_seat srv_seat;

      // Agents interested in new workspaces/windows
    struct wl_list workspace_interest_list; // connections using workspace registry
    struct wl_list    window_interest_list; // connections using window    registry

    struct twc_workspace *default_workspace;    // on new outputs

    // Compositor Configuration

    bool focus_follows_pointer;
    bool click_to_raise;
    bool auto_raise;
    bool dont_move_off;
    bool staddle_outputs;
    bool raise_on_move;
    bool raise_on_resize;
      // There may or may not be a different active workspace for each
      // output.  In the former case, there should be a switcher
      // window for each output.
    bool workspace_output_unison;
      // Fix: Can windows straddle two outputs?
    bool allow_output_straddle;
      // Fix? Can windows spill beyond the layout?
    bool allow_layout_spillover;

    enum twc_decoration_policy default_decoration_policy;
    xkb_mod_mask_t             shortcut_modifiers;

    toml_table_t *service_table;
    toml_table_t *  agent_table;
};

#endif  // TWC_STATE_IMPL_H
