#ifndef SRV_KEYBOARD_H
#define SRV_KEYBOARD_H

  // System
#include <stdint.h>                 // uint32_t

  // Protocols
#include <wayland-client-protocol.h>    // enum wl_keyboard_keymap_format

  // Wayland
#include <wayland-util.h>           // wl_list

  // Keyboard modifiers.
  //
  // When the wcf is wlroots, these match the WLR_MODIFIER_xxx values.
  // See
  //   https://github.com/swaywm/wlroots/blob/master/include/wlr/types/wlr_keyboard.h
  //
  // In XKB, each modifier has an index which varies by keyboard.
  // Wlroots maps them (using their names) into the standard ordering
  // shown below.  The Wlroots mapping is wlr_keyboard->mod_indexes[].
  // See wlr_keyboard_set_keymap() here
  //   https://gitlab.freedesktop.org/wlroots/wlroots/-/blob/master/types/wlr_keyboard.c
  //

  // Opaque types
struct srv_keyboard;
struct wcf_keyboard;

typedef enum srv_dev_keyboard_modifiers {
    KB_MOD_NONE  = 0x0     ,
    KB_MOD_SHIFT = 0x1 << 0, // Shift
    KB_MOD_CAPS  = 0x1 << 1, // Lock
    KB_MOD_CTRL  = 0x1 << 2, // Control
    KB_MOD_ALT   = 0x1 << 3, // Mod1
    KB_MOD_NUM   = 0x1 << 4, // Mod2
    KB_MOD_MOD3  = 0x1 << 5, // Mod3
    KB_MOD_LOGO  = 0x1 << 6, // Mod4, aka windows key or super key
    KB_MOD_MOD5  = 0x1 << 7, // Mod5
} srv_dev_keyboard_modifiers_t;

struct srv_keyboard*
srv_keyboard_create(
    struct wcf_keyboard           *wcf_keyboard,
    char                          *keyboard_name,
    enum wl_keyboard_keymap_format wl_keyboard_keymap_format,
    int32_t                        keymap_fd
);

extern
void
srv_keyboard_destroy(
    struct srv_keyboard *srv_keyboard
);

#endif // SRV_KEYBOARD_H
