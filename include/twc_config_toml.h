#ifndef TWC_CONFIG_TOML_H
#define TWC_CONFIG_TOML_H

  // System
#include <xkbcommon/xkbcommon.h>

  // APIs
#include "twc_window.h"
#include "twc_seat.h"
#include "twc_decoration.h"

  // Opaque types
struct twc_state;
struct twc_window;

#define SERVICE_UNDEFINED_INDEX ((int8_t) -1)

extern
void
twc_config_read_file(
    char             *config_file,
    struct twc_state *twc
);

 // ----------------------
 // Config lookup routines

extern
bool
twc_config_lookup_OccupyAll(
    char *app_id,
    char *title
);

extern
bool
twc_config_lookup_StartIconified(
    char *app_id,
    char *title
);

extern
bool
twc_config_lookup_IconifyByMasking(
    char *app_id,
    char *title
);

extern
bool
twc_config_lookup_HoldIcon(
    char *app_id,
    char *title
);

extern
bool
twc_config_lookup_AnchoredWindows(
    char                *app_id,
    char                *title,
    enum window_gravity *window_gravity,
    char               **geometry_string
);

extern
enum twc_decoration_policy
twc_config_lookup_DefaultDecorationPolicy(
    void
);

extern
bool
twc_config_lookup_DecorationPolicy(
    char                       *app_id,
    char                       *title,
    enum twc_decoration_policy *decoration_policy
);

extern
int8_t
twc_config_lookup_ReoccupyIndex(
    void
);

extern
void
twc_config_lookup_ButtonMapping(
    button_mapping_array button_mapping
);

extern
void
twc_config_lookup_LibinputConfig(
    void
);

extern
void
twc_config_lookup_XKBKeyBoard(
    char **XKBRules,
    char **XKBModel,
    char **XKBLayout,
    char **XKBVariant,
    char **XKBOptions
);

#define TWC_DEFAULT_SHORTCUT_MODS KB_MOD_ALT

extern
xkb_mod_mask_t
twc_config_lookup_KeyBoardShortCutMods(
    void
);

#define SHORTCUT_KEY_MAX 128

extern
void
twc_config_lookup_shortcut_keys(
    bool (*is_shortcut_key)[SHORTCUT_KEY_MAX]
);

#endif // TWC_CONFIG_TOML_H
