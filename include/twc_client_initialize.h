#ifndef TWC_CLIENT_INITIALIZE_H
#define TWC_CLIENT_INITIALIZE_H

  // System
#include <stdbool.h>

  // TWC
#include <twc/toml.h>

struct twc_config_data {
    toml_table_t *service_table;
    toml_table_t *  agent_table;
};

typedef
struct wl_display*
client_initialize_f(
    bool                   *running,
    struct twc_config_data *twc_config_data
);

#endif // TWC_CLIENT_INITIALIZE_H
