#ifndef TWC_WINDOW_IMPL_H
#define TWC_WINDOW_IMPL_H

  // System
#include <stdbool.h>

  // Wayland
#include <wayland-util.h>   // wl_list

  // TWC
#include <twc/wayland_types.h>  // wld_<type>_t

  // APIs
#include "twc_window.h"     // Our API to ensure consistancy
#include "twc_decoration.h"
#include "twc_workspace.h"

  // Imported Data Types
#include "bic_server/wm_agent_bic.h"

  // Opaque types
struct wl_surface;
struct twc_tag;
struct srv_output;

 /*
 **   -----------------------
 **   Anatomy of a TWC Window
 **
 **   Window           Views             Surfaces          Surface Types (Role)
 **
 **  +----------+     +------------+    +---------------+
 **  |twc_window|<--->|client_view |<-->|  view_surface |  ST_XDG_TOPLEVEL
 **  |          |     |            |    +---------------+
 **  |          |     |            |
 **  |          |     |            +--------------------+
 **  |          |     |              xx_wm_decoration   |
 **  |          |     |            .                    |
 **  |          |     |            .        top_surface |  ST_XX_WM_DECO
 **  |          |     |            .     bottom_surface |      |
 **  |          |     |            .       left_surface |      |
 **  |          |     |            .      right_surface |      v
 **  |          |     +---------------------------------+
 **  |          |
 **  |          |     +------------+    +---------------+
 **  |          |<--->|icon_view   |<-->|  view_surface |  ST_XX_WM_ICON
 **  |          |     |            |    +---------------+
 **  +----------+     |            |
 **                   |            +--------------------+
 **                   |              xx_wm_decoration   |
 **                   |            .                    |
 **                   |            .        top_surface |  ST_XX_WM_DECO
 **                   |            .     bottom_surface |      |
 **                   |            .       left_surface |      |
 **                   |            .      right_surface |      v
 **                   +---------------------------------+
 */

#define TWC_WINDOW_HAS_APP_ID(w) ((w)->app_id[0] != '\0' )
#define TWC_WINDOW_HAS_TITLE( w) ((w)-> title[0] != '\0' )

  // According to the wl_surface interface description in the Wayland
  // Core Protocol, windows are shell surfaces as defined by a shell
  // protocol.  Eg, see the description of the xdg_wm_base interface
  // in the xdg_shell protocol.
struct twc_window {
    struct wl_list link;
    bool           is_in_window_list;
    uint32_t       numeric_name;

      // For display purposes, a window has two images, the client's
      // surface and an icon surface.
    struct twc_view *client_view;
    struct twc_view *  icon_view;

    wksp_occupancy_t    wksp_occupancy;
    enum window_gravity window_gravity;

    bool is_iconified;          // display icon view
    bool iconify_by_masking;    // display nothing when iconified
    bool hold_icon;             // display icon when not iconified

    wld_geometry_t pre_initial_commit_geometry;

      // Some agent windows care about context.
    struct srv_output    *preferred_output;
    struct twc_workspace *preferred_workspace;

      // Some windows provide desktop services.
    struct xx_zwm_service_window           *xx_zwm_service_window;
    enum   xx_zwm_service_assert_method     assert_method;
    enum   xx_zwm_service_stand_down_method stand_down_method;

      // A list of xx_wm_window objects advertised to and bound by xdg
      // agents.
    struct wl_list xx_wm_window_list;

    char app_id[MAX_APP_ID_LENGTH];
    char title [MAX_TITLE_LENGTH ];

    enum window_configured {
        WC_no_configuration   = 0x0,
        WC_gravity            = 0x1 << 1,
        WC_geometry           = 0x1 << 2,
        WC_occupy_all         = 0x1 << 3,
        WC_start_iconified    = 0x1 << 4,
        WC_iconify_by_masking = 0x1 << 5,
        WC_hold_icon          = 0x1 << 6,
        WC_decoration_policy  = 0x1 << 7,
        WC_wm_capabilities    = 0x1 << 8,
    } window_configured;

      // For dynamic resizing, don't send a subsequent
      // toplevel.configure (resize) event until the client has
      // committed a new buffer based on the outstanding resize event.
      //
      // Fix: This behavoir breaks for terminal emulator foot.  It
      // refuses to go below 10 pixels wide, so doesn't ever commit a
      // new buffer.  We waited forever for the new size.
      //
    /*bool outstanding_resize_event;*/
};

 /*
 ** Twc View:
 **
 ** A view is a collection of related surfaces.  When creating an
 ** aggregate image for output, the server iterates over the list of
 ** displayable views.  For each such view, the compositor paints all
 ** appropriate surfaces belonging to the view before moving on to the
 ** next view.
 **
 ** A twc_view consists of an xdg_toplevel surface, as denoted by the
 ** member named view_surface, plus 0 to 4 decoration surfaces as
 ** described though the member named xx_wm_decoration.
 **
 ** The view origin is the upper, left-most point of these surfaces.
 **
 **
 ** Surface Enclosures:
 **
 ** The documentation for xdg_surface.set_window_geometry() shows
 ** that clients are allowed to create surface imagery which may
 **
 **  "... extend outside of the wl_surface itself ..."
 **
 ** In other words, clients may create subsurfaces which lie outside
 ** of the primary surface boundaries.  Compositors are expected to
 ** display these "stray" pixels subject to some constraints.  The
 ** constraints are self-imposed by the client and are communicated to
 ** the server using the xdg_surface.set_window_geometry() request.
 **
 ** The set_window_geometry() request tells the compositor how much
 ** latitude the client will take.  It does this by declaring an
 ** enclosing rectangle(*).  In this situation, the set_geometry plus
 ** the primary surface would be arranged like this:
 **
 **    _____________
 **   |E
 **   |    ________
 **   |   |S
 **   |   |  .
 **   |   |    .
 **   |   |      .
 **   |
 **
 ** Where S is the location of the primary xdg_surface and E marks the
 ** surface enclosure, i.e., the area where some subsurfaces may
 ** actually render.
 **
 ** The compositor is expected to display these additional pixels.  If
 ** the enclosure offsets relative to the primary surface change, the
 ** compositor is encouraged to make location adjustsments so that the
 ** point S does not move on screen.  Any such adjustment must also
 ** include adjustments to the decorations.
 **
 ** When there is no set_geometry, E and S are the same and refer to
 ** the origin of the surface.
 **
 ** (*) Note: the compositor must not paint outside of the surface
 ** enclosure.  If the primary surface extends beyond the enclosure,
 ** those areas will not be rendered.  I.e., clients may "clip" their
 ** own primary surfaces.
 */

enum display_format {
    DF_NOT_SPECIFIED,
    DF_VIEW_ONLY,
    DF_VIEW_PLUS_DECO,
    DF_ROLLED_UP,       // aka shade or shaded. see KDE
};

#define TWC_VIEW_IS_CLIENT_VIEW(v) ( \
    (v) == (v)->twc_window->client_view \
)

#define TWC_VIEW_IS_ICON_VIEW(v) ( \
    (v) == (v)->twc_window->icon_view \
)

#define TWC_VIEW_IS_DECORATED(v) ( \
    ( (v)->display_format == DF_VIEW_PLUS_DECO || \
      (v)->display_format == DF_ROLLED_UP         \
    ) &&                                          \
    (v)->xx_wm_decoration.is_active                 \
)

#define TWC_VIEW_HAS_COORDIATES(v) ( \
    ( (v)->layout_x != WLD_INVALID_COORD ) \
)

struct twc_view {
    struct wl_list link;
    bool           is_in_display_list;

    struct twc_window *twc_window;
    struct wl_surface *view_surface;    // client or icon surface

    enum twc_decoration_policy twc_decoration_policy;

    enum display_format display_format;

      // Layout coordinates of the view origin.
    wld_coordinate_t layout_x,   layout_y;
      // View dimensions inclusive of decoraction edges
    wld_dimension_t  width,      height;
    wld_coordinate_t hotspot_sx, hotspot_sy;

      // View relative coordinates of various view surfaces.
    wld_dimension_t     deco_top_vx,     deco_top_vy;
    wld_dimension_t  deco_bottom_vx,  deco_bottom_vy;
    wld_dimension_t    deco_left_vx,    deco_left_vy;
    wld_dimension_t   deco_right_vx,   deco_right_vy;
    wld_dimension_t view_surface_vx, view_surface_vy;
    wld_dimension_t    enclosure_vx,    enclosure_vy;   // set_window_geometry(),
                                                        // See comments above.

      // The accessory surfaces.
    struct xx_wm_decoration xx_wm_decoration;

    bool has_alternate_size;    // Eg, zoom or fullscreen
      // Go back to these when alternate_size is over.
    wld_coordinate_t prezoom_layout_x,      prezoom_layout_y;
    wld_dimension_t  prezoom_surface_width, prezoom_surface_height;
};

#endif // TWC_WINDOW_IMPL_H
