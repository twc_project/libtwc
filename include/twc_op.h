#ifndef TWC_OP_H
#define TWC_OP_H

  // System
#include <stdbool.h>

#include <xkbcommon/xkbcommon.h>
#include <libinput.h>

  // Protocols
#include <xdg-shell-client-protocol.h>
#include <wm-agent-v1-client-protocol.h>

  // TWC
#include <twc/wayland_types.h>      // wld_millisecond_t

  // Opaque types
struct twc_view;
struct xdg_toplevel;

extern
void
twc_op_overlay_surface_annexed(
    bool overlay_surface_state  // annexed or not
);

 // ------
 // Resize

extern
void
twc_op_resize_view(
    struct twc_view              *twc_view,
    enum xdg_toplevel_resize_edge resize_edges
);

 // -----------------
 // Window Intrinsics

extern
void
twc_action_window_intrinsic(
    struct twc_view            *twc_view,
    enum xx_wm_window_intrinsic xx_wm_window_intrinsic
);

extern
void
twc_action_select_window_intrinsic(
    enum xx_wm_window_intrinsic xx_wm_window_intrinsic
);

extern
void
twc_action_apply_window_intrinsic(
    // doesn't make sense for menu agent to request - it has the ptr.
    // Keyboard Shortcut is OK tho
    enum xx_wm_window_intrinsic xx_wm_window_intrinsic
);

 // -------------
 // Agent Actions

extern
void
twc_action_select_agent_action(
    enum wm_agent_base_v1_agent_id agent_id,
    uint32_t                       action_id
);

 // --------------
 // Compositor Ops

extern
void
twc_op_compositor_op(
    enum xx_wm_menu_agent_compositor_op compositor_op
);

extern
void
twc_op_dismiss_agent(
    void
);

 // -----------
 // Interactive

extern
struct twc_view*
twc_op_select_at(
    wl_fixed_t layout_x,
    wl_fixed_t layout_y
);

extern
void
twc_op_tracking_at(
    wl_fixed_t layout_x,
    wl_fixed_t layout_y
);

extern
void
twc_op_drop(
    void
);

#endif  // TWC_OP_H
