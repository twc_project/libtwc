#ifndef TWC_WORKSPACE_H
#define TWC_WORKSPACE_H

  // System
#include <stdint.h>     // uint32_t

  // Protocols
#include <wm-agent-v1-client-protocol.h>   // XX_WM_WORKSPACE_ID_WKSP_NONE

 // Opaque types
struct twc_state;
struct twc_workspace;
enum   xx_wm_workspace_id;

  // An xx_wm_workspace_id is an enum with exactly one bit set.  The
  // integer type wksp_occupancy_t is a bitmap of
  // xx_wm_workspace_id's.
typedef enum xx_wm_workspace_id wksp_occupancy_t;

#define XX_WM_WORKSPACE_ID_WKSP_ALL ((wksp_occupancy_t)(~0))

#define WKSP_INDEX_to_ID(i) (enum xx_wm_workspace_id)( 0x1 << (i) )

#define WKSP_OCCUPANCY_ALL  ((wksp_occupancy_t)(-1))
#define WKSP_OCCUPANCY_NONE ((wksp_occupancy_t)( 0))

#define WKSP_OCCUPANCY_ADD(      o,id) ( (o) |  (id) )  // or
#define WKSP_OCCUPANCY_REMOVE(   o,id) ( (o) & ~(id) )  // and with complement
#define WKSP_OCCUPANCY_IS_MEMBER(o,id) ( (o) &  (id) )  // and

extern
void
twc_workspace_initialize(
    struct twc_state *twc
);

extern
void
twc_workspace_new(  // Called by config file parser
    char *workspace_name
);

extern
void
twc_workspace_swap(
    struct twc_workspace *twc_workspace_a,
    struct twc_workspace *twc_workspace_b
);

extern
uint32_t
twc_workspace_index(
    struct twc_workspace *twc_workspace
);

extern
enum xx_wm_workspace_id
twc_workspace_id(
    struct twc_workspace *twc_workspace
);

extern
struct twc_workspace*
twc_workspace_numeric_name_lookup(
    uint32_t numeric_name
);

extern
struct twc_workspace*
twc_workspace_default(
    void
);

#endif // TWC_WORKSPACE_H
