#ifndef TWC_SETUP_H
#define TWC_SETUP_H

  // Opaque types
struct twc_config_data;

extern
struct twc_config_data
twc_setup(
    char *config_file
);

extern
void
twc_load_plugins(
    struct twc_config_data *twc_config_data
);

extern
void
twc_run(
    void
);

#endif // TWC_SETUP_H
