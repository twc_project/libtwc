#ifndef SRV_OUTPUT_IMPL_H
#define SRV_OUTPUT_IMPL_H

  // System
#include <stdint.h>     // uint32_t, ...

  // Wayland
#include <wayland-util.h>               // wl_list
#include <wayland-server-protocol.h>    // wl_output_transform

  // TWC
#include <twc/wayland_types.h>  // wld_<type>_t

  // Opaque types
struct twc_workspace;
struct wcf_output;
struct wl_surface;

#define CONNECTOR_NAME_LENGTH 24

#define MAX_WORKSPACE_COUNT 32

struct srv_output {

    struct wl_list link;

    wld_uint_t numeric_name;

      // Connector name and monitor resolution, provided by srv
    char connector_name[CONNECTOR_NAME_LENGTH];

      // Monitor orientation specified in the configuration file.
    enum wl_output_transform transform;

      // The width and height of the output as transformed.
    wld_dimension_t width, height;

      // Layout Coordinates as requested by twc.
    wld_coordinate_t layout_x, layout_y;

      // Output Coordinates of the next window position.
    wld_coordinate_t next_window_x;
    wld_coordinate_t next_window_y;

      // Output Coordinates of the next icon position.
    wld_coordinate_t next_icon_x;
    wld_coordinate_t next_icon_y;

    struct twc_workspace *active_workspace;
    struct wl_surface    *(wallpaper_surfaces)[MAX_WORKSPACE_COUNT];

    struct wcf_output *wcf_output;
};

#endif // SRV_OUTPUT_IMPL_H
