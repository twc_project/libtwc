#ifndef TWC_SERVICE_H
#define TWC_SERVICE_H

  // Protocols
#include <wm-agent-v1-client-protocol.h>    // enum agent tags
#include <wm-service-v1-client-protocol.h>  // enum xx_zwm_service_assert_method

struct twc_service_methods {
    enum xx_zwm_service_assert_method         assert_method;
    enum xx_zwm_service_stand_down_method stand_down_method;
};

extern
void
twc_service_init(
    void
);

extern
void
twc_service_set_methods(
    const char                     *app_id,
    const char                     *service_name,
    enum xx_zwm_service_assert_method         assert_method,
    enum xx_zwm_service_stand_down_method stand_down_method
);

extern
void
twc_service_get_methods(
    const char                 *app_id,
    const char                 *service_name,
    struct twc_service_methods *twc_service_methods
);

#endif // TWC_SERVICE_H
