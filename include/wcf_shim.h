#ifndef WCF_SHIM_H
#define WCF_SHIM_H

  // System
#include <stdint.h>     // uint32_t
#include <stdbool.h>

  // Wayland
#include <wayland-util.h>             // wl_fixed_t
#include <wayland-server-protocol.h>  // wl_output_transform

  // TWC
#include "twc/wayland_types.h"      // wld_<type>_t

  // Opaque types
struct twc_state;
struct wl_surface;
struct wlr_texture;
struct xdg_toplevel;
struct wcf_output;
struct srv_output;
struct wcf_external_surface;

typedef void nested_surface_t;

extern
void
log_init(
    void
);

extern
void
wcf_initialize(
    void
);

 //

extern
void
wcf_start(
    void
);

 //

extern
void
wcf_terminate(
    void
);

 // ---------------
 // wcf_xdg_surface

struct wcf_xdg_surface;

extern
nested_surface_t*
wcf_external_surface_at(
    struct wcf_external_surface *wcf_external_surface,
    wld_coordinate_t        ptr_loc_sx,     // int   format, surface coord
    wld_coordinate_t        ptr_loc_sy,
    wl_fixed_t             *ptr_loc_fix_sx, // fixed format, surface coord
    wl_fixed_t             *ptr_loc_fix_sy
);

 // ----------------
 // wcf_pixel_buffer

extern
void
wcf_pixel_texture_destroy(
    struct wlr_texture *wcf_pixel_texture
);

extern
struct wlr_texture*
wcf_pixel_texture_create(
    int32_t  width,
    int32_t  height,
    int32_t  stride,
    uint32_t format,
    void    *pix_data,
    bool    *in_use
);

 // ------------------
 // Display Controller
 //
 // A dsply_ctrl is a video channel operating through some physical
 // connector (wl_output) associatied with a GPU.

extern
void
wcf_dsply_ctrl_display_surface(
    struct srv_output *srv_output,
    struct wl_surface *wl_surface,
    wld_coordinate_t   layout_x,
    wld_coordinate_t   layout_y
);

extern
void
wcf_dsply_ctrl_set_cursor(
    struct srv_output *srv_output,
    struct wl_surface *wl_surface,
    int32_t            hotspot_x,
    int32_t            hotspot_y
);

extern
void
wcf_dsply_ctrl_output_placement(
    struct srv_output        *srv_output,
    enum wl_output_transform  transform,
    wld_coordinate_t          layout_x,
    wld_coordinate_t          layout_y
);

#endif // WCF_SHIM_H
