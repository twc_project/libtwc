#ifndef EXTERNAL_BUFFER__H
#define EXTERNAL_BUFFER__H

  //TWC
#include <twc/wayland_types.h>      // wld_coordinate_t

  // Imported data types
#include "external_surface.h"

  // Opaque types
struct wcf_external_surface;
struct wl_surface;

extern
struct wl_buffer*
external_buffer_create(
    struct wcf_external_surface *wcf_external_surface,
    enum   external_surface_type external_surface_type
);

extern
void
external_buffer_destroy(
    struct wl_buffer *wl_buffer
);

extern
void
external_buffer_xwayland_child_update_location(
    struct wl_surface *child_surface,
    struct wl_surface *top_level_surface,
    wld_coordinate_t   surface_x,
    wld_coordinate_t   surface_y
);

#endif // EXTERNAL_BUFFER__H
