# libtwc

### Description

Libtwc is a library for building Wayland compositors.  It was created
to support [TWC], the demonstration base compositor of the [TWC
Project].  Libtwc depends on [wlroots].

The intrinsic capabilities of libtwc are minimal.  It's central
feature is that it supports both Built-in Clients and the [Agent
Protocol].  See the [TWC Project] for more context.

Libtwc is modular.  Built-in Clients can be introduced at link time or
dynamically as a plug-in at run time.  Built-in Clients may also
utilize the Wayland, xdg_shell and xdg_decoration protocols.

A simple example of a compositor using libtwc can be found at [smpl].
See the file smpl_compositor_main.c.  There is one Built-in Client
which is statically linked.

A more complex example is [TWC].  See the file main.c.  In this case,
the Built-in Clients are dynamically loaded.

### Motivation & Design Evolution

Many Wayland compositors provide the user with dynamic control over
the desktop.  The simplest form of interaction is though keyboard
shortcuts.

Some compositors would like to include visual elements for dynamic
control, i.e., a GUI.  For example, to change virtual desktops by
clicking on a grid image.  The obvious approach is to use wl_surfaces
for imaging and input.  However, wl_surfaces are the purview of
Wayland clients, not the server.

At first glance, every GUI-based compositor must invent its own form
of surface-like mechanism(s) for imaging and input[^1].  It seems a
shame to reinvent all that machinery for each compositor
implementation.  However, recall that the Wayland philosophy precludes
the sharing of global state outside the compositor.  So, offloading
compositor controls to a client is off the table.

But what if a 'client' could run in the context of the compositor
process, i.e., the compositor address-space and protection domain?
The idea would be to make the request/event interfaces of the Wayland
and xdg_shell protocols available to sub-components running within the
server.  The author of a sub-component could use familiar mechanisms
for graphics (xdg_toplevel) and input (button/keyboard events).  In
addition, a sub-component could safely access all of the compositor
state and solicit visual feedback from the user for compositor
control.  We call such a client a Built-in Client or BiC[^2].

Best practices suggest there should be a proper API for BiCs to query
and alter internal compositor state.  Again, it would be a shame to
reinvent such an API for each compositor.  So it seems reasonable to
consider such an API which is generic and could potentially be reused
in any number of compositors[^3].  This also suggests the concept of a
modular compositor.

So far in the evolution, a BiC, developer interacts with the
compositor for imaging and input by using the request/event concept of
Wayland.  Yet, the query/control API is something else.  You can
probably guess the next refinement.  The query/control API is given as
a Wayland protocol extension[^4].  The [Agent Protocol] servers this
purpose.

Thus far in the progression, sub-components (BiCs) are statically
linked to form an executable compositor with a specific personality.
On startup, a single initialization routine (surrogate main) is
invoked for each BiC.  The BiC makes requests, creates protocol
objects and registers events.  Just like any client, the request
routines are all well known in advance.  In addition, all entry points
to a BiC are events.  Event routines are explicitly registered as
dynamic callbacks using various add_listener requests.  At the end of
the day, this means that the only BiC specific entry point which must
be known to the base compositor is the initialization routine.  This
situation is perfect for a plug-in architecture[^5].

That is how we arrived at a feature-less modular compositor where
optional plug-ins provide a feature-rich personality with user GUIs.

The first iteration of the TWC compositor was planned with a specific
and fixed personality.  You wouldn't have liked it.  But, with the
current design using libtwc, you have a shot at making your own
compositor simply by writing (or 'borrowing') a few Wayland clients.

Enjoy.

[^1]: This was the approach taken in the initial design of the TWC
      compositor.  Many compositors were examined to see how they
      managed imaging and input.

[^2]: This was the approach taken in the second design of the TWC
      compositor.  It makes use of a 'built-in scanner' for post
      processing following the wayland-scanner.  This results in
      linkages suited to BiCs.

[^3]: This was the approach taken in the third design of the TWC
      compositor.  Most of the TWC abstractions were identified at
      this time.

[^4]: This was the approach taken in the fourth design of the TWC
      compositor.  It is actually somewhat cumbersome to define the
      query/control API in this way.  However, this disadvantage is
      outweighed by the benefit of consistency.

[^5]: This is the approach taken in the current design of the TWC
      compositor.  Third-party plug-ins are encouraged.

[TWC Project]:    https://gitlab.com/twc_project
[Agent Protocol]: https://gitlab.com/twc_project/agent_protocol
[TWC]:            https://gitlab.com/twc_project/twc
[smpl]:           https://gitlab.com/twc_project/plug-ins/smpl
[wlroots]:        https://gitlab.freedesktop.org/wlroots

