
  // System
#include <stdint.h> // uint32_t, ...
#include <stdlib.h> // NULL, calloc, free
#include <stdio.h>  // size_t, sizeof, NULL
#include <string.h> // strncpy

  // APIs
#include "srv_output.h"   // Our API to ensure consistancy
#include "twc_window.h"
#include "twc_workspace.h"
#include "bic_server/wayland_pre-defined.h"
#include "wcf_shim.h"
#include "bic_server/wayland_event.h"
#include <twc/wayland_types.h>      // wld_<type>_t
#include "twc_seat.h"
#include "twc_window.h"
#include <twc/twc_log.h>

  // Imported data types
#include "srv_output-impl.h"
#include "twc_state-impl.h"
#include "bic_server/server_state-impl.h"
#include "twc_window-impl.h"
#include "bic_server/wayland_bic.h"

#define srv_output_list_insert(o) \
    wl_list_insert( BIC_SERVER_output_list, &((o)->link) )

#define srv_output_list_remove(o) \
    wl_list_remove( &((o)->link) )

#define srv_output_output_list_for_each(o) \
    wl_list_for_each( (o), BIC_SERVER_output_list, link)

extern
struct srv_output*
srv_output_create(
    struct wcf_output *wcf_output,
    char              *output_name,
    wld_dimension_t    width,
    wld_dimension_t    height
) {
    //
    // For each new output, set the output transform and calculate the
    // new output-layout.
    //
    // recalc the entire layout each time a new output arrives.  Look for
    // the row 1 outputs that have been reported.  For the one that has
    // the left most ordering, place it first.  This means if the
    // rightful first output is missing, the left most reported one takes
    // its place.  Same for row 2.
    //

    static uint32_t output_numeric_name = 0;

    struct srv_output *srv_output = (
       calloc( 1, sizeof(struct srv_output) )
    );

    srv_output->numeric_name = output_numeric_name++;

      // Record the connector name
    strncpy(
        srv_output->connector_name,
        output_name,
        CONNECTOR_NAME_LENGTH
    );
    srv_output->connector_name[CONNECTOR_NAME_LENGTH - 1] = '\0';

    srv_output->transform        = WL_OUTPUT_TRANSFORM_NORMAL;
    srv_output->width            = width;
    srv_output->height           = height;
    srv_output->layout_x         = WLD_INVALID_COORD;
    srv_output->layout_y         = WLD_INVALID_COORD;
    srv_output->next_window_x    = 50;
    srv_output->next_window_y    = 50;
    srv_output->next_icon_x      = 50;
    srv_output->active_workspace = twc_workspace_default();
    srv_output->wcf_output       = wcf_output;

    for (int i = 0; i < MAX_WORKSPACE_COUNT; i++ ) {
        srv_output->wallpaper_surfaces[i] = NULL;
    }

    srv_output_list_insert(srv_output);

    twc_seat_new_output_dimensions(
        width,
        height
    );

    LOG_INFO("srv_output_new: srv_output %p", (void *) srv_output);

    static wld_coordinate_t largest_x = 0;
    static wld_coordinate_t largest_y = 0;

    // Fix: consult the twc config to determine any custom transform.
    enum wl_output_transform transform = WL_OUTPUT_TRANSFORM_NORMAL;
//  enum wl_output_transform transform = WL_OUTPUT_TRANSFORM_FLIPPED;
//  enum wl_output_transform transform = WL_OUTPUT_TRANSFORM_270;

      // Fix: For now outputs are arranged in one row from left to
      // right.  Each output has layout coordinates (X, 0), where X is
      // the sum over the widths of the previous outputs.
    wcf_dsply_ctrl_output_placement(
        srv_output,
        transform,
        largest_x,
        largest_y
    );

    srv_output->layout_x = largest_x;
    srv_output->layout_y = largest_y;

    largest_x += srv_output->width;
    largest_y += 0;

    wl_registry_global(&wl_output_interface, srv_output->numeric_name);

    return srv_output;
}

extern
void
srv_output_destroy(
    struct srv_output *srv_output
) {
    LOG_INFO("srv_output_destroy: srv_output %p", (void *) srv_output);

    wl_registry_global_remove(srv_output->numeric_name);

    srv_output_list_remove(srv_output);
    free(srv_output);

    return;
}

extern
void
srv_output_dimensions(
    struct srv_output *srv_output,
    wld_dimension_t   *width,
    wld_dimension_t   *height
) {
    *width  = srv_output->width;
    *height = srv_output->height;

    return;
}

extern
void
srv_output_set_wallpaper(
    struct srv_output    *srv_output,
    struct twc_workspace *twc_workspace,
    struct wl_surface    *wallpaper_surface
) {
    uint32_t wksp_index, wksp_limit;

    if ( twc_workspace == NULL ) {
          // Iterate over all workspaces
        wksp_index = 0;
        wksp_limit = MAX_WORKSPACE_COUNT;
    } else {
          // Only address the given workspace
        wksp_index = twc_workspace_index( twc_workspace );
        wksp_limit = wksp_index;
    }

    struct srv_output *srv_output_candidate;
    srv_output_output_list_for_each(srv_output_candidate) {

        if ( srv_output == srv_output_candidate ||
             srv_output == NULL
        ) {
            for ( ; wksp_index <= wksp_limit; wksp_index++ ) {
                srv_output->wallpaper_surfaces[wksp_index] = wallpaper_surface;
            }
        }
    }

    return;
}

extern
void
srv_output_next_window_coord(
    struct srv_output *srv_output,
    wld_coordinate_t  *next_window_x,
    wld_coordinate_t  *next_window_y
) {
    *next_window_x = srv_output->next_window_x;
    *next_window_y = srv_output->next_window_y;

    return;
}

extern
void
srv_output_set_next_window_coord(
    struct srv_output *srv_output,
    wld_coordinate_t   next_window_x,
    wld_coordinate_t   next_window_y
) {
     srv_output->next_window_x = next_window_x;
     srv_output->next_window_y = next_window_y;

    return;
}

extern
void
srv_output_next_icon_coord(
    struct srv_output *srv_output,
    wld_coordinate_t  *next_icon_x,
    wld_coordinate_t  *next_icon_y
) {
    *next_icon_x = srv_output->next_icon_x;
    *next_icon_y = srv_output->next_icon_y;

    return;
}

extern
void
srv_output_set_next_icon_coord(
    struct srv_output *srv_output,
    wld_coordinate_t   next_icon_x,
    wld_coordinate_t   next_icon_y
) {
     srv_output->next_icon_x = next_icon_x;
     srv_output->next_icon_y = next_icon_y;

    return;
}

extern
struct twc_workspace*
srv_output_active_workspace(
    struct srv_output *srv_output
) {
    return srv_output->active_workspace;
}

extern
wld_uint_t
srv_output_numeric_name(
    struct srv_output *srv_output
) {
    return srv_output->numeric_name;
}

extern
void
srv_output_set_active_workspace(
    struct srv_output    *srv_output,
    struct twc_workspace *twc_workspace
) {
    srv_output->active_workspace = twc_workspace;

    return;
}

extern
struct srv_output*
srv_output_at(
    wl_fixed_t pointer_location_fix_lx,
    wl_fixed_t pointer_location_fix_ly
) {
      // Recall Y-axis is positive going down.
    wld_coordinate_t left_x,  right_x;
    wld_coordinate_t  top_y, bottom_y;

    wld_coordinate_t ptr_x = wl_fixed_to_int(pointer_location_fix_lx);
    wld_coordinate_t ptr_y = wl_fixed_to_int(pointer_location_fix_ly);

    struct srv_output *srv_output;
    srv_output_list_for_each(srv_output) {

        left_x = srv_output->layout_x;
         top_y = srv_output->layout_y;

        right_x  = left_x + srv_output->width;
        bottom_y = top_y  + srv_output->height;

          // Is ptr in range?
        if (( left_x <= ptr_x ) && ( ptr_x <  right_x ) &&
            (  top_y <= ptr_y ) && ( ptr_y < bottom_y )
        ) {
            return srv_output;
        }
    }

    return NULL;
}

extern
void
srv_output_compose(
    struct srv_output *srv_output
) {
    enum  xx_wm_workspace_id active_workspace_id = (
        twc_workspace_id( srv_output->active_workspace )
    );

    uint32_t active_workspace_index = (
        twc_workspace_index( srv_output->active_workspace )
    );

    struct wl_surface *wallpaper_surface = (
        srv_output->wallpaper_surfaces[active_workspace_index]
    );
    if ( wallpaper_surface != NULL ) {
        if ( SURFACE_HAS_THIS_ROLE(wallpaper_surface, ST_XX_WM_WALLPAPER) ) {
            wcf_dsply_ctrl_display_surface(
                srv_output,
                wallpaper_surface,
                0,
                0
            );
        }
    }

      // Surfaces are listed from top to bottom.  Compose them in
      // reverse order, so upper surfaces appear above lower ones.
    struct twc_view      *twc_view;
    twc_display_list_for_each_reverse(twc_view) {

        wksp_occupancy_t wksp_occupancy = twc_view_wksp_occupancy(twc_view);

        if (! WKSP_OCCUPANCY_IS_MEMBER(
                wksp_occupancy,
                active_workspace_id
              )
        ) { continue; }

        // To be consistant with the "surface_at" algorithm, we paint
        // the app content first and then the decorations.
        // I.e. decorations stack higher.  See the "GTK border grips"
        // comment in twc_window_surface_at() in twc_window.c.

          // Fix: can the view_surface ever be INERT?
        if ( twc_view->view_surface != INERT_WL_SURFACE ) {

            // See the discussion of Surface Enclosures in
            // twc_window-impl.h.
            //
            // The wcf compositor places the origin of the surface
            // enclosure at the given coordinates.  The (visible)
            // primary surface is allowed to be inside the enclosure,
            // in which case, the primary surface will not appear in
            // the proper place on screen.  To compensate, we supply
            // the enclosure-relative coordinates.  This means that
            // the wl_surface_at() must also compensate.

            wcf_dsply_ctrl_display_surface(
                srv_output,
                twc_view->view_surface,
                twc_view->layout_x + twc_view->enclosure_vx,    // Enclosure-relative
                twc_view->layout_y + twc_view->enclosure_vy     // coordinates.
            );
        }

        if ( ! TWC_VIEW_IS_DECORATED(twc_view) ) { continue; }

        // Fix: DF_VIEW_ONLY not followed

        if (SURFACE_IS_MAPPED( twc_view->xx_wm_decoration.top_surface )) {
            wcf_dsply_ctrl_display_surface(
                srv_output,
                twc_view->xx_wm_decoration.top_surface,
                twc_view->layout_x + twc_view->deco_top_vx,
                twc_view->layout_y + twc_view->deco_top_vy
            );
        }

        if (SURFACE_IS_MAPPED( twc_view->xx_wm_decoration.bottom_surface )) {
            wcf_dsply_ctrl_display_surface(
                srv_output,
                twc_view->xx_wm_decoration.bottom_surface,
                twc_view->layout_x + twc_view->deco_bottom_vx,
                twc_view->layout_y + twc_view->deco_bottom_vy
            );
        }

        if (SURFACE_IS_MAPPED( twc_view->xx_wm_decoration.left_surface )) {
            wcf_dsply_ctrl_display_surface(
                srv_output,
                twc_view->xx_wm_decoration.left_surface,
                twc_view->layout_x + twc_view->deco_left_vx,
                twc_view->layout_y + twc_view->deco_left_vy
            );
        }

        if (SURFACE_IS_MAPPED( twc_view->xx_wm_decoration.right_surface )) {
            wcf_dsply_ctrl_display_surface(
                srv_output,
                twc_view->xx_wm_decoration.right_surface,
                twc_view->layout_x + twc_view->deco_right_vx,
                twc_view->layout_y + twc_view->deco_right_vy
            );
        }
    }

    return;
}

extern
struct srv_output*
srv_output_numeric_name_lookup(
    uint32_t name
) {
    struct srv_output *srv_output;
    srv_output_list_for_each(srv_output) {

        if ( srv_output->numeric_name == name ) {
            return srv_output;
        }
    }

    return NULL;
}
