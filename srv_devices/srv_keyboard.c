  // System
#include <stdint.h> // uint32_t, ...
#include <stdlib.h> // NULL, calloc, free
#include <stdio.h>  // size_t, sizeof, NULL
#include <string.h> // strncpy

  // Protocols
#include <wayland-client-protocol.h>    // enum wl_keyboard_keymap_format

  // APIs
#include "srv_keyboard.h"       // Our API to ensure consistancy
#include "srv_keyboard-impl.h"
#include "twc_seat.h"

  // Imported data types
#include "bic_server/server_state-impl.h"

#define srv_keyboard_list_insert(o) \
    wl_list_insert( BIC_SERVER_keyboard_list, &((o)->link) )

#define srv_keyboard_list_remove(o) \
    wl_list_remove( &((o)->link) )

extern
struct srv_keyboard*
srv_keyboard_create(
    struct wcf_keyboard           *wcf_keyboard,
    char                          *keyboard_name,
    enum wl_keyboard_keymap_format wl_keyboard_keymap_format,
    int32_t                        keymap_fd
) {

    struct srv_keyboard *srv_keyboard = (
       calloc( 1, sizeof(struct srv_keyboard) )
    );

    strncpy(
        srv_keyboard->keyboard_name,
        keyboard_name,
        KEYBOARD_NAME_LENGTH
    );
    srv_keyboard->keyboard_name[KEYBOARD_NAME_LENGTH - 1] = '\0';

    srv_keyboard->wcf_keyboard              = wcf_keyboard;
    srv_keyboard->wl_keyboard_keymap_format = wl_keyboard_keymap_format;
    srv_keyboard->keymap_fd                 = keymap_fd;

    twc_seat_new_keyboard( srv_keyboard );

    return srv_keyboard;
}

extern
void
srv_keyboard_destroy(
    struct srv_keyboard *srv_keyboard
) {
    srv_keyboard_list_remove(srv_keyboard);
    free(srv_keyboard);

    return;
}
