
  // System
#include <stdlib.h>  // NULL, calloc, free
#include <stdbool.h>

  // Wayland
#include <wayland-util.h>  // wl_list, wl_fixed_t, wl_array

  // Protocols
#include <wm-agent-v1-client-protocol.h>  // XX_WM_WORKSPACE_ID_WKSP_NONE
#include <wm-service-v1-client-protocol.h>

  // libtwc_util
#include <twc/wayland_types.h>  // wld_<type>_t
#include <twc/ParseGeom.h>
#include <twc/twc_log.h>

  // bic_server APIs
#include "bic_server/wl_surface.h"
#include "bic_server/wm_agent_event_wrappers.h"
#include "bic_server/xdg_shell_event.h"
#include "bic_server/wm_agent_event.h"
#include "bic_server/wayland_pre-defined.h"
#include "bic_server/xdg_decoration_event.h"
#include "bic_server/wm_agent_pre-defined.h"
#include "bic_server/wm_agent_bic.h"

  // APIs
#include "twc_window.h"     // Our API to ensure consistancy
#include "twc_seat.h"
#include "srv_output.h"
#include "twc_workspace.h"
#include "twc_config_toml.h"
#include "twc_service.h"
#include "twc_decoration.h"

  // Imported Data Types
#include "twc_window-impl.h"
#include "twc_state-impl.h"
#include "srv_output-impl.h"
#include "bic_server/wayland_bic.h"
#include "bic_server/xdg_decoration_bic.h"

static
struct twc_window null_twc_window = {
    .is_in_window_list  = false,
    .numeric_name       = 0xffffffff,
    .client_view        = NULL_TWC_VIEW,
    .icon_view          = NULL_TWC_VIEW,
    .wksp_occupancy     = XX_WM_WORKSPACE_ID_WKSP_NONE,
    .window_gravity     = WG_NorthWest,
    .app_id             = "Null TWC Window",
    .title              = "Null TWC Window",
};

struct twc_view null_twc_view = {
    .twc_window          = &null_twc_window,
    .is_in_display_list  = false,
    .display_format      = DF_NOT_SPECIFIED,
    .view_surface        = INERT_WL_SURFACE,
    .layout_x            = WLD_INVALID_COORD,
    .layout_y            = WLD_INVALID_COORD,
    .deco_top_vx         = WLD_INVALID_COORD,
    .deco_top_vy         = WLD_INVALID_COORD,
    .deco_bottom_vx      = WLD_INVALID_COORD,
    .deco_bottom_vy      = WLD_INVALID_COORD,
    .deco_left_vx        = WLD_INVALID_COORD,
    .deco_left_vy        = WLD_INVALID_COORD,
    .deco_right_vx       = WLD_INVALID_COORD,
    .deco_right_vy       = WLD_INVALID_COORD,

// Fix: should have an inert_xx_wm_decoration
    .xx_wm_decoration.top_surface    = INERT_WL_SURFACE,
    .xx_wm_decoration.bic_connection = INERT_BIC_CONNECTION,
};

static bool overlay_window_active = false;

  // display and window list insert

#define twc_window_list_insert(v) { \
    if ((v)->is_in_window_list == false ) {            \
        wl_list_insert( TWC_window_list, &(v)->link ); \
        (v)->is_in_window_list =  true;                \
    }                                                  \
}

#define twc_window_list_remove(v) { \
    if ((v)->is_in_window_list == true ) {  \
        wl_list_remove( &(v)->link );       \
        (v)->is_in_window_list =  false;    \
    }                                       \
}

#define twc_display_list_insert(v) \
    if ((v)->is_in_display_list == false ) {           \
        wl_list_insert(TWC_display_list, &(v)->link ); \
        (v)->is_in_display_list =  true;               \
    }

#define twc_display_list_remove(v) \
    if ((v)->is_in_display_list == true ) { \
        wl_list_remove( &(v)->link );       \
        (v)->is_in_display_list =  false;   \
    }

#define twc_display_list_insert_last(v) \
    if ((v)->is_in_display_list == false ) {                \
        wl_list_insert(TWC_display_list->prev, &(v)->link); \
        (v)->is_in_display_list =  true;                    \
    }

  // Information about the selected view, if any.
static struct {
    struct twc_view *view;

      // For moving
    wld_coordinate_t window_grab_offset_x;
    wld_coordinate_t window_grab_offset_y;

      // For resizing
    wld_coordinate_t orig_left;
    wld_coordinate_t orig_right;
    wld_coordinate_t orig_top;
    wld_coordinate_t orig_bottom;
    wld_coordinate_t left_right_anchor;
    wld_coordinate_t top_bottom_anchor;
    wld_coordinate_t deco_width;
    wld_coordinate_t deco_height;

} G_selected_null = {
    .view = NULL_TWC_VIEW,

    .window_grab_offset_x = WLD_INVALID_COORD,
    .window_grab_offset_y = WLD_INVALID_COORD,

    .orig_left   = WLD_INVALID_COORD,
    .orig_right  = WLD_INVALID_COORD,
    .orig_top    = WLD_INVALID_COORD,
    .orig_bottom = WLD_INVALID_COORD,

    .left_right_anchor = WLD_INVALID_COORD,
    .top_bottom_anchor = WLD_INVALID_COORD,

    .deco_width  = 0,
    .deco_height = 0,
}, G_selected;

static enum {
    // Resizing Modes
    //
    // Each surface has a horizontal and a vertical axis.  Each axis
    // has two edges.  They are left/right for the horizontal axis and
    // top/bottom for the vertical axis.  While resizing, each axis
    // transitions among three ordered states.  The state for each
    // axis advances independently as resizing progresses.

    RM_FINDING_ANCHOR,  // The resizing edge along the axis is not yet
                        // determined.  This is the starting state for
                        // select_and_window_op(resize).

    RM_RESIZING,        // One edge is anchored, the other is
                        // resizing.  This is the starting state for
                        // any axis selected by xdg_toplevel_resize().

    RM_DONE,            // The surface is no longer resizing along
                        // this axis.  This is the starting state for
                        // any axis not selected in
                        // xdg_toplevel_resize().
} horiz_resizing_mode,
   vert_resizing_mode;

 /*
 ** A Reoccupy action sometimes uses interactive mode to select a
 ** window, but does not use interactive mode as the user sets the new
 ** occupation.  Therefore, during this latter time, the
 ** interactive_state_machine is available for other purposes.
 ** However, the subject window must be remembered for the time
 ** when/if the workspace agent reports the new occupancy.
 */

static
struct twc_window *twc_window_selected_for_reoccupy = NULL;

extern
void
twc_window_initialize(
    void
) {
    G_selected = G_selected_null;

    wl_list_init(&null_twc_window.xx_wm_window_list);

    return;
}

static
struct twc_view*
twc_view_create(
    struct twc_window *twc_window,
    wld_dimension_t   width,
    wld_dimension_t   height
) {
    struct twc_view *twc_view = (
        calloc( 1, sizeof(struct twc_view) )
    );

    twc_view->twc_window         = twc_window;
    twc_view->is_in_display_list = false;
    twc_view->display_format     = DF_NOT_SPECIFIED;

    twc_view->view_surface = INERT_WL_SURFACE;

    twc_view->xx_wm_decoration.is_active      = false;
    twc_view->xx_wm_decoration.   top_surface = INERT_WL_SURFACE;
    twc_view->xx_wm_decoration.bottom_surface = INERT_WL_SURFACE;
    twc_view->xx_wm_decoration.  left_surface = INERT_WL_SURFACE;
    twc_view->xx_wm_decoration. right_surface = INERT_WL_SURFACE;
    twc_view->xx_wm_decoration.bic_connection = INERT_BIC_CONNECTION;

    twc_view->layout_x = WLD_INVALID_COORD;
    twc_view->layout_y = WLD_INVALID_COORD;

    twc_view->width  = width;
    twc_view->height = height;

    twc_view->deco_top_vx    = WLD_INVALID_COORD;
    twc_view->deco_top_vy    = WLD_INVALID_COORD;
    twc_view->deco_bottom_vx = WLD_INVALID_COORD;
    twc_view->deco_bottom_vy = WLD_INVALID_COORD;
    twc_view->deco_left_vx   = WLD_INVALID_COORD;
    twc_view->deco_left_vy   = WLD_INVALID_COORD;
    twc_view->deco_right_vx  = WLD_INVALID_COORD;
    twc_view->deco_right_vy  = WLD_INVALID_COORD;

    twc_view->view_surface_vx = 0;
    twc_view->view_surface_vy = 0;

    twc_view->has_alternate_size     = false;
    twc_view->prezoom_layout_x       = WLD_INVALID_COORD;
    twc_view->prezoom_layout_y       = WLD_INVALID_COORD;
    twc_view->prezoom_surface_width  = 0;
    twc_view->prezoom_surface_height = 0;

    return twc_view;
}

static inline
uint32_t
generate_window_numeric_name(
    void
) {
    static uint32_t numeric_name = 127;
    // Fix: This may overflow
    return numeric_name++;
}

extern
struct twc_window*
twc_window_create(
    struct wl_surface *client_surface
) {
    //LOG_INFO("twc_window_create");
    struct twc_window *twc_window = (
        calloc( 1, sizeof(struct twc_window) )
    );

    twc_window->numeric_name = (
        generate_window_numeric_name()
    );
    twc_window->window_gravity = WG_NorthWest;

    twc_window->app_id[0] = '\0';
    twc_window-> title[0] = '\0';

    twc_window->window_configured = WC_no_configuration;

    // Fix: check config for any initial values of these items.
    twc_window->is_iconified       = false;
    twc_window->iconify_by_masking = false;
    twc_window->hold_icon          = false;

    enum xdg_toplevel_state xdg_toplevel_state_index;
    for ( xdg_toplevel_state_index = LOWEST_XDG_TOPLEVEL_STATE;
          xdg_toplevel_state_index <= HIGHEST_XDG_TOPLEVEL_STATE;
          xdg_toplevel_state_index++
    ) {
        client_surface->xdg_toplevel.xdg_toplevel_states[0] = false;
    }

    twc_window->pre_initial_commit_geometry.x      = WLD_INVALID_COORD;
    twc_window->pre_initial_commit_geometry.y      = WLD_INVALID_COORD;
      // Default is to let client choose dimensions
    twc_window->pre_initial_commit_geometry.width  = 0;
    twc_window->pre_initial_commit_geometry.height = 0;

    twc_window->preferred_output = NULL;
    twc_window->wksp_occupancy   = XX_WM_WORKSPACE_ID_WKSP_NONE;

    struct twc_view* client_view = (
        twc_view_create(
            twc_window,
            client_surface->width,
            client_surface->height
        )
    );
    twc_window->client_view = client_view;

      // Mutually link the client_view and the view_surface
    client_surface->twc_view  = client_view;
    client_view->view_surface = client_surface;

    client_view->twc_decoration_policy = TWC.default_decoration_policy;
    client_view->display_format        = DF_VIEW_PLUS_DECO;

    struct twc_view* icon_view = (
        twc_view_create(
            twc_window,
            0,
            0
        )
    );
    twc_window->icon_view = icon_view;

// Fix: icon deoration policy
//    icon_view->twc_decoration_policy = DP_Never_Decorate;
//    icon_view->display_format        = DF_VIEW_ONLY;

    icon_view->twc_decoration_policy = DP_Always_SSD;
    icon_view->display_format        = DF_VIEW_PLUS_DECO;

    wl_list_init(&twc_window->xx_wm_window_list);

      // Set the workspace.
    struct twc_workspace *workspace = (
        twc_seat_workspace_with_pointer_focus()
    );

    twc_window->wksp_occupancy = (
        WKSP_OCCUPANCY_ADD(
            XX_WM_WORKSPACE_ID_WKSP_NONE,
            twc_workspace_id(workspace)
        )
    );

    twc_window->assert_method         = XX_ZWM_SERVICE_ASSERT_METHOD_NOP;
    twc_window->stand_down_method     = XX_ZWM_SERVICE_STAND_DOWN_METHOD_NOP;
    twc_window->xx_zwm_service_window = NULL;

      // Set the initial decoration policy calculations.  The
      // client has not yet asked for a mode nor even created a
      // decoration.  So no respone to the client is necessary.
    twc_update_decoration_mode(client_view, DM_None);
    twc_update_decoration_mode(  icon_view, DM_None);

    twc_window_list_insert(twc_window);

    xx_wm_window_new(twc_window);

    return twc_window;
}

static
void
twc_view_destroy(
    struct twc_view* twc_view
) {
    free(twc_view);

    return;
}

extern
void
twc_window_destroy(
    struct twc_window* twc_window
) {
    if ( G_selected.view->twc_window == twc_window ) {
        G_selected = G_selected_null;
    }

    if ( twc_window_selected_for_reoccupy == twc_window ) {
        twc_window_selected_for_reoccupy = NULL;
    }

    xx_wm_window_expired(twc_window);

    twc_window_list_remove(twc_window);

    xx_zwm_service_window_destroy( twc_window->xx_zwm_service_window );

    twc_view_destroy(twc_window->  icon_view);
    twc_view_destroy(twc_window->client_view);

    free(twc_window);

    return;
}

extern
struct twc_window*
twc_window_numeric_name_lookup(
    uint32_t numeric_name
) {
    struct twc_window *twc_window;

    twc_window_list_for_each(twc_window) {
        if ( twc_window->numeric_name == numeric_name ) {
            return twc_window;
        }
    }

    return NULL;
}

static inline
void
connect_surface_to_view(
    struct wl_surface *deco_surface,
    struct twc_view   *twc_view
) {
      // Only connect actual surfaces.
    if (deco_surface           != INERT_WL_SURFACE) {
        deco_surface->twc_view  = twc_view;
    }
}

extern
void
twc_view_commit_decoration(
    struct xx_wm_decoration *xx_wm_decoration
) {
    struct twc_view *twc_view = (
        wl_container_of(xx_wm_decoration, twc_view, xx_wm_decoration)
    );

    // For each pending edge:
    //   1 disconnect the outgoing decoration surface from the twc_view
    //   2 connect    the new      decoration surface to   the twc_view

    if ( xx_wm_decoration->is_top_pending ) {

        connect_surface_to_view(xx_wm_decoration->        top_surface, NULL_TWC_VIEW);
        connect_surface_to_view(xx_wm_decoration->pending_top_surface, twc_view);
    }

    if ( xx_wm_decoration->is_bottom_pending ) {

        connect_surface_to_view(xx_wm_decoration->        bottom_surface, NULL_TWC_VIEW);
        connect_surface_to_view(xx_wm_decoration->pending_bottom_surface, twc_view);
    }

    if ( xx_wm_decoration->is_left_pending ) {

        connect_surface_to_view(xx_wm_decoration->        left_surface, NULL_TWC_VIEW);
        connect_surface_to_view(xx_wm_decoration->pending_left_surface, twc_view);
    }

    if ( xx_wm_decoration->is_right_pending ) {

        connect_surface_to_view(xx_wm_decoration->        right_surface, NULL_TWC_VIEW);
        connect_surface_to_view(xx_wm_decoration->pending_right_surface, twc_view);
    }

    return;
}

static
void
recalc_layout_coordinates(
    struct twc_view *twc_view,
    wld_dimension_t  new_width,
    wld_dimension_t  new_height
) {
    /*
    ** The layout coordinates of a view are infulenced by the
    ** dimensions and the gravity.  To calculate new coordinates, we
    ** back out the influence from the old dimensions and then
    ** recalculate using the new dimensions.  See 'case WG_NorthEast'
    ** and 'WG_West' below.  Other cases are similar.
    */

    wld_dimension_t old_width  = twc_view->width;
    wld_dimension_t old_height = twc_view->height;

    if ( new_width  == old_width &&
         new_height == old_height
    ) { return; }

    wld_coordinate_t old_layout_x = twc_view->layout_x,
                     old_layout_y = twc_view->layout_y;

      // Default is no change
    wld_coordinate_t new_layout_x = old_layout_x,
                     new_layout_y = old_layout_y;

      // Gravity attribute does not apply to interactive resize.
    enum window_gravity window_gravity = (
        twc_view == G_selected.view
        ? WG_NorthWest
        : twc_view->twc_window->window_gravity
    );

    //LOG_INFO("old %4d %4d %4d %4d %2d a: %s", old_layout_x, old_layout_y, old_width, old_height, window_gravity, twc_view->twc_window->app_id);

      // Relocate view depending on gravity.
    switch ( window_gravity ) {

        default:
        case WG_NorthWest:
        break;

        case WG_NorthEast:

            /*
            ** For this gravity value, the top right corner of the
            ** view is an anchor point.  Therefore the top and right
            ** sides remain at fixed locations.
            **
            ** As the height changes, the top side (layout_y) does not
            ** change, but the view bottom will move up and down.
            **
            ** As the width changes, the right side does not move,
            ** but the left side moves back and forth.
            **
            ** To accomdate the back and forth, layout_x needs to
            ** change.  The change amount is the difference between
            ** the old and new widths.  (back out the old, recalc with
            ** the new)
            */

            new_layout_x = old_layout_x + old_width  - new_width;
        break;

        case WG_SouthWest:
            new_layout_y = old_layout_y + old_height - new_height;
        break;

        case WG_SouthEast:
            new_layout_x = old_layout_x + old_width  - new_width;
            new_layout_y = old_layout_y + old_height - new_height;
        break;

        case WG_West:

            /*
            ** For this gravity value, the left side of the view is an
            ** anchored side.  Therefore layout_x is unaffected.  This
            ** gravity value calls for changes in height to be split
            ** evenly up and down.  (A move could relocate the whole
            ** view.)
            */

            new_layout_y = old_layout_y + old_height/2 - new_height/2;
        break;

        case WG_East:
            new_layout_x = old_layout_x + old_width    - new_width;
            new_layout_y = old_layout_y + old_height/2 - new_height/2;
        break;

        case WG_North:
            new_layout_x = old_layout_x + old_width/2  - new_width/2;
        break;

        case WG_South:
            new_layout_x = old_layout_x + old_width/2  - new_width/2;
            new_layout_y = old_layout_y + old_height   - new_height;
        break;

        case WG_Center:
            new_layout_x = old_layout_x + old_width/2  - new_width/2;
            new_layout_y = old_layout_y + old_height/2 - new_height/2;
        break;
    }

    //LOG_INFO("new %4d %4d %4d %4d", new_layout_x, new_layout_y, new_width, new_height);

    twc_view->width  = new_width;
    twc_view->height = new_height;

    if ( new_layout_x == old_layout_x &&
         new_layout_y == old_layout_y
    ) { return; }

    twc_view_update_location(
        twc_view,
        new_layout_x,
        new_layout_y
    );

    return;
}

static inline
wld_coordinate_t
min_coord(
      // note: surface relative coordinates may be negative
    wld_coordinate_t a,
    wld_coordinate_t b
) { return  a < b ? a  : b ; }

static inline
wld_coordinate_t
max_coord(
      // note: surface relative coordinates may be negative
    wld_coordinate_t a,
    wld_coordinate_t b
) { return  a < b ? b  : a ; }

static
void
northwest_decoration_surface_coord(
    struct twc_view  *twc_view,
    wld_coordinate_t *leftmost_decoration_sx,
    wld_coordinate_t * highest_decoration_sy
) {
    // Find the decoration edges that are leftmost and highest
    // relative to the client surface.

    struct xx_wm_decoration *xx_wm_decoration = (
        &twc_view->xx_wm_decoration
    );

    wld_coordinate_t leftmost_sx = 0;
    wld_coordinate_t  highest_sy = 0;

    if ( twc_view->xx_wm_decoration.is_active ) {

        switch ( twc_view->display_format ) {

            case DF_NOT_SPECIFIED:
            case DF_VIEW_ONLY:
                  // No decoration surfaces
            break;

            case DF_ROLLED_UP:
                  // Only consider the titlebar
                switch ( xx_wm_decoration->titlebar_surface ) {

                    case XX_WM_DECORATION_EDGE_TOP:
                        leftmost_sx = xx_wm_decoration-> top_sx;
                         highest_sy = xx_wm_decoration-> top_sy;
                    break;

                    case XX_WM_DECORATION_EDGE_BOTTOM:
                        leftmost_sx = xx_wm_decoration->bottom_sx;
                         highest_sy = xx_wm_decoration->bottom_sy;
                    break;

                    case XX_WM_DECORATION_EDGE_LEFT:
                        leftmost_sx = xx_wm_decoration->left_sx;
                         highest_sy = xx_wm_decoration->left_sy;
                    break;

                    case XX_WM_DECORATION_EDGE_RIGHT:
                        leftmost_sx = xx_wm_decoration-> right_sx;
                         highest_sy = xx_wm_decoration-> right_sy;
                    break;

                    case XX_WM_DECORATION_EDGE_NONE:
                        leftmost_sx = 0;
                         highest_sy = 0;
                    break;
                }
            break;

            case DF_VIEW_PLUS_DECO:
                // Find the leftmost/highest coordinate over all
                // decoration surfaces.
                //
                // Recall that decoration edge names are NOT indicative of
                // their relative locations.  The 'top' decoration could
                // be the lowest surface due to the view-relative
                // coordinates it was given by the deco agent.

                leftmost_sx = (
                    min_coord(
                        min_coord(xx_wm_decoration-> top_sx, xx_wm_decoration->bottom_sx),
                        min_coord(xx_wm_decoration->left_sx, xx_wm_decoration-> right_sx)
                    )
                );
                highest_sy = (
                    min_coord(
                        min_coord(xx_wm_decoration-> top_sy, xx_wm_decoration->bottom_sy),
                        min_coord(xx_wm_decoration->left_sy, xx_wm_decoration-> right_sy)
                    )
                );
            break;
        }

    }

    *leftmost_decoration_sx = leftmost_sx;
    * highest_decoration_sy = highest_sy;

    return;
}

static
void
southeast_decoration_surface_coord(
    struct twc_view  *twc_view,
    wld_coordinate_t *rightmost_decoration_sx,
    wld_coordinate_t *   lowest_decoration_sy
) {
    // Find the decoration edges that are rightmost and lowest
    // relative to the client surface.

    struct xx_wm_decoration *xx_wm_decoration = (
        &twc_view->xx_wm_decoration
    );

    wld_coordinate_t rightmost_sx = 0;
    wld_coordinate_t    lowest_sy = 0;

    if ( twc_view->xx_wm_decoration.is_active ) {

        switch ( twc_view->display_format ) {

            case DF_NOT_SPECIFIED:
            case DF_VIEW_ONLY:
                  // No decoration surfaces
            break;

            case DF_ROLLED_UP:
                  // Only consider the titlebar
                switch ( xx_wm_decoration->titlebar_surface ) {

                    case XX_WM_DECORATION_EDGE_TOP:
                        rightmost_sx = twc_view->deco_top_vx    + xx_wm_decoration->   top_surface->width;
                           lowest_sy = twc_view->deco_top_vy    + xx_wm_decoration->   top_surface->height;
                    break;

                    case XX_WM_DECORATION_EDGE_BOTTOM:
                        rightmost_sx = twc_view->deco_bottom_vx + xx_wm_decoration->bottom_surface->width;
                           lowest_sy = twc_view->deco_bottom_vy + xx_wm_decoration->bottom_surface->height;
                    break;

                    case XX_WM_DECORATION_EDGE_LEFT:
                        rightmost_sx = twc_view->deco_left_vx   + xx_wm_decoration->  left_surface->width;
                           lowest_sy = twc_view->deco_left_vy   + xx_wm_decoration->  left_surface->height;
                    break;

                    case XX_WM_DECORATION_EDGE_RIGHT:
                        rightmost_sx = twc_view->deco_right_vx  + xx_wm_decoration-> right_surface->width;
                           lowest_sy = twc_view->deco_right_vy  + xx_wm_decoration-> right_surface->height;
                    break;

                    case XX_WM_DECORATION_EDGE_NONE:
                        rightmost_sx = 0;
                           lowest_sy = 0;
                    break;
                }
            break;

            case DF_VIEW_PLUS_DECO:
                // Find the rightmost/lowest coordinates over all
                // decoration surfaces.
                //
                // Recall that decoration edge names are NOT indicative of
                // their relative locations.  The 'top' decoration could
                // be the lowest surface due to the view-relative
                // coordinates it was given by the deco agent.

                rightmost_sx = (
                    max_coord(
                        max_coord( twc_view->deco_top_vx    + xx_wm_decoration->   top_surface->width,
                                   twc_view->deco_bottom_vx + xx_wm_decoration->bottom_surface->width
                        ),
                        max_coord( twc_view->deco_left_vx   + xx_wm_decoration->  left_surface->width,
                                   twc_view->deco_right_vx  + xx_wm_decoration-> right_surface->width
                        )
                    )
                );
                lowest_sy = (
                    max_coord(
                        max_coord( twc_view->deco_top_vy    + xx_wm_decoration->   top_surface->height,
                                   twc_view->deco_bottom_vy + xx_wm_decoration->bottom_surface->height
                        ),
                        max_coord( twc_view->deco_left_vy   + xx_wm_decoration->  left_surface->height,
                                   twc_view->deco_right_vy  + xx_wm_decoration-> right_surface->height
                        )
                    )
                );
            break;
        }
    }

    *rightmost_decoration_sx = rightmost_sx;
    *   lowest_decoration_sy =    lowest_sy;

    return;
}

extern
void
twc_view_recalc_geometry(
    struct twc_view *twc_view
) {
    /*
    ** A view consists of a client surface plus four decoration edges.
    **
    ** View geometry denpends on
    **
    **   - client surface dimension
    **
    **   - edge surface dimension
    **
    **   - view-relative coordinates of edges
    **
    ** This routine calculates the view dimension, ie, the effective
    ** width/height of the collection.
    */

    struct xx_wm_decoration *xx_wm_decoration = (
        &twc_view->xx_wm_decoration
    );

    wld_dimension_t old_width  = twc_view->width;
    wld_dimension_t old_height = twc_view->height;

      // Default to no change
    wld_dimension_t new_width  = old_width;
    wld_dimension_t new_height = old_height;

    bool view_has_dimension = (
        old_width  != 0 &&
        old_height != 0
    );

    bool surface_has_dimension = (
        twc_view->view_surface->width  != 0 &&
        twc_view->view_surface->height != 0
    );

    if ( ! surface_has_dimension &&
         !    view_has_dimension
        ) { /* no dimensions */ return; }

    if ( ! surface_has_dimension &&
              view_has_dimension
    ) {
        // The client surface dimension has become zero

        twc_view->width  = 0;
        twc_view->height = 0;

        twc_view->layout_x = WLD_INVALID_COORD;
        twc_view->layout_y = WLD_INVALID_COORD;

        return;
    }

    /*
    ** The following code calculates the new width and height for the
    ** view in the case where the displayed image includes
    ** decorations.
    **
    ** We start be calculating view-relative coordinates for the
    ** various surfaces within the view.
    **
    ** The Decoration Stylist provides edge offsets that are in
    ** surface-relative coordinates.
    **
    ** Within the diagram below, surface-relative coordinates are,
    **   the surface   is at ( 0, 0)
    **   the top edge  is at ( 0,-3)
    **   the left edge is at (-6,-3)
    **
    **    *----++----------------------------+      * is the view origin
    **    |l   ||top edge                    |        it is ( 0, 0) in    view-relative coords
    **    |e   |+----------------------------+        it is (SX,SY) in surface-relative coords
    **    |f   |+----------------------------+
    **    |t   ||surface                     |
    **    |    ||                            |
    **    |e   ||  (icon or client surface)  |
    **    |d   ||                            |
    **    |g   ||                            |
    **    |e   ||                            |
    **    |    ||                            |
    **    +----++----------------------------+
    **
    ** Converting to view-relative coordinates,
    **   the client surface is at (6,3)
    **   the top edge       is at (6,0)
    **   the left edge      is at (0,0)
    */

    if ( xx_wm_decoration->has_auto_relocate ) {
          // Compensate for new view dimensions in the offset values
          // of the right and bottom decorations.
        xx_wm_decoration-> right_sx = twc_view->view_surface->width;
        xx_wm_decoration->bottom_sy = twc_view->view_surface->height;
    }

      // Part 1of3.  Find the decoration edges that are leftmost and
      // highest relative to the client surface.
    wld_coordinate_t SX;
    wld_coordinate_t SY;
    northwest_decoration_surface_coord(
        twc_view,
        &SX,
        &SY
    );

      // Set the view-relative coords of the client surface.  If a
      // deco egde is right of (or below) the client surface, use 0.
    twc_view->view_surface_vx = 0 - min_coord(0, SX);
    twc_view->view_surface_vy = 0 - min_coord(0, SY);

      // Part 2of3.  Set the view-relative coords of each deco edge.
      // These settings get used in part 3 for the calculation of the
      // rightmost and lowest extent of the view.
    twc_view->deco_top_vx     = xx_wm_decoration->top_sx    - SX;
    twc_view->deco_top_vy     = xx_wm_decoration->top_sy    - SY;
    twc_view->deco_bottom_vx  = xx_wm_decoration->bottom_sx - SX;
    twc_view->deco_bottom_vy  = xx_wm_decoration->bottom_sy - SY;
    twc_view->deco_left_vx    = xx_wm_decoration->left_sx   - SX;
    twc_view->deco_left_vy    = xx_wm_decoration->left_sy   - SY;
    twc_view->deco_right_vx   = xx_wm_decoration->right_sx  - SX;
    twc_view->deco_right_vy   = xx_wm_decoration->right_sy  - SY;

      // Part 3of3.  Find the decoration edges that are rightmost and
      // lowest relative to the client surface.  This must follow part
      // 2.
    wld_coordinate_t MAX_DECO_SX;
    wld_coordinate_t MAX_DECO_SY;
    southeast_decoration_surface_coord(
        twc_view,
        &MAX_DECO_SX,
        &MAX_DECO_SY
    );

      // Calculate the new view width and height.  The deco edges
      // could be anywhere, so don't assume they "wrap" the client
      // surface.
    new_width  = max_coord( MAX_DECO_SX, twc_view->view_surface_vx + twc_view->view_surface->width );
    new_height = max_coord( MAX_DECO_SY, twc_view->view_surface_vy + twc_view->view_surface->height);

      // Shorthand name for view-relative coords of the clinet
      // surface.
    wld_coordinate_t surface_vx = twc_view->view_surface_vx;
    wld_coordinate_t surface_vy = twc_view->view_surface_vy;

      // The (signed) offsets from the surface to the enclosure.
    wld_coordinate_t surface_to_enclosure_offset_x = twc_view->view_surface->current_wl_buffer->enclosure_sx;
    wld_coordinate_t surface_to_enclosure_offset_y = twc_view->view_surface->current_wl_buffer->enclosure_sy;

      // Calc values for view-relative coordinates of the enclosure
    twc_view->enclosure_vx = surface_vx + surface_to_enclosure_offset_x;
    twc_view->enclosure_vy = surface_vy + surface_to_enclosure_offset_y;

    recalc_layout_coordinates(
        twc_view,
        new_width,
        new_height
    );

    return;
}

extern
enum decoration_mode
twc_update_decoration_mode(
    struct twc_view       *twc_view,
    enum   decoration_mode request_mode
) {
    /*
    ** This routine sets the display_format for the view based on
    ** decoration policy.  It returns an appropriate decoration mode
    ** which can be emitted to the client when a mode is requested.
    **
    ** There are 3 deco choices: SSD, CSD, NOD (no deco).  Using 3
    ** levels of ranked priority, there are 6 possible deco
    ** combinations (3x2x1).  They are listed in the following table.
    ** We condense them into 4 named policies.
    **
    **    Priority Ranking      Named Policy
    **  -------------------     ------------
    **      1st 2nd 3rd
    **      --- --- ---
    **      SSD CSD NOD         Always SSD      client has no say
    **      SSD NOD CSD         Always SSD
    **
    **      CSD SSD NOD         AllowCSD_else_SSD
    **      CSD NOD SSD         AllowCSD_else_No_Decorate
    **
    **      NOD SSD CSD         Never           client has no say
    **      NOD CSD SSD         Never
    */

    enum twc_decoration_policy twc_decoration_policy = (
        twc_view->twc_decoration_policy
    );

    enum decoration_mode new_status         = DM_None;
    enum decoration_mode response_to_client = DM_Server_Side;

    /*
    ** The following is a decision table for determining the
    ** decoration status of a toplevel given one of the named policies
    ** listed above.
    **
    ** Each result is a pair (status, response) where
    **
    **   status     is the new decoration status
    **   response   is the response emitted to the client
    **
    **      N   is DM_None              the default request mode
    **      C   is DM_Client_Side
    **      S   is DM_Server_Side
    **
    ** Note that the response tells the client how it should decorate.
    ** The value must be DM_Client_Side or DM_Server_Side.  The latter
    ** simply means the client must not decorate.
    **
    **     window_deco_policy        vs.         request_mode
    ** -----------------------------    ------------------------------
    **
    **                                              CLIENT      SERVER
    **                                  None        SIDE        SIDE
    **                                  ------      ------      ------
    ** Never_Decorate                   (N, S)      (N, S)      (N, S)
    **
    ** Allow_CSD_Default_No_Decorate    (N, S)      (C, C)      (N, S)
    **
    ** Always_SSD                       (S, S)      (S, S)      (S, S)
    **
    ** Allow_CSD_Default_SSD            (S, S)      (C, C)      (S, S)
    */

    switch ( twc_decoration_policy ) {
        default:
        case DP_Never_Decorate:
              // decision table Line 1
            new_status         = DM_None;
            response_to_client = DM_Server_Side;
        break;

        case DP_Allow_CSD_else_No_Decorate:
              // decision table Line 2
            if ( request_mode == DM_Client_Side ) {

                new_status         = DM_Client_Side;
                response_to_client = DM_Client_Side;

            } else {

                new_status         = DM_None;
                response_to_client = DM_Server_Side;
            }
        break;

        case DP_Always_SSD:
              // decision table Line 3
            new_status         = DM_Server_Side;
            response_to_client = DM_Server_Side;
        break;

        case DP_Allow_CSD_else_SSD:
              // decision table Line 4
            if ( request_mode == DM_Client_Side ) {
                new_status         = DM_Client_Side;
                response_to_client = DM_Client_Side;
            } else {

                new_status         = DM_Server_Side;
                response_to_client = DM_Server_Side;
            }
        break;
    }

    enum display_format new_display_format = (
        new_status == DM_Server_Side
        ? DF_VIEW_PLUS_DECO
        : DF_VIEW_ONLY
    );

    if ( new_display_format != twc_view->display_format ) {

        twc_view->display_format = new_display_format;
        twc_view_recalc_geometry( twc_view );
    }

    return response_to_client;
};

extern
void
twc_window_update_surface_size(
    struct wl_surface *wl_surface
) {
    if ( ! SURFACE_HAS_ROLE(wl_surface) ) { return; }

    struct twc_view *twc_view = wl_surface->twc_view;

    twc_view_recalc_geometry(twc_view);

    if ( SURFACE_IS_WINDOW(wl_surface) ) {

        xx_wm_window_update_dimension( twc_view->twc_window );

    } else if ( wl_surface->type == ST_XX_WM_ICON ) {

        xx_wm_window_update_icon_dimension( twc_view->twc_window );

    } else if ( wl_surface->type == ST_XX_WM_DECO      ) {
    } else if ( wl_surface->type == ST_XX_WM_WALLPAPER ) {
        // Nothing to do.
    }

      // Check for focus changes.
    twc_seat_recalc_pointer_focus();

    return;
}

extern
struct wl_surface*
twc_window_surface_at(
    wl_fixed_t  ptr_loc_fix_lx, // fixed format, layout  coord
    wl_fixed_t  ptr_loc_fix_ly,
    wl_fixed_t *ptr_loc_fix_sx, // fixed format, surface coord
    wl_fixed_t *ptr_loc_fix_sy,
    void      **nested_surface
) {
    // A toplevel surface may be composed of nested surfaces.  In
    // addition to finding the window containing the pointer (if any),
    // this routine determines which nested surface has the pointer.

      // The overlay window is always on top.
    if ( overlay_window_active ) {

          // The overlay covers everything so the overlay surface
          // coords are simply the pointer layout coords.
        *ptr_loc_fix_sx = ptr_loc_fix_lx;
        *ptr_loc_fix_sy = ptr_loc_fix_ly;

        *nested_surface = OVERLAY_NESTED_WL_SURFACE;

        return OVERLAY_WL_SURFACE;
    }

    struct twc_workspace *active_workspace = (
        srv_output_active_workspace(
            srv_output_at(
                ptr_loc_fix_lx,
                ptr_loc_fix_ly
            )
        )
    );

      // Iterate though all the client windows.
    struct twc_view *twc_view;
    twc_display_list_for_each(twc_view) {

        struct wl_surface *view_surface = twc_view->view_surface;

        wksp_occupancy_t wksp_occupancy = twc_view->twc_window->wksp_occupancy;

        if (! WKSP_OCCUPANCY_IS_MEMBER(
                wksp_occupancy,
                twc_workspace_id( active_workspace )
              )
        ) { continue; }

        // The GTK toolkit contains a widget that will place an
        // invisible rim around the visible content.  When this rim
        // has pointer focus, GTK will will show a "resize cursor"
        // which can be used to resize the window.  GTK has used the
        // term "border grips" to describe this mechanism.
        //
        // Since server-side decorations wrap the visible content, the
        // rim overlaps them.  In particular, the titlebar will not
        // get pointer focus unless the pointer is well away from the
        // visible app content.  This reduces the accessible area of
        // the titlebar.
        //
        // The solution is to perform "surface_at" on the decorations
        // first taking into account their entire size.  I.e., probe
        // as if the decorations stack higher.  For consistancy, we
        // should paint the decorations last.
        //
        // Note: When a GTK app calls for border grips, you'll get
        // this rim.  Since GTK 3.12, a user cannot globally disable
        // this widget using their GTK configuration files.  The only
        // remedy is if an app doesn't use the widget at all, or
        // provides a way to disable them.

          // pointer in view-relative coordinates
        wld_coordinate_t ptr_vx = wl_fixed_to_int( ptr_loc_fix_lx ) - twc_view->layout_x;
        wld_coordinate_t ptr_vy = wl_fixed_to_int( ptr_loc_fix_ly ) - twc_view->layout_y;

        if ( TWC_VIEW_IS_DECORATED(twc_view) ) {

            // Check each of the four deco surfaces in turn

            struct wl_surface *deco_surface;
            wld_coordinate_t   ptr_dx;      // pointer in some deco_surface-relative coordinates
            wld_coordinate_t   ptr_dy;

            deco_surface = twc_view->xx_wm_decoration.top_surface;
            if ( SURFACE_IS_MAPPED(deco_surface) ) {
                  // deco_top-relative coordinates.
                ptr_dx = ptr_vx - twc_view->deco_top_vx;
                ptr_dy = ptr_vy - twc_view->deco_top_vy;

                *nested_surface = (
                    wl_surface_at(
                        deco_surface,
                        ptr_dx,
                        ptr_dy,
                        ptr_loc_fix_sx,
                        ptr_loc_fix_sy
                    )
                );
                if ( *nested_surface ) { return deco_surface; }
            }

            deco_surface = twc_view->xx_wm_decoration.bottom_surface;
            if ( SURFACE_IS_MAPPED(deco_surface) ) {
                  // deco_bottom-relative coordinates.
                ptr_dx = ptr_vx - twc_view->deco_bottom_vx;
                ptr_dy = ptr_vy - twc_view->deco_bottom_vy;

                *nested_surface = (
                    wl_surface_at(
                        deco_surface,
                        ptr_dx,
                        ptr_dy,
                        ptr_loc_fix_sx,
                        ptr_loc_fix_sy
                    )
                );
                if ( *nested_surface ) { return deco_surface; }
            }

            deco_surface = twc_view->xx_wm_decoration.left_surface;
            if ( SURFACE_IS_MAPPED(deco_surface) ) {
                  // deco_left-relative coordinates.
                ptr_dx = ptr_vx - twc_view->deco_left_vx;
                ptr_dy = ptr_vy - twc_view->deco_left_vy;

                *nested_surface = (
                    wl_surface_at(
                        deco_surface,
                        ptr_dx,
                        ptr_dy,
                        ptr_loc_fix_sx,
                        ptr_loc_fix_sy
                    )
                );
                if ( *nested_surface ) { return deco_surface; }
            }

            deco_surface = twc_view->xx_wm_decoration.right_surface;
            if ( SURFACE_IS_MAPPED(deco_surface) ) {
                  // deco_right-relative coordinates.
                ptr_dx = ptr_vx - twc_view->deco_right_vx;
                ptr_dy = ptr_vy - twc_view->deco_right_vy;

                *nested_surface = (
                    wl_surface_at(
                        deco_surface,
                        ptr_dx,
                        ptr_dy,
                        ptr_loc_fix_sx,
                        ptr_loc_fix_sy
                    )
                );

                if ( *nested_surface ) { return deco_surface; }
            }
        }

        if ( SURFACE_IS_MAPPED(view_surface) ) {
            // Fix: can an unmapped view_surface be in the display list??

              // pointer in view_surface-relative coordinates
            wld_coordinate_t ptr_sx  = ptr_vx - twc_view->view_surface_vx;
            wld_coordinate_t ptr_sy  = ptr_vy - twc_view->view_surface_vy;;

           *nested_surface = (
                wl_surface_at(
                    view_surface,
                    ptr_sx,
                    ptr_sy,
                    ptr_loc_fix_sx,  // nested surface coordinates
                    ptr_loc_fix_sy
                )
            );
            if ( *nested_surface ) { return view_surface; }
        }
    }

    // Fix: Is there one default surface or one per workspace?
    *ptr_loc_fix_sx = ptr_loc_fix_lx;
    *ptr_loc_fix_sy = ptr_loc_fix_ly;

    *nested_surface = DEFAULT_NESTED_WL_SURFACE;

    return DEFAULT_WL_SURFACE;
}

extern
void
twc_window_activate_overlay(
    void
) {
    // Fix: Has overlay surface been annexed?

    overlay_window_active = true;

      // Check for focus changes.
    twc_seat_recalc_pointer_focus();
}

extern
void
twc_window_deactivate_overlay(
    void
) {
    overlay_window_active = false;

      // Check for focus changes.
    twc_seat_recalc_pointer_focus();
}

extern
void
twc_window_reoccupy(
    struct twc_window      *twc_window,
    enum xx_wm_workspace_id wksp_occupancy
) {
    if ( twc_window == NULL ) { return; }

    twc_window->wksp_occupancy = wksp_occupancy;

    xx_wm_window_update_occupancy(
        twc_window
    );

    return;
}

extern
void
twc_window_set_context(
    struct twc_window      *twc_window,
    enum xx_wm_workspace_id wksp_occupancy,
    struct wl_output       *wl_output
) {
    //LOG_INFO("twc_window_set_context");

    if ( twc_window == NULL ) { return; }

    struct wl_surface *wl_surface = (
        twc_window->client_view->view_surface
    );

    if ( twc_window->preferred_output != NULL       ||
         wl_surface->xdg_surface.has_initial_commit
    ) {
        // Protocol error
        return;
    }

    wksp_occupancy_t old_occupancy = twc_window->wksp_occupancy;

    if ( wksp_occupancy != XX_WM_WORKSPACE_ID_WKSP_NONE &&
         wksp_occupancy != old_occupancy
    ) {
        twc_window->wksp_occupancy = wksp_occupancy;

        xx_wm_window_update_occupancy(
            twc_window
        );
    }

    if ( wl_output != NULL ) {
        twc_window->preferred_output = wl_output->srv_output;
    }

    return;
}

extern
void
twc_window_set_hotspot(
    struct twc_window *twc_window,
    int32_t            hotspot_sx,
    int32_t            hotspot_sy
) {
    if ( twc_window == NULL ) { return; }

    twc_window->client_view->hotspot_sx = hotspot_sx;
    twc_window->client_view->hotspot_sy = hotspot_sy;

    return;
}

extern
void
twc_view_update_location(
    struct twc_view *twc_view,
    wld_coordinate_t layout_x,
    wld_coordinate_t layout_y
) {
    twc_view->layout_x = layout_x;
    twc_view->layout_y = layout_y;

    if ( TWC_VIEW_IS_CLIENT_VIEW(twc_view) ) {
        xx_wm_window_update_location( twc_view->twc_window );
    } else {
        xx_wm_window_update_icon_location( twc_view->twc_window );
    }

    return;
}

extern
wksp_occupancy_t
twc_view_wksp_occupancy(
    struct twc_view *twc_view
) {
    return twc_view->twc_window->wksp_occupancy;
}

extern
struct twc_view*
twc_view_at(
    wl_fixed_t  ptr_loc_fix_lx, // fixed format, layout  coord
    wl_fixed_t  ptr_loc_fix_ly
) {
    wl_fixed_t ptr_loc_fix_sx;  // fixed format, surface coord
    wl_fixed_t ptr_loc_fix_sy;
    void       *unused;

    bool overlay_window_active_saved = overlay_window_active;

      // Temporarily deactiveate overlay window.
    overlay_window_active = false;

    struct wl_surface *wl_surface = (
        twc_window_surface_at(
             ptr_loc_fix_lx,    // fixed format, layout  coord
             ptr_loc_fix_ly,
            &ptr_loc_fix_sx,    // fixed format, surface coord
            &ptr_loc_fix_sy,
            &unused
        )
    );

      // Restore overlay window status.
    overlay_window_active = overlay_window_active_saved;

    struct twc_view *twc_view = wl_surface->twc_view;

    return twc_view;
}

extern
wld_coordinate_t
twc_view_horizontal_fit(
    struct twc_view  *twc_view,
    wld_coordinate_t  output_x,
    wld_dimension_t   output_width
) {
    // Try to locate the view at output_x on an output with given
    // width.  If it won't fit, try adjusting (reducing) the x_coord
    // so it will.

      // Calc the right most x_coord that works.
    wld_coordinate_t max_x = output_width - twc_view->width;

    if ( 0 <= max_x ) {
          // view will fit horizontally on output but may need
          // adjustment
        return (
            output_x < max_x
            ? output_x          // no adjustment
            : max_x             //    adjustment
        );
    }

      // The view is wider than the output.
    return 0;
}

extern
wld_coordinate_t
twc_view_vertical_fit(
    struct twc_view  *twc_view,
    wld_coordinate_t  output_y,
    wld_dimension_t   output_height
) {
    // See twc_view_horizontal_fit()

    wld_coordinate_t max_y = output_height - twc_view->height;

    if ( 0 <= max_y ) {
          // view will fit veritcally on output but may need
          // adjustment
        return (
            output_y < max_y
            ? output_y          // no adjustment
            : max_y             //    adjustment
        );
    }

    return 0;
}

extern
struct twc_view*
twc_view_with_pointer(
    void
) {
    wl_fixed_t ptr_loc_fix_lx;  // fixed format, layout coord
    wl_fixed_t ptr_loc_fix_ly;

      // Where is the pointer now?
    twc_seat_pointer_location(
        &ptr_loc_fix_lx,
        &ptr_loc_fix_ly
    );

    struct twc_view *twc_view = (
        twc_view_at(
            ptr_loc_fix_lx,
            ptr_loc_fix_ly
        )
    );

    return twc_view;
}

static
struct srv_output*
twc_view_applicable_output(
    struct twc_view *twc_view
) {

    if ( TWC_VIEW_HAS_COORDIATES(twc_view) ) {

        return (
            srv_output_at(
                wl_fixed_from_int(twc_view->layout_x),
                wl_fixed_from_int(twc_view->layout_y)
            )
        );
    }

    return twc_seat_output_with_pointer_focus();
}

extern
void
twc_view_select(
    struct twc_view *twc_view
) {
    wl_fixed_t ptr_loc_fix_lx;  // fixed format, layout coord
    wl_fixed_t ptr_loc_fix_ly;

      // Where is the pointer now?
    twc_seat_pointer_location(
        &ptr_loc_fix_lx,
        &ptr_loc_fix_ly
    );

    G_selected = G_selected_null;

      // The grab point is set on selection except for resize.
    G_selected.window_grab_offset_x = twc_view->layout_x - wl_fixed_to_int(ptr_loc_fix_lx);
    G_selected.window_grab_offset_y = twc_view->layout_y - wl_fixed_to_int(ptr_loc_fix_ly);

    G_selected.view = twc_view;

    return;
}

extern
bool
twc_view_select_for_resize(
    struct twc_view              *twc_view,
    enum xdg_toplevel_resize_edge resize_edges
) {
      // Return true <==> the view was actually selected.
    if ( resize_edges == XDG_TOPLEVEL_RESIZE_EDGE_NONE ||
         twc_view     == NULL_TWC_VIEW
    ) { return false; }

      // Fix: xx_wm_icon surfaces are not xdg_toplevels.  Can they be
      // resized interactively by the user?
    if (TWC_VIEW_IS_ICON_VIEW( twc_view )) { return false; }

      // Where is the pointer now?
    wl_fixed_t ptr_loc_fix_lx;  // fixed format, layout coord
    wl_fixed_t ptr_loc_fix_ly;
    twc_seat_pointer_location(
        &ptr_loc_fix_lx,
        &ptr_loc_fix_ly
    );

    wld_coordinate_t ptr_x      = wl_fixed_to_int(ptr_loc_fix_lx);
    wld_coordinate_t ptr_y      = wl_fixed_to_int(ptr_loc_fix_ly);

    wld_dimension_t view_width  = twc_view->width;
    wld_dimension_t view_height = twc_view->height;

    G_selected      = G_selected_null;
    G_selected.view = twc_view;

    G_selected.orig_left   = twc_view->layout_x;
    G_selected.orig_right  = twc_view->layout_x + view_width;

    G_selected.orig_top    = twc_view->layout_y;
    G_selected.orig_bottom = twc_view->layout_y + view_height;

    G_selected.deco_width  = view_width  - twc_view->view_surface->width;
    G_selected.deco_height = view_height - twc_view->view_surface->height;

    if ( resize_edges == RESIZE_EDGE_NOT_PRE_SPECIFIED ) {

          // Neither axis has a pre-specified edge.
          // Both axes start in FINDING_ANCHOR mode ...
        horiz_resizing_mode = RM_FINDING_ANCHOR;
         vert_resizing_mode = RM_FINDING_ANCHOR;
          // ... and grab point(s) are simply the pointer.
        G_selected.window_grab_offset_x = 0;
        G_selected.window_grab_offset_y = 0;

        return true;
    }

    // A client has asked to resize along some specific edges.  The
    // grab points are relative to the resizing edge.

      // Set default resizing modes.  Will be reset in the switch
      // below as required.
    horiz_resizing_mode = RM_RESIZING;
     vert_resizing_mode = RM_RESIZING;

    switch (resize_edges) {

        // The initial cases override the default resizing_modes.

        case XDG_TOPLEVEL_RESIZE_EDGE_LEFT:
            vert_resizing_mode = RM_DONE;

            G_selected.left_right_anchor    = G_selected.orig_right;
            G_selected.window_grab_offset_x = G_selected.orig_left - ptr_x;
            G_selected.window_grab_offset_y = 0;
        break;

        case XDG_TOPLEVEL_RESIZE_EDGE_RIGHT:
            vert_resizing_mode = RM_DONE;

            G_selected.left_right_anchor    = G_selected.orig_left;
            G_selected.window_grab_offset_x = G_selected.orig_right - ptr_x;
            G_selected.window_grab_offset_y = 0;
        break;

        case XDG_TOPLEVEL_RESIZE_EDGE_TOP:
            horiz_resizing_mode = RM_DONE;

            G_selected.top_bottom_anchor    = G_selected.orig_bottom;
            G_selected.window_grab_offset_x = 0;
            G_selected.window_grab_offset_y = G_selected.orig_top - ptr_y;
        break;

        case XDG_TOPLEVEL_RESIZE_EDGE_BOTTOM:
            horiz_resizing_mode = RM_DONE;

            G_selected.top_bottom_anchor    = G_selected.orig_top;
            G_selected.window_grab_offset_x = 0;
            G_selected.window_grab_offset_y = G_selected.orig_bottom - ptr_y;
        break;

        // The remaining cases all use the default resizing_modes.

        case XDG_TOPLEVEL_RESIZE_EDGE_TOP_LEFT:
            G_selected.left_right_anchor    = G_selected.orig_right;
            G_selected.top_bottom_anchor    = G_selected.orig_bottom;
            G_selected.window_grab_offset_x = G_selected.orig_left - ptr_x;
            G_selected.window_grab_offset_y = G_selected.orig_top  - ptr_y;

        break;

        case XDG_TOPLEVEL_RESIZE_EDGE_TOP_RIGHT:
            G_selected.left_right_anchor    = G_selected.orig_left;
            G_selected.top_bottom_anchor    = G_selected.orig_bottom;
            G_selected.window_grab_offset_x = G_selected.orig_right - ptr_x;
            G_selected.window_grab_offset_y = G_selected.orig_top   - ptr_y;
        break;

        case XDG_TOPLEVEL_RESIZE_EDGE_BOTTOM_LEFT:
            G_selected.left_right_anchor    = G_selected.orig_right;
            G_selected.top_bottom_anchor    = G_selected.orig_top;
            G_selected.window_grab_offset_x = G_selected.orig_left   - ptr_x;
            G_selected.window_grab_offset_y = G_selected.orig_bottom - ptr_y;
        break;

        case XDG_TOPLEVEL_RESIZE_EDGE_BOTTOM_RIGHT:
            G_selected.left_right_anchor    = G_selected.orig_left;
            G_selected.top_bottom_anchor    = G_selected.orig_top;
            G_selected.window_grab_offset_x = G_selected.orig_right  - ptr_x;
            G_selected.window_grab_offset_y = G_selected.orig_bottom - ptr_y;
        break;

        default:
            G_selected = G_selected_null;
            return false;
        break;
    }

    return true;
}

extern
void
twc_view_unselect(
    void
) {
    G_selected = G_selected_null;

    return;
}

extern
void
twc_view_relocate_selected(
    wl_fixed_t ptr_loc_fix_lx,  // fixed format, layout coord
    wl_fixed_t ptr_loc_fix_ly
) {
    if ( G_selected.view != NULL_TWC_VIEW) {

        wld_coordinate_t ptr_x         = wl_fixed_to_int(ptr_loc_fix_lx);
        wld_coordinate_t ptr_y         = wl_fixed_to_int(ptr_loc_fix_ly);
        struct twc_view  *selected_view = G_selected.view;

        twc_view_update_location(
            selected_view,
            ptr_x + G_selected.window_grab_offset_x,
            ptr_y + G_selected.window_grab_offset_y
        );
    }

    return;
}

 /*
 ** Window resizing
 ** ---------------
 **
 ** A window consists of two pairs of edges, left/right & top/bottom.
 ** Pointer motion determines which edges are (re)located and by how
 ** much.
 **
 ** In the general case, any edge could be relocated.  For each pair,
 ** the first edge to be assigned a coordinate is called the anchor.
 ** Once the anchor is set, the location of the other edge is
 ** adjusted.  An anchor is always one of the original edges.
 **
 ** Each edge pair goes thru 3 states:
 **
 **   1 Determine the anchor edge for the pair.
 **
 **   2 With the anchor edge chosen, position the other edge.
 **
 **   3 Stop resizing.
 **
 ** 1. In this state, the window displays using the pair's original
 **    edges.  When the pointer encounters or crosses an edge, the
 **    opposite edge in its pair becomes the anchor edge.  The anchor
 **    edge retains its original location.  The crossed edge is called
 **    the grabbed edge.
 **
 ** 2. Pointer motion determines a provisional location for the
 **    grabbed edge.  The window appearence changes as the pointer
 **    adjusts the grabbed edge.
 **
 ** 3. The xx_wm_interactive_agent agent issues a drop request.  The
 **    window is resized to the locations of the anchor and current
 **    grabbed edges.  If some edge pair is still in state 1, that
 **    edge pair retains its original locations.
 **
 ** Sometimes an anchor edge for one or both pairs is pre-specified as
 ** when grabbing a surface by an edge or corner.  In this case, a
 ** pair starts in state 2.
 */

extern
void
twc_view_resize_selected(
    wl_fixed_t ptr_loc_fix_lx,  // fixed format, layout coord
    wl_fixed_t ptr_loc_fix_ly
) {
    // This code grabs an edge of the view including decorations, not
    // the client surface.

    if ( G_selected.view == NULL_TWC_VIEW) { return; }

    // Fix: Only XDG toplevels are resizable??  Maybe icons are too??
    if ( G_selected.view->view_surface->type != ST_XDG_TOPLEVEL ) { return; }

    wld_coordinate_t ptr_x = wl_fixed_to_int(ptr_loc_fix_lx) + G_selected.window_grab_offset_x;
    wld_coordinate_t ptr_y = wl_fixed_to_int(ptr_loc_fix_ly) + G_selected.window_grab_offset_y;

    wld_coordinate_t new_left   = G_selected.view->layout_x;
    wld_coordinate_t new_right  = G_selected.view->layout_x + G_selected.view->width;

    wld_coordinate_t new_top    = G_selected.view->layout_y;
    wld_coordinate_t new_bottom = G_selected.view->layout_y + G_selected.view->height;

    switch (horiz_resizing_mode) {

        case RM_FINDING_ANCHOR:

            if ( ptr_x < G_selected.orig_left ) {
                G_selected.left_right_anchor = G_selected.orig_right;
                horiz_resizing_mode          = RM_RESIZING;
            }

            if ( G_selected.orig_right < ptr_x ) {
                G_selected.left_right_anchor = G_selected.orig_left;
                horiz_resizing_mode          = RM_RESIZING;
            }
        break;

        case RM_RESIZING:
              // Guess that the G_selected.left_right_anchor is to the left
              // of ptr_x.  Corrected below if not.
            new_left  = G_selected.left_right_anchor;
            new_right = ptr_x;
        break;

        case RM_DONE:
        default:
        break;
    }

    switch (vert_resizing_mode) {

        case RM_FINDING_ANCHOR:

            if ( ptr_y < G_selected.orig_top ) {
                G_selected.top_bottom_anchor = G_selected.orig_bottom;
                vert_resizing_mode           = RM_RESIZING;
            }

            if ( G_selected.orig_bottom < ptr_y ) {
                G_selected.top_bottom_anchor = G_selected.orig_top;
                vert_resizing_mode           = RM_RESIZING;
            }
        break;

        case RM_RESIZING:
              // Guess that the G_selected.top_bottom_anchor is above ptr_y.
              // Corrected below if not.
            new_top    = G_selected.top_bottom_anchor;
            new_bottom = ptr_y;
        break;

        case RM_DONE:
        default:
        break;
    }

    if (( horiz_resizing_mode == RM_FINDING_ANCHOR ) &&
        (  vert_resizing_mode == RM_FINDING_ANCHOR )
    ) { return; }


      // The left/right or top/bottom relationship may have flipped.
    wld_coordinate_t temp;
    if ( new_right < new_left ) {

        temp      = new_left;
        new_left  = new_right;
        new_right = temp;

        twc_view_update_location(
            G_selected.view,
            new_left,
            G_selected.view->layout_y
        );
    }
    if ( new_bottom < new_top ) {

        temp       = new_top;
        new_top    = new_bottom;
        new_bottom = temp;

        twc_view_update_location(
            G_selected.view,
            G_selected.view->layout_x,
            new_top
        );
    }

      // The new dimensions given to the client are for the client
      // surface - not including decorations.
    wld_dimension_t new_surface_width  = new_right  - new_left - G_selected.deco_width;;
    wld_dimension_t new_surface_height = new_bottom - new_top  - G_selected.deco_height;

    // Fix: should we impose a larger minimum?
    if (( new_surface_width  == 0 ) ||
        ( new_surface_height == 0 )
    ) { return; }

    // Fix: Don't emit if the geom didn't change???

      // Create a wl_array of XDG_TOPLEVEL_STATEs having exactly one
      // item.
    // Fix: make the state_array and states static and init in
    // twc_window_initialize()
    struct wl_array  state_array;
    struct wl_array *states = &state_array;
    wl_array_init(   states);
    enum xdg_toplevel_state *state_0 = (
        wl_array_add( states, sizeof(enum xdg_toplevel_state) )
    );
    *state_0 = XDG_TOPLEVEL_STATE_RESIZING;

    xdg_toplevel_emit_configure(
        &G_selected.view->view_surface->xdg_toplevel,
         new_surface_width,
         new_surface_height,
         states
    );

    wl_array_release(states);

    xdg_surface_emit_configure(
        &G_selected.view->view_surface->xdg_surface
    );

    return;
}

static
void
twc_window_toggle_iconify(
    struct twc_window *twc_window
) {
    bool was_icon_displayed =   twc_window->icon_view->is_in_display_list;
    bool becomes_iconified  = ! twc_window->is_iconified;

    twc_window->is_iconified = becomes_iconified;

    twc_window_update_display_list(twc_window);

    if ( becomes_iconified ) {
        // window -> icon

        twc_seat_surface_remove_all_focus(
            twc_window->client_view->view_surface
        );

    } else {
        // icon -> window

        if ( was_icon_displayed ) {
            twc_seat_surface_remove_all_focus(
                twc_window->icon_view->view_surface
            );
        } else {
            twc_seat_recalc_pointer_focus();
        }
    }

    xx_wm_window_update_iconify_state(twc_window);

    return;
}

extern
void
twc_view_toggle_iconify(
    struct twc_view *twc_view
) {
    twc_window_toggle_iconify(twc_view->twc_window);
}

extern
void
twc_view_toggle_iconify_selected(
    void
) {
    twc_view_toggle_iconify( G_selected.view );

    return;
}

extern
void
twc_window_set_iconify_state(
    struct twc_window *twc_window,
    bool               iconify_state
) {
    if ( twc_window->is_iconified == iconify_state ) { return; }

    twc_window_toggle_iconify(twc_window);

    return;
}

static inline
bool
twc_view_overlap(
    struct twc_view *view_A,
    struct twc_view *view_B
) {
    // Fix: This routine does not account for xdg_popups as they may
    // extend beyond the nominal view rectangle.  If popups are
    // addressed immediately and then dismissed, this may not be a big
    // issue.

    if ( ( ! view_A->is_in_display_list ) ||
         ( ! view_B->is_in_display_list )
    ) { return false; }

    wld_coordinate_t AL = view_A->layout_x;
    wld_coordinate_t AR = view_A->layout_x + view_A->width;

    wld_coordinate_t BL = view_B->layout_x;
    wld_coordinate_t BR = view_B->layout_x + view_B->width;

    if ( AR < BL ||
         BR < AL
    ) { return false; }

    wld_coordinate_t AT = view_A->layout_y;
    wld_coordinate_t AB = view_A->layout_y + view_A->height;

    wld_coordinate_t BT = view_B->layout_y;
    wld_coordinate_t BB = view_B->layout_y + view_B->height;

      // Y-axis is positive going down
#   define LOWER_THAN(l,h) (l) > (h)

    if ( LOWER_THAN( AT, BB ) ||
         LOWER_THAN( BT, AB )
    ) { return false; }

    return true;
}

static
bool
twc_view_is_occluded(
    struct twc_view *subject_view
) {
    struct twc_workspace* subject_workspace = (
        twc_seat_workspace_with_pointer_focus()
    );

    struct twc_view  *list_view;
    wl_list_for_each( list_view, TWC_display_list, link) {

        if ( list_view == subject_view ) { return false; }

        bool list_view_in_workspace = (
            WKSP_OCCUPANCY_IS_MEMBER(
                list_view->twc_window->wksp_occupancy,
                twc_workspace_id(subject_workspace)
             )
        );

        if ( list_view_in_workspace                      &&
             twc_view_overlap( subject_view, list_view )
        ) {
              // subject_view occluded by list_view
            return true;
        }
    }

    return false;
}

extern
void
twc_view_raise_lower(
    struct twc_view *twc_view
) {
    if ( twc_view->is_in_display_list ) {

        bool view_occluded = twc_view_is_occluded(twc_view);

        twc_display_list_remove(twc_view);

        if ( view_occluded ) {
            // raise
            twc_display_list_insert(twc_view);
        } else {
            // lower
            twc_display_list_insert_last(twc_view);
        }
          // Check for focus changes.
        twc_seat_recalc_pointer_focus();
    }

    return;
}

extern
void
twc_view_vzoom(
    struct twc_view *twc_view
) {
    struct twc_window *twc_window = twc_view->twc_window;

    if ( TWC_VIEW_IS_ICON_VIEW(twc_view) ) { return; }

    // Fix: How will/should gravity affect zoom???  Vzoom is an
    // explicit user request.
    if ( twc_window->window_gravity != WG_NorthWest ) { return; }

    struct srv_output *srv_output = (
       srv_output_at(
           twc_view->layout_x,
           twc_view->layout_y
       )
    );

    int new_surface_height;

    if (twc_view->has_alternate_size) {
        twc_view->has_alternate_size = false;

        new_surface_height = twc_view->prezoom_surface_height;
        twc_view->layout_y = twc_view->prezoom_layout_y;

          // Defined only when zoomed
        twc_view->prezoom_layout_y       = WLD_INVALID_COORD;
        twc_view->prezoom_surface_height = 0;

    } else {
        twc_view->has_alternate_size = true;

        int old_surface_height = twc_view->view_surface->height;

        new_surface_height = (
            old_surface_height +
            srv_output->height -
            twc_view->height
        );

        twc_view->prezoom_surface_height = old_surface_height;
        twc_view->prezoom_layout_y       = twc_view->layout_y;

          // Move window to top-of-screen.  Happens before client has
          // a chance to resize or even if client chooses not to.
        twc_view->layout_y = 0;

          // Check for focus changes.
        twc_seat_recalc_pointer_focus();
    }

      // new dimension, same states
    xdg_toplevel_emit_configure(
        &twc_view->view_surface->xdg_toplevel,
         twc_view->view_surface->width,
         new_surface_height,
         NULL    //states
    );

    xdg_surface_emit_configure(
        &twc_view->view_surface->xdg_surface
    );

    return;
}

extern
void
twc_view_close(
    struct twc_view *twc_view
) {
    xdg_toplevel_emit_close(
        &twc_view->view_surface->xdg_toplevel
    );
}

extern
void
twc_window_update_display_list(
    struct twc_window *twc_window
) {
    // Usually, only one of client_view or icon_view is displayed.
    // However, sometimes neither are displayed, and sometimes both
    // are displayed.

    struct twc_view *client_view = twc_window->client_view;
    struct twc_view *icon_view   = twc_window->icon_view;

      // Before a wayland surface may be displayed, it must be mapped,
      // i.e., have a role and a non-null buffer.
    bool client_is_mapped = SURFACE_IS_MAPPED(client_view->view_surface);
    bool   icon_is_mapped = SURFACE_IS_MAPPED(  icon_view->view_surface);

      // Even if mapped, a compositor may choose to display a surface
      // or not depending on other circumstances.
    bool is_window_withheld = twc_window->is_iconified;

      // Assume the icon will not be displayed.  There are 2
      // exceptions.  Check for them below.
    bool is_icon_withheld = true;

    if ( twc_window->is_iconified ) {
        if ( ! twc_window->iconify_by_masking ) {
            is_icon_withheld = false;
        }
    } else {
        if ( twc_window->hold_icon ) {
            is_icon_withheld = false;
        }
    }

      // We'll compare old with new.
    enum xx_wm_window_display_state old_display_state = (
        client_view->is_in_display_list
        ? XX_WM_WINDOW_DISPLAY_STATE_WINDOW
        : 0
    ) | (
        icon_view->is_in_display_list
        ? XX_WM_WINDOW_DISPLAY_STATE_ICON
        : 0
    );
    enum xx_wm_window_display_state new_display_state = XX_WM_WINDOW_DISPLAY_STATE_MASKED;

    if (   is_window_withheld ||
         ! client_is_mapped
    ) {
        twc_display_list_remove(client_view);
    } else {
        twc_display_list_insert(client_view);
        new_display_state |= XX_WM_WINDOW_DISPLAY_STATE_WINDOW;
    }

    if (   is_icon_withheld ||
         ! icon_is_mapped
    ) {
        twc_display_list_remove(icon_view);
    } else {
        twc_display_list_insert(icon_view);
        new_display_state |= XX_WM_WINDOW_DISPLAY_STATE_ICON;
    }

    if ( new_display_state != old_display_state ) {
        xx_wm_window_update_display_state(
            twc_window,
            new_display_state
        );
    }

    return;
}

extern
void
twc_window_raise(
    struct twc_window *twc_window
) {
    twc_window_set_iconify_state(
        twc_window,
        false
    );

    struct twc_view *twc_view = twc_window->client_view;

    if ( twc_view->is_in_display_list ) {

        twc_display_list_remove(twc_view);

         twc_display_list_insert(twc_view);

          // Check for focus changes.
        twc_seat_recalc_pointer_focus();
    }

    return;
}

extern
void
twc_window_lower(
    struct twc_window *twc_window
) {
    struct twc_view *twc_view;
    if ( twc_window->is_iconified ) {
        twc_view = twc_window->icon_view;
    } else {
        twc_view = twc_window->client_view;
    }

    if ( twc_view->is_in_display_list ) {

        twc_display_list_remove(twc_view);

        twc_display_list_insert_last(twc_view);

          // Check for focus changes.
        twc_seat_recalc_pointer_focus();
    }

    return;
}

extern
void
twc_window_relocate_to_pointer(
    struct twc_window *twc_window
) {
    wl_fixed_t ptr_loc_fix_lx;
    wl_fixed_t ptr_loc_fix_ly;

    twc_seat_pointer_location(
        &ptr_loc_fix_lx,
        &ptr_loc_fix_ly
    );

    wld_coordinate_t lx = wl_fixed_to_int(ptr_loc_fix_lx);
    wld_coordinate_t ly = wl_fixed_to_int(ptr_loc_fix_ly);

    struct twc_view *client_view = twc_window->client_view;

    twc_view_update_location(
        client_view,
        lx - client_view->view_surface_vx - client_view->hotspot_sx,
        ly - client_view->view_surface_vy - client_view->hotspot_sy
    );

    twc_window_raise( twc_window );

    return;
}

extern
void
twc_window_initial_configure(
    struct wl_surface *wl_surface
) {
    struct twc_view   *client_view = wl_surface->twc_view;
    struct twc_window *twc_window  = client_view->twc_window;

      // The pre_initial_commit layout coordinates will be used.  When
      // there was no user config, the defaults are 0.
    client_view->layout_x = twc_window->pre_initial_commit_geometry.x;
    client_view->layout_y = twc_window->pre_initial_commit_geometry.y;

      // The pre_initial_commit sizes will be emitted below.  When
      // there is no user config, the emitted values default to 0.
      // Here, we preset the dimensions to 0.  That way, when the app
      // resizes due to the emit_configure, the window will properly
      // relocate in compliance with its gravity.
    client_view->width  = 0;
    client_view->height = 0;

    wld_dimension_t configured_width  = twc_window->pre_initial_commit_geometry.width;
    wld_dimension_t configured_height = twc_window->pre_initial_commit_geometry.height;

      // Reset default values
    twc_window->pre_initial_commit_geometry.x      = WLD_INVALID_COORD;
    twc_window->pre_initial_commit_geometry.y      = WLD_INVALID_COORD;
    twc_window->pre_initial_commit_geometry.width  = 0;
    twc_window->pre_initial_commit_geometry.height = 0;

    // Fix: add states

    xdg_toplevel_emit_configure(
      &client_view->view_surface->xdg_toplevel,
         configured_width,
         configured_height,
         NULL    //states
    );

    xdg_surface_emit_configure(
        &client_view->view_surface->xdg_surface
    );

    return;
}

extern
void
twc_window_apply_user_config(
    struct twc_window *twc_window
) {
    // Calculate the user configuration for a window based on app_id
    // and/or title.  Called when these attributes change.

    // Fix: check for
    //  initial output
    //  initial workspace (other than ALL)
    //  initial iconify state

    struct twc_view *client_view = twc_window->client_view;

    enum window_gravity window_gravity;
    char               *geometry_string = NULL;

      // check for gravity AND geometry
    bool window_has_gravity = (
        twc_config_lookup_AnchoredWindows(
             twc_window->app_id,
             twc_window->title,
            &window_gravity,
            &geometry_string
        )
    );

    bool is_gravity_user_set = false;

    // Geometry calculations depend on gravity, so apply gravity
    // first.
      // Gravity
    if ( window_has_gravity ) {
        is_gravity_user_set        = true;
        twc_window->window_gravity = window_gravity;
    }

      // Geometry
    if ( geometry_string != NULL ) {

        int          parse_x,     parse_y;
        unsigned int parse_width, parse_height;

        int parse_bits = (
            XParseGeometry(
                 geometry_string,
                &parse_x,
                &parse_y,
                &parse_width,
                &parse_height
            )
        );

        if ( ! (parse_bits & NoValue) ) {

            wld_dimension_t current_width  = client_view->view_surface->width;
            wld_dimension_t current_height = client_view->view_surface->height;

            struct srv_output *srv_output = (
                twc_view_applicable_output( client_view )
            );

            wld_dimension_t output_width, output_height;
            srv_output_dimensions(
                 srv_output,
                &output_width,
                &output_height
            );

              // Calculate the user specified geometry.
            wld_coordinate_t geom_x      = parse_bits & XValue      ? parse_x      : 0;
            wld_coordinate_t geom_y      = parse_bits & YValue      ? parse_y      : 0;
            wld_dimension_t  geom_width  = parse_bits & WidthValue  ? parse_width  : 0;
            wld_dimension_t  geom_height = parse_bits & HeightValue ? parse_height : 0;

            wld_coordinate_t output_right  = output_width  - 1;
            wld_coordinate_t output_bottom = output_height - 1;

            /*
            ** When the compositor supplies a new dimension, the
            ** client will resize.  Normally, the left side of the
            ** image stays fixed while the right side expands or
            ** contracts.  If the window has an easterly gravity, the
            ** right side is fixed while the left side moves.
            ** Similarly for the top/bottom and southerly gravity.
            **
            ** new_lx and new_ly are the coordinates for the fixed edges.
            */

              // In a geometry_string, +0 is different from -0.
              // XNegative differentiates this.
            wld_coordinate_t new_lx = (
                parse_bits & XNegative
                ? output_right + geom_x    // geom_x negative
                : 0            + geom_x    // geom_x positive
            );

            wld_coordinate_t new_ly = (
                parse_bits & YNegative
                ? output_bottom + geom_y
                : 0             + geom_y
            );

            bool has_initial_commit = (
                client_view->view_surface->xdg_surface.has_initial_commit
            );

            if ( has_initial_commit ) {

                if ( is_gravity_user_set ) {

                    /*
                    ** Reset the window geometry as if it had zero
                    ** dimension.  Then recalculate the layout using
                    ** the current dimensions together with the new
                    ** gravity.  This will relocate the window to a
                    ** place comensurate with its current dimensions
                    ** and geometry given location.  When the client
                    ** resizes to the user configured dimensions, the
                    ** window will, due to its gravity, relocate to
                    ** the user specified size and coordinates.
                    */

                    client_view->layout_x = new_lx;
                    client_view->layout_y = new_ly;
                    client_view->width    = 0;
                    client_view->height   = 0;

                    recalc_layout_coordinates(
                        client_view,
                        current_width,
                        current_height
                    );
                }

                if ( geom_width  != current_width  ||
                     geom_height != current_height
                ) {
                    // Tell the client to apply the user configured
                    // size.

                    xdg_toplevel_emit_configure(
                      &client_view->view_surface->xdg_toplevel,
                         geom_width,
                         geom_height,
                         NULL    //states
                    );

                    xdg_surface_emit_configure(
                        &client_view->view_surface->xdg_surface
                    );
                }

            } else {
                // The window will resize after the initial commit
                twc_window->pre_initial_commit_geometry.x      = new_lx;
                twc_window->pre_initial_commit_geometry.y      = new_ly;
                twc_window->pre_initial_commit_geometry.width  = geom_width;
                twc_window->pre_initial_commit_geometry.height = geom_height;
            }
        }
    }

      // OccupyAll
    bool window_occupy_all = (
         twc_config_lookup_OccupyAll(
             twc_window->app_id,
             twc_window->title
         )
    );
    if ( window_occupy_all ) {
        twc_window->wksp_occupancy = WKSP_OCCUPANCY_ALL;
    }

      // IconifyByMasking
    bool window_iconify_by_masking = (
           twc_config_lookup_IconifyByMasking(
             twc_window->app_id,
             twc_window->title
         )
    );
    if ( window_iconify_by_masking ) {
        twc_window->iconify_by_masking = true;
    }

      // HoldIcon
    bool window_hold_icon = (
           twc_config_lookup_HoldIcon(
             twc_window->app_id,
             twc_window->title
         )
    );
    if ( window_hold_icon ) {
        twc_window->hold_icon = true;
    }

      // StartIconified
    bool window_start_iconified = (
           twc_config_lookup_StartIconified(
             twc_window->app_id,
             twc_window->title
         )
    );
    if ( window_start_iconified ) {
        twc_window->is_iconified = true;
    }

      // DecorationPolicy
    enum twc_decoration_policy current_policy = (
        client_view->twc_decoration_policy
    );

    enum twc_decoration_policy new_policy;

    bool window_decoration_policy = (
        twc_config_lookup_DecorationPolicy(
             twc_window->app_id,
             twc_window->title,
            &new_policy
        )
    );

    if ( window_decoration_policy == true ) {

        if ( new_policy != current_policy ) {

            client_view->twc_decoration_policy = new_policy;

            struct zxdg_toplevel_decoration_v1 *xdg_toplevel_decoration = (
                &client_view->view_surface->xdg_toplevel.xdg_toplevel_decoration
            );

            enum decoration_mode current_preference = (
                xdg_toplevel_decoration->client_preference
            );

            enum decoration_mode last_response = (
                xdg_toplevel_decoration->response_to_client
            );

              // Recalculate response using new policy.  Client preference is
              // not changing, but the response might.
            enum decoration_mode response_to_client = (
                twc_update_decoration_mode(
                    client_view,
                    current_preference
                )
            );
            if ( response_to_client != last_response ) {

                xdg_toplevel_decoration->response_to_client = response_to_client;

                xdg_decoration_configure(
                    xdg_toplevel_decoration,
                    response_to_client
                );
            }
        }
    }

    return;
}

#if 0

    // ----------------------------
    // xdg_toplevel_wm_capabilities

        struct wl_array  capability_array;
        struct wl_array *capabilities = &capability_array;
        wl_array_init(   capabilities);
        enum xdg_toplevel_wm_capabilities *capability_0 = (
            wl_array_add( capabilities, sizeof(enum xdg_toplevel_wm_capabilities) )
        );
        *capability_0 = XDG_TOPLEVEL_WM_CAPABILITIES_MINIMIZE;

        xdg_toplevel_emit_wm_capabilities(
           &client_view->view_surface->xdg_toplevel,
            capabilities
        );

        wl_array_release(capabilities);

#endif



#if 0
 // Are things below here TWC_OPS???
 // Are things below here TWC_OPS???
 // Are things below here TWC_OPS???
 // Are things below here TWC_OPS???
 // Are things below here TWC_OPS???
 // Are things below here TWC_OPS???

// icon must be in conf file
// https://specifications.freedesktop.org/icon-theme-spec/icon-theme-spec-latest.html
extern
void
twc_window_add_icon(void){}

extern
void
twc_window_add_to_icon_mgr(void){}

extern
void
twc_window_map(void){}

extern
void
twc_window_iconize(void){}

extern
void
twc_window_unmap(void){}
#endif
