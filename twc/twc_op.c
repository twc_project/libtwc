
  // System
#include <stdbool.h>
#include <xkbcommon/xkbcommon.h>

  // Wayland
#include <wayland-util.h>  // wl_fixed

  // Protocols
#include <wm-agent-v1-client-protocol.h>   // XX_WM_WINDOW_INTRINSIC_...

  // libtwc_util
#include <twc/wayland_types.h>  // wld_<type>_t
#include <twc/twc_log.h>

  // APIs
#include "twc_op.h"                 // Our API to ensure consistancy
#include "wcf_shim.h"               // wcf_terminate
#include "twc_window.h"
#include "bic_server/wm_agent_event_wrappers.h" // xx_wm_interactive_...()

  // Imported data types
#include "twc_window-impl.h"

  // If the overlay surface is not annexed, there's no working
  // interactive agent to make select, track and drop requests.
static bool overlay_surface_available = false;

extern
void
twc_op_overlay_surface_annexed(
    bool annex_state
) {
    overlay_surface_available = annex_state;
}

  // A window intrinsic or agent action may start with the user
  // interactively selecting a window.  Record details of the user
  // request.
static bool intrinsic_in_progress = false;
static bool    action_in_progress = false;

static enum xx_wm_window_intrinsic    awaiting_window_intrinsic = XX_WM_WINDOW_INTRINSIC_NOP;
static enum wm_agent_base_v1_agent_id awaiting_agent_id         = WM_AGENT_BASE_V1_AGENT_ID_NONE;
static uint32_t                       awaiting_action_id        = UINT32_MAX;

 // ----
 // Move

static
void
twc_op_move_view(
    struct twc_view *twc_view
) {
    if ( ! overlay_surface_available ) { return; }

    awaiting_window_intrinsic = XX_WM_WINDOW_INTRINSIC_MOVE;

    twc_view_select(twc_view);

    twc_window_activate_overlay();
    xx_wm_interactive_track();

    return;
}

 // ------
 // Resize

extern
void
twc_op_resize_view(
    struct twc_view              *twc_view,
    enum xdg_toplevel_resize_edge resize_edges
) {
    if ( ! overlay_surface_available ) { return; }

    bool select_for_resize_success = (
        twc_view_select_for_resize(
            twc_view,
            resize_edges
        )
    );

    if ( ! select_for_resize_success ) {
        return;
    }

    awaiting_window_intrinsic = XX_WM_WINDOW_INTRINSIC_RESIZE;

    twc_window_activate_overlay();
    xx_wm_interactive_track();

    return;
}

 // ----------
 // Window Ops

extern
void
twc_action_window_intrinsic(
    struct twc_view            *twc_view,
    enum xx_wm_window_intrinsic xx_wm_window_intrinsic
) {
    if ( twc_view                  == NULL_TWC_VIEW              ||
         awaiting_window_intrinsic != XX_WM_WINDOW_INTRINSIC_NOP
    ) { return; }

    switch (xx_wm_window_intrinsic) {

        case XX_WM_WINDOW_INTRINSIC_MOVE:
            twc_op_move_view( twc_view );
        break;

        case XX_WM_WINDOW_INTRINSIC_RESIZE:
            twc_op_resize_view(
                twc_view,
                RESIZE_EDGE_NOT_PRE_SPECIFIED
            );
        break;

        case XX_WM_WINDOW_INTRINSIC_ICONIFY:
            twc_view_toggle_iconify( twc_view );
        break;

        case XX_WM_WINDOW_INTRINSIC_RAISE_LOWER:
            twc_view_raise_lower( twc_view );
        break;

        case XX_WM_WINDOW_INTRINSIC_VZOOM:
            twc_view_vzoom( twc_view );
        break;

        case XX_WM_WINDOW_INTRINSIC_CLOSE:
            twc_view_close( twc_view );
        break;

        default:
            // Fix: add more ops
        break;
    }

    return;
}

extern
void
twc_action_select_window_intrinsic(
    enum xx_wm_window_intrinsic xx_wm_window_intrinsic
) {
#   define XX_WM_WINDOW_INTRINSIC_LAST 16    // xdg_agent version 1

    if ( xx_wm_window_intrinsic      <= XX_WM_WINDOW_INTRINSIC_NOP ||
         XX_WM_WINDOW_INTRINSIC_LAST <  xx_wm_window_intrinsic     ||
         ! overlay_surface_available                               ||
         awaiting_window_intrinsic   != XX_WM_WINDOW_INTRINSIC_NOP
    ) { return; }

    intrinsic_in_progress     = true;
    awaiting_window_intrinsic = xx_wm_window_intrinsic;

    twc_window_activate_overlay();

    xx_wm_interactive_select();
}

extern
void
twc_action_apply_window_intrinsic(
    enum xx_wm_window_intrinsic xx_wm_window_intrinsic
) {
    struct twc_view *twc_view = twc_view_with_pointer();

    twc_action_window_intrinsic(twc_view, xx_wm_window_intrinsic);

    return;
}

 // -------------
 // Agent Actions

extern
void
twc_action_select_agent_action(
    enum wm_agent_base_v1_agent_id agent_id,
    uint32_t                       action_id
) {
    action_in_progress = true;

    awaiting_agent_id  = agent_id;
    awaiting_action_id = action_id;

    twc_window_activate_overlay();

    xx_wm_interactive_select();
}

 // --------------
 // Compositor Ops

extern
void
twc_op_compositor_op(
    enum xx_wm_menu_agent_compositor_op compositor_op
) {
    switch (compositor_op) {

        case XX_WM_MENU_AGENT_COMPOSITOR_OP_EXIT:
            wcf_terminate();
        break;

        default:
            // Fix: add more ops
        break;
    }

    return;
}

extern
void
twc_op_dismiss_agent(
    void
) {
    // Fix: ToDo

    return;
}

 // -----------
 // Interactive

extern
struct twc_view*
twc_op_select_at(
    wl_fixed_t layout_x,
    wl_fixed_t layout_y
) {
    /*
    ** A select operation is started by emitting an event to the
    ** xx_wm_interactive_agent as follows:
    **
    **   twc_window_activate_overlay();
    **   xx_wm_interactive_select()
    **
    ** The xx_wm_interactive_agent eventually calls
    **
    **   xx_wm_interactive_agent_select_at()
    **
    ** which invokes this routine.
    */

    struct twc_view *twc_view;

    if ( layout_x == WLD_INVALID_COORD ) {
        // Fix: can this happen??
        twc_view = (
            twc_view_with_pointer()
        );

    } else {
        twc_view = (
            twc_view_at(
                layout_x,
                layout_y
            )
        );
    }

    if (twc_view == NULL_TWC_VIEW) {
        twc_window_deactivate_overlay();
        awaiting_window_intrinsic = XX_WM_WINDOW_INTRINSIC_NOP;
        return NULL_TWC_VIEW;
    }

    if ( action_in_progress ) {

        action_in_progress = false;

        xx_wm_window_action (
            twc_view->twc_window,
            awaiting_agent_id,
            awaiting_action_id
        );
    }

    if ( intrinsic_in_progress ) {

        intrinsic_in_progress = false;

        switch ( awaiting_window_intrinsic ) {
            // Fix: add other states

            case XX_WM_WINDOW_INTRINSIC_MOVE:

                twc_view_select(twc_view);

                  // overlay must remain activated
                xx_wm_interactive_track();

                return twc_view;
            break;

            case XX_WM_WINDOW_INTRINSIC_RESIZE:

                bool select_for_resize_success = (
                    twc_view_select_for_resize(
                        twc_view,
                        RESIZE_EDGE_NOT_PRE_SPECIFIED
                    )
                );

                if ( ! select_for_resize_success ) {

                    twc_window_deactivate_overlay();
                    twc_view_unselect();

                    awaiting_window_intrinsic = XX_WM_WINDOW_INTRINSIC_NOP;

                    return NULL_TWC_VIEW;
                }

                  // overlay must remain activated
                xx_wm_interactive_track();

                return twc_view;
            break;

            case XX_WM_WINDOW_INTRINSIC_ICONIFY:
                twc_view_toggle_iconify(twc_view);
            break;

            default:
            break;
        }
    }

    twc_window_deactivate_overlay();

    awaiting_window_intrinsic = XX_WM_WINDOW_INTRINSIC_NOP;
    awaiting_agent_id         = WM_AGENT_BASE_V1_AGENT_ID_NONE;
    awaiting_action_id        = UINT32_MAX;

    return twc_view;
}

 // -----
 // Track

extern
void
twc_op_tracking_at(
    wl_fixed_t layout_x,
    wl_fixed_t layout_y
) {
    /*
    ** Tracking is used for moving and resizing windows. It is started
    ** by emitting an event to the xx_wm_interactive_agent as follows:
    **
    **   twc_window_activate_overlay();
    **   xx_wm_interactive_track()
    **
    ** The xx_wm_interactive_agent repeatedly calls
    **
    **   xx_wm_interactive_agent_tracking_at()
    **
    ** which invokes this routine.
    */

    switch (awaiting_window_intrinsic) {

        case XX_WM_WINDOW_INTRINSIC_MOVE:
            twc_view_relocate_selected( layout_x, layout_y );
        break;

        case XX_WM_WINDOW_INTRINSIC_RESIZE:
            twc_view_resize_selected( layout_x, layout_y );
        break;

        default:
            // Only the above ops requiring tracking
        break;
    }

    return;
}

 // ----
 // Drop

extern
void
twc_op_drop(
    void
) {
    /*
    ** When the xx_wm_interactive_agent is done interacting with the
    ** user, it calls
    **
    **   xx_wm_interactive_agent_drop()
    **
    ** which invokes this routine
    */

    twc_window_deactivate_overlay();
    twc_view_unselect();

    if ( awaiting_window_intrinsic == XX_WM_WINDOW_INTRINSIC_RESIZE ) {
        // Fix: emit configure event without RESIZE state.
    }

    awaiting_window_intrinsic = XX_WM_WINDOW_INTRINSIC_NOP;

    // Fix: call twc_seat_recalc_pointer_focus() in case window was
    // moved or resized?

    return;
}
