
  // Wayland
#include <wayland-util.h>   // wl_list

  // libtwc_util
#include <twc/wayland_types.h>      // wld_<type>_t
#include <twc/twc_log.h>

  // Protocols
#include <wm-agent-v1-client-protocol.h>  // XX_WM_WORKSPACE_ID_WKSP_NONE

  // bic_server APIs
#include "bic_server/wm_agent_event_wrappers.h"

  // APIs
#include "twc_surface.h"    // Our API to ensure consistancy
#include "twc_window.h"
#include "twc_seat.h"
#include "twc_decoration.h"
#include "srv_output.h"

  // Imported data types
#include "twc_window-impl.h"
#include "bic_server/wayland_bic.h"
#include "bic_server/xdg_decoration_bic.h"

static
void
initial_icon_placement(
    struct wl_surface *wl_surface
) {
    // See client placement below.

    struct twc_view   *twc_view    = wl_surface->twc_view;
    struct twc_window *twc_window  =   twc_view->twc_window;
    struct twc_view   *icon_view   = twc_window->icon_view;

    if (( icon_view->layout_x != WLD_INVALID_COORD) ||
        ( icon_view->layout_y != WLD_INVALID_COORD)
    ) { return; }

    struct srv_output *srv_output = (
        twc_seat_output_with_pointer_focus()
    );

    wld_dimension_t  output_width;
    wld_dimension_t  output_height;
    wld_coordinate_t next_icon_x;
    wld_coordinate_t next_icon_y;

    srv_output_dimensions(
        srv_output,
       &output_width,
       &output_height
    );

    srv_output_next_icon_coord(
        srv_output,
       &next_icon_x,
       &next_icon_y
    );

    wld_coordinate_t new_icon_x = (
        twc_view_horizontal_fit(
            twc_view,
            next_icon_x,
            output_width
        )
    );

    wld_coordinate_t next_y = output_height - 100;

    twc_view_update_location(
        icon_view,
         new_icon_x,
        next_y
    );

    //LOG_INFO("initial_icon_placement %d %d", new_icon_x, next_y);

#   define ICON_HORIZ_PITCH 170

    next_icon_x = (new_icon_x + ICON_HORIZ_PITCH) % output_width;

    srv_output_set_next_icon_coord(
        srv_output,
        next_icon_x < 100 ? 100 : next_icon_x,
        next_icon_y
    );

    return;
}

static
void
initial_client_placement(
    struct wl_surface *wl_surface
) {
    /*
    ** Choose an output for the window origin.  This output also
    ** determines the initial workspace.  The pointer focus uniquely
    ** determines an output, so is a good candidate(*).
    **
    ** Choose an origin within that output.
    **
    ** Check if the surface will spillover the output.  If so, move
    ** origin up and left until it all fits.  If it can't, let it
    ** spillover.
    **
    ** (*) Since a surface may straddle outputs and may be in multiple
    **     workspaces, the surface with keyboard or pointer focus does
    **     not uniquely determine an output nor a workspace.
    **     Therefore, focus surfaces make poor candidates for
    **     determining a workspace.
    */

    struct twc_view   *twc_view    = wl_surface->twc_view;
    struct twc_window *twc_window  =   twc_view->twc_window;
    struct twc_view   *client_view = twc_window->client_view;

    /*
    LOG_INFO(
        "initial_client_placement %-20s %d %d %d %d",
        twc_window->title,
        wl_surface->type,
        wl_surface->committed_image_type,
        wl_surface->width,
        wl_surface->height
    );
    */

      // Some windows already have an occupancy based on config files.
    if ( twc_window->wksp_occupancy == XX_WM_WORKSPACE_ID_WKSP_NONE ) {

          // Fix: Is this the only way to default the initial workspace?
        struct twc_workspace* workspace = (
            twc_seat_workspace_with_pointer_focus()
        );

        twc_window->wksp_occupancy = (
            WKSP_OCCUPANCY_ADD(
                XX_WM_WORKSPACE_ID_WKSP_NONE,
                twc_workspace_id(workspace)
            )
        );

        xx_wm_window_update_occupancy(
            twc_window
        );
    }

    struct srv_output *srv_output = (
        twc_seat_output_with_pointer_focus()
    );

      // Some windows already have a location based on config files or
      // on Xwayland DropDown/Popup status.
    if ( client_view->layout_x == WLD_INVALID_COORD ||
         client_view->layout_y == WLD_INVALID_COORD
    ) {
        wld_dimension_t  output_width;
        wld_dimension_t  output_height;
        wld_coordinate_t next_window_x;
        wld_coordinate_t next_window_y;

        srv_output_dimensions(
            srv_output,
           &output_width,
           &output_height
        );

        srv_output_next_window_coord(
            srv_output,
           &next_window_x,
           &next_window_y
        );

        wld_coordinate_t new_window_x = (
            twc_view_horizontal_fit(
                twc_view,
                next_window_x,
                output_width
            )
        );

        wld_coordinate_t new_window_y = (
            twc_view_vertical_fit(
                twc_view,
                next_window_y,
                output_height
            )
        );

        twc_view_update_location(
            client_view,
            new_window_x,
            new_window_y
        );

        //LOG_INFO("initial_client_placement location %4d %4d %s", next_window_x, next_window_y, twc_window->app_id);

#       define CLIENT_HORIZ_INITIAL  50
#       define CLIENT_HORIZ_PITCH   100
#       define CLIENT_VERT_INITIAL   50
#       define CLIENT_VERT_PITCH     50

        // Fix: should the increments below be configurable?
        // Increment to the next position and wrap around the
        // output.

        srv_output_set_next_window_coord(
            srv_output,

            ( new_window_x == next_window_x
              ? (new_window_x + CLIENT_HORIZ_PITCH) % output_width
              : CLIENT_HORIZ_INITIAL
            ),

            (  new_window_y == next_window_y
              ? (new_window_y + CLIENT_VERT_PITCH) % output_height
              : CLIENT_VERT_INITIAL
            )
        );

    } else {
        // The client already had layout coordinates

        twc_view_update_location(
            client_view,
            client_view->layout_x,
            client_view->layout_y
        );
    }

    xx_wm_window_update_enter_output(
        twc_window,
        srv_output
    );

    return;
}

 /*
 ** Suppose some portion of a surface is visible on an output.  The
 ** extent of the surface may reach beyond the edges of the output.
 ** This is called an overflow surface.  This can occur if the surface
 ** is placed close to the egde or if the surface is simply larger
 ** than the output.
 **
 ** A surface commit may include a new buffer.  The new buffer may
 ** have different dimensions than the previous buffer.  On any change
 ** in dimension, the keyboard/pointer focus may need updating.  Note
 ** that a commit can cause a non-empty surface to become empty or
 ** vice-versa.
 **
 ** When commiting a buffer, there are three situations:
 **
 **   1. The buffer size does not change.
 **
 **      No action is required.  An empty surface will remain empty.
 **
 **   2. The surface has invalid coordinates (new surface):
 **
 **      The surface will be empty.
 **
 **      If the new buffer is empty, the surface remains empty and no
 **      coordintes are assigned.
 **
 **      Otherwise, the surface becomes non-empty and will be assigned
 **      coordinates.
 **
 **   3. The surface has valid coordinates:
 **
 **      The surface is non-empty.
 **
 **      If the committed buffer is empty, the surface becomes empty
 **      and its coordinates are set to invalid.
 **
 **      If the committed buffer is non-empty, the surface remains
 **      non-empty.  New coordinates are not assigned.  The result may
 **      be an overflow surface.
 **
 ** Note that coordinates are only assigned when their pre-commit
 ** values are invalid, e.g., a new surface.  Depending on
 ** configuration, TWC may attempt to place the surface so that it is
 ** completely contained within on one output, ie, doesn't overflow.
 **
 ** Well behaved clients probably change buffer dimension only on
 ** initial buffer commit or when asked by the compositor.  (See
 ** xdg_toplevel.configure)
 */

 /*
 ** Life cycle of the "mapped" condition for xdg_surfaces.
 **
 ** From the protocol file xdg-shell.xml:
 **
 ** The "state" of an xdg_surface is a collection of "enum state"
 ** values.  For some states there is a "set" and an "unset" request
 ** for the client to use, eg, set_fullscreen().  Other states are
 ** controlled by the server using the xdg_toplevel.configure event.
 **
 ** In addition, a client may assign attributes to xdg_surfaces.
 ** E.g., window_geometry, title, max_size ...
 **
 ** The following table show the steps to create a mapped surface.
 **
 **     -> denotes request from client to   server
 **     <- denotes event   to   client from server
 **
 ** --  --  ----------------
 **     ->  create_surface          wl_surface
 **
 **     ->  get_xdg_surface         must not have buffer
 **
 ** 1   ->  get_toplevel            create surface role
 **
 ** 2a  ->  xdg_toplevel_set_<state>    eg, xdg_toplevel_set_minimized
 **                                     these are requests, not demands
 **
 ** 2b  ->  wl_surface_commit       with no buffer
 **
 **     <-  xdg_toplevel::configure     server sets toplevel "state"
 **
 **     <-  xdg_surface.configure       server has completed initial commit
 **     ->  xdg_surface.ack_configure
 **
 ** 3a  ->  wl_surface_attach       attach buffer
 ** 3b  ->  wl_surface_commit       with buffer
 **
 ** The surface is now "mapped".  If the first buffer attach is a non
 ** empty buffer, the surface dimension will change.
 **
 ** Two ways to "unmap"
 **     commit a null buffer (NULL, not an empty buffer)
 **     destroy role
 */

extern
void
twc_surface_becomes_mapped(
    struct wl_surface *wl_surface
) {
    if ( ! SURFACE_HAS_ROLE(wl_surface) ) { return; }

    wl_surface->is_mapped = true;

    switch (wl_surface->type ) {
        case ST_DND_ICON:
        case ST_CURSOR:
        case ST_SUB_SURFACE:
            return;
        break;

        default:
        break;
    }

    struct twc_view   *twc_view   = wl_surface->twc_view;
    struct twc_window *twc_window = twc_view->twc_window;

    if ( SURFACE_IS_WINDOW(wl_surface) ) {

        /*
        LOG_INFO("twc_surface_becomes_mapped %3d %4d %s %s",
            wl_surface->count,
            wl_surface->width,
            wl_surface->twc_view->twc_window->app_id,
            wl_surface->twc_view->twc_window->title
        );
        */

        twc_window_update_surface_size(wl_surface);

        initial_client_placement(wl_surface);

        twc_window_update_display_list(twc_window);

    } else if ( wl_surface->type == ST_XX_WM_DECO      ) {
    } else if ( wl_surface->type == ST_XX_WM_WALLPAPER ) {
    } else if ( wl_surface->type == ST_XX_WM_ICON      ) {

        twc_window_update_display_list(twc_window);

        initial_icon_placement(wl_surface);

        // Fix: ???  should this be here??
        //twc_window_update_display_list(twc_window);
    }

    return;
}

extern
void
twc_surface_no_longer_mapped(
    struct wl_surface *wl_surface
) {
    // Fix: what happens on unmapping?
    //
    // Documentation in the wayland XML file says a surface will
    // "forget its position and z-order"
    //
    // Documentation in the xdg_shell XML file says (for xdg roles)
    // "all attributes (e.g. title, state, stacking, ...)  are discarded"

    wl_surface->is_mapped = false;

    if ( ! SURFACE_HAS_ROLE(wl_surface) ) {
        return;
    }

      // Remapping requires an Initial Commit
    wl_surface->xdg_surface.has_initial_commit = false;

    struct twc_view *twc_view = wl_surface->twc_view;

    switch (wl_surface->type ) {
        case ST_DND_ICON:
        case ST_CURSOR:
        case ST_SUB_SURFACE:
        case ST_XDG_POPUP:
            return;
        break;

        case ST_XX_WM_DECO:
            switch ( wl_surface->xx_wm_decoration_edge ) {

                case XX_WM_DECORATION_EDGE_TOP:
                    twc_view->deco_top_vx = WLD_INVALID_COORD;
                    twc_view->deco_top_vy = WLD_INVALID_COORD;
                break;
                case XX_WM_DECORATION_EDGE_BOTTOM:
                    twc_view->deco_bottom_vx = WLD_INVALID_COORD;
                    twc_view->deco_bottom_vy = WLD_INVALID_COORD;
                break;
                case XX_WM_DECORATION_EDGE_LEFT:
                    twc_view->deco_left_vx = WLD_INVALID_COORD;
                    twc_view->deco_left_vy = WLD_INVALID_COORD;
                break;
                case XX_WM_DECORATION_EDGE_RIGHT:
                    twc_view->deco_right_vx = WLD_INVALID_COORD;
                    twc_view->deco_right_vy = WLD_INVALID_COORD;
                break;
                default:
                break;
            }

            return;
        break;

        default:
        break;
    }

    // surface is ST_XX_WM_ICON or ST_XDG_TOPLEVEL.

    struct twc_window *twc_window = twc_view->twc_window;

    if ( SURFACE_IS_WINDOW(wl_surface) ) {
        twc_window->window_configured = WC_no_configuration;

        // Fix: ST_XDG_TOPLEVEL should forget all properties.
        /*
         twc_view_update_location(
            twc_view,
            0,0
        );
        */
    }

    twc_window_update_display_list(twc_window);

    twc_seat_surface_remove_all_focus(wl_surface);

    return;
}
