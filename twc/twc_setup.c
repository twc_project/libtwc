
  // Wayland
#include <wayland-util.h>       // wl_list

  // libtwc_util
#include <twc/twc_log.h>

  // APIs
#include "twc_server.h"     // Our API to ensure consistancy
#include "wcf_shim.h"
#include "twc_seat.h"
#include "twc_window.h"
#include "twc_service.h"
#include "wcf_client_initialize.h"
#include "twc_workspace.h"
#include "bic_server.h"

  // Imported data types
#include "twc_state-impl.h"
#include "twc_config_toml.h"

struct twc_state TWC;

extern
struct twc_config_data
twc_setup(
    char *config_file
) {
    struct twc_config_data twc_config_data;

    log_init();

    bic_server_setup();

    wl_list_init(TWC_workspace_list);

    wl_list_init(TWC_window_list);
    wl_list_init(TWC_display_list);

    wl_list_init(TWC_workspace_interest_list);
    wl_list_init(TWC_window_interest_list);

    twc_workspace_initialize( &TWC );

    twc_service_init();

    twc_config_read_file( config_file, &TWC );

    TWC.default_decoration_policy = twc_config_lookup_DefaultDecorationPolicy();
    TWC.shortcut_modifiers        = twc_config_lookup_DefaultDecorationPolicy();

    twc_seat_initialize();

      // This routine depends on above initializations
    wcf_initialize();

    twc_window_initialize();

    twc_config_data.service_table = TWC.service_table;
    twc_config_data.  agent_table = TWC.  agent_table;

    bool wcf_running;
    wcf_client_initialize(
        &wcf_running,
        &twc_config_data
    );

    return twc_config_data;
}

extern
void
twc_run(
    void
) {
    wcf_start();
}
