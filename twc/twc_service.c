
  // System
#include <string.h>     // strncmp, strncpy

  // Protocols
#include <wm-agent-v1-client-protocol.h>

  // courtesy_librarys
#include <twc/twc_log.h>

  // APIs
#include "twc_service.h"        // Our API to ensure consistancy
#include "twc_config_toml.h"

  // Imported data types
#include "bic_server/wm_service_bic.h"  // MAX_SERVICE_NAME_LENGTH

typedef uint32_t service_index_t;
#define NULL_SERVICE_INDEX (UINT32_MAX)

#define SERVICE_COUNT     32
struct service {
      // Many windows may implemnent the same service.  Typically, the
      // windows for any given service will have complementary
      // contexts, ie, workspace/output.  A window may implement only
      // one service.
    struct wl_list window_list; // windows implementing this service.

    bool in_use;

    enum xx_zwm_service_assert_method         assert_method;
    enum xx_zwm_service_stand_down_method stand_down_method;

    char provider_name[MAX_PROVIDER_NAME_LENGTH];
    char  service_name[MAX_SERVICE_NAME_LENGTH];

} service_array[SERVICE_COUNT];

extern
void
twc_service_init(
    void
) {
    for( int i = 0; i < SERVICE_COUNT; i++ ) {
        wl_list_init(&service_array[i].window_list);

        service_array[i].in_use = false;

        service_array[i].    assert_method = XX_ZWM_SERVICE_ASSERT_METHOD_NOP;
        service_array[i].stand_down_method = XX_ZWM_SERVICE_STAND_DOWN_METHOD_NOP;

        service_array[i].provider_name[0] = '\0';
        service_array[i]. service_name[0] = '\0';
    }

    return;
}

static
service_index_t
service_allocate(
    const char *provider_name,
    const char *service_name
) {
    for( int i = 0; i < SERVICE_COUNT; i++ ) {

        if ( ! service_array[i].in_use ) {

            service_array[i].in_use = true;

            strncpy(service_array[i].provider_name, provider_name, MAX_PROVIDER_NAME_LENGTH);
            service_array[i].service_name[MAX_PROVIDER_NAME_LENGTH - 1] = '\0';

            strncpy(service_array[i].service_name, service_name, MAX_SERVICE_NAME_LENGTH);
            service_array[i].service_name[MAX_SERVICE_NAME_LENGTH - 1] = '\0';

            return i;
        }
    }

    return NULL_SERVICE_INDEX;
}

static
service_index_t
service_lookup(
    const char *provider_name,
    const char *service_name
) {
    service_index_t service_index = NULL_SERVICE_INDEX;

    for( int i = 0; i < SERVICE_COUNT; i++ ) {

        int cmp = (
            strncmp(
                provider_name,
                service_array[i].provider_name,
                MAX_PROVIDER_NAME_LENGTH
            )
        );

        if ( cmp == 0 ) {

            cmp = (
                strncmp(
                    service_name,
                    service_array[i].service_name,
                        MAX_SERVICE_NAME_LENGTH
                )
            );

            if ( cmp == 0 ) {
                service_index = i;
            }
        }
    }

    return service_index;
}

#if 0
static
void
print_service_tab(
    void
) {
    for( int i = 0; i < SERVICE_COUNT; i++ ) {
        if ( service_array[i].in_use ) {
            LOG_INFO("index %2d %20s %20s",
                i,
                service_array[i].provider_name,
                service_array[i].service_name
            );

        }
    }
}
#endif

extern
void
twc_service_set_methods(
    const char                           *provider_name,
    const char                           *service_name,
    enum xx_zwm_service_assert_method     assert_method,
    enum xx_zwm_service_stand_down_method stand_down_method
) {
    service_index_t service_id = (
        service_lookup(
            provider_name,
            service_name
        )
    );

    if (service_id == NULL_SERVICE_INDEX) {
        service_id  = (
            service_allocate(
                provider_name,
                service_name
            )
        );

        if ( service_id == NULL_SERVICE_INDEX ) { return; }
    }

    service_array[service_id].    assert_method =     assert_method;
    service_array[service_id].stand_down_method = stand_down_method;

    //print_service_tab();

    return;
}

extern
void
twc_service_get_methods(
    const char                 *provider_name,
    const char                 *service_name,
    struct twc_service_methods *twc_service_methods
) {
    service_index_t service_index = (
        service_lookup(
            provider_name,
            service_name
        )
    );

    if ( service_index == NULL_SERVICE_INDEX ) { return; }

    twc_service_methods->assert_method     = service_array[service_index].    assert_method;
    twc_service_methods->stand_down_method = service_array[service_index].stand_down_method;

    return;
}
