
  // System
#include <stdbool.h>
#include <stdint.h>     // uint32_t
#include <limits.h>     // CHAR_BIT

  // Wayland
#include <wayland-util.h>  // wl_list

  // APIs
#include "twc_workspace.h"              // Our API to ensure consistancy
#include "twc_workspace-impl.h"         // Our API to ensure consistancy
#include "bic_server/wm_agent_event.h"  // xx_wm_workspace_registry_emit_new()

  // Imported data types
#include "twc_state-impl.h"
#include "bic_server/bic_connection.h"

#define MAX_WORKSPACE_COUNT (sizeof(wksp_occupancy_t) * CHAR_BIT)

static
struct twc_workspace workspace_table[MAX_WORKSPACE_COUNT];

#define twc_workspace_list_insert(w) \
    wl_list_insert(TWC_workspace_list, &((w)->link))

#define twc_workspace_list_remove(w) \
    wl_list_remove(&((w)->link))

extern
void
twc_workspace_initialize(
    struct twc_state *twc
) {
    struct twc_workspace *default_workspace = &workspace_table[0];

    default_workspace->index = 0;
    default_workspace->id    = 0x1 << 0;

    wl_list_init( &default_workspace->xx_wm_workspace_list );

      // Make sure there is at least one workspace.
    twc_workspace_list_insert( default_workspace );

    twc->default_workspace = default_workspace;

    return;
}

extern
void
twc_workspace_new(
    char *workspace_name
) {
    static uint32_t wksp_count = 0;

    if ( MAX_WORKSPACE_COUNT <= wksp_count ) { return; }

    struct twc_workspace *twc_workspace = (
        &workspace_table[wksp_count]
    );

    if ( wksp_count == 0 ) {
        // workspace 0 has already been created.

    } else {

        twc_workspace->index = wksp_count;
        twc_workspace->id    = 0x1 << wksp_count;

        wl_list_init( &twc_workspace->xx_wm_workspace_list );

        twc_workspace_list_insert( &workspace_table[wksp_count] );
    }

     twc_workspace->name = workspace_name;

    wksp_count++;

      // Notify clients that are interested in workspaces
    struct bic_connection *bic_connection;
    twc_workspace_interest_list_for_each(bic_connection) {

        xx_wm_workspace_registry_emit_new(
            &bic_connection->wm_agent_base_v1.xx_wm_workspace_registry,
            twc_workspace->id,
            0
        );
    }

    return;
}

extern
void
twc_workspace_swap(
    struct twc_workspace *twc_workspace_a,
    struct twc_workspace *twc_workspace_b
) {
    // Fix: ToDo

    /* Exchange places in the twc_workspace_list.  Affects ordering
    ** for wksp_cycle_forward etc.
    */

}

extern
uint32_t
twc_workspace_index(
    struct twc_workspace *twc_workspace
) {
    return twc_workspace->index;
}

extern
enum xx_wm_workspace_id
twc_workspace_id(
    struct twc_workspace *twc_workspace
) {
    return twc_workspace->id;
}

extern
struct twc_workspace*
twc_workspace_numeric_name_lookup(
    uint32_t    numeric_name
) {
    struct twc_workspace *twc_workspace;
    twc_workspace_list_for_each(twc_workspace) {

        if ( twc_workspace->id == numeric_name ) {
            return twc_workspace;
        }

    }

    return NULL;
}

extern
struct twc_workspace*
twc_workspace_default(
    void
) {
    return TWC.default_workspace;
}
