#define _POSIX_C_SOURCE 200809L     // strnlen

  // System
#include <stdio.h>      // strncmp, snprintf, fopen, errno
#include <string.h>     // strncmp, strerror
#include <dlfcn.h>      // dlopen, dlclose, dlerror
#include <stdbool.h>

  // Protocols
#include <wm-agent-v1-client-protocol.h>

  // libtwc_util
#include <twc/toml.h>
#include <twc/toml_iterators.h>
#include <twc/twc_log.h>

  // APIs
#include "twc_config_toml.h"    // Our API to ensure consistancy
#include "twc_server.h"         // Our API to ensure consistancy (twc_load_plugins)
#include "twc_workspace.h"
#include "twc_client_initialize.h"
#include "twc_decoration.h"
#include "twc_seat.h"
#include "twc_service.h"
#include "srv_keyboard.h"

  // Imported data types
#include "twc_state-impl.h"

  // When no default is configured
#define TWC_DEFAULT_DECORATION_POLICY DP_Always_SSD

bool FocusFollowsPointer;

bool ClickToRaise;
bool AutoRaise;

bool DontMoveOff;
bool StaddleOutputs;
bool RaiseOnMove;
bool RaiseOnResize;

char *DefaultDecorationPolicy;
char *KeyBoardShortCutMods = NULL;

static toml_array_t *                  Plugins_array = NULL;
static toml_array_t *                 HoldIcon_array = NULL;
static toml_array_t *                OccupyAll_array = NULL;
static toml_array_t *                WorkSpace_array = NULL;
static toml_array_t *                AlwaysSSD_array = NULL;
static toml_array_t *               PluginPath_array = NULL;
static toml_array_t *              XKBKeyBoard_array = NULL;
static toml_array_t *            ButtonMapping_array = NULL;
static toml_array_t *            NeverDecorate_array = NULL;
static toml_array_t *           StartIconified_array = NULL;
static toml_array_t *           LibinputConfig_array = NULL;
static toml_array_t *          AnchoredWindows_array = NULL;
static toml_array_t *         IconifyByMasking_array = NULL;
static toml_array_t *        AllowCSD_else_SSD_array = NULL;
static toml_array_t *     KeyBoardShortCutKeys_array = NULL;
static toml_array_t *AllowCSD_else_No_Decorate_array = NULL;

static toml_table_t *DesktopService_table = NULL;
static toml_table_t *         Agent_table = NULL;

  // Start Agents
int MoveDelta;

static toml_array_t *Cursor_array = NULL;

  // End Agents

#define KEY_EQ_STRING(k,s) ( strncmp((k), (s), sizeof((s)) ) == 0 )

static
bool
window_is_listed(
    toml_array_t *array,
    char         *app_id,
    char         *title
) {
    if ( array == NULL ) { return false; }

    /*
    ** This routine checks if a window "matches" some (inline) table
    ** of the array.
    **
    ** The array must be a collection of inline tables.  The following
    ** 'NeverDecorate' policy array is an example with two inline
    ** tables.
    **
    ** NeverDecorate = [
    **     { App_Id = "Workspace", Title = "Switcher" },
    **     { App_Id = "TaskIconMgr" },
    ** ]
    **
    ** Each inline table must have at least one of these
    ** two keys:  "App_Id"    "Title"
    **
    ** If the arguments match all keys that are present, a match is
    ** generated.
    */

    FOR_EACH_TABLE_IN_ARRAY( array, member_table ) {
        if ( member_table == NULL ) { continue; }

        int   key_count = 0;
        int match_count = 0;

        const char *key;
        FOR_EACH_KEY_IN_TABLE( member_table, key ) {
            toml_datum_t d = toml_string_in( member_table, key );
            if (d.ok) {
                if ( KEY_EQ_STRING(key, "App_Id") ) {

                    key_count++;
                    char *conf_app_id = d.u.s;

                    if ( strncmp(conf_app_id, app_id, MAX_APP_ID_LENGTH) ==   0 ||
                         conf_app_id[0]                                  == '\0'
                    ) {
                        match_count++;
                    }
                }
                if ( KEY_EQ_STRING(key, "Title") ) {

                    key_count++;
                    char *conf_title = d.u.s;

                    if ( strncmp(conf_title, title, MAX_TITLE_LENGTH) ==   0 ||
                         conf_title[0]                                == '\0'
                    ) {
                        match_count++;
                    }
                }
            }
        }
        if ( match_count != 0         &&
             match_count == key_count
        ) {
            return true;
        }
    }

    return false;
}

extern
bool
twc_config_lookup_OccupyAll(
    char *app_id,
    char *title
) {
    return (
        window_is_listed(
            OccupyAll_array,
            app_id,
            title
        )
    );
}

extern
bool
twc_config_lookup_IconifyByMasking(
    char *app_id,
    char *title
) {
    return (
        window_is_listed(
            IconifyByMasking_array,
            app_id,
            title
        )
    );
}

extern
bool
twc_config_lookup_HoldIcon(
    char *app_id,
    char *title
) {
    return (
        window_is_listed(
            HoldIcon_array,
            app_id,
            title
        )
    );
}

extern
bool
twc_config_lookup_StartIconified(
    char *app_id,
    char *title
) {
    return (
        window_is_listed(
            StartIconified_array,
            app_id,
            title
        )
    );
}

extern
bool
twc_config_lookup_AnchoredWindows(
    char                *app_id,
    char                *title,
    enum window_gravity *window_gravity,
    char               **geometry_string
) {
    *window_gravity  = WG_NorthWest;
    *geometry_string = NULL;

    bool has_gravity = false;
    *geometry_string = NULL;

    if ( AnchoredWindows_array == NULL ) { return has_gravity; }

    FOR_EACH_TABLE_IN_ARRAY(AnchoredWindows_array, member_table) {
        if ( member_table == NULL ) { continue; }

        int   key_count = 0;
        int match_count = 0;

        char * gravity_str = NULL;
        char *geometry_str = NULL;

        const char *key;
        FOR_EACH_KEY_IN_TABLE( member_table, key ) {

            toml_datum_t d;

            d = toml_string_in( member_table, key );
            if (d.ok) {
                if ( KEY_EQ_STRING(key, "Gravity" ) )  gravity_str = d.u.s;
                if ( KEY_EQ_STRING(key, "Geometry") ) geometry_str = d.u.s;

                if ( KEY_EQ_STRING(key, "App_Id"  ) ) {

                    key_count++;
                    char *conf_app_id = d.u.s;

                    if ( strncmp(conf_app_id, app_id, MAX_APP_ID_LENGTH) ==   0 ||
                         conf_app_id[0]                                  == '\0'
                    ) {
                        match_count++;
                    }
                }
                if ( KEY_EQ_STRING(key, "Title") ) {

                    key_count++;
                    char *conf_title = d.u.s;

                    if ( strncmp(conf_title, title, MAX_TITLE_LENGTH) ==   0 ||
                         conf_title[0]                                == '\0'
                    ) {
                        match_count++;
                    }
                }
            }
        }
        if ( match_count != 0         &&
             match_count == key_count
        ) {
            *geometry_string = geometry_str;

            if ( gravity_str != NULL ) {

                enum window_gravity gravity;
                has_gravity = true;

                switch ( gravity_str[0] ) {

                    case 'N':
                        switch( gravity_str[5] ) {
                            case 'W': gravity = WG_NorthWest; break;
                            case 'E': gravity = WG_NorthEast; break;
                            default:  gravity = WG_North;     break;
                        }
                    break;

                    case 'E': gravity = WG_East; break;
                    case 'W': gravity = WG_West; break;

                    case 'S':
                        switch( gravity_str[5] ) {
                            case 'W': gravity = WG_SouthWest; break;
                            case 'E': gravity = WG_SouthEast; break;
                            default:  gravity = WG_South;     break;
                        }
                    break;

                    case 'C': gravity = WG_Center;  break;

                    default: has_gravity = false;   break;
                }

                if ( has_gravity ) {
                    *window_gravity = gravity;
                }
            }

            return has_gravity;
        }
    }

    return has_gravity;
}

extern
void
twc_config_lookup_XKBKeyBoard(
    char **XKBRules,
    char **XKBModel,
    char **XKBLayout,
    char **XKBVariant,
    char **XKBOptions
) {
    *XKBRules   = NULL;
    *XKBModel   = NULL;
    *XKBLayout  = NULL;
    *XKBVariant = NULL;
    *XKBOptions = NULL;

    if ( XKBKeyBoard_array == NULL ) { return; }

    FOR_EACH_TABLE_IN_ARRAY(XKBKeyBoard_array, member_table) {
        if ( member_table == NULL ) { continue; }

        const char *key;
        FOR_EACH_KEY_IN_TABLE( member_table, key ) {
            toml_datum_t d = toml_string_in( member_table, key );
            if (d.ok) {
                if ( KEY_EQ_STRING(key, "XKBRules"  ) ) *XKBRules   = d.u.s;
                if ( KEY_EQ_STRING(key, "XKBModel"  ) ) *XKBModel   = d.u.s;
                if ( KEY_EQ_STRING(key, "XKBLayout" ) ) *XKBLayout  = d.u.s;
                if ( KEY_EQ_STRING(key, "XKBVariant") ) *XKBVariant = d.u.s;
                if ( KEY_EQ_STRING(key, "XKBOptions") ) *XKBOptions = d.u.s;
                continue;
            }
        }
    }

    return;
}

extern
xkb_mod_mask_t
twc_config_lookup_KeyBoardShortCutMods(
    void
) {
    if ( KeyBoardShortCutMods == NULL) { return TWC_DEFAULT_SHORTCUT_MODS; }

    xkb_mod_mask_t shortcut_mask = 0;

    if ( strstr(KeyBoardShortCutMods, "CTRL" ) != NULL ) { shortcut_mask |= KB_MOD_CTRL;  }
    if ( strstr(KeyBoardShortCutMods, "ALT"  ) != NULL ) { shortcut_mask |= KB_MOD_ALT;   }
    if ( strstr(KeyBoardShortCutMods, "MOD3" ) != NULL ) { shortcut_mask |= KB_MOD_MOD3;  }
    if ( strstr(KeyBoardShortCutMods, "SUPER") != NULL ) { shortcut_mask |= KB_MOD_LOGO;  }
    if ( strstr(KeyBoardShortCutMods, "MOD5" ) != NULL ) { shortcut_mask |= KB_MOD_MOD5;  }

    if ( shortcut_mask == 0 ) shortcut_mask = TWC_DEFAULT_SHORTCUT_MODS;

    return shortcut_mask;
}

 /*
 ** Key values for event codes are listed in
 **
 **   /usr/include/linux/input-event-codes.h
 **
 ** The key values of interest lie between 0 and 88.
 **
 ** The configuration names for shortcut keys fall into 3 categories:
 **
 **   1) names which are a single character.  IOW, a letter, a number
 **      or a punctuation char.  Eg, 'j' or '4' or ';' etc
 **
 **   2) names of the form Fx or Fxx.  Eg, F1 or F11 etc
 **
 **   3) the words ESCAPE BACKSPACE TAB SPACE
 */

static
uint32_t
shortcut_key(
    const char *key_config_name
) {
#   define BACKSPACE_LEN 9
    size_t key_name_len = strnlen(key_config_name, BACKSPACE_LEN);

    uint32_t key = 0;

    if ( key_name_len == 1 ) {

          // map select ascii chars to event codes
        switch ( key_config_name[0] ) {
            //   KEY_RESERVED     0
            //   KEY_ESC          1
            case '1':  key =      2; break;
            case '2':  key =      3; break;
            case '3':  key =      4; break;
            case '4':  key =      5; break;
            case '5':  key =      6; break;
            case '6':  key =      7; break;
            case '7':  key =      8; break;
            case '8':  key =      9; break;
            case '9':  key =     10; break;
            case '0':  key =     11; break;
            case '-':  key =     12; break;
            case '=':  key =     13; break;
            //    KEY_BACKSPACE  14
            //    KEY_TAB        15
            case 'q':  key =     16; break;
            case 'w':  key =     17; break;
            case 'e':  key =     18; break;
            case 'r':  key =     19; break;
            case 't':  key =     20; break;
            case 'y':  key =     21; break;
            case 'u':  key =     22; break;
            case 'i':  key =     23; break;
            case 'o':  key =     24; break;
            case 'p':  key =     25; break;
            case '[':  key =     26; break;
            case ']':  key =     27; break;
            //    KEY_ENTER      28
            //    KEY_LEFTCTRL   29
            case 'a':  key =     30; break;
            case 's':  key =     31; break;
            case 'd':  key =     32; break;
            case 'f':  key =     33; break;
            case 'g':  key =     34; break;
            case 'h':  key =     35; break;
            case 'j':  key =     36; break;
            case 'k':  key =     37; break;
            case 'l':  key =     38; break;
            case ';':  key =     39; break;
            case '\'': key =     40; break;
            case '`':  key =     41; break;
            //    KEY_LEFTSHIFT  42
            //    KEY_BACKSLASH  43
            case 'z':  key =     44; break;
            case 'x':  key =     45; break;
            case 'c':  key =     46; break;
            case 'v':  key =     47; break;
            case 'b':  key =     48; break;
            case 'n':  key =     49; break;
            case 'm':  key =     50; break;
            case ',':  key =     51; break;
            case '.':  key =     52; break;
            case '/':  key =     53; break;
            //    KEY_RIGHTSHIFT 54
            //    KEY_KPASTERISK 55
            //    KEY_LEFTALT    56
            //    KEY_SPACE      57
            //    . . .
            //    KEY_102ND      86
            //    KEY_F11        87
            //    KEY_F12        88

            default: break;
        }
    } else if ( key_config_name[0] == 'F' ) {
        if ( key_name_len == 2) {
              // Test for F1 F2 F3 F4 F5 F6 F7 F8 F9
            switch ( key_config_name[1] ) {
                case '1': key = 59; break;
                case '2': key = 60; break;
                case '3': key = 61; break;
                case '4': key = 62; break;
                case '5': key = 63; break;
                case '6': key = 64; break;
                case '7': key = 65; break;
                case '8': key = 66; break;
                case '9': key = 67; break;
                default: break;
            }
        } else if ( key_name_len == 3) {
              // Test for F10 F12 F12
            if ( key_config_name[1] == '1' ) {
                switch ( key_config_name[2] ) {
                    case '0': key = 68; break;
                    case '1': key = 87; break;
                    case '2': key = 88; break;
                    default: break;
                }
            }
        }
    } else {
        switch ( key_name_len ) {
            case 6: if (strncmp(key_config_name, "ESCAPE",    6) == 0 ) {key =  1;} break;
            case 9: if (strncmp(key_config_name, "BACKSPACE", 9) == 0 ) {key = 14;} break;
            case 3: if (strncmp(key_config_name, "TAB",       3) == 0 ) {key = 15;} break;
            case 5: if (strncmp(key_config_name, "SPACE",     5) == 0 ) {key = 57;} break;
            default: break;
       }
    }

    return key;
}

extern
void
twc_config_lookup_shortcut_keys(
    bool (*is_shortcut_key)[SHORTCUT_KEY_MAX]
) {
    for (int i = 0; i < SHORTCUT_KEY_MAX; i++) {
        (*is_shortcut_key)[i] = false;
    }

    if ( KeyBoardShortCutKeys_array == NULL ) { return; }

    FOR_EACH_ELEMENT_IN_ARRAY( KeyBoardShortCutKeys_array, index ) {

        toml_datum_t d = toml_string_at( KeyBoardShortCutKeys_array, index );
        if ( d.ok ) {
            char *key_config_name = d.u.s;

            //LOG_INFO("key_config_name %s", key_config_name);

            uint32_t key_index = (
                shortcut_key( key_config_name )
            );

            // The event code value for a key is used as an index into
            // an array denoting configured shortcut keys.  0 is not a
            // valid event code.

            if ( 0         <          key_index &&
                 key_index < SHORTCUT_KEY_MAX
            ) {
                (*is_shortcut_key)[key_index] = true;
            }
        }
    }

    return;
};

extern
enum twc_decoration_policy
twc_config_lookup_DefaultDecorationPolicy(
    void
) {
    if ( DefaultDecorationPolicy == NULL) { return TWC_DEFAULT_DECORATION_POLICY; }

    if ( strncmp(DefaultDecorationPolicy, "NeverDecorate"            , 13) == 0 ) { return DP_Never_Decorate;             }
    if ( strncmp(DefaultDecorationPolicy, "AlwaysSSD"                ,  9) == 0 ) { return DP_Always_SSD;                 }
    if ( strncmp(DefaultDecorationPolicy, "AllowCSD_else_No_Decorate", 25) == 0 ) { return DP_Allow_CSD_else_No_Decorate; }
    if ( strncmp(DefaultDecorationPolicy, "AllowCSD_else_SSD"        , 17) == 0 ) { return DP_Allow_CSD_else_SSD;         }

    return TWC_DEFAULT_DECORATION_POLICY;
}

extern
bool
twc_config_lookup_DecorationPolicy(
    char                       *app_id,
    char                       *title,
    enum twc_decoration_policy *decoration_policy
) {
    bool match;

    match = window_is_listed(
        AllowCSD_else_SSD_array,
        app_id,
        title
    );
    if (match) {
        *decoration_policy = DP_Allow_CSD_else_SSD;
        return true;
    }

    match = window_is_listed(
        AllowCSD_else_No_Decorate_array,
        app_id,
        title
    );
    if (match) {
        *decoration_policy = DP_Allow_CSD_else_No_Decorate;
        return true;
    }

    match = window_is_listed(
        AlwaysSSD_array,
        app_id,
        title
    );
    if (match) {
        *decoration_policy = DP_Always_SSD;
        return true;
    }

    match = window_is_listed(
        NeverDecorate_array,
        app_id,
        title
    );
    if (match) {
        *decoration_policy = DP_Never_Decorate;
        return true;
    }

    return false;
}

extern
void
twc_config_lookup_ButtonMapping(
    button_mapping_array button_mapping
) {
      // The button mapping should be a permutation.
      // button_id 0 is not valid, so mark it seen.
    button_mapping_array button_id_seen = { 1, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

      // First valid button is at index 1
    button_mapping[0] = MB_Button_None;
    int button_index  = 1;

    bool mapping_valid = false;

    if ( ButtonMapping_array != NULL ) {

        mapping_valid = true;

        FOR_EACH_ELEMENT_IN_ARRAY( ButtonMapping_array, index ) {
            toml_datum_t d = toml_int_at( ButtonMapping_array, index );
            if ( d.ok ) {
                uint8_t button_id = d.u.i;

                if ( BUTTON_MAPPING_LIMIT      <  button_index ||   // arrray overflow
                     NUMBER_OF_BUTTONS         <  button_id    ||   // id out of range
                     button_id_seen[button_id] == 1                 // not permutation
                ) {
                    mapping_valid = false;
                    break;
                }

                button_mapping[button_index++] = button_id;
                button_id_seen[button_id]      = 1;
            }
        }
    }

    //LOG_INFO("ButtonMapping %s", (mapping_valid ? "Good" : "Bad") );

    if ( mapping_valid ) { return; }

      // return identity array
    for (uint8_t i = 0; i < BUTTON_MAPPING_LIMIT; i++ ) {
         button_mapping[i] = i;
     }

    return;
}

extern
void
twc_config_lookup_LibinputConfig(
    void
) {
    // Fix: ToDo
}

static
void
set_workspaces(
    void
) {
    if ( WorkSpace_array == NULL ) { return; }

    FOR_EACH_TABLE_IN_ARRAY(WorkSpace_array, member_table) {
        if ( member_table == NULL ) { continue; }

        char *workspace_name = NULL;
        bool found_workspace = false;

        const char *key;
        FOR_EACH_KEY_IN_TABLE( member_table, key ) {
            toml_datum_t d = toml_string_in( member_table, key );
            if (d.ok) {
                if ( KEY_EQ_STRING(key, "Name") ) {
                    workspace_name = d.u.s;
                    found_workspace = true;
                }
                continue;
            }
        }

        if ( found_workspace ) {
            twc_workspace_new( workspace_name );
        }
    }

    return;
}

#if 0
/* Machinery for saving dl handles so they can be closed later. */

#define MAX_DL_LIB_COUNT 8
#define MAX_DL_NAME_LEN 24

char  dlnames  [MAX_DL_LIB_COUNT][MAX_DL_NAME_LEN] = {
    "one",
    "two",
};
void *dlhandles[MAX_DL_LIB_COUNT];

bool dl_loaded[MAX_DL_LIB_COUNT];
#endif

static
bool
load_plugin(
    char *path,
    char *plugin,
    struct twc_config_data *twc_config_data
) {

#   define LIB_NAME_LEN 128
    char   lib_name[LIB_NAME_LEN];
    snprintf( &lib_name[0], LIB_NAME_LEN, "%s%s%s%s", path, "lib", plugin, "-plg.so");

#   define INIT_NAME_LEN 32
    char   init_name[INIT_NAME_LEN];
    snprintf(&init_name[0], INIT_NAME_LEN, "%s%s", plugin, "_initialize");

    void *dlhandle = (
        dlopen( lib_name, RTLD_LAZY | RTLD_LOCAL )
    );

    if ( dlhandle != NULL ) {
        client_initialize_f *plugin_initialize;

          // Clear any prior errors and bind the "plugin_initialize"
          // symbol from the "plugin" library
        dlerror();
        plugin_initialize = dlsym (dlhandle, init_name );

        if ( dlerror() != NULL ) {
            dlclose( dlhandle );
            LOG_ERROR("ERROR: plugin %s has no initialize routine", plugin);
        } else {
            bool running;
            plugin_initialize(
                &running,
                twc_config_data
            );
            return true;
        }
    }

    // Fix: Save dlhandles so they can be closed later

    // Unload dynamic libraries.  The RTLD_NODELETE flag on dlopen
    // would allow dlclose to occur just after symbol binding above,
    // but it is not Posix.

//    if ( dl_loaded[0] ) {
//        dlclose( dlhandles[0] );
//    }

    return false;
}

extern
void
twc_load_plugins(
    struct twc_config_data *twc_config_data
) {
    if ( Plugins_array    == NULL ||
         PluginPath_array == NULL
    ) { return; }

    FOR_EACH_ELEMENT_IN_ARRAY( Plugins_array, index ) {

        bool plugin_found = false;

        toml_datum_t d = toml_string_at( Plugins_array, index );
        if ( d.ok ) {
            char *plugin = d.u.s;
            FOR_EACH_ELEMENT_IN_ARRAY( PluginPath_array, index ) {

                toml_datum_t d = toml_string_at( PluginPath_array, index );
                if ( d.ok ) {
                    char *path = d.u.s;

                    plugin_found = load_plugin(path, plugin, twc_config_data);
                    if ( plugin_found ) { break; }
                }
            }
            if ( ! plugin_found ) {
                LOG_ERROR("ERROR: plugin %s not found", plugin);
            }
        }
    }

    return;
}

#if 0
static
void
set_cursor_table(
  toml_table_t *subject_table
) {
    const char *key;
    FOR_EACH_KEY_IN_TABLE( subject_table, key ) {

        toml_datum_t d = toml_string_in( subject_table, key );
        if (d.ok) {
            if ( KEY_EQ_STRING(key, "Button" ) ) printf("Cursor Button %s\n", d.u.s);
            if ( KEY_EQ_STRING(key, "Destroy") ) printf("Cursor Button %s\n", d.u.s);
            if ( KEY_EQ_STRING(key, "Frame"  ) ) printf("Cursor Button %s\n", d.u.s);
            if ( KEY_EQ_STRING(key, "Resize" ) ) printf("Cursor Button %s\n", d.u.s);
            if ( KEY_EQ_STRING(key, "Select" ) ) printf("Cursor Button %s\n", d.u.s);
            continue;
        }
    }

    return;
}

static
void
set_cursors(
    toml_array_t *subject_array
) {
    FOR_EACH_TABLE_IN_ARRAY(subject_array, member_table) {
        if ( member_table == NULL ) { continue; }

        cursor_table( member_table );
    }

    return;
}
#endif

static
void
walk_service_array(
   const char   *provider_name,
   toml_array_t *provider_array
) {
    /*
    ** A provider_array contains inline tables, each of which is a
    ** service specification for the agent.
    */

    if ( provider_array   == NULL ||
         provider_name    == NULL ||
         provider_name[0] == '\0'
    ) { return; }

      // For each service_spec in the provider_array
    FOR_EACH_TABLE_IN_ARRAY(provider_array, service_spec_table) {
        if ( service_spec_table == NULL ) { continue; }

        //LOG_INFO("found an inline table for provider %s", provider_name);

        char *service_name = NULL;

        enum xx_zwm_service_assert_method         assert_method = XX_ZWM_SERVICE_ASSERT_METHOD_NOP;
        enum xx_zwm_service_stand_down_method stand_down_method = XX_ZWM_SERVICE_STAND_DOWN_METHOD_NOP;

          // For each key in the service_spec_table
        const char *key;
        FOR_EACH_KEY_IN_TABLE( service_spec_table, key ) {

            toml_datum_t d = toml_string_in( service_spec_table, key );

            if (d.ok) {

                if ( KEY_EQ_STRING(key, "Assert") ) {

                      // Disambiguate options based on fourth char.
                    switch ( d.u.s[3] ) {
                        case 'c': assert_method = XX_ZWM_SERVICE_ASSERT_METHOD_DEICONIFY; break;
                        case 's': assert_method = XX_ZWM_SERVICE_ASSERT_METHOD_RAISE;     break;
                        case 'o': assert_method = XX_ZWM_SERVICE_ASSERT_METHOD_RELOCATE;  break;
                        default : assert_method = XX_ZWM_SERVICE_ASSERT_METHOD_NOP;       break;
                    }
                }

                if ( KEY_EQ_STRING(key, "StandDown") ) {

                      // Disambiguate options based on first char.
                    switch ( d.u.s[0] ) {
                        case 'L': stand_down_method = XX_ZWM_SERVICE_STAND_DOWN_METHOD_LOWER;   break;
                        case 'I': stand_down_method = XX_ZWM_SERVICE_STAND_DOWN_METHOD_ICONIFY; break;
                        default : stand_down_method = XX_ZWM_SERVICE_STAND_DOWN_METHOD_NOP;     break;
                    }
                }

                if ( KEY_EQ_STRING(key, "Service" ) ) service_name = d.u.s;
            }
        }

        //LOG_INFO("walk_assert_method_table: AG %s SN %s AM %d SM %d", provider_name, service_name, assert_method, stand_down_method);

        if (service_name == NULL || service_name[0] == '\0' ) { continue; }

        twc_service_set_methods(
            provider_name,
            service_name,
            assert_method,
            stand_down_method
        );
    }

    return;
}

static
void
walk_desktop_service_table(
    toml_table_t *service_table
) {
    /*
    ** The DesktopService table contains named arrays.  Each name is a
    ** Provider name.
    */

    const char *provider_key;
    FOR_EACH_KEY_IN_TABLE( service_table, provider_key ) {

        //LOG_INFO("found a named array in the DesktopService table: %s", provider_key);

        toml_array_t *provider_array = toml_array_in( service_table, provider_key );
        if (provider_array != NULL ) {
            walk_service_array( provider_key, provider_array );
        }
    }

    return;
}

static
void
walk_agent_table(
    toml_table_t *agent_table
) {
    const char *key;
    FOR_EACH_KEY_IN_TABLE( agent_table, key ) {
        //LOG_INFO("walk_agent_table %s", key);
    }

    return;
}

static
void
walk_root_table(
    toml_table_t     *root_table,
    struct twc_state *twc
) {
    const char *key;
    FOR_EACH_KEY_IN_TABLE( root_table, key ) {

          // Named Array in Root Table
        toml_array_t *member_array = (
            toml_array_in( root_table, key )
        );
        if ( member_array != NULL ) {

            if ( KEY_EQ_STRING(key, "Plug-ins"                 ) ) {                  Plugins_array = member_array; }
            if ( KEY_EQ_STRING(key, "HoldIcon"                 ) ) {                 HoldIcon_array = member_array; }
            if ( KEY_EQ_STRING(key, "OccupyAll"                ) ) {                OccupyAll_array = member_array; }
            if ( KEY_EQ_STRING(key, "AlwaysSSD"                ) ) {                AlwaysSSD_array = member_array; }
            if ( KEY_EQ_STRING(key, "Plug-inPath"              ) ) {               PluginPath_array = member_array; }
            if ( KEY_EQ_STRING(key, "XKBKeyBoard"              ) ) {              XKBKeyBoard_array = member_array; }
            if ( KEY_EQ_STRING(key, "ButtonMapping"            ) ) {            ButtonMapping_array = member_array; }
            if ( KEY_EQ_STRING(key, "NeverDecorate"            ) ) {            NeverDecorate_array = member_array; }
            if ( KEY_EQ_STRING(key, "StartIconified"           ) ) {           StartIconified_array = member_array; }
            if ( KEY_EQ_STRING(key, "LibinputConfig"           ) ) {           LibinputConfig_array = member_array; }
            if ( KEY_EQ_STRING(key, "AnchoredWindows"          ) ) {          AnchoredWindows_array = member_array; }
            if ( KEY_EQ_STRING(key, "IconifyByMasking"         ) ) {         IconifyByMasking_array = member_array; }
            if ( KEY_EQ_STRING(key, "AllowCSD_else_SSD"        ) ) {        AllowCSD_else_SSD_array = member_array; }
            if ( KEY_EQ_STRING(key, "KeyBoardShortCutKeys"     ) ) {     KeyBoardShortCutKeys_array = member_array; }
            if ( KEY_EQ_STRING(key, "AllowCSD_else_No_Decorate") ) {AllowCSD_else_No_Decorate_array = member_array; }

            if ( KEY_EQ_STRING(key, "WorkSpaces") ) {
                WorkSpace_array = member_array;
                set_workspaces();
            }

            if ( KEY_EQ_STRING(key, "Cursor") ) { Cursor_array = member_array; }

            if ( KEY_EQ_STRING(key, "Menu") ) {
                //LOG_INFO("walk_root_table %s", key);
            }

            continue;
        }

        toml_datum_t d;

          // Named Table in Root Table
        toml_table_t *member_table = (
            toml_table_in( root_table, key)
        );
        if (member_table != NULL ) {

            if ( KEY_EQ_STRING(key, "DesktopService") ) {
                //LOG_INFO("found DesktopService table in the RootTable %s", key);
                DesktopService_table = member_table;
                walk_desktop_service_table(member_table);
            }

            if ( KEY_EQ_STRING(key, "Agent") ) {
                Agent_table = member_table;
                //LOG_INFO("found agent table in root_table %s", key);
                walk_agent_table(member_table);
            }
            continue;
        }

          // Named String in Root Table
        d = toml_string_in( root_table, key );
        if (d.ok) {
            if ( KEY_EQ_STRING(key, "DefaultDecorationPolicy" ) ) DefaultDecorationPolicy = d.u.s;
            if ( KEY_EQ_STRING(key, "KeyBoardShortCutMods"    ) ) KeyBoardShortCutMods    = d.u.s;

            continue;
        }

          // Named Boolean in Root Table
        d = toml_bool_in( root_table, key );
        if (d.ok) {
            // key:value in record

            if ( KEY_EQ_STRING(key, "DontMoveOff"        ) ) twc->dont_move_off         = d.u.b;
            if ( KEY_EQ_STRING(key, "StaddleOutputs"     ) ) twc->staddle_outputs       = d.u.b;
            if ( KEY_EQ_STRING(key, "RaiseOnMove"        ) ) twc->raise_on_move         = d.u.b;
            if ( KEY_EQ_STRING(key, "RaiseOnResize"      ) ) twc->raise_on_resize       = d.u.b;
            if ( KEY_EQ_STRING(key, "FocusFollowsPointer") ) twc->focus_follows_pointer = d.u.b;

            continue;
        }

          // Named Integer in Root Table
        d = toml_int_in( root_table, key );
        if (d.ok) {

            // Fix: belongs in Agent section.
            if ( KEY_EQ_STRING(key, "MoveDelta") ) MoveDelta = d.u.i;

            continue;
        }
    }

    return;
}

static
toml_table_t*
parse(
    FILE *fp
) {
    char errbuf[200];

    toml_table_t *root_table = (
        toml_parse_file(fp, errbuf, sizeof(errbuf))
    );
    if ( root_table == NULL ) {
        LOG_ERROR("ERROR: %s\n", errbuf);
        return NULL;
    }

    return root_table;
}

extern
void
twc_config_read_file(
    char             *config_file,
    struct twc_state *twc
) {
    FILE *fp = fopen(config_file, "r");

    if ( !fp ) {
        return;
    }

    toml_table_t *root_table = (
        parse(fp)
    );

    walk_root_table(root_table, twc);

    twc->focus_follows_pointer   = true;
    twc->workspace_output_unison = false;

    twc->service_table = DesktopService_table;
    twc->  agent_table =   Agent_table;

    // Don't release the tree as some parts will be revisited when new
    // windows appear, eg, for looking up window titles etc.
    // toml_free(root_table);

    fclose(fp);

    return;
}
