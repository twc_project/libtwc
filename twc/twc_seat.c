
  // Add the nsec member to struct timespec in time.h
#define _POSIX_C_SOURCE 200112L

  // System
#include <stdlib.h>             // NULL
#include <time.h>               // clock_gettime()
#include <libinput.h>
#include <xkbcommon/xkbcommon.h>
#include <linux/input-event-codes.h>    // BTN_LEFT, BTN_RIGHT, ...

  // Wayland
#include <wayland-util.h>   // wl_container_of, wl_list

  // libtwc_util
#include <twc/wayland_types.h>    // wld_millisecond_t
#include <twc/twc_log.h>

  // bic_server APIs
#include "bic_server/wayland_event.h"
#include "bic_server/wayland_pre-defined.h"
#include "bic_server/wm_agent_event.h"
#include "bic_server/wm_agent_event_wrappers.h"
#include "bic_server/wm_agent_bic.h"

  // APIs
#include "twc_seat.h"       // Our API to ensure consistancy
#include "twc_window.h"
#include "twc_op.h"
#include "srv_output.h"
#include "srv_keyboard.h"
#include "twc_config_toml.h"

  // Imported data types
#include "twc_window-impl.h"
#include "twc_state-impl.h"
#include "srv_keyboard-impl.h"
#include "bic_server/server_state-impl.h"
#include "bic_server/bic_connection.h"

 /*
 ** Wayaland Modifiers
 ** ------------------
 **
 ** Consider the wl_keyboard::modifiers event in the Wayland protocol.
 ** There are four (non-serial) arguments.  These notes provide
 ** background.
 **
 ** keyboard layouts:
 **
 ** Keyboards have a layout (us, cz, de, ...).  It is possible to
 ** simultaneously configure multiple layouts for a single physical
 ** keyboard.  A multi-layout is sometimes called a group.  You must
 ** also configure a key combination (say, ALT+SHIFT) to switch among
 ** member layouts.  At any moment, there is exactly one "currently
 ** effective layout."  See the "Advanced Configuration" section at
 **
 **   https://www.x.org/releases/X11R7.7/doc/xorg-docs/input/XKB-Config.html
 **
 ** xkb_state_component:
 **
 ** Shortly after the "Detailed Description" section at
 **
 **   https://xkbcommon.org/doc/current/group__state.html
 **
 ** is the documentation for the members of
 **
 **   enum xkb_state_component
 **
 ** The enum values are bitmaskable.  There are two sets of defined
 ** masks.
 **
 **   XKB_STATE_MODS_DEPRESSED
 **   XKB_STATE_MODS_LATCHED
 **   XKB_STATE_MODS_LOCKED
 **   XKB_STATE_MODS_EFFECTIVE       logical OR of previous three
 **
 ** The first set above refers to the modifiers: shift, alt, cntl, etc.
 **
 **   XKB_STATE_LAYOUT_DEPRESSED
 **   XKB_STATE_LAYOUT_LATCHED
 **   XKB_STATE_LAYOUT_LOCKED
 **   XKB_STATE_LAYOUT_EFFECTIVE     logical OR of previous three
 **
 ** The second set refers to the configured layouts (see layouts
 ** above).  The last item represents the currently selected layout.
 ** The first three items show the status of the layout switching
 ** key-combination.
 **
 ** Now reconsider the wl_keyboard::modifiers event.  The "modifiers"
 ** arguments are self explanatory.  The "group" argument refers to the
 ** currently effective layout in a mulit-layout configuration.
 **
 **
 ** Also see "Detailed Description" section at
 **   https://xkbcommon.org/doc/0.2.0/structxkb__state.html
 */

  // KeyBoard ShortCuts
  // is_shortcut_key[] is indexed by raw input code, not by keycode
static xkb_mod_mask_t shortcut_modifiers = TWC_DEFAULT_SHORTCUT_MODS;
static bool           is_shortcut_key[SHORTCUT_KEY_MAX];

  // Track the pointer position.  When there is a change in the
  // arrangement of surfaces, this position is used to re-establish
  // pointer/keyboard focus.  Fixed format, layout coordinates
static wl_fixed_t pointer_location_fix_lx;
static wl_fixed_t pointer_location_fix_ly;

  // Track the pointer/keyboard focus and which client.
static struct wl_surface     *wl_surface_with_ptr_focus = INERT_WL_SURFACE;
static struct wl_surface     *wl_surface_with_kbd_focus = INERT_WL_SURFACE;
static struct bic_connection *bic_client_with_kbd_focus = INERT_BIC_CONNECTION;

  // Pointer events plus focus policy determine which surface has
  // keyboard focus.  Using focus_window() requests, the Decoration
  // and Task Agents may reassign their keyboard focus.  The "from"
  // surface is recorded.  See comments in
  // twc_seat_keyboard_focus_event()
#define FOCUS_NOT_REASSIGNED NULL
static struct wl_surface *wl_surface_reassignment_source = FOCUS_NOT_REASSIGNED;

// Fix: For click to focus, should the kbd_focus be per workspace and
// saved/restored when switching??

static button_mapping_array button_mapping;

struct bic_seat {
    struct wl_list link;

    struct wl_list srv_pointer_list;
    struct wl_list srv_keyboard_list;

    xkb_mod_mask_t     xkb_depressed;       // XKB state machine
    xkb_mod_mask_t     xkb_latched;         //      |
    xkb_mod_mask_t     xkb_locked;          //      |
    xkb_layout_index_t xkb_layout_index;    //      |
                                            //      |
    struct xkb_state  *xkb_state;           //      |
    struct xkb_keymap *xkb_keymap;          //      v
};

static
struct bic_seat twc_seat = {
    // Fix: there needs to be multiple "seats" each with its own
    // kbd/ptr focus.

    .xkb_depressed    = 0,
    .xkb_latched      = 0,
    .xkb_locked       = 0,
    .xkb_layout_index = 0,

    .xkb_state        = NULL,
    .xkb_keymap       = NULL,
};

extern
void
twc_seat_initialize(
    void
) {
    char *XKBRules;
    char *XKBModel;
    char *XKBLayout;
    char *XKBVariant;
    char *XKBOptions;

    // We construct only one seat.

    wl_list_init(&twc_seat.srv_pointer_list);
    wl_list_init(&twc_seat.srv_keyboard_list);

    wl_list_insert(&BIC_SERVER.bic_seat_list, &twc_seat.link);

    twc_config_lookup_ButtonMapping(
        button_mapping
    );

      // Don't allow mapping of wheel spin
    for (twc_button_id i = MB_Button_None; i < BUTTON_MAPPING_LIMIT; i++ ) {
        if ( button_mapping[i] == MB_Button_Spin_Forward  ) button_mapping[i] = i;
        if ( button_mapping[i] == MB_Button_Spin_Backward ) button_mapping[i] = i;
    }
    button_mapping[MB_Button_Spin_Forward ] = MB_Button_Spin_Forward;
    button_mapping[MB_Button_Spin_Backward] = MB_Button_Spin_Backward;

      // Get the RMLVO settings from the configuration.
    twc_config_lookup_XKBKeyBoard(
        &XKBRules,
        &XKBModel,
        &XKBLayout,
        &XKBVariant,
        &XKBOptions
    );

    /*
    LOG_INFO("twc_seat_initialize %s %s %s %s %s",
        XKBRules,
        XKBModel,
        XKBLayout,
        XKBVariant,
        XKBOptions
    );
    */
      // https://xkbcommon.org/doc/current/structxkb__rule__names.html
    struct xkb_rule_names names = {
        .rules   = XKBRules,
        .model   = XKBModel,
        .layout  = XKBLayout,
        .variant = XKBVariant,
        .options = XKBOptions
    };

    struct xkb_context *context = xkb_context_new( XKB_CONTEXT_NO_FLAGS );

    twc_seat.xkb_keymap  = (
        xkb_keymap_new_from_names(
            context,
            &names,
            XKB_KEYMAP_COMPILE_NO_FLAGS
        )
    );
    twc_seat.xkb_state = xkb_state_new( twc_seat.xkb_keymap );

    xkb_context_unref(context);

    shortcut_modifiers = (
        twc_config_lookup_KeyBoardShortCutMods()
    );

    twc_config_lookup_shortcut_keys( &is_shortcut_key );

    return;
}

extern
void*
twc_seat_get_default_seat(
    void
) {
    return &twc_seat;
}

extern
void
twc_seat_new_keyboard(
    struct srv_keyboard *srv_keyboard
) {
LOG_INFO("twc_seat_new_keyboard %s", srv_keyboard->keyboard_name);

    wl_list_insert(&twc_seat.srv_keyboard_list, &srv_keyboard->link);

    // Fix: There should be a remove keyboard routine.
}

extern
struct xkb_keymap*
twc_seat_get_xkb_keymap(
    void
) {
    return twc_seat.xkb_keymap;
}

extern
void
twc_seat_new_output_dimensions(
    wld_dimension_t width,
    wld_dimension_t height
) {
    if ( pointer_location_fix_lx == 0 ) {

        // This is the first output to be reported.

        pointer_location_fix_lx = wl_fixed_from_int( width/2);
        pointer_location_fix_ly = wl_fixed_from_int(height/2);
    }

    return;
}

static
void
twc_seat_keyboard_focus_set(
    struct wl_surface* new_wl_surface
) {
    struct wl_surface* old_wl_surface = wl_surface_with_kbd_focus;

    if ( old_wl_surface == new_wl_surface ) { return; }

    wl_keyboard_leave( NULL, old_wl_surface );

    wl_surface_with_kbd_focus = new_wl_surface;
    bic_client_with_kbd_focus = new_wl_surface->bic_connection;

    wl_keyboard_enter( NULL, new_wl_surface, NULL );

      // Must also send wl_keyboard.modifiers to this client's keyboard.
    wl_keyboard_modifiers(
        &bic_client_with_kbd_focus->wl_seat.wl_keyboard,
        twc_seat.xkb_depressed,
        twc_seat.xkb_latched,
        twc_seat.xkb_locked,
        twc_seat.xkb_layout_index
    );

    // Notify those interested in this window

    struct twc_view *old_twc_view = old_wl_surface->twc_view;
    struct twc_view *new_twc_view = new_wl_surface->twc_view;

    struct twc_window *old_twc_window = old_twc_view->twc_window;
    struct twc_window *new_twc_window = new_twc_view->twc_window;

    if ( old_twc_window == new_twc_window ) { return; }

    xx_wm_window_update_keyboard_focus(
        old_twc_window,
        XX_WM_WINDOW_FOCUS_VACATE
    );

    xx_wm_window_update_keyboard_focus(
        new_twc_view->twc_window,
        XX_WM_WINDOW_FOCUS_POSSESS
    );

    return;
}

enum mouse_event {
    ME_Motion,
    ME_Click,
};

static
void
twc_seat_keyboard_focus_event(
    enum mouse_event mouse_event
) {
    //LOG_INFO("twc_seat_keyboard_focus_event");

      // Which event?
    bool have_click  = mouse_event == ME_Click;
    bool have_motion = mouse_event == ME_Motion;

      // Which policy?
    bool click_to_focus    =   TWC_click_to_focus;
    bool focus_follows_ptr = ! TWC_click_to_focus;

    /*
    ** Calculate keyboard focus based on pointer events and focus
    ** policy.  We call this the policy focus (P).
    **
    ** Since agents may reassign keyboard focus, current focus (C) may
    ** not match the previous policy focus.  In this case, we say that
    ** a reassignment is in effect.  This routine needs to determine
    ** if a reassinment is in effect and if so, should it continue.
    **
    ** In a reassignemt, there is a "from" and "to" a surface.
    **   The to    surface is             wl_surface_with_kbd_focus.
    **   The from  surface is recorded in wl_surface_reassignment_source.
    **
    ** If no reassignemt is in effect, the from surface is denoted by
    ** NULL, aka FOCUS_NOT_REASSIGNED.  On recalculating the policy
    ** focus (P), there are two situations.
    **
    **   A. P = C: The calculated policy focus is the same as the
    **      current focus.  No action is required.
    **
    **   B. P != C: The calculated policy focus has changed.  The
    **      active focus should become P.
    **
    ** When a reassinment is in effect, the current surface is the
    ** "to" surface.  The calculated policy focus (P) may be the
    ** "from" surface (F), the "to" surface (T) or some other surface
    ** (O).  There are three situations
    **
    **   1. P == F: The calculated policy focus has not changed.
    **
    **      i ) if click-to-focus, the reassignment should terminate.
    **      The active focus should become P (revert back to F).
    **
    **      ii) if ptr-follows-focus, the pointer is simply moving
    **      around in F.  The reassignment should stay in efect.
    **
    **   2. P == T: The calculated policy focus has changed.  The
    **      policy focus is in T as a result of specific user pointer
    **      activity.  The reassignment should terminate.
    **      Coincidentally, however, the current focus need not
    **      change.
    **
    **   3. P == O: The calculated policy focus has changed.  As in 2,
    **      the reassignment should terminate.  This time, however,
    **      the active focus should become P (== O).
    */

      // Should the mouse_event trigger a keyboard focus calculation?
    if ( (focus_follows_ptr && have_motion) ||
         (click_to_focus    && have_click)
    ) {
          // Calulating the nominal keyboard focus is simple.
        struct wl_surface *policy_focus  = wl_surface_with_ptr_focus;

          // Are the keyboard and pointer focus different?
        bool kbd_ptr_mismatch = ! (wl_surface_with_kbd_focus == wl_surface_with_ptr_focus);

          // Policy focus is different from current focus
        bool policy_NE_current = ! (wl_surface_with_kbd_focus == wl_surface_with_ptr_focus);

        bool reassignemt_in_effect = wl_surface_reassignment_source != FOCUS_NOT_REASSIGNED;
        bool no_reassignemt        = ! reassignemt_in_effect;

        if ( no_reassignemt    &&
             policy_NE_current
        ) {
              // Case B.
            twc_seat_keyboard_focus_set(
                policy_focus
            );
        }

        if ( reassignemt_in_effect ) {

              // Does P equal F?
            if ( policy_focus == wl_surface_reassignment_source ) {
                  // Case 1
                if ( click_to_focus ) {
                      // Case 1.i
                    wl_surface_reassignment_source = FOCUS_NOT_REASSIGNED;

                    twc_seat_keyboard_focus_set(
                        policy_focus
                    );
                }

            } else {
                  // Case 2 and Case 3
                  // reassignment terminates in both cases
                wl_surface_reassignment_source = FOCUS_NOT_REASSIGNED;

                if ( kbd_ptr_mismatch ) {
                      // Case 3
                    twc_seat_keyboard_focus_set(
                        policy_focus
                    );
                }
            }
        }
    }

      // Case A and Case 1.ii require no action.
    return;
}

extern
void
twc_seat_enter_proxy_on_new_listener(
    struct wl_surface *wl_surface
) {
    /*
    ** This routine is called when an annexing agent adds a listener
    ** to a proxy surface.
    **
    ** The DEFAULT and OVERLAY surfaces are pre-existing and mapped.
    ** An annexing agent is therefore unable to register a listener
    ** for proxy surfaces before the mapping.  We generate an enter
    ** event in the case where the proxy surface has current pointer
    ** focus.
    */

    wl_fixed_t ptr_loc_fix_sx;
    wl_fixed_t ptr_loc_fix_sy;
    void      *nested_surface;

    if ( ! SURFACE_IS_PROXY(wl_surface) ) { return; }

    struct wl_surface *wl_surface_with_pointer_location = (
        twc_window_surface_at(
             pointer_location_fix_lx,
             pointer_location_fix_ly,
            &ptr_loc_fix_sx,
            &ptr_loc_fix_sy,
           &nested_surface
        )
    );

      // It's unlikely that the OVERLAY surface is activated, but
      // check anyway.
    if ( (DEFAULT_WL_SURFACE == wl_surface_with_pointer_location) ||
         (OVERLAY_WL_SURFACE == wl_surface_with_pointer_location)
    ) {
        struct bic_connection *bic_connection = (
            wl_surface->bic_connection
        );
        struct wl_pointer *wl_pointer = (
            &bic_connection->wl_seat.wl_pointer
        );

        wl_pointer_enter(
            wl_pointer,
            wl_surface,
            pointer_location_fix_lx,
            pointer_location_fix_ly
        );
    }

    return;
}


 /*
 ** Nested Surfaces
 **
 ** Recall that a toplevel surface may have 1 or more nested surfaces.
 **
 ** A pointer may move
 **
 **   a.) from toplevel to toplevel, or
 **
 **   b.) from nested surface to nested surface within a toplevel.
 **
 ** In the first case, there is clearly a new surface.  Since TWC only
 ** tracks toplevel surfaces, it can't even be made aware of the
 ** second case.  Fortunately, the only information needed is whether
 ** the nested surface has changed.  The twc_seat tracks nested
 ** surfaces from the wcf using an opaque pointer (void*).  This is
 ** enough to determine when a nested surface change occurs.
 */

extern
void
twc_seat_update_pointer_position(
    wl_fixed_t        ptr_loc_fix_lx,   // fixed format, layout coord
    wl_fixed_t        ptr_loc_fix_ly,
    wld_millisecond_t time_millisec
) {
    static void *nested_surface_with_ptr_focus = NULL;

    /*
    ** The current placement of pointer focus may need updating.
    ** Surfaces which lose or gain focus must be notified.  The
    ** resulting client event callbacks might make requests which
    ** affect the window stack.  E.g., by unmapping or iconizing a
    ** surface.  Such changes could result in a re-entrant call to
    ** this routine.  Any such cascading call will be a NOP due to the
    ** following boolean state variable.
    */

    static bool pointer_in_transition = false;

    if ( pointer_in_transition ) { return; }

      // Record the new position.
    pointer_location_fix_lx = ptr_loc_fix_lx;
    pointer_location_fix_ly = ptr_loc_fix_ly;

      // pointer location, fixed format, surface coordinates
    wl_fixed_t ptr_loc_fix_sx;
    wl_fixed_t ptr_loc_fix_sy;
    void      *nested_surface;

    struct wl_surface *wl_surface_with_pointer_location = (
        twc_window_surface_at(
             ptr_loc_fix_lx,
             ptr_loc_fix_ly,
            &ptr_loc_fix_sx,
            &ptr_loc_fix_sy,
            &nested_surface
        )
    );

      // Did the pointer change nested surfaces?
    if (nested_surface_with_ptr_focus == nested_surface ) {
          // No new surface, motion only
        wl_pointer_motion(
            wl_surface_with_pointer_location,
            time_millisec,
            ptr_loc_fix_sx,
            ptr_loc_fix_sy
        );

        return;
    }

      // The pointer focus is in transition.  Disable re-entrant calls
      // to this routine.
    pointer_in_transition = true;

    /*
    ** This invocation will handle the pointer focus change detected
    ** above _AND_ any subsequent changes necessitated as a result of
    ** emited events which may change the state of the window stack.
    */

    while ( nested_surface_with_ptr_focus !=
            nested_surface
    ) {
        //int count = 0;
        //LOG_INFO("twc_seat_update_pointer_position %d enter loop", ++count);

        struct wl_surface *old_ptr_surface = wl_surface_with_ptr_focus;
        struct wl_pointer *old_pointer     = &old_ptr_surface->bic_connection->wl_seat.wl_pointer;

        wl_pointer_leave(
            old_pointer,
            old_ptr_surface
        );

          // The above leave may have changed the window stack.  E.g.,
          // the menu agent may unmap a menu upon losing pointer
          // focus.
        wl_surface_with_pointer_location = (
            twc_window_surface_at(
                 ptr_loc_fix_lx,
                 ptr_loc_fix_ly,
                &ptr_loc_fix_sx,
                &ptr_loc_fix_sy,
                &nested_surface
            )
        );

          // The surface having pointer location is officially made
          // the surface with pointer focus.
        wl_surface_with_ptr_focus     = wl_surface_with_pointer_location;
        nested_surface_with_ptr_focus = nested_surface;

        wl_surface_with_ptr_focus->nested_surface_with_ptr_focus = (
            nested_surface_with_ptr_focus
        );

        struct wl_pointer *new_pointer = (
            &wl_surface_with_ptr_focus->bic_connection->wl_seat.wl_pointer
        );

//LOG_INFO("twc_seat_update_pointer_position %d %d", wl_fixed_to_int(ptr_loc_fix_lx), wl_fixed_to_int(ptr_loc_fix_ly));
//LOG_INFO("twc_seat_update_pointer_position %d %d", wl_fixed_to_int(ptr_loc_fix_sx), wl_fixed_to_int(ptr_loc_fix_sy));
        wl_pointer_enter(
            new_pointer,
            wl_surface_with_ptr_focus,
            ptr_loc_fix_sx,
            ptr_loc_fix_sy
        );

        xx_wm_window_update_pointer_focus(
            old_ptr_surface->twc_view->twc_window,
            XX_WM_WINDOW_FOCUS_VACATE
        );

        xx_wm_window_update_pointer_focus(
            wl_surface_with_ptr_focus->twc_view->twc_window,
            XX_WM_WINDOW_FOCUS_POSSESS
        );

        if ( wl_surface_with_ptr_focus->type == ST_XX_WM_DECO ) {

            // keep decoration apprised of the modifiers in case the
            // decoration gets a button event.  See
            // wl_keyboard::modifiers event in the Wayland protocol.

            struct bic_connection *decoration_bic_connection = (
                wl_surface_with_ptr_focus->
                twc_view->
                xx_wm_decoration.bic_connection
            );

            struct wl_keyboard *decoration_wl_keyboard = (
                &decoration_bic_connection->wl_seat.wl_keyboard
            );

            wl_keyboard_modifiers(
                decoration_wl_keyboard,
                twc_seat.xkb_depressed,
                twc_seat.xkb_latched,
                twc_seat.xkb_locked,
                twc_seat.xkb_layout_index
            );
        }

          // The above wl_pointer_enter may have changed the window
          // stack.  E.g., a client may map or unmap some surface upon
          // entry to a given surface.  Hopefully this is rare and a
          // second pass through the loop will be unnecessary.
        wl_surface_with_pointer_location = (
            twc_window_surface_at(
                 ptr_loc_fix_lx,
                 ptr_loc_fix_ly,
                &ptr_loc_fix_sx,
                &ptr_loc_fix_sy,
                &nested_surface
            )
        );

        //LOG_INFO("twc_seat_update_pointer_position %d leave loop", count);
    }

    pointer_in_transition = false;

    if ( TWC_focus_follows_pointer ) {
        twc_seat_keyboard_focus_event( ME_Motion );
    }

    return;
}

extern
void
twc_seat_recalc_pointer_focus(
    void
) {
    struct timespec now;
    clock_gettime(CLOCK_MONOTONIC, &now);

    twc_seat_update_pointer_position(
        pointer_location_fix_lx,
        pointer_location_fix_ly,
        now.tv_nsec / 1000
    );

    return;
}

extern
void
twc_seat_reassign_keyboard_focus(
    struct wl_surface* new_surface
) {
    // See comments in twc_seat_keyboard_focus_event()

      // Fix: should we care???
    bool from_deco = false;
    bool from_task = true;

    struct wl_surface* current_focus = wl_surface_with_kbd_focus;

    if (wl_surface_reassignment_source == FOCUS_NOT_REASSIGNED) {

        // No reassignment is in effect

        if ( current_focus == new_surface ) {
            // caller is reassigning to itself
        } else {

              // Set the "from" surface of the reassignment
            wl_surface_reassignment_source = current_focus;

              // Make the assignment happen
            twc_seat_keyboard_focus_set(
                new_surface
            );
        }

    } else {

        // A reassignment is in effect

        if ( new_surface == wl_surface_reassignment_source ) {

              // caller wants to reassign back to the "from" surface
              // terminatning the focus reassignment
            wl_surface_reassignment_source = FOCUS_NOT_REASSIGNED;

            twc_seat_keyboard_focus_set(
                new_surface
            );

            return;
        }

        if ( current_focus == new_surface ) {

            // redundant reassignment

        } else {

            // caller wants to change the "to" surface of an existing
            // reassignment.

            if ( from_deco ) {
                 // Fix: Is this an error??
            }

            if ( from_task ) {
                twc_seat_keyboard_focus_set(
                    new_surface
                );
            }
        }
    }

    return;
}

extern
void
twc_seat_surface_remove_all_focus(
    struct wl_surface *wl_surface
) {
    // Fix: handle multiple seats.

    // This routine is called when a surface should not be displayed.
    // That is, when it is masked; unmapped; is being destroyed; or
    // the iconify state of its window changes.

    struct timespec now;
    clock_gettime(CLOCK_MONOTONIC, &now);
    // Fix: is clock_gettime the correct time value??  check
    // wlroots source code.

    twc_seat_update_pointer_position(
        pointer_location_fix_lx,
        pointer_location_fix_ly,
        now.tv_nsec / 1000
    );

    if ( TWC_click_to_focus ) {

          // terminate any focus reassignment
        wl_surface_reassignment_source = FOCUS_NOT_REASSIGNED;

        twc_seat_keyboard_focus_set(
            INERT_WL_SURFACE
        );
    }

    return;
}

extern
struct srv_output*
twc_seat_output_with_pointer_focus(
    void
) {
    struct srv_output* srv_output = (
        srv_output_at(
            pointer_location_fix_lx,
            pointer_location_fix_ly
        )
    );

    return srv_output;
}

extern
struct twc_workspace*
twc_seat_workspace_with_pointer_focus(
    void
) {
    struct srv_output* srv_output = (
        srv_output_at(
            pointer_location_fix_lx,
            pointer_location_fix_ly
        )
    );

    return srv_output_active_workspace(srv_output);
}

extern
struct twc_view*
twc_seat_view_with_keyboard_focus(
    void
) {
    return wl_surface_with_kbd_focus->twc_view;
}

extern
void
twc_seat_pointer_location(
    wl_fixed_t *ptr_loc_fix_lx,
    wl_fixed_t *ptr_loc_fix_ly
) {
    *ptr_loc_fix_lx = pointer_location_fix_lx;
    *ptr_loc_fix_ly = pointer_location_fix_ly;

    return;
}

static
xkb_mod_mask_t
keyboard_modifier_update(
    void
) {
    // Update the focused client with new modifier state

    if (twc_seat.xkb_state == NULL) { return 0; }

    // Call only if modifier state is known to have changed.

    xkb_mod_mask_t depressed = (
        xkb_state_serialize_mods(
            twc_seat.xkb_state,
            XKB_STATE_MODS_DEPRESSED
        )
    );

    xkb_mod_mask_t latched = (
        xkb_state_serialize_mods(
            twc_seat.xkb_state,
            XKB_STATE_MODS_LATCHED
        )
    );

    xkb_mod_mask_t locked = (
        xkb_state_serialize_mods(
            twc_seat.xkb_state,
            XKB_STATE_MODS_LOCKED
        )
    );

    xkb_layout_index_t layout = (
        xkb_state_serialize_layout(
            twc_seat.xkb_state,
            XKB_STATE_LAYOUT_EFFECTIVE
        )
    );

    twc_seat.xkb_depressed    = depressed;
    twc_seat.xkb_latched      = latched;
    twc_seat.xkb_locked       = locked;
    twc_seat.xkb_layout_index = layout;

    //LOG_INFO("twc_seat_keyboard_modifiers %x", twc_seat_depressed);

    xkb_mod_mask_t xkb_modifiers = (
        (    twc_seat.xkb_depressed | twc_seat.xkb_latched | twc_seat.xkb_locked) &
        ( ~ (KB_MOD_SHIFT | KB_MOD_CAPS)                                        )
    );

    return xkb_modifiers;
}

static
void
send_keyboard_modifiers(
    void
) {
    // Fix: The physical srv_keyboard needs to be converted to one of
    // the client's wl_keyboards.  For now, each client has only one,
    // so use that.
    struct wl_keyboard *client_wl_keyboard = (
        &bic_client_with_kbd_focus->wl_seat.wl_keyboard
    );

    wl_keyboard_modifiers(
        client_wl_keyboard,
        twc_seat.xkb_depressed,
        twc_seat.xkb_latched,
        twc_seat.xkb_locked,
        twc_seat.xkb_layout_index
    );

    // Next, keep any decoration apprised of the modifiers in case the
    // decoration gets a button event.  See wl_keyboard::modifiers
    // event in the Wayland protocol.

    struct bic_connection *decoration_bic_connection = (
        wl_surface_with_kbd_focus->
        twc_view->
        xx_wm_decoration.bic_connection
    );

    struct wl_keyboard *decoration_wl_keyboard = (
        &decoration_bic_connection->wl_seat.wl_keyboard
    );

      // Don't send to the same keyboard twice.
    if ( decoration_wl_keyboard != client_wl_keyboard ) {

        wl_keyboard_modifiers(
            decoration_wl_keyboard,
            twc_seat.xkb_depressed,
            twc_seat.xkb_latched,
            twc_seat.xkb_locked,
            twc_seat.xkb_layout_index
        );
    }

    return;
}

extern
void
twc_seat_keyboard_key(
    struct srv_keyboard   *srv_keyboard,
    uint32_t               key,
    enum xkb_key_direction key_direction,
    wld_millisecond_t      time_millisec
) {
    //LOG_INFO("     Press %d", key);

    /*
    ** In
    **
    **   https://gitlab.freedesktop.org/wlroots/wlroots/-/blob/master/include/wlr/types/wlr_keyboard.h
    **
    ** the struct wlr_event_keyboard_key has a uint32_t member named
    ** keycode.  This seems to be a libinput keycode.  Keycode
    ** interpretation can be found in
    **
    **   /usr/include/linux/input-event-codes.h
    **
    ** Note that XKB keycode values are between 8 and 225.  To
    ** translate from a libinput keycode to an XKB keycode, add 8.
    */

    const uint32_t keycode = key + 8;

    /*
    ** xkb_state_key_get_syms() is described here
    **
    **   https://xkbcommon.org/doc/0.2.0/group__state.html#ga47311e7268935dd2fe3e6ef057a82cb0
    **   https://github.com/xkbcommon/libxkbcommon/issues/136
    **     So far, xkb_state_key_get_syms() never produces more than
    **     one keysym.  More than one is some kind of extension.
    **     xkb_state_key_get_one_sym() should work fine.
    **
    ** xkb_keysym_t is described here
    **
    **   https://xkbcommon.org/doc/0.2.0/xkbcommon_8h.html#a79e604a22703391bdfe212cfc10ea007
    **
    ** KEYSYM Encoding is described here
    **   https://www.x.org/releases/X11R7.7/doc/xproto/x11protocol.html#keysym_encoding
    **
    ** The xkb_keysym values for keyboard keys lie between 0xff0 and
    ** 0xfff, although not all values are assigned.
    **
    ** There are also xkb_keysyms for non-key values.  I.e., the
    ** 'Function Keysysms'.  An important Function Keysym is
    ** XKB_KEY_Terminate_Server.  It is the reported key_sym when
    ** "terminate:ctrl_alt_bksp" is enabled.
    **
    ** The values for Function Keysyms lie between 0xfe00 and 0xfeff.
    ** Again not all values are assigned.  The Function Keysyms are
    ** decribed in "The X Keyboard Extension: Protocol Specification",
    ** Appendix C.  This document can be found here.
    **
    **   https://x.org/releases/current/doc/kbproto/xkbproto.pdf
    **
    ** Read Appendix C: New KeySyms.  The actual values in that
    ** document are outdated.  You should consult the header file for
    ** current value assignments.  It is here.
    **
    **   https://github.com/xkbcommon/libxkbcommon/blob/master/include/xkbcommon/xkbcommon-keysyms.h
    */


    // See the documentation for the next two xkb routines at
    //   https://xkbcommon.org/doc/current/group__state.html
    //
    // The convention is to first translate keycode into keysym using
    // the current xkb_state and then update the state.

    const xkb_keysym_t keysym = (
        xkb_state_key_get_one_sym(
            twc_seat.xkb_state,
            keycode
        )
    );

    enum xkb_state_component changed_xkb_state_component = (
        xkb_state_update_key(
            twc_seat.xkb_state,
            keycode,
            key_direction
        )
    );

    if ( keysym == XKB_KEY_Terminate_Server ) {
        twc_op_compositor_op(XX_WM_MENU_AGENT_COMPOSITOR_OP_EXIT);
    }

    // There are 14 XKB_KEYSYM modifiers.  They lie in an ordered
    // range as follows:
    //
    //   XKB_KEY_Shift_L    0xffe1      not a twc_shortcut modifier
    //   XKB_KEY_Shift_R    0xffe2      not a twc_shortcut modifier
    //   XKB_KEY_Control_L  0xffe3
    //   . . .
    //   XKB_KEY_Hyper_R    0xffee
    //
    //  See
    //    https://github.com/xkbcommon/libxkbcommon/blob/master/include/xkbcommon/xkbcommon-keysyms.h

    static xkb_mod_mask_t xkb_modifiers = 0;
    static bool shortcut_key_delivered  = false;

    if ( changed_xkb_state_component != 0 ) {
        xkb_modifiers = keyboard_modifier_update();
    }

      // Check for a shortcut key
    if ( xkb_modifiers == shortcut_modifiers &&
        is_shortcut_key[key]
     ) {
        // Send the shortcut key to the menu agent and . . .

        uint32_t shortcut_key  = key;
        shortcut_key_delivered = true;

        //LOG_INFO("twc_seat_keyboard_key %4d %4x %4x %4d", key, keysym, xkb_modifiers, key_direction);

        xx_wm_menu_agent_emit_keyboard_shortcut(
            granted_xx_wm_menu_agent,
            shortcut_key,
            key_direction,
            twc_seat.xkb_depressed,
            XX_WM_MENU_AGENT_SHORTCUT_MOD_STATE_ACTIVE
        );

        // . . . don't send the shortcut to any other client (*).

        return;

        // (*) Under normal conditions, a client may get/lose
        // keyboard focus while a key is being held down.  This
        // means that clients may see a key release/press without
        // ever seeing a corresponding press/release.  The above
        // code provides another way for this to happen.
        //
        // Fix: should these routines be called before/after
        // delivering the shortcut?
        //
        //   twc_seat_keyboard_focus_set( null_wl_surface          );
        //   twc_seat_keyboard_focus_set( wl_surface_with_kbd_focus);

    } else {
        // The shortcut modifiers are inactive

        if ( shortcut_key_delivered ) {
            // The shortcut modifiers are inactive, but were active
            // and some key had been sent.

            xx_wm_menu_agent_emit_keyboard_shortcut(
                granted_xx_wm_menu_agent,
                0,  // no key value
                key_direction,
                twc_seat.xkb_depressed,
                XX_WM_MENU_AGENT_SHORTCUT_MOD_STATE_INACTIVE
            );
        }

        shortcut_key_delivered = false;
    }

    // Fix: The physical srv_keyboard needs to be converted to one of
    // the client's wl_keyboards.  For now, each client has only one,
    // so use that.
    struct wl_keyboard *client_wl_keyboard = (
        &bic_client_with_kbd_focus->wl_seat.wl_keyboard
    );

      // Send the key to the client
    wl_keyboard_key(
        client_wl_keyboard,
        time_millisec,
        key,
        key_direction
    );

    if ( changed_xkb_state_component != 0 ) {

        // Since we're running the keycode-to-keysym state machine, we
        // know what the modifiers are.  (We use it to detect
        // shortcuts and the keysym XKB_KEY_Terminate_Server.)  We
        // might as well send the modifiers to the client right now.

        send_keyboard_modifiers();
    }

    return;
}

  /*
  ** Summary of wl_pointer button and wheel events.
  **
  **   Buttons:
  **
  **     wl_pointer::button
  **
  **     parm name =    button
  **     parm type =  uint32_t (1)
  **                  --------
  **     BTN_LEFT        0x110
  **     BTN_RIGHT       0x111
  **     BTN_MIDDLE      0x112
  **     BTN_SIDE        0x113   THUMB_REAR
  **     BTN_EXTRA       0x114   THUMB_FRONT
  **
  **     Button events also have a state parameter
  **       WL_POINTER_BUTTON_STATE_PRESSED
  **       WL_POINTER_BUTTON_STATE_RELEASED
  **
  **   Wheel:
  **
  **     wl_pointer::axis
  **
  **     parm names =     axis(2)     value (3)
  **                     --------    ----------
  **     parm types =    uint32_t    wl_fixed_t
  **
  **     spin-backward          0             1 (4)
  **     spin-forward           0            -1
  **
  **     tilt-right             1             1
  **     tilt-left              1            -1
  **
  **     Wheel (axis) events DO NOT a pressed/released state parameter.
  **
  **   Mapping Discussion:
  **
  **     It seems unlikey tht one would want to map the wheel spin
  **     events.
  **
  **     Mapping a tilt event to a button event requires emiting both
  **     PRESSED and RELEASED event.
  **
  **     Mapping a button event to a tilt event requires discarding
  **     the RELEASED event.
  **
  ** (1) interpreted as input-event-codes. See
  **     https://gitlab.freedesktop.org/libinput/libinput/-/blob/main/include/linux/linux/input-event-codes.h
  **
  ** (2) One parameter of the axis event is also named axis.
  **
  ** (3) value is "vector length along along axis"
  **
  ** (4) For dicrete mice, one unit for each wheel click
  */

#define is_wheel_tilt( b ) \
        (b) == MB_Button_Tilt_Right ||  \
        (b) == MB_Button_Tilt_Left

static
enum twc_mouse_button
map_event_code(
    uint32_t event_code
) {
    // Buttonn event_codes (BTN_xyz) are in the range 0x110 - 0x114.
    // https://gitlab.freedesktop.org/libinput/libinput/-/blob/main/include/linux/linux/input-event-codes.h

      // Convert event_code.
    enum twc_mouse_button twc_mouse_button;
    switch ( event_code ) {
        case BTN_LEFT:   twc_mouse_button = MB_Button_Left;        break;
        case BTN_MIDDLE: twc_mouse_button = MB_Button_Middle;      break;
        case BTN_RIGHT:  twc_mouse_button = MB_Button_Right;       break;
        case BTN_SIDE:   twc_mouse_button = MB_Button_Thumb_Rear;  break;
        case BTN_EXTRA:  twc_mouse_button = MB_Button_Thumb_Front; break;
        default: twc_mouse_button = MB_Button_None; break;
    }

      // map the button_id
    enum twc_mouse_button twc_mapped_button = (
        button_mapping[twc_mouse_button]
    );

      // Return valid button only if actually mapped
    return (
        twc_mapped_button != twc_mouse_button ? twc_mapped_button : MB_Button_None
    );
}

static
uint32_t
convert_to_event_code(
    enum twc_mouse_button twc_mouse_button
) {
      // Convert twc_mouse_button to event_code
    uint32_t event_code;
    switch ( twc_mouse_button ) {
        case MB_Button_Left:        event_code = BTN_LEFT;   break;
        case MB_Button_Middle:      event_code = BTN_MIDDLE; break;
        case MB_Button_Right:       event_code = BTN_RIGHT;  break;
        case MB_Button_Thumb_Rear:  event_code = BTN_SIDE;   break;
        case MB_Button_Thumb_Front: event_code = BTN_EXTRA;  break;

        default: event_code = MB_Button_None;
    }

    return event_code;
}

extern
void
twc_seat_pointer_button(
    uint32_t                   event_code,
    enum libinput_button_state button_direction,
    wld_millisecond_t          time_millisec
) {
#   define MOUSE_WHEEL_TILT_RIGHT wl_fixed_from_int( 1)
#   define MOUSE_WHEEL_TILT_LEFT  wl_fixed_from_int(-1)

    struct wl_surface *wl_surface = wl_surface_with_ptr_focus;

      // In case of CLICK_TO_FOCUS.
    // Fix: only call on BTN_LEFT press????
    twc_seat_keyboard_focus_event( ME_Click );

      // The event code corresponds to some button.  Apply the
      // configured ButtonMapping.
    enum twc_mouse_button mapped_button = (
        map_event_code( event_code )
    );

    //LOG_INFO("twc_seat_pointer_button %x %d", event_code, mapped_button);

    if ( mapped_button != MB_Button_None ) {
        // button has been mapped

        if ( is_wheel_tilt(mapped_button) ) {
            // button has been mapped to a tilt.  Emit an axis event
            // instead of a button event.

            if ( button_direction == LIBINPUT_BUTTON_STATE_PRESSED ) {

                wl_fixed_t value = (
                    mapped_button == MB_Button_Tilt_Right
                    ? MOUSE_WHEEL_TILT_RIGHT
                    : MOUSE_WHEEL_TILT_LEFT
                );

                wl_pointer_event_axis(
                   &wl_surface->bic_connection->wl_seat.wl_pointer,
                    time_millisec,
                    WL_POINTER_AXIS_HORIZONTAL_SCROLL,
                    value
                );

            } else {
                // when the original button is released, there is no
                // tilt equivalent.  The release event is discarded.
            }

            return;

        } else {
            // button has been mapped to another button.

              // Find the event code for the mapped button
            event_code = convert_to_event_code( mapped_button );
        }
    }

    wl_pointer_button(
        &wl_surface->bic_connection->wl_seat.wl_pointer,
        time_millisec,
        event_code,
        button_direction
    );

    return;
}

static
enum twc_mouse_button
map_axis(
    enum wl_pointer_axis axis,
    wl_fixed_t           value
) {
    enum twc_mouse_button twc_mouse_button = MB_Button_None;

    if ( axis == WL_POINTER_AXIS_HORIZONTAL_SCROLL ) {
        // Tilt case. Note: Using < or > to compare wl_fixed_t with 0
        // is a valid way to test for the sign.

        if ( 0 < value ) {
              // right
            twc_mouse_button = MB_Button_Tilt_Right;
        } else {
              // left
            twc_mouse_button = MB_Button_Tilt_Left;
        }
    } else {
        // Spin case.  Mapping to/from the spin axis is disallowed.
        // For the record,
        //     0     < value   means spin backward
        //     value <     0   means spin forward
    }

      // map the button_id
    enum twc_mouse_button twc_mapped_button = (
        button_mapping[twc_mouse_button]
    );

      // Return valid button only if actually mapped
    return (
        twc_mapped_button == twc_mouse_button ? 0 : twc_mapped_button
    );
}

extern
void
twc_seat_pointer_axis(
    uint32_t             time,
    enum wl_pointer_axis axis,
    wl_fixed_t           value
) {
    struct wl_surface *wl_surface = wl_surface_with_ptr_focus;

    enum twc_mouse_button mapped_button = (
        map_axis( axis, value )
    );

    //LOG_INFO("twc_seat_pointer_axis axis/value mapped button %d %d %d", axis, value, mapped_button);

    if ( mapped_button != MB_Button_None ) {
        // An axis event was mapped.  Mapping the spin axis is
        // disallowed, the mapping must be swapping left and right
        // tilt.

        if ( is_wheel_tilt(mapped_button)
        ) {
              // The original and mapped button are both tilt.
              // They've been swapped.  Flip the sign of the
              // wl_fixed_t.
            value = -1 * value;

        } else {
            // The mapped_button is not a tilt and can't be a wheel
            // spin.  It must correspond to a non-axis button.

            uint32_t event_code = (
                convert_to_event_code( mapped_button )
            );

            // Since the source event will never generate a RELEASE,
            // we emit the PRESS event and immediately simulate a
            // RELEASE.

            wl_pointer_button(
                &wl_surface->bic_connection->wl_seat.wl_pointer,
                time,
                event_code,
                WL_POINTER_BUTTON_STATE_PRESSED
            );

            wl_pointer_button(
                &wl_surface->bic_connection->wl_seat.wl_pointer,
                time,
                event_code,
                WL_POINTER_BUTTON_STATE_RELEASED
            );
        }
    }

    wl_pointer_event_axis(
       &wl_surface->bic_connection->wl_seat.wl_pointer,
        time,
        axis,
        value
    );

    return;
}

#if 0
extern
void
twc_new_seat(
    struct twc_seat *twc_seat
) {
    wl_list_insert(&TWC.twc_seat_list, &twc_seat->link);

    return;
}

    // Fix: add for multiple seats
static
struct wl_seat*
find_seat(
    struct wl_surface *wl_surface
) {
    return NULL;
}
#endif

#if 0
extern
void
twc_seat_keyboard_modifiers(
    struct wl_keyboard *wl_keyboard,
    xkb_mod_mask_t      depressed,
    xkb_mod_mask_t      latched,
    xkb_mod_mask_t      locked,
    xkb_mod_mask_t      keyboard_layout
) {
    // This routine is obsoleted by sending the
    // wl_keyboard_modifiers() after updating xkb_state in
    // twc_seat_keyboard_key().

    twc_seat.xkb_depressed    = depressed;
    twc_seat.xkb_latched      = latched;
    twc_seat.xkb_locked       = locked;
    twc_seat.xkb_layout_index = keyboard_layout;

    //LOG_INFO("twc_seat_keyboard_modifiers %x", twc_seat_depressed);

    // Fix: The physical srv_keyboard needs to be converted to one of
    // the client's wl_keyboards.  For now, each client has only one,
    // so use that.
    struct wl_keyboard *client_wl_keyboard = (
        &bic_client_with_kbd_focus->wl_seat.wl_keyboard
    );

    /*
    LOG_INFO("KK twc_seat_keyboard_modifiers %x %x %x %x",
        depressed,
        latched,
        locked,
        keyboard_layout
    );
    */

    wl_keyboard_modifiers(
        client_wl_keyboard,
        depressed,
        latched,
        locked,
        keyboard_layout
    );

    // Next, keep any decoration apprised of the modifiers in case the
    // decoration gets a button event.  See wl_keyboard::modifiers
    // event in the Wayland protocol.

    struct bic_connection *decoration_bic_connection = (
        wl_surface_with_kbd_focus->
        twc_view->
        xx_wm_decoration.bic_connection
    );

    struct wl_keyboard *decoration_wl_keyboard = (
        &decoration_bic_connection->wl_seat.wl_keyboard
    );

      // Don't send to the same keyboard twice.
    if ( decoration_wl_keyboard != client_wl_keyboard ) {

        wl_keyboard_modifiers(
            decoration_wl_keyboard,
            depressed,
            latched,
            locked,
            keyboard_layout
        );
    }

    return;
}
#endif
